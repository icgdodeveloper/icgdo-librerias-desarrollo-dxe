//******************************************************************************
// Unit que contiene los procedimientos y funciones de uso Comun dentro del
// Programa
// Empresas: ICG Dominicana, SRL.
// Autor: Rafael Rangel
// Fecha: 2019-01-06
// Ultima Modificacion: 2019-01-06
//******************************************************************************
unit UnitComunesRest;

interface


Uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Dialogs,  WinSock, WinSvc, Forms ,  System.UITypes,
  IniFiles,ADODB ,Data.DB, REST.Response.Adapter, System.JSON,
  XMLDoc,Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI,  FireDAC.Comp.Client,Registry,TlHelp32;

//Tipos de Registros
type TDatosPC   = record
  Nombre, IP, Usuario :String;
end;

//-----------------------------------------------------------------------------
//Tipos de Registros  de Parametros Generales
//------------------------------------------------------------------------------
type TParametrosApps   = record
   IP,ServerDB,NameDB,UserDB,PasswordDB,NameDBGestion,NameDBGeneral,DateFormat,PathDB,DllFiscal:String;
   Puerto,GeneraLogErrores,GeneraLogData,IdEmpresa,VersionICG,VersionAPI,
   CodApps,TipoApi,TConexDBLocal,TConexDBRemota,TipoClte,DocIDClte,
   ImprimirFiscal,PuertoImpFiscal,TimeoutImpFiscal,TipoFront:Integer;
end;


//Para la Orientaci�n del caracateres
type
   TOrientacion = (toLeft, toRight);
Var
  IniFile : TIniFile;
  //Funciones-------------------------------------------------------------------
  //Funci�n Para Abrir una Conexi�n a una Base de datos (ADO)
  function Open_DB (CnADO :TADOConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta:String;var MensajeError: String;MostrarError:Boolean ): Boolean;
  //Funci�n para abrir un Data Set (ADO)
  Function Open_ADO_Qry(CnADO:TADOConnection; RsDataSet:TADOQuery ;StrSql:string; var MensajeError:String;Lectura,MostrarError:Boolean): Boolean;
  //Funci�n Para ejecutar acciones en la base de datos  (ADO)
  Function DB_Function(CnADO:TADOConnection;SQL_Function:string;var MensajeError:string;Opcion_Function:Integer;MostrarError:Boolean): Boolean;


  //Funci�n Para Abrir una Conexi�n a una Base de datos FireDac  (FD)
  function Open_DB_FD (CnFD :TFDConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta:String;var MensajeError: String;MostrarError:Boolean ): Boolean;
  //Funci�n para abrir un Data Set  (FD)
 { Function Open_ADO_Qry_FD(CnADO:TADOConnection; RsDataSet:TADOQuery ;StrSql:string; var MensajeError:String;Lectura,MostrarError:Boolean): Boolean;
  //Funci�n Para ejecutar acciones en la base de datos (FD)
  Function DB_Function_FD(CnADO:TADOConnection;SQL_Function:string;var MensajeError:string;Opcion_Function:Integer;MostrarError:Boolean): Boolean;
}
  //Funciones encruptar
  function Encrypt(tr: string): string;
  //function descifrar(cadena, Key: string): String;
  function Decrypt(pr: string): string;

  //------------------------------------------------------------------------------
  //Funci�n que busca un valor en la tapla de parametros
  //------------------------------------------------------------------------------
  Function ValorTablaParametros(CnADO:TADOConnection; Pclave,Psubclave,PUsuario,MensajeError:String;MostrarError,Encriptado:Boolean): String;
  //Con base de datos SQL Lite utilizando el Firedac
  Function ValorTablaParametrosFD(Pclave,Psubclave,PUsuario,MensajeError,NameDBLocal,RutaDBLocal:String;MostrarError,Encriptado:Boolean): String;



  function encriptar(aStr: String; aKey: Integer): String;
  function desencriptar(aStr: String; aKey: Integer): String;

  //Funci�n para Obtener la versi�n de la aplicaci�n
  function Version_Info: String;

  //Comprueba si un valor es num�rico
  function esNumero (valor : string) : boolean;

  //Funci�n realizada que permite obtener a partir de una fecha gregoriana (dd/mm/aaaa) la fecha juliana (n�mero de d�as transcurridos desde inicio de a�o).
  function fechaJuliana (fechaGregoriana : TDateTime) : Integer;

  //Funcion que cierra las aplicaciones abiertas
  function KillTask(ExeFileName: string): Integer;

  //------------------------------------------------------------------------------
  //Funci�n que quita de un string los caracteres especiales
  //------------------------------------------------------------------------------
  function QuitarEspeciales(Cad: string): string;

  //Funci�n indica el estado de un servicio de windows
  function EstadoServicioWindos(sPC, sServicio : string;TipoInfo:Integer ) : string;
  //Funci�n que inicia un servicio de windos.
  function IniciarServicioWindows(sMachine, sService: String):Integer;

  //Funci�n que detiene un servicio de windos.
  function DetenerServicioWindows(sMachine, sService: String): Boolean;

  //Funci�n que convierte un Json Array en un data set.
  function JsonToDataset(aDataset : TDataSet; aJSON : string):TDataSet;


  //Funci�n que verifica si el email es valido
  function emailValido(CONST Value: String): boolean;

  //----------------------------------------------------------------------------
  //Funci�n que se encarga de tomar la informaci�n de la fecha del ejecutable
  //----------------------------------------------------------------------------
  function GetFileTimes(FileName : string; var Created : TDateTime;
    var Modified : TDateTime; var Accessed : TDateTime) : boolean;

  //-----------------------------------------------------------------------------
  //Se lee el archivo texto y se convierte en una variable
  //------------------------------------------------------------------------------
  function LeerTXT_A_Variable(Archivo:string): WideString;


  //Funci�n para Leer Valores del Registro----------------------------------------
  function GetRegistryData(RootKey: HKEY; Key, Value: string): variant;

  //Funci�n para Verificar si existe una clave en el registro---------------------
  function ExisteKey_Registry (Root_Key:HKEY;App_Key :String):Boolean;


  //----------------------------------------------------------------------------
  //Funci�n que genera el xml de la cabecera para la impresora Fiscal
  //----------------------------------------------------------------------------
  function Xml_PCabFiscal(CnADO:TADOConnection;SerieDoc,ModoDoc: String;TipoFront,NumeroDoc,TipoDoc:Integer; var MensajeError:String): String;
  //----------------------------------------------------------------------------
  //Funci�n que genera el xml de la Lineas para la impresora Fiscal
  //----------------------------------------------------------------------------
  function Xml_PLinFiscal(CnADO:TADOConnection;SerieDoc,ModoDoc: String;TipoFront,NumeroDoc,TipoDoc:Integer; var MensajeError:String): String;
 //----------------------------------------------------------------------------
  //Funci�n que genera el xml de las formas de pago para la impresora Fiscal
  //----------------------------------------------------------------------------
  function Xml_PFPaFiscal(CnADO:TADOConnection;SerieDoc,ModoDoc: String;TipoFront,NumeroDoc,TipoDoc:Integer; var MensajeError:String): String;
   //----------------------------------------------------------------------------
  //Funci�n que genera el xml del cliente para la impresora Fiscal
  //----------------------------------------------------------------------------
  function Xml_PCliFiscal(CnADO:TADOConnection;SerieDoc,ModoDoc: String;TipoFront,NumeroDoc,TipoDoc:Integer; var MensajeError:String): String;

  //----------------------------------------------------------------------------
  //Procedimientos--------------------------------------------------------------
  //----------------------------------------------------------------------------
  //Procedimiento Para Cerra la Conexion a la base de datos
  Procedure CloseConnection(CnADO:TADOConnection; RsDataSet:TADOQuery);
  //Procedimiento para cerrar las aplicaciones
  procedure ObtenerDatosPC (var Datos:TDatosPC );
  //EScribe en el log de la aplicaci�n
  procedure WriteLogApps(TipoLog:Integer;Data:String;GeneraLogErrores,GeneraLogDataRecibida:Integer);

  //Procedimiento Para Obtener la Version de la Aplicaci�n
  procedure GetBuild_Info(var V1, V2, V3, V4: Word);

   //Definicion del procedimiento para borrar Archivos
  procedure EliminarArchivo (sArchivo: String);

  //Procedimiento para ejcutar una aplicaci�n o un .bat
  procedure EjecutarApps(sApp: String; Esperar: Cardinal; Visible: Boolean);

  //Procedimiento para escribir valores en el Registro de Windows-----------------
  procedure SetRegistryData(RootKey: HKEY; Key, Value: string; RegDataType: Integer; Data: variant);

implementation
//******************************************************************************
//Funciones
//******************************************************************************

//------------------------------------------------------------------------------
//Funci�n Para Abrir la Base de datos utilizando Ado y varios Provedores de base de datos
//------------------------------------------------------------------------------
Function Open_DB (CnADO :TADOConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta:String; var MensajeError: String;MostrarError:Boolean ): Boolean;

//Variables de la Funcion
Var
  ConStr: String; // Cadena de Conexion

Begin

  //Cerramos la Conexion
  if CnADO.Connected then CnADO.close ;
  Case OpcionBd  of
    1 :  //'Excel
      Begin
        //Se Arma la cadena de Conexion
        ConStr := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + Ruta + BD + ';Persist Security Info=False; Extended Properties=Excel 8.0;';

      End;

    2 : //SQL Server
      Begin
        // Se Arma La Cadena de Conexion
        ConStr:=  'Provider=sqloledb;' +
                  'Data Source=' + Servidor + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'User id=' + Usuario + ';' +
                  'Password=' + Clave + ';' +
                  'Trusted_Connection=False;' ;



      End;

      3: // Access
      Begin
        ConStr:= 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                  'Data Source=' + Ruta + Bd + '.mdb' +
                  ';Persist Security Info=False ' +  ';' +
                  'Jet OLEDB:Database Password=' + Clave ;

      End;

      4: //Oracle
      Begin
        ConStr:= 'Provider=msdaora;' +
                  'Data Source=' + Servidor + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'User id=' + Usuario + ';' +
                  'Password=' + Clave + ';' ;
      End;
      5: // ODBC  sin Indicar el usuario y Password
      Begin
        ConStr:= 'DSN=' + Bd +  ';pwd=' + Clave ;



      End;
      6:// Texto
      Begin
        ConStr:='DRIVER={Microsoft Text Driver (*.txt; *.csv)};' +
                'DBQ=' +  Ruta + ';';
      End;

      7:// MySql
      Begin

      End;

      8: // dBase
      Begin
        ConStr:= 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                  'Data Source=' + Ruta + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'Extended Properties=dBASE IV;'

      End;
      9: // Paradox
      Begin
        ConStr:= 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                  'Data Source=' + Ruta + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'Extended Properties=Paradox 5.x;'
      End;

      10: // SQL Lite
      Begin
       ConStr:= 'Provider=SQLITEDB;' +
                  'Data Source=' + Ruta + Bd + '.db' ;
                 // ';Persist Security Info=False ' +  ';' +
                 // 'Jet OLEDB:Database Password=' + Clave ;

       End;
       11: // ODBC  indicando el usuario y Password
      Begin
        ConStr:= 'DSN=' + Bd +  ';Uid=' + Usuario + ';pwd=' + Clave ;



      End;

       12 : //SQL Server  con Trusted Connection
      Begin
        // Se Arma La Cadena de Conexion
        ConStr:=  'Provider=sqloledb;' +
                  'Data Source=' + Servidor + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'User id=' + Usuario + ';' +
                  'Password=' + Clave + ';' +
                  'Trusted_Connection=False;' ;
                //  'integrated security=False;'    ;

      End;




  end;

  Result:=false;
  CnADO.Close ;
  CnADO.ConnectionString:=ConStr  ;
  CnADO.LoginPrompt:=false;
  CnADO.Mode:=cmReadWrite;


  if (NOT CnADO.Connected) Then
  Try
    //Se Abre la Conexion a la Base de datos
    //CnADO.Mode:=cmRead;
    CnADO.Open;
    Result:=True;
  // Si no se puede conectar se devuelve falso y el mensaje de error
  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      if MostrarError=True then
      Begin
        MessageDlg('No se Puede Conectar a ' + Servidor + ':' + BD + ' El error es : ' + #13#10 + E.Message,
        mterror, [mbok],0);
      End;
      MensajeError:=E.Message;
      Result:=False;
    End;
  End;

End;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Funci�n Para Abrir la Base de datos utilizando FireDac y varios Provedores de base de datos
//------------------------------------------------------------------------------
Function Open_DB_FD (CnFD :TFDConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta:String; var MensajeError: String;MostrarError:Boolean ): Boolean;

//Variables de la Funci�n
Var
  ConStr: String; // Cadena de Conexi�n

Begin

  //Cerramos la Conexi�n
  ConStr:='';
  CnFD.Params.Clear;
  if CnFD.Connected then CnFD.close ;
  Case OpcionBd  of
    1 :  //'Excel
      Begin

      End;

    2 : //SQL Server
      Begin

      End;

      3: // Access
      Begin

      End;

      4: //Oracle
      Begin

      End;
      5: // ODBC  sin Indicar el usuario y Password
      Begin

      End;
      6:// Texto
      Begin

      End;

      7:// MySql
      Begin

      End;

      8: // dBase
      Begin

      End;
      9: // Paradox
      Begin

      End;

      10: // SQL Lite
      Begin
        ConStr:= '';
        CnFD.DriverName:='SQLite';
       End;
       11: // ODBC  indicando el usuario y Password
      Begin

      End;

       12 : //SQL Server  con Trusted Connection
      Begin

      End;
  end;

  //----------------------------------------------------------------------------
  //Se verifica que la base de datos este creada
  //----------------------------------------------------------------------------
  if Not FileExists(Ruta+BD)then
  begin
    //--------------------------------------------------------------------------
    //Se Crea la base de datos
    //--------------------------------------------------------------------------
    try
      //------------------------------------------------------------------------
      // Parametros de Conexion a la base de datos
      //------------------------------------------------------------------------

      CnFD.Params.Add('Database=' +Ruta+BD);
      CnFD.Params.Add('OpenMode=omCreateUTF16');
      CnFD.Open();
      CnFD.Connected:=True;


    except
    on E:Exception do
      Begin
        // Se guarda el error en el log
        MensajeError:=E.Message;
        //------------------------------------------------------------------------
        // Si hay un error
        //--------------------------------------------------------------------------
        WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + MensajeError + ' ',1,0);
      End;
    End;
  end;

  Result:=false;
  CnFD.Close ;

  //Si no esta conectado
  if (NOT CnFD.Connected) Then
  Try
    //Se Abre la Conexi�n a la Base de datos
    CnFD.Open;
    CnFD.Connected:=True;
    Result:=True;
  // Si no se puede conectar se devuelve falso y el mensaje de error
  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      if MostrarError=True then
      Begin
        MessageDlg('No se Puede Conectar a ' + Servidor + ':' + BD + ' El error es : ' + #13#10 + E.Message,
        mterror, [mbok],0);
      End;
      MensajeError:=E.Message;
      Result:=False;
    End;
  End;

End;
//------------------------------------------------------------------------------






//Funcion que Abre un DataSet
Function Open_ADO_Qry(CnADO:TADOConnection; RsDataSet:TADOQuery ;StrSql:string;var MensajeError:String;Lectura,MostrarError:Boolean): Boolean;
Begin
  Result:=False;

  if CnADO.Connected Then
  Begin
    with RsDataSet do begin
      Close;
      Connection:=CnADO;
      SQL.Clear;
      SQL.Text:=StrSQL;
      //LockType:=ltReadOnly;

      // Verificamos que se pueda abrir la Consulta
      try
        Open;
        Result:=True;

      Except On E:Exception do
        Begin

          //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
          if MostrarError=True then
          Begin
            MessageDlg('No se Puede Consultar la Informacion Solicitada, el Error es :' + E.Message,
            mterror, [mbok],0);
          End;
          MensajeError:=E.Message;
          Result:=False;
        End;
      end;
    End;
  End;
End ;

//------------------------------------------------------------------------------
//Funci�n para realizar diferentes acciones en la base de datos-----------------
//------------------------------------------------------------------------------

Function DB_Function(CnADO:TADOConnection;SQL_Function:string;var MensajeError:string;Opcion_Function:Integer;MostrarError:Boolean): Boolean;
Var
  LaAccion: String; // Cadena de Conexi�n

begin
  try

    if CnADO.Connected Then ; // Si la conexion esta Abierta
    Begin
      LaAccion:=' ';
      Case Opcion_Function of
        1 :  //Crear una Base de datos
        Begin
          LaAccion:='CREATE DATABASE IF NOT EXISTS ';
        End;
        2 :  //Crear una Tabla
        Begin
          LaAccion:='CREATE TABLE IF NOT EXISTS ';
        End;

        3 :  //Borrar una Tabla
        Begin
          LaAccion:='DROP TABLE IF EXISTS ';
        End;

        4 :  //Ejecutar SQL
        Begin
          LaAccion:=' '
        End;
      End;
      CnADO.Execute(LaAccion + SQL_Function,cmdText);
    End
  Except On E:Exception Do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      if MostrarError=True then
      Begin
            MessageDlg('No se Puede Consultar la Informacion Solicitada, el Error es :' + E.Message,
            mterror, [mbok],0);
      End;
      MensajeError:=E.Message;
      Result:=False;
      Exit;
    end ;
  end;
  Result:=True;
end;

  //------------------------------------------------------------------------------

//Encrypt
function Encrypt(tr: string): string;
var
  i: Integer;
  Temp: string;

begin
  for i := 1 to Length(tr) do
  begin
    Temp := Temp + Chr(Ord(tr[i]) + 1);
  end;
  Encrypt := Temp;
end;
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//Descifrar una Cadena
//----------------------------------------------------------------------------

//Decrypt
function Decrypt(pr: string): string;
var
  i: Integer;
  Temp: string;
  begin
  for i := 1 to Length(pr) do
  begin
    Temp := Temp + Chr(Ord(pr[i]) - 1);
  end;
  Decrypt := Temp;
end;
//------------------------------------------------------------------------------





//------------------------------------------------------------------------------
//Funci�n que busca un valor en la tapla de parametros
//------------------------------------------------------------------------------
Function ValorTablaParametros(CnADO:TADOConnection; Pclave,Psubclave,PUsuario,MensajeError:String;MostrarError,Encriptado:Boolean): String;
Var
  rsParametro: TADOQuery; //Recordset tabla de Parametros
  ValorParametro,
  StrSQL:String;
Begin

  ValorParametro:='';
  StrSQL:='';
  try
    //si hay conexi�n a la base de datos
    if CnADO.Connected Then
    Begin
      //Se crea el objeto de base de datos AdoQry
      rsParametro:= TADOQuery.Create(nil);
      with rsParametro do
      begin
        Close;
        Connection:=CnADO;
        SQL.Clear;
        //se verifica si deben encriptarse los valores
        if Encriptado  then
        Begin
            Psubclave:= (Encrypt(PSubClave)) ;
            Pusuario:= (Encrypt(PUsuario)) ;
        End;

        StrSQL:='SELECT  VALOR FROM ICGDO_TB_PARAMETROS WITH (NOLOCK) ' +
          ' WHERE (CLAVE =  ' + QuotedSTR(Pclave) + ')' +
          ' AND (SUBCLAVE = ' + QuotedSTR(Psubclave) + ')' +
          ' AND (USUARIO = ' + QuotedSTR(Pusuario) + ')';
        SQL.Text:=StrSQL;
        Open;

        //Si existe el registro se toma el valor
        if not eof then
        Begin
            if Encriptado  then ValorParametro:= Decrypt(Fields[0].AsString)
            Else
              ValorParametro:= Fields[0].AsString;
        End;
        Close;
      End;
      rsParametro.Free;
    End;

  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      if MostrarError=True then
      Begin
        MessageDlg('No se Puede Consultar la Informacion Solicitada, el Error es :' + E.Message,
        mterror, [mbok],0);
      End;
      MensajeError:=E.Message;
    End;
  End;

  //Retornar el valor del parametro
  Result:=ValorParametro;

End ;


//------------------------------------------------------------------------------
//Funci�n que busca un valor en la tapla de parametros
//------------------------------------------------------------------------------
Function ValorTablaParametrosFD(Pclave,Psubclave,PUsuario,MensajeError,NameDBLocal,RutaDBLocal:String;MostrarError,Encriptado:Boolean): String;
Var
  ValorParametro,
  StrSQL:String;
  FDCn10:TFDConnection;
  FDRs10:TFDQuery;
  SQLStrings:TStrings;

Begin

  ValorParametro:='';
  StrSQL:='';
  try

    //Se crean los objetos de la base de datos
    FDCn10:=TFDConnection.Create(nil);
    try
      FDRs10:=TFDQuery.Create(nil);
      try
        FDCn10.DriverName:='SQLite';
        FDRs10.Connection:=FDCn10;
        if FileExists(RutaDBLocal+NameDBLocal)then
        begin
          try
            //------------------------------------------------------------------------
            // Parametros de Conexi�n a la base de datos
            //------------------------------------------------------------------------
            FDCn10.Params.Add('Database=' +RutaDBLocal+NameDBLocal );
            FDCn10.Open();
            FDCn10.Connected:=True;
          except
            on E: EDatabaseError do
            Begin
              // Se guarda el error en el log
              MensajeError:=E.Message;
              //----------------------------------------------------------------------
              // Si hay un error
              //----------------------------------------------------------------------
              if MostrarError
                then
                MessageDlg(MensajeError, mterror, [mbOK], 0);
              //----------------------------------------------------------------------
              // Si hay un error
              //----------------------------------------------------------------------
              WriteLogApps(2,Mensajeerror,1,1);
            End;
          end;
        end;

        //si hay conexi�n a la base de datos
        if FDCn10.Connected Then
        Begin
          with FDRs10 do
          begin

            //se verifica si deben encriptarse los valores
            if Encriptado  then
            Begin
              Psubclave:= (Encrypt(PSubClave)) ;
              Pusuario:= (Encrypt(PUsuario)) ;
            End;

            StrSQL:='SELECT  VALOR FROM ICGDO_TB_PARAMETROS ' +
            ' WHERE (CLAVE =  ' + QuotedSTR(Pclave) + ')' +
            ' AND (SUBCLAVE = ' + QuotedSTR(Psubclave) + ')' +
            ' AND (USUARIO = ' + QuotedSTR(Pusuario) + ')';
            Close;
            SQLStrings:=TStringList.Create ;
            try
              SQLStrings.Add(StrSQL);
              SQL:=SQLStrings;
              //Se Abre el DataSet
              Open;

              //Si existe el registro se toma el valor
              if not eof then
              Begin
                if Encriptado  then ValorParametro:= Decrypt(Fields[0].AsString)
                Else
                  ValorParametro:= Fields[0].AsString;
              End;
              Close;
            finally
               SQLStrings.Free ;
            end;
          End;
        End;
      finally
         FDRs10.Free;
      end;

    finally
      FDCn10.Free;
    end;

  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      if MostrarError=True then
      Begin
        MessageDlg('No se Puede Consultar la Informacion Solicitada, el Error es :' + E.Message,
        mterror, [mbok],0);
      End;
      MensajeError:=E.Message;
      //----------------------------------------------------------------------
      // Si hay un error
      //----------------------------------------------------------------------
      WriteLogApps(2,Mensajeerror,1,1);
    End;
  End;





  //Retornar el valor del parametro
  Result:=ValorParametro;

End ;




//------------------------------------------------------------------------------
//encriptar datos
//------------------------------------------------------------------------------
function encriptar(aStr: String; aKey: Integer): String;
begin
   Result:='';
   RandSeed:=aKey;
   for aKey:=1 to Length(aStr) do
       Result:=Result+Chr(Byte(aStr[aKey]) xor random(256));
end;

//------------------------------------------------------------------------------
//desencriptar datos
//------------------------------------------------------------------------------
function desencriptar(aStr: String; aKey: Integer): String;
begin
   Result:='';
   RandSeed:=aKey;
   for aKey:=1 to Length(aStr) do
       Result:=Result+Chr(Byte(aStr[aKey]) xor random(256));
end;


//------------------------------------------------------------------------------
//Funcion para Obtener la versi�n de la aplicaci�n
//------------------------------------------------------------------------------
function Version_Info: String;
var
  V1,       // Major Version
  V2,       // Minor Version
  V3,       // Release
  V4: Word; // Build Number
begin
  GetBuild_Info(V1, V2, V3, V4);
  Result := IntToStr(V1) + '.'
            + IntToStr(V2) + '.'
            + IntToStr(V3) + '.'
            + IntToStr(V4);
end;
//------------------------------------------------------------------------------

//Comprueba si un valor es num�rico
function esNumero (valor : string) : boolean;
var
  numero : integer;
begin
  try
    numero := strtoint(valor);
    result := true;
  except
    result := false;
  end;
end;

//------------------------------------------------------------------------------
//Funci�n realizada que permite obtener a partir de una fecha gregoriana (dd/mm/aaaa) la fecha juliana (n�mero de d�as transcurridos desde inicio de a�o).
//-----------------------------------------------------------------------------
function fechaJuliana (fechaGregoriana : TDateTime) : Integer;
var
  dia, mes, ano : Word;
begin
  fechaGregoriana := Trunc(fechaGregoriana );
  DecodeDate(fechaGregoriana, ano, mes, dia);
  Result := Trunc(fechaGregoriana - EncodeDate(ano , 1 , 1)+1);
end;

//------------------------------------------------------------------------------
//Funci�n que quita de un string los caracteres especiales
//------------------------------------------------------------------------------
function QuitarEspeciales(Cad: string): string;
var
  i: Integer;
begin
  Result:= '';
  for i:= 1 to Length(Cad) do
  if  Ord(Cad[i])  > 31  then Result:= Result+Cad[i]
end;



//------------------------------------------------------------------------------
// Funcion indica el estado de un servicio
//------------------------------------------------------------------------------
{Este proceso se encarga de indicar cual es el estado de un servicio de windows,
tiene dos opciones,
TipoInfo=0 devuelve el nombre del estado del servicio;
TipoInfo=1 devuelve el Id del estado del servicio
}
//------------------------------------------------------------------------------
function EstadoServicioWindos(sPC, sServicio : string;TipoInfo:Integer ) : string;
var
  schm, schs   : SC_Handle;
  ss     : TServiceStatus;
  dwStat : DWord;
begin
  dwStat := 0;
  schm := OpenSCManager(PChar(sPC), Nil, SC_MANAGER_CONNECT);
  if (schm > 0) then
  begin
    schs := OpenService(schm, PChar(sServicio), SERVICE_QUERY_STATUS);
    if (schs > 0) then
    begin
      if (QueryServiceStatus(schs, ss)) then
      begin
        dwStat := ss.dwCurrentState;
      end;
      CloseServiceHandle(schs);
    end;
    CloseServiceHandle(schm);
  end;
  case dwStat of
    0 : Result := 'No disponible';
    1 : Result := 'Detenido';
    2 : Result := 'Iniciando';
    3 : Result := 'Deteniendo';
    4 : Result := 'Iniciado';
    7 : Result := 'Pausado';
  else
    result := inttostr(dwstat);
  end;

  //Se verifica si se envia la descripci�n o el codigo del estado del servicio
  if TipoInfo=1 then Result :=inttostr(dwstat);
end;



//------------------------------------------------------------------------------
// Funcion que inicia un servicio de windows
//------------------------------------------------------------------------------
{Este proceso se encarga de iniciar un servcio de windows , se verifica si el servicio
esta instalado
}
//------------------------------------------------------------------------------
function IniciarServicioWindows(sMachine, sService: String): Integer;
const
   TimeLimit : Word = 60000;
var
   OpenScm, OpenSvr : SC_Handle;
   SrvSts : TServiceStatus;
   SrvArgVec : PChar;
   WaitTime : Word;
   StartTickCount, StopTickCount : Word;

begin

   try

      OpenScm := OpenSCManager(PChar(sMachine), SERVICES_ACTIVE_DATABASE, SC_MANAGER_CONNECT);

      if (OpenScm > 0) then
      begin

         OpenSvr := OpenService(OpenScm, PChar(sService),
                                SERVICE_START or
                                SERVICE_QUERY_STATUS);

         if (OpenSvr > 0) and (QueryServiceStatus(OpenSvr, SrvSts)) then
         begin

            if (StartService(OpenSvr, 0, SrvArgVec)) then
            begin

               StartTickCount := GetTickCount;

               while QueryServiceStatus(OpenSvr, SrvSts) do
               begin

                  WaitTime := SrvSts.dwWaitHint div 10;

                  if (WaitTime < 1000) then
                     WaitTime := 1000
                  else
                  if (WaitTime > 10000) then
                     WaitTime := 10000;

                  Sleep(SrvSts.dwWaitHint);

                  StopTickCount := GetTickCount;

                  if (StopTickCount - StartTickCount) > TimeLimit then
                     Break;

                  case SrvSts.dwCurrentState of
                     SERVICE_START_PENDING : Continue;
                     SERVICE_RUNNING : Break;
                     SERVICE_STOP_PENDING : Break;
                     SERVICE_STOPPED : Break;
                  end;

                  if (SrvSts.dwCheckPoint = 0) then
                     Break;

               end;

            end;

            CloseServiceHandle(OpenSvr);

         end;

         CloseServiceHandle(OpenScm);

      end;

      Result := SrvSts.dwCurrentState;

   except

      SysErrorMessage(GetLastError);
      Result := GetLastError;

   end;

end;




//------------------------------------------------------------------------------
// Funcion que detiene un servicio de windows
//------------------------------------------------------------------------------
{Este proceso se encarga de parar la ejecuci�n de un servcio de windows , se verifica si el servicio
esta instalado
}
//------------------------------------------------------------------------------

function DetenerServicioWindows(sMachine, sService: String): Boolean;
var
  schm,
  schs: SC_Handle;
  ss: TServiceStatus;
  dwChkP: DWord;
begin
  schm := OpenSCManager(PChar(sMachine), nil, SC_MANAGER_CONNECT);
  if (schm>0) then
  begin
    schs := OpenService(schm, PChar(sService), SERVICE_STOP or
      SERVICE_QUERY_STATUS);
    if (schs>0) then
    begin
      if (ControlService(schs, SERVICE_CONTROL_STOP, ss)) then
        if (QueryServiceStatus(schs, ss)) then
          while (SERVICE_STOPPED<>ss.dwCurrentState) do
          begin
            dwChkP := ss.dwCheckPoint;
            Sleep(ss.dwWaitHint);
            if (not QueryServiceStatus(schs, ss)) then
              Break;
            if (ss.dwCheckPoint < dwChkP) then
              Break;
          end;
      CloseServiceHandle(schs);
    end;
    CloseServiceHandle(schm);
  end;
  Result := SERVICE_STOPPED=ss.dwCurrentState;
end;

//------------------------------------------------------------------------------
//Verifica si el emai es valido
//------------------------------------------------------------------------------
function emailValido(CONST Value: String): boolean;
  function CheckAllowed(CONST s: String): boolean;
  var i: Integer;
  begin
    Result:= False;
    FOR i:= 1 TO Length(s) DO // illegal char in s -> no valid address
    IF NOT (s[i] IN ['a'..'z','A'..'Z','0'..'9','_','-','.']) THEN Exit;
    Result:= true;
  end;
var
  i,len: Integer;
  namePart, serverPart: String;
begin // of IsValidEmail
  Result:= False;
  i:= Pos('@', Value);
  IF (i=0) OR (Pos('..',Value) > 0) then Exit;
  namePart:= Copy(Value, 1, i - 1);
  serverPart:= Copy(Value,i+1,Length(Value));
  len:=Length(serverPart);
  // must have dot and at least 3 places from end, 2 places from begin
  IF (len<4) OR
     (Pos('.',serverPart)=0) OR
     (serverPart[1]='.') OR
     (serverPart[len]='.') OR
     (serverPart[len-1]='.') then Exit;
  Result:= CheckAllowed(namePart) AND CheckAllowed(serverPart);
end;

//-----------------------------------------------------------------------------
//Se lee el archivo texto y se convierte en una variable
//------------------------------------------------------------------------------
function LeerTXT_A_Variable(Archivo:string): WideString;
var F: TextFile;
    sLinea:String;
    VariableStr :  WideString;
begin
  //
  VariableStr:='';
  if FileExists(ExtractFilePath( Application.ExeName ) + Archivo)then
  begin

    AssignFile( F, ExtractFilePath( Application.ExeName ) + Archivo );
    Reset( F );
    sLinea:='';
    while not Eof( F ) do
    begin
      ReadLn( F, sLinea );
      VariableStr:= VariableStr+( sLinea )+   sLineBreak ;
    end;
    CloseFile( F );
  end;
  Result:=VariableStr;

end;

//Funcion que elimina o cierra la ejecuci�n en memoria de aplicaciones
function KillTask(ExeFileName: string): Integer;
const
  PROCESS_TERMINATE = $0001;
var
  ContinueLoop: BOOL;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  Result := 0;
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);

  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName))) then
      Result := Integer(TerminateProcess(
                        OpenProcess(PROCESS_TERMINATE,
                                    BOOL(0),
                                    FProcessEntry32.th32ProcessID),
                                    0));
     ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;




//------------------------------------------------------------------------------
//Procedimientos
//------------------------------------------------------------------------------

//Procedimiento para Cerrar la Conexion a la Base de datos
Procedure CloseConnection  (CnADO:TADOConnection; RsDataSet:TADOQuery);
var
Mensaje:string;
Begin
  Try
    //Si la Conexion a la Base de datos esta Abierta
    if CnADO.Connected Then
    Begin
      with RsDataSet do begin
        if  RsDataSet.Active then
        Begin
          Close;
          Free;
        End;
        CnADO.Close;
        CnADO.Free;
      end;
    End;
  except
    on E:Exception do
      Begin
        if Mensaje='' then Mensaje:=E.Message;
        //------------------------------------------------------------------------
        // Si hay un error
        //--------------------------------------------------------------------------
        // Se guarda el error en el log
        WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + ' ' + Mensaje, 1,0);
      End
    end;


End;

//------------------------------------------------------------------------------
// Funci�n que convierte de Json a Data Set
//------------------------------------------------------------------------------
function JsonToDataset(aDataset : TDataSet; aJSON : string):TDataSet;
var
  Mensaje:string;
  JObj: TJSONArray;
  vConv : TCustomJSONDataSetAdapter;
begin
  Try
    if (aJSON = EmptyStr) then
    begin
      Exit;
    end;

    JObj := TJSONObject.ParseJSONValue(aJSON) as TJSONArray;
    vConv := TCustomJSONDataSetAdapter.Create(Nil);

    try
      vConv.Dataset := aDataset;
      vConv.UpdateDataSet(JObj);
      aDataset:= vConv.Dataset;
    finally
      vConv.Free;
      JObj.Free;
    end;
    Result:= aDataset;
  except
    on E:Exception do
      Begin
        if Mensaje='' then Mensaje:=E.Message;
        //------------------------------------------------------------------------
        // Si hay un error
        //--------------------------------------------------------------------------
        // Se guarda el error en el log
        WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + ' ' + Mensaje, 1,0);
      End
    end;
end;

//------------------------------------------------------------------------------
//Funci�n que retorna la informaci�n de fechas de un ejecutable
//------------------------------------------------------------------------------
function GetFileTimes(FileName : string; var Created : TDateTime;
    var Modified : TDateTime; var Accessed : TDateTime) : boolean;
var
   FileHandle : integer;
   Retvar : boolean;
   FTimeC,FTimeA,FTimeM : TFileTime;
   LTime : TFileTime;
   STime : TSystemTime;
begin
  // Abrir el fichero
  FileHandle := FileOpen(FileName,fmShareDenyNone);
  // inicializar
  Created := 0.0;
  Modified := 0.0;
  Accessed := 0.0;
  // Ha tenido acceso al fichero?
  if FileHandle < 0 then
    RetVar := false
  else begin

    // Obtener las fechas
    RetVar := true;
    GetFileTime(FileHandle,@FTimeC,@FTimeA,@FTimeM);
    // Cerrar
    FileClose(FileHandle);
    // Creado
    FileTimeToLocalFileTime(FTimeC,LTime);

    if FileTimeToSystemTime(LTime,STime) then begin
      Created := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
      Created := Created + EncodeTime(STime.wHour,STime.wMinute,
              STime.wSecond, STime.wMilliSeconds);
    end;

    // Accedido
    FileTimeToLocalFileTime(FTimeA,LTime);

    if FileTimeToSystemTime(LTime,STime) then begin
      Accessed := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
      Accessed := Accessed + EncodeTime(STime.wHour,STime.wMinute,
              STime.wSecond, STime.wMilliSeconds);
    end;

    // Modificado
    FileTimeToLocalFileTime(FTimeM,LTime);

    if FileTimeToSystemTime(LTime,STime) then begin
      Modified := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
      Modified := Modified + EncodeTime(STime.wHour,STime.wMinute,
                     STime.wSecond, STime.wMilliSeconds);
    end;
  end;
  Result := RetVar;
end;


//------------------------------------------------------------------------------
//Procedimiento Para Saber el Nombre del Pc, IP y el Usuario en Windows
//------------------------------------------------------------------------------
Procedure ObtenerDatosPC (var Datos:TDatosPC );
const LARGO_MAXIMO = 50;
var
  buffer:Array [0..LARGO_MAXIMO+1] of char;
  Largo:Cardinal;
  PuntHost: PHostEnt;
  PuntIP: PAnsichar;
  wVersionRequested: WORD;
  wsaData: TWSAData;
begin
  Largo := LARGO_MAXIMO +1;
  Datos.Nombre := '';
  If GetComputerName (buffer, Largo) then Datos.Nombre := buffer;
  Datos.Usuario := '';
  if GetUserName(buffer, largo) then Datos.Usuario := buffer;
  wVersionRequested := MAKEWORD( 1, 1 );
  WSAStartup( wVersionRequested, wsaData );
  GetHostName( @buffer, LARGO_MAXIMO );
  PuntHost := GetHostByName( @buffer );
  PuntIP := iNet_ntoa( PInAddr( PuntHost^.h_addr_list^ )^ );
  Datos.IP := PuntIP ;
  WSACleanup;
end;
//------------------------------------------------------------------------------





//----------------------------------------------------------------------------
//Se Genera el Archivo Log de la aplicaci�n
//-----------------------------------------------------------------------------
procedure WriteLogApps(TipoLog:Integer;Data:String;GeneraLogErrores,GeneraLogDataRecibida:Integer);
Var
  fichero : TStringList;
  FicheroLog,NombreFichero,Carpeta,RutaLog,Mensaje:String;
  Conectado:Boolean ;
begin

  //----------------------------------------------------------------------------
  //Se Verifica el tipo de Log de la aplicaci�n
  //----------------------------------------------------------------------------
  NombreFichero:='Fichero.txt';
  Carpeta:='';
  RutaLog:= ExtractFilePath(ParamStr(0));
  Case TipoLog  of
    1 :  //Log de la Aplicaci�n
      Begin
        NombreFichero:='Log_Apps_';
        Carpeta:='Log';

      End;
    2 :  //Log de Errores
      Begin
         //Si la aplicaci�n no esta configurada para generar LOG de errores  se retorna
        if (GeneraLogErrores=0) then Exit();
        NombreFichero:='Log_Error_';
        Carpeta:='Log';

      End;

    3 :  //Log de la data recibida
      Begin
          //Si la aplicaci�n no esta configurada para generar LOG de la data recibida
        //if (Parametros.GeneraLogDataRecibida=0) then Exit();
        NombreFichero:='Log_Data';
        Carpeta:='Log';
      End;

    4 :  //Log de la data recibida
      Begin
          //Si la aplicaci�n no esta configurada para generar LOG de la data recibida
        //if (Parametros.GeneraLogDataRecibida=0) then Exit();
        NombreFichero:='ErrorImpFiscal';
        Carpeta:='Log';
        if FileExists(RutaLog+ Carpeta+ '\' +NombreFichero)then
        begin
          EliminarArchivo (RutaLog+ Carpeta+ '\' +NombreFichero)
        end;
      End;


  End;


   //Se Verifica si existe la carpeta
  Conectado:=DirectoryExists(RutaLog+Carpeta+'\');
  if Conectado = False then
  Begin
    Try
      //Se crea La carpeta
      Conectado:=CreateDir(RutaLog+Carpeta);
      // Si no se puede crear se controla el error
    Except On E:Exception do
      Begin
        // Se guarda el error en el log
        Mensaje:='No se Puede Crear la Carpeta  ' + RutaLog+ Carpeta + ' El error es : ' + #13#10 + E.Message;
      End;
    End;
  End;
  //Genera Archivo texto con data enviada
  Try

    if TipoLog <> 4
      then
        FicheroLog := RutaLog+ Carpeta+ '\' + NombreFichero + formatdatetime('yyyymmdd', now)+ '.txt'
       else
        FicheroLog := RutaLog+ Carpeta+ '\' + NombreFichero + '.txt';


    fichero := TStringList.Create;
    //Si Existe el archivo se carga
    if FileExists(FicheroLog) then fichero.LoadFromFile(FicheroLog);
    fichero.Add(Data);
    fichero.SaveToFile(FicheroLog);
    //Se Cierra el archivo
    Fichero.Free;
    // Si no se puede crear se controla el error
  Except On E:Exception do
    Begin
      // Se guarda el error en el log
      Mensaje:='No se Puede Crear El Archivo El error es : ' + #13#10 + E.Message;
    End;
  End;

end;

 // -----------------------------------------------------------------------------
// Construir la informacion con la Version de la aplicacion
//------------------------------------------------------------------------------
procedure GetBuild_Info(var V1, V2, V3, V4: Word);
var
   VerInfoSize, VerValueSize, Dummy : DWORD;
   VerInfo : Pointer;
   VerValue : PVSFixedFileInfo;
begin
VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
GetMem(VerInfo, VerInfoSize);
GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
With VerValue^ do
begin
  V1 := dwFileVersionMS shr 16;
  V2 := dwFileVersionMS and $FFFF;
  V3 := dwFileVersionLS shr 16;
  V4 := dwFileVersionLS and $FFFF;
end;
FreeMem(VerInfo, VerInfoSize);
end;

//------------------------------------------------------------------------------
//Procedimiento para Borrar Archivos
//------------------------------------------------------------------------------
procedure EliminarArchivo ( sArchivo: String);
begin
 // sArchivo := ExtractFilePath( Application.ExeName ) + 'prueba.txt';
  if FileExists( sArchivo ) then
    DeleteFile( sArchivo );
end;

//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//Procedimiento que ejecuta las aplicaciones
//------------------------------------------------------------------------------
procedure EjecutarApps(sApp: String; Esperar: Cardinal; Visible: Boolean);
var
  StarInfo: TStartupInfo;
  ProcInfo: TProcessInformation;
begin
  Try
    FillChar(StarInfo, SizeOf(TStartupInfo), 0);
    StarInfo.cb := SizeOf(TStartupInfo);
    if not Visible then
    begin
      StarInfo.dwFlags := STARTF_USESHOWWINDOW;
      StarInfo.wShowWindow := SW_HIDE;
    end;
    if CreateProcess(nil, PChar(sApp), nil, nil, False, NORMAL_PRIORITY_CLASS,
      nil, PChar(ExtractFilePath(sApp)), StarInfo, ProcInfo) then
    begin
      WaitForSingleObject(ProcInfo.hProcess, Esperar);
      CloseHandle(ProcInfo.hProcess);
      CloseHandle(ProcInfo.hThread);
    end
    else
      if CreateProcess(nil, PChar(sApp), nil, nil, False, NORMAL_PRIORITY_CLASS,
        nil, nil, StarInfo, ProcInfo) then
      begin
        WaitForSingleObject(ProcInfo.hProcess, Esperar);
        CloseHandle(ProcInfo.hProcess);
        CloseHandle(ProcInfo.hThread);
      end
      else
        ShowMessage(SysErrorMessage(GetLastError));
  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;

//Procedimiento para escribir valores en el Registro de Windows-----------------
procedure SetRegistryData(RootKey: HKEY; Key, Value: string; RegDataType: Integer; Data: variant);
var
  Reg: TRegistry;
  s: string;
begin
  Reg := TRegistry.Create(KEY_WRITE);
  try
    Reg.RootKey := RootKey;
    if Reg.OpenKey(Key, True) then begin
      try
        if RegDataType = 0  then
          Reg.WriteString(Value, Data);
        if RegDataType = 1 then
          Reg.WriteString(Value, Data)
        else if RegDataType = 2 then
          Reg.WriteExpandString(Value, Data)
        else if RegDataType = 3 then
          Reg.WriteInteger(Value, Data)
        else if RegDataType = 4 then begin
          s := Data;
          Reg.WriteBinaryData(Value, PChar(s)^, Length(s));
        end else
          raise Exception.Create(SysErrorMessage(ERROR_CANTWRITE));
      except
        Reg.CloseKey;
        raise;
      end;
      Reg.CloseKey;
    end else
      raise Exception.Create(SysErrorMessage(GetLastError));
  finally
    Reg.Free;
  end;
end;
//------------------------------------------------------------------------------

//Funci�n para Verificar si existe una clave en el registro---------------------
Function ExisteKey_Registry (Root_Key:HKEY;App_Key :String):Boolean;
var
  reg : TRegistry;
begin
  reg := TRegistry.Create(KEY_READ);
  Result:=False;
  reg.RootKey := Root_Key;
  if (not reg.KeyExists(App_Key)) then
    begin
      MessageDlg('No se encontro en el Registro la llave: ' + App_Key,mterror, [mbok],0);
      Result:=False
    end
    Else
    Begin
      Result:=True;
    End;
  //Cerramos el Registro
  reg.CloseKey();
  reg.Free;
end;
//------------------------------------------------------------------------------


//Funci�n para Leer Valores del Registro----------------------------------------
function GetRegistryData(RootKey: HKEY; Key, Value: string): variant;
var
  Reg: TRegistry;
  RegDataType: TRegDataType;
  DataSize, Len: integer;
  s: string;
label cantread;
begin
  Reg := nil;
  try
    Reg := TRegistry.Create(KEY_QUERY_VALUE);
    Reg.RootKey := RootKey;
    if Reg.OpenKeyReadOnly(Key) then begin
      try
        RegDataType := Reg.GetDataType(Value);
        if (RegDataType = rdString) or
           (RegDataType = rdExpandString) then
          Result := Reg.ReadString(Value)
        else if RegDataType = rdInteger then
          Result := Reg.ReadInteger(Value)
        else if RegDataType = rdBinary then begin
          DataSize := Reg.GetDataSize(Value);
          if DataSize = -1 then goto cantread;
          SetLength(s, DataSize);
          Len := Reg.ReadBinaryData(Value, PChar(s)^, DataSize);
          if Len <> DataSize then goto cantread;
          Result := s;
        end else
cantread:
      raise Exception.Create(SysErrorMessage(ERROR_CANTREAD));
      except
        s := ''; // Deallocates memory if allocated
        Reg.CloseKey;
        raise;
      end;
      Reg.CloseKey;
    end else
    //    raise Exception.Create(SysErrorMessage(GetLastError));
    // Si se ubica el valor en el registro se detiene la ejecucion del programa
    raise Exception.Create('No se ubico el Valor en el Registro ' + Key + ' No se puede CONTINUAR con la Ejecucion del Programa');

  except
    Reg.Free;
    raise;
  end;
  Reg.Free;
end;
//------------------------------------------------------------------------------


//----------------------------------------------------------------------------
//Funci�n que genera el xml de la cabecera para la impresora Fiscal
//----------------------------------------------------------------------------
function Xml_PCabFiscal(CnADO:TADOConnection;SerieDoc,ModoDoc: String;TipoFront,NumeroDoc,TipoDoc:Integer; var MensajeError:String): string;
var
  DataFile,
  StrSQL01,
  RutaApps,
  VarXML:string;

  rs200 :TADOQuery;  //Recordset01 de las lineas del albaran
  Conectado:Boolean;
  pfileXML : TXMLDocument;
  xmlCabecera:String;
  CDS: TClientDataSet;
  DataSetProvider: TDataSetProvider;
  i:Integer;
begin
  //
  RutaApps:= ExtractFilePath(ParamStr(0));
  DataFile:='ICGDO_pcab.xml';
  if Length(DataFile)=0
    then
      DataFile:='ICGDO_pcab.xml';
  Try
    //Si no esta abierta la conexi�n a la base de datos se retorna
    if CnADO.Connected=False then
    Begin
      MensajeError:='La conexi�n a la base de datos del Front no esta disponible';
      Result := xmlCabecera;
      Exit;
    End;

    //Se crea los objetos ado y data set requeridos
    rs200:= TADOQuery.Create(nil);
    CDS := TClientDataSet.Create(nil);
    DataSetProvider := TDataSetProvider.Create(nil);


    //De arma el sql se acuerdo al tipo de Front requerido
    Case TipoFront  of
      1: // FrontRest
      Begin
        StrSQL01:='';


      End;

      2: // FrontRetail, hotel o manager
      Begin
        StrSQL01:='';
          StrSQL01:='SELECT 0 AS FO, B.NUMSERIE AS SERIE,B.NUMALBARAN AS NUMERO, A.NUMSERIE AS SERIEFAC,' +
        ' A.NUMFACTURA,B.SALA,B.MESA, A.DTOCOMERCIAL,A.TOTALBRUTO,A.TOTALNETO, A.ENTREGADO,' +
        ' A.CAMBIO,A.CODCLIENTE,A.TOTDTOCOMERCIAL,A.DTOPP,A.TOTDTOPP,A.FECHA,A.HORA,A.CAJA,' +
        ' B.IMPRESIONES,A.CODVENDEDOR, C.NOMVENDEDOR,C.NOMBRECORTO,A.PROPINA, ' +
        ' A.IVAINCLUIDO AS IMPUESTOSINCLUIDOS, -1 AS ABONODE_NUMFISCAL,'''' AS ABONODE_SERIEFISCAL,' +
        ' '''' AS ABONODE_NUMSERIE, -1 ABONODE_NUMALBARAN, 2 AS NUMDECIMALES, ' +
        ' A.TOTALIMPUESTOS AS TOTALIMPUESTO1, 0 AS TOTALIMPUESTO2,-1 AS CODIFIDEL, ' +
        ' '''' AS DESCUNID1,'''' AS DESCUNID2,'''' AS DESCUNID3, '''' AS DESCUNID4,' +
        ' '''' AS ABONODE_SERIEFACTURA, -1 AS ABONODE_NUMFACTURA, D.* ' +
        ' FROM FACTURASVENTA AS A WITH (NOLOCK)' +
        ' INNER JOIN ALBVENTACAB AS B WITH (NOLOCK) ON A.NUMSERIE=B.NUMSERIEFAC AND A.NUMFACTURA=B.NUMFAC AND A.N=B.NFAC ' +
        ' LEFT JOIN VENDEDORES AS C WITH (NOLOCK) ON A.CODVENDEDOR=C.CODVENDEDOR  ' +
        ' LEFT JOIN FACTURASVENTACAMPOSLIBRES AS D ON A.NUMFACTURA=D.NUMFACTURA ' +
        ' AND A.NUMSERIE=D.NUMSERIE AND A.N=D.N ' +
        ' WHERE A.NUMSERIE = ' + QuotedStr(SerieDoc) +
        ' AND A.NUMFACTURA = ' + IntToStr(NumeroDoc) +
        ' AND A.N=' +  QuotedStr(ModoDoc) +
        ' AND A.TIPODOC=' +  IntToStr(TipoDoc) ;
      End;
    End;


    //----------------------------------------------------------------------
    //Se Abre el DataSet  con los calculos del impuesto FPTE
    //----------------------------------------------------------------------
    Conectado:= Open_ADO_Qry(CnADO, rs200,StrSQL01,MensajeError,false,False);
    //----------------------------------------------------------------------

    //Si no hay conexi�n se interrumpe la ejecuci�n de la aplicaci�n
    if Conectado=False then Exit;

    //------------------------------------------------------------------------
    //Se Verifica si el archivo del subtotal existe, para borrarlo
    //------------------------------------------------------------------------

    if FileExists(RutaApps+DataFile)then
    begin
      EliminarArchivo (RutaApps+DataFile)
    end;

    //se asigna la propiedad del data set que permite leer y escribir
    DataSetProvider.DataSet := rs200;
    CDS.FieldDefs.Assign(rs200.FieldDefs);
    CDS.CreateDataSet;
    //Se recorren todos los campos
    for i := 0 to CDS.FieldCount - 1 do begin
        cds.Fields[i].ReadOnly := False;
    end;

    CDS.Active:=True;
    rs200.First;
    //se recorre el data set
    while not rs200.Eof do begin
      cds.Append;
      for i := 0 to rs200.FieldCount - 1 do begin
        cds.Fields[i].Value := rs200.Fields[i].Value;
      end;
      cds.Post;
      rs200.Next;
    end;
    //Se abre el data set
    CDS.Open;
    CDS.SaveToFile(RutaApps+DataFile, dfXMLUTF8);
    pfileXML := TXMLDocument.Create(nil);
    pfileXML.XML.Text:= xmlDoc.FormatXMLData(pfileXML.XML.Text);
    pfileXML.Active := true;
    pfileXML.LoadFromFile(RutaApps+DataFile);
    // Se Guarda el archivo creado
    VarXML:=pfileXML.XML.Text;
//    xmlCabecera:=PAnsiChar(AnsiString(Varxml));
    xmlCabecera:=(Varxml);

    pfileXML.Active := False;
    MensajeError:='';
  Finally
     rs200.Close;
     rs200.free;
     pfileXML.Free;
     DataSetProvider.Free;
     CDS.Close;
     CDS.Free;
  End;
  Result := xmlCabecera;
end;

//----------------------------------------------------------------------------
//Funci�n que genera el xml de la Lineas para la impresora Fiscal
//----------------------------------------------------------------------------
function Xml_PLinFiscal(CnADO:TADOConnection;SerieDoc,ModoDoc: String;TipoFront,NumeroDoc,TipoDoc:Integer; var MensajeError:String): string;
var
  DataFile,
  StrSQL01,
  RutaApps,
  VarXML:string;

  rs001 :TADOQuery;  //Recordset01 de las lineas del albaran
  Conectado:Boolean;
  pfileXML : TXMLDocument;
  Detalle:string;
  CDS: TClientDataSet;
  Dsp: TDataSetProvider;
  i:Integer;
begin
  //
  RutaApps:= ExtractFilePath(ParamStr(0));
  DataFile:='ICGDO_plin.xml';
  if Length(DataFile)=0 then  DataFile:='ICGDO_plin.xml';
  Try
    //Si no esta abierta la conexi�n a la base de datos se retorna
    if CnADO.Connected=False then
    Begin
      MensajeError:='La conexi�n a la base de datos del Front no esta disponible';
      Result := Detalle;
      Exit;
    End;

    //Se crea los objetos ado y data set requeridos
    rs001:= TADOQuery.Create(nil);
    CDS := TClientDataSet.Create(nil);
    Dsp := TDataSetProvider.Create(nil);
    CDS.FieldDefs.Clear;

    //De arma el sql se acuerdo al tipo de Front requerido
    Case TipoFront  of
      1: // FrontRest
      Begin
        StrSQL01:='';


      End;

      2: // FrontRetail, hotel o manager
      Begin
        StrSQL01:='';
        StrSQL01:='SELECT B.NUMSERIE ,B.NUMALBARAN , C.NUMLIN, C.REFERENCIA, C.SUPEDIDO,  ' +
          ' C.TALLA,C.COLOR,C.DESCRIPCION, C.UNIDADESTOTAL AS UNIDADES, C.PRECIO,  ' +
          ' C.IVA, C.TOTAL, C.DTO,C.TIPOIMPUESTO, C.PRECIOIVA, C.TOTALEXPANSION,  ' +
          ' C.UNID1,C.UNID2,C.UNID3,C.UNID4, C.CODARTICULO, E.DPTO,E.SECCION,  ' +
          ' E.PORPESO,F.DESCRIPCION AS DESCSECCION, G.CODBARRAS, C.LINEAOCULTA,  ' +
          '  '''' AS MODIFICADORES,C.CODALMACEN, H.NOMBREALMACEN,E.DESCRIPADIC, C.CODVENDEDOR,D.NOMVENDEDOR,      ' +
          ' '''' AS DESCPROMOCION, '''' AS DTOPROMOCION,'''' AS IMPORTEDTOPROMOCION, '''' AS IMPORTEDTOIVAPROMOCION, ' +
          ' '''' AS IMPORTEPROMOCION , '''' AS IMPORTEIVAPROMOCION FROM FACTURASVENTA AS A WITH (NOLOCK)                           ' +
          ' INNER JOIN ALBVENTACAB AS B WITH (NOLOCK) ON A.NUMSERIE=B.NUMSERIEFAC AND A.NUMFACTURA=B.NUMFAC AND A.N=B.NFAC ' +
          ' INNER JOIN ALBVENTALIN AS C WITH (NOLOCK) ON B.NUMSERIE=C.NUMSERIE AND B.NUMALBARAN=C.NUMALBARAN AND B.N=C.N   ' +
          ' LEFT JOIN VENDEDORES AS D WITH (NOLOCK) ON A.CODVENDEDOR=D.CODVENDEDOR     ' +
          ' LEFT JOIN ARTICULOS AS E WITH (NOLOCK) ON C.CODARTICULO=E.CODARTICULO   ' +
          ' LEFT JOIN SECCIONES AS F WITH (NOLOCK) ON E.DPTO=F.NUMDPTO AND E.SECCION=F.NUMSECCION ' +
          ' LEFT JOIN ARTICULOSLIN AS G WITH (NOLOCK) ON C.CODARTICULO=G.CODARTICULO AND C.TALLA=G.TALLA AND C.COLOR=G.COLOR ' +
          ' LEFT JOIN ALMACEN AS H WITH (NOLOCK) ON C.CODALMACEN=H.CODALMACEN   ' +
          ' WHERE A.NUMSERIE = ' + QuotedStr(SerieDoc) +
          ' AND A.NUMFACTURA = ' + IntToStr(NumeroDoc) +
          ' AND A.N=' +  QuotedStr(ModoDoc) +
          ' AND A.TIPODOC=' +  IntToStr(TipoDoc) ;
      End;
    End;

    //----------------------------------------------------------------------
    //Se Abre el DataSet  con los calculos del impuesto FPTE
    //----------------------------------------------------------------------
    Conectado:= Open_ADO_Qry(CnADO, rs001,StrSQL01,MensajeError,false,False);
    //----------------------------------------------------------------------

    //Si no hay conexi�n se interrumpe la ejecuci�n de la aplicaci�n
    if Conectado=False then Exit;

    //------------------------------------------------------------------------
    //Se Verifica si el archivo del subtotal existe, para borrarlo
    //------------------------------------------------------------------------

    if FileExists(RutaApps+DataFile)then
    begin
      EliminarArchivo (RutaApps+DataFile)
    end;

    // rs001.SaveToFile(RutaApps+DataFile,  pfileXML);
     Dsp.DataSet := rs001;
    CDS.FieldDefs.Assign(rs001.FieldDefs);
    CDS.CreateDataSet;
    //Se recorren todos los campos
    for i := 0 to CDS.FieldCount - 1 do begin
        cds.Fields[i].ReadOnly := False;
    end;

    CDS.Active:=True;
    rs001.First;
    //se recorre el data set
    while not rs001.Eof do begin
      cds.Append;
      for i := 0 to rs001.FieldCount - 1 do begin
        cds.Fields[i].Value := rs001.Fields[i].Value;
      end;
      cds.Post;
      rs001.Next;
    end;
    //Se abre el data set
    CDS.Open;
    CDS.Open;
    CDS.SaveToFile(RutaApps+DataFile, dfXMLUTF8);
    pfileXML := TXMLDocument.Create(nil);
    pfileXML.XML.Text:= xmlDoc.FormatXMLData(pfileXML.XML.Text);
    pfileXML.Active := true;
    pfileXML.LoadFromFile(RutaApps+DataFile);
    // Se Guarda el archivo creado
    VarXML:=pfileXML.XML.Text;
    Detalle:=Varxml;
    //XML.SaveToFile(rutaFichero);
    pfileXML.Active := False;
  Finally
     rs001.Close;
     rs001.free;
     pfileXML.Free;
     Dsp.Free;
     CDS.Close;
     CDS.Free;
  End;
    Result := Detalle;
end;

//----------------------------------------------------------------------------
//Funci�n que genera el xml de las formas de pago para la impresora Fiscal
//----------------------------------------------------------------------------
function Xml_PFPaFiscal(CnADO:TADOConnection;SerieDoc,ModoDoc: String;TipoFront,NumeroDoc,TipoDoc:Integer; var MensajeError:String):string;
var
  DataFile,
  StrSQL01,
  RutaApps,
  VarXML:string;

  rs001 :TADOQuery;  //Recordset01 de las lineas del albaran
  Conectado:Boolean;
  pfileXML : TXMLDocument;
  FPago:string;
  CDS: TClientDataSet;
  Dsp: TDataSetProvider;
  i:Integer;
begin
  //
  RutaApps:= ExtractFilePath(ParamStr(0));
  DataFile:='ICGDO_pfPa.xml';
  if Length(DataFile)=0 then  DataFile:='ICGDO_pfPa.xml';
  Try
    //Si no esta abierta la conexi�n a la base de datos se retorna
    if CnADO.Connected=False then
    Begin
      MensajeError:='La conexi�n a la base de datos del Front no esta disponible';
      Result := FPago;
      Exit;
    End;

    //Se crea los objetos ado y data set requeridos
    rs001:= TADOQuery.Create(nil);
    CDS := TClientDataSet.Create(nil);
    Dsp := TDataSetProvider.Create(nil);
    CDS.FieldDefs.Clear;

    //De arma el sql se acuerdo al tipo de Front requerido
    Case TipoFront  of
      1: // FrontRest
      Begin
        StrSQL01:='';


      End;

      2: // FrontRetail, hotel o manager
      Begin
        StrSQL01:='';
          StrSQL01:='SELECT A.CODFORMAPAGO AS CodFP, B.DESCRIPCION AS Descripcion,    '  +
        ' A.IMPORTE AS Cantidad, SUBSTRING(C.DESCRIPCION,1,1) AS Moneda,  '  +
        ' A.FACTORMONEDA AS Cotizacion, C.NUMDECIMALES AS NumDecimales,   '  +
        ' CONVERT(nvarchar,A.FECHAVENCIMIENTO,103) + ''|'' + FORMAT(A.IMPORTE,''000000000.00'') + ''|'' AS Vencimientos  '  +
        ' FROM TESORERIA AS A WITH (NOLOCK) '  +
        ' LEFT JOIN FORMASPAGO AS B WITH (NOLOCK) ON A.CODFORMAPAGO=B.CODFORMAPAGO   '  +
        ' LEFT JOIN MONEDAS AS C WITH (NOLOCK) ON A.CODMONEDA=C.CODMONEDA   '  +
        ' WHERE A.ORIGEN=''C''  '  +
        ' AND A.TIPODOCUMENTO=''F''    '  +
        ' AND A.SERIE = ' + QuotedStr(SerieDoc) +
        ' AND A.NUMERO = ' + IntToStr(NumeroDoc) +
        ' AND A.N=' +  QuotedStr(ModoDoc) ;
      End;
    End;

    //----------------------------------------------------------------------
    //Se Abre el DataSet  con los calculos del impuesto FPTE
    //----------------------------------------------------------------------
    Conectado:= Open_ADO_Qry(CnADO, rs001,StrSQL01,MensajeError,false,False);
    //----------------------------------------------------------------------

    //Si no hay conexi�n se interrumpe la ejecuci�n de la aplicaci�n
    if Conectado=False then Exit;

    //------------------------------------------------------------------------
    //Se Verifica si el archivo del subtotal existe, para borrarlo
    //------------------------------------------------------------------------

    if FileExists(RutaApps+DataFile)then
    begin
      EliminarArchivo (RutaApps+DataFile)
    end;

    // rs001.SaveToFile(RutaApps+DataFile,  pfileXML);
    Dsp.DataSet := rs001;
    CDS.FieldDefs.Assign(rs001.FieldDefs);
    CDS.CreateDataSet;
    //Se recorren todos los campos
    for i := 0 to CDS.FieldCount - 1 do begin
        cds.Fields[i].ReadOnly := False;
    end;
    CDS.Active:=True;
    rs001.First;
    //se recorre el data set
    while not rs001.Eof do begin
      cds.Append;
      for i := 0 to rs001.FieldCount - 1 do begin
        cds.Fields[i].Value := rs001.Fields[i].Value;
      end;
      cds.Post;
      rs001.Next;
    end;
    //Se abre el data set
    CDS.Open;
    CDS.Open;
    CDS.SaveToFile(RutaApps+DataFile, dfXMLUTF8);
    pfileXML := TXMLDocument.Create(nil);
    pfileXML.XML.Text:= xmlDoc.FormatXMLData(pfileXML.XML.Text);
    pfileXML.Active := true;
    pfileXML.LoadFromFile(RutaApps+DataFile);
    // Se Guarda el archivo creado
    VarXML:=pfileXML.XML.Text;
    FPago:=Varxml;
    //XML.SaveToFile(rutaFichero);
    pfileXML.Active := False;
  Finally
     rs001.Close;
     rs001.free;
     pfileXML.Free;
     Dsp.Free;
     CDS.Close;
     CDS.Free;
  End;
    Result := FPago;

end;

//----------------------------------------------------------------------------
//Funci�n que genera el xml del cliente para la impresora Fiscal
//----------------------------------------------------------------------------
function Xml_PCliFiscal(CnADO:TADOConnection;SerieDoc,ModoDoc: String;TipoFront,NumeroDoc,TipoDoc:Integer; var MensajeError:String): String;
var
  DataFile,
  StrSQL01,
  RutaApps,
  VarXML:string;
  rs001 :TADOQuery;  //Recordset01 de las lineas del albaran
  Conectado:Boolean;
  pfileXML : TXMLDocument;
  Cliente:String;
  CDS: TClientDataSet;
  Dsp: TDataSetProvider;
  i:Integer;
begin
  //
  RutaApps:= ExtractFilePath(ParamStr(0));
  DataFile:='ICGDO_PCli.xml';
  if Length(DataFile)=0 then  DataFile:='ICGDO_PCli.xml';
  Try
    //Si no esta abierta la conexi�n a la base de datos se retorna
    if CnADO.Connected=False then
    Begin
      MensajeError:='La conexi�n a la base de datos del Front no esta disponible';
      Result := Cliente;
      Exit;
    End;

    //Se crea los objetos ado y data set requeridos
    rs001:= TADOQuery.Create(nil);
    CDS := TClientDataSet.Create(nil);
    Dsp := TDataSetProvider.Create(nil);
    CDS.FieldDefs.Clear;

    //De arma el sql se acuerdo al tipo de Front requerido
    Case TipoFront  of
      1: // FrontRest
      Begin
        StrSQL01:='';


      End;

      2: // FrontRetail, hotel o manager
      Begin
        StrSQL01:='';
        StrSQL01:='SELECT '''' AS NomDisp,A.NUMSERIE AS Serie, A.NUMFACTURA AS Numero,' +
        ' A.N, '''' AS FormaPago , 0 AS NumPedido, ''FALSE'' AS AbrirCajon ,    ' +
        ' B.NOMBRECOMERCIAL AS NomCliente, B.TELEFONO1 AS Telefono, B.CIF,  ' +
        ' B.DIRECCION1 AS Direccion1, B.DIRECCION2 AS Direccion2, B.CODPOSTAL AS CodPostal, ' +
        ' B.POBLACION AS Poblacion, B.PROVINCIA AS Provincia, B.PAIS AS Pais, ' +
        ' B.REGIMFACT AS RegimFact, B.TIPODOC AS TipoDocId, CL.*  ' +
        ' FROM FACTURASVENTA AS A WITH (NOLOCK)    ' +
        ' LEFT JOIN CLIENTES AS B WITH (NOLOCK) ON A.CODCLIENTE=B.CODCLIENTE  ' +
        ' INNER JOIN ALBVENTACAB AS C WITH (NOLOCK) ON A.NUMSERIE=C.NUMSERIEFAC AND A.NUMFACTURA=C.NUMFAC AND A.N=C.NFAC ' +
        ' LEFT JOIN CLIENTESCAMPOSLIBRES AS CL ON A.CODCLIENTE = CL.CODCLIENTE ' +
        ' AND A.NUMFACTURA=C.NUMFAC AND A.N=C.NFAC   ' +
        ' WHERE A.NUMSERIE = ' + QuotedStr(SerieDoc) +
        ' AND A.NUMFACTURA = ' + IntToStr(NumeroDoc) +
        ' AND A.N=' +  QuotedStr(ModoDoc) +
        ' AND A.TIPODOC=' +  IntToStr(TipoDoc) ;
      End;
    End;

    //----------------------------------------------------------------------
    //Se Abre el DataSet  con los calculos del impuesto FPTE
    //----------------------------------------------------------------------
    Conectado:= Open_ADO_Qry(CnADO, rs001,StrSQL01,MensajeError,false,False);
    //----------------------------------------------------------------------

    //Si no hay conexi�n se interrumpe la ejecuci�n de la aplicaci�n
    if Conectado=False then Exit;

    //------------------------------------------------------------------------
    //Se Verifica si el archivo del subtotal existe, para borrarlo
    //------------------------------------------------------------------------

    if FileExists(RutaApps+DataFile)then
    begin
      EliminarArchivo (RutaApps+DataFile)
    end;

    // rs001.SaveToFile(RutaApps+DataFile,  pfileXML);
   //se asigna la propiedad del data set que permite leer y escribir
    Dsp.DataSet := rs001;
    CDS.FieldDefs.Assign(rs001.FieldDefs);
    CDS.CreateDataSet;
    //Se recorren todos los campos
    for i := 0 to CDS.FieldCount - 1 do begin
        cds.Fields[i].ReadOnly := False;
    end;

    CDS.Active:=True;
    rs001.First;
    //se recorre el data set
    while not rs001.Eof do begin
      cds.Append;
      for i := 0 to rs001.FieldCount - 1 do begin
        cds.Fields[i].Value := rs001.Fields[i].Value;
      end;
      cds.Post;
      rs001.Next;
    end;
    //Se abre el data set
    CDS.Open;
    CDS.SaveToFile(RutaApps+DataFile, dfXMLUTF8);
    pfileXML := TXMLDocument.Create(nil);
    pfileXML.XML.Text:= xmlDoc.FormatXMLData(pfileXML.XML.Text);
    pfileXML.Active := true;
    pfileXML.LoadFromFile(RutaApps+DataFile);
    // Se Guarda el archivo creado
    VarXML:=pfileXML.XML.Text;
    Cliente:=Varxml;
    //XML.SaveToFile(rutaFichero);
    pfileXML.Active := False;
  Finally
    rs001.Close;
    rs001.free;
    pfileXML.Free;
    Dsp.Free;
    CDS.Close;
    CDS.Free;
  End;
    Result := Cliente;

//
end;


end.
