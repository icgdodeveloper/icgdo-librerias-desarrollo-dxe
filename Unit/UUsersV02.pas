//------------------------------------------------------------------------------
{Unit que gestiona el funcionamiento de los usuarios en el servidor Rest
ICG Dominicana,SRL
Rafael Rangel
20180119}
//------------------------------------------------------------------------------
unit uUsersV02;

interface
  Uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
    Vcl.Dialogs,  WinSock,
    IniFiles,ADODB ,Data.DB,ActiveX, UnitComunesRest,

    //Bibliotecas de Bases de datos
  FireDAC.Stan.ExprFuncs, FireDAC.UI.Intf, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.Comp.Client, FireDAC.Comp.DataSet, FireDAC.Comp.UI,
  FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef   ;



//Informaci�n del Usuario
type TUserData   = record
  UserID:Integer;
  LoginName, FullName, Password:string;
  LastAccess, CreatedDate:TDateTime;
  Enabled:Boolean;
end;

//Informaci�n de los datos de conexi�n a la base de datos
type TDatosConexion   = record
  ServerDB,NameDB,UsersDB,PasswordDB:String;
  GeneraLogErrores,GeneraLogData:Integer;
end;




Var
  IniFile : TIniFile;
  MensajeError:string;
  FUserNameField,FKeyField,
  Mensaje:string;
  Conectado:Boolean;
  StrSQL10:TStrings;
  //----------------------------------------------------------------------------
  //Funci�n que verifica si el login del usuario es correcto
  //----------------------------------------------------------------------------
  function LoginUsers (LoginName,LoginPassword:String;  InfoConexion:TParametrosApps): Boolean;
  //----------------------------------------------------------------------------

  //----------------------------------------------------------------------------
  //Funci�n que inserta o modifica el usuario
  //----------------------------------------------------------------------------
  function Insert_UpdateUsers (LoginName,FullName,LoginPassword:string;Enabled:Boolean;  InfoConexion:TParametrosApps): Boolean;
  //----------------------------------------------------------------------------

  //----------------------------------------------------------------------------
  //Funci�n que verifica si existen usuarios
  //----------------------------------------------------------------------------
  function ExistUsers(InfoConexion:TParametrosApps) : Integer;

  //----------------------------------------------------------------------------
  //Funci�n que carga en memoeria lo datos del usuario
  //----------------------------------------------------------------------------
  function DataUsers (LoginName:String;  InfoConexion:TParametrosApps): TUserData;
  //----------------------------------------------------------------------------

  //----------------------------------------------------------------------------
  //Funci�n que ubica los miembros del grupo
  //----------------------------------------------------------------------------
  function GroupMembershipLocate(const KeyValues: Variant;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;

  //----------------------------------------------------------------------------
  //Funci�n que ubica un registro en un data set
  //----------------------------------------------------------------------------
  function LocateValue(const KeyValues: Variant;FKeyField:string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;

  //------------------------------------------------------------------------------
  //Busca en la tabla de Accesos de los grupos
  //------------------------------------------------------------------------------
  function HasGroupAccess(Group,Permission, GroupNameField: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;

  //------------------------------------------------------------------------------
  //Remueve los accesos del grupo
  //------------------------------------------------------------------------------
  function RemoveGroupAccess(Group,Permission,GroupNameField: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;

  //------------------------------------------------------------------------------
  //Agrego los grupos accesos
  //------------------------------------------------------------------------------
  function AddGroupAccess(Group,Permission,GroupNameField: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;

  //------------------------------------------------------------------------------
  //Agrego los usuarios de los grupos
  //------------------------------------------------------------------------------
  function AddUserToGroup(Username, GroupName: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;

  //----------------------------------------------------------------------------
  //se borra los usuarios de los grupos
  //------------------------------------------------------------------------------
  function RemoveUserFromGroup(Username, GroupName: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;


  //------------------------------------------------------------------------------
 //Se busca en la tabla de permisos de usuarios
 //------------------------------------------------------------------------------
 function HasUserAccess(UserName,Permission: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;

 //------------------------------------------------------------------------------
 //Se busca en la tabla de permisos de usuarios
 //------------------------------------------------------------------------------
 function RemoveUserAccess(UserName,Permission: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;


 //------------------------------------------------------------------------------
 //Se agrega en la tabla de permisos de usuarios
 //------------------------------------------------------------------------------
 function AddUserAccess(UserName,  Permission: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;



implementation
//Uses  Usrv;

//----------------------------------------------------------------------------
//Funci�n que verifica si el login del usuario es correcto
//----------------------------------------------------------------------------
function LoginUsers (LoginName,LoginPassword:String; InfoConexion:TParametrosApps): Boolean;
var
  Logueado:Boolean;
  //Objetos de Base de datos
  u03_Cn001: TADOConnection; //Conexi�n a la Base de datos
  u03_Rs001: TADOQuery;
  StrSql01, NameDBLocal,  RutaDBLocal:string;

  u03_FDCn01: TFDConnection;
  u03_FDRs01: TFDQuery;


begin
  Try
    //--------------------------------------------------------------------------
    // Validaci�n de usuarios
    //--------------------------------------------------------------------------
    Logueado:=False;
    Result:=Logueado;
    //--------------------------------------------------------------------------
    //Se Verifica si la existe el usuario
    //--------------------------------------------------------------------------
    StrSQL01:='';
    StrSQL01:='SELECT LoginName FROM ICGDO_TB_AC_Users AS A WITH (NOLOCK) ' +
      ' WHERE LoginName=  ' + QuotedStr(LoginName) +
      ' AND Enabled=1 ' +
      ' AND  Password=' + QuotedStr(encriptar(LoginPassword,10));

    if InfoConexion.TConexDBLocal <> 2 then  //Ado
    Begin

      CoInitialize(nil) ;
      //Creamos el Objeto de Conexi�n a la base de datos Master
      u03_Cn001  := TADOConnection.Create(nil);
      //--------------------------------------------------------------------------
      //Conexi�n a la Base de datos master
      //--------------------------------------------------------------------------
      Logueado:=Open_DB (u03_Cn001 , 12 , InfoConexion.ServerDB,InfoConexion.NameDB, InfoConexion.UserDB, InfoConexion.PasswordDB, '',MensajeError,False );
      //---------------------------------------------------------------------------
      //si no hay conexi�n con el la base de datos, se interrumpe el proceso
      //---------------------------------------------------------------------------
      if Logueado =False then
      begin
        u03_Cn001.Close;
        u03_Cn001.Free;
        CoUninitialize;
        Result:=Logueado;
        WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + MensajeError,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
        Exit;
      end;

      //------------------------------------------------------------------------
      //Se Verfica si existe la base de datos del Servicio Rest.
      //------------------------------------------------------------------------
      u03_Rs001  := TADOQuery.Create(nil);
      Logueado:= Open_ADO_Qry(u03_Cn001,u03_rs001,StrSQL01,MensajeError,True,False);
      if (not u03_rs001.Eof) and (u03_Rs001.Fields[0].AsString=LoginName) then Logueado:=True;
      //--------------------------------------------------------------------------
      //Cerramos los objetos ado y el hilo de ejecuci�n.
      //--------------------------------------------------------------------------
      u03_Rs001.Close;
      u03_Rs001.Free;
      u03_Cn001.Close;
      u03_Cn001.Free;
      CoUninitialize;
    End
    else //Si la conexi�n es FireDac
    begin
      //----------------------------------------------------------------------------
      //Se verifica que la base de datos este creada
      //----------------------------------------------------------------------------
      u03_FDCn01:=TFDConnection.Create(nil);
      u03_FDRs01:=TFDQuery.Create(nil);
      u03_FDCn01.DriverName:='SQLite';
      u03_FDRs01.Connection:=u03_FDCn01;
      NameDBLocal:= InfoConexion.NameDB;
      RutaDBLocal :=InfoConexion.PathDB;
      if FileExists(RutaDBLocal+NameDBLocal)then
      begin
        try
          //------------------------------------------------------------------------
          // Parametros de Conexion a la base de datos
          //------------------------------------------------------------------------
          u03_FDCn01.Params.Add('Database=' +RutaDBLocal+NameDBLocal );
          u03_FDCn01.Open();
          u03_FDCn01.Connected:=True;
        except
        on E: EDatabaseError do
          Begin
            // Se guarda el error en el log
            Mensaje:=E.Message;
            //------------------------------------------------------------------------
            // Si hay un error
            //------------------------------------------------------------------------
            //LogErrores(Mensaje,True);
            //------------------------------------------------------------------------
          End;
        end;
      end;
      //Se prepara el script del usuario
      StrSQL01:='';
      StrSQL01:='SELECT LoginName FROM ICGDO_TB_AC_Users ' +
      ' WHERE LoginName=  ' + QuotedStr(LoginName) +
      ' AND Enabled=1 ' +
      ' AND  Password=' + QuotedStr(encriptar(LoginPassword,10));

      StrSQL10:=TStringList.Create;
      StrSQL10.Add(StrSQL01);
      u03_FDRs01.SQL:=StrSQL10;
      //Se Abre el DataSet
      u03_FDRs01.Open;
      //se Verifica si el usuario existe
      if (not u03_FDRs01.Eof) and (u03_FDRs01.Fields[0].AsString=LoginName) then Logueado:=True;

      //Se cierran los objetos de base de datos
      StrSQL10.Free;
      u03_FDRs01.Close();
      u03_FDCn01.close;
      u03_FDCn01.Free;
      u03_FDRs01.Free;
    end;
  Except On E:Exception
  do
    Begin
      Logueado:=False;
      // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
    End;
  End;
  Result:=Logueado;
end;


//----------------------------------------------------------------------------
//Funci�n que verifica si el login del usuario es correcto
//----------------------------------------------------------------------------
function ExistUsers (InfoConexion:TParametrosApps): Integer;
var
  NroUsers:Integer;
  //Objetos de Base de datos
  u01_Cn001: TADOConnection; //Conexi�n a la Base de datos
  u01_rs001: TADOQuery;
  StrSql01:string;
  ConectadoDB:Boolean;
begin
  Try
    //--------------------------------------------------------------------------
    // Validaci�n de usuarios
    //--------------------------------------------------------------------------
    NroUsers:=0;
    //--------------------------------------------------------------------------
    //Se Verifica si la base de datos esta creada
    //--------------------------------------------------------------------------
    StrSQL01:='';
    StrSQL01:='SELECT COUNT(LoginName) AS USUARIOS FROM ICGDO_TB_AC_Users AS A WITH (NOLOCK) ';
    CoInitialize(nil) ;
    //Creamos el Objeto de Conexi�n a la base de datos Master
    u01_Cn001  := TADOConnection.Create(nil);
    u01_rs001  := TADOQuery.Create(nil);
    //--------------------------------------------------------------------------
    //Conexi�n a la Base de datos master
    //--------------------------------------------------------------------------
    ConectadoDB:=Open_DB (u01_Cn001 , 12 ,InfoConexion.ServerDB,InfoConexion.NameDB, InfoConexion.UserDB, InfoConexion.PasswordDB, '',MensajeError,False );
    //---------------------------------------------------------------------------
    //si no hay conexi�n con el la base de datos, se interrumpe el proceso
    //---------------------------------------------------------------------------
    if ConectadoDB =False then
    begin
       Exit;
    end;


    //------------------------------------------------------------------------
    //Se Verfica si existe la base de datos del Servicio Rest.
    //------------------------------------------------------------------------
    ConectadoDB:= Open_ADO_Qry(u01_Cn001,u01_rs001,StrSQL01,MensajeError,True,False);
    if u01_rs001.Eof <> True then NroUsers:=u01_rs001.Fields[0].AsInteger;
    //--------------------------------------------------------------------------
    //Cerramos los objetos ado y el hilo de ejecuci�n.
    //--------------------------------------------------------------------------
    u01_Rs001.Close;
    u01_Rs001.Free;
    u01_Cn001.Close;
    u01_Cn001.Free;
   CoUninitialize;

 Except On E:Exception do
    Begin
      // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
    End;
  End;
  //Retorna la informaci�n.
  Result:=NroUsers;
end;



//------------------------------------------------------------------------------
// Se crea o modifica un usuario
//------------------------------------------------------------------------------
function Insert_UpdateUsers (LoginName,FullName,LoginPassword:string;Enabled:Boolean; InfoConexion:TParametrosApps): Boolean;
var
  Logueado:Boolean;
  //Objetos de Base de datos
  us02_Cn001: TADOConnection; //Conexi�n a la Base de datos
  us02_Rs001: TADOQuery;
  tmpStrSql01:string;
begin
  Try
    //--------------------------------------------------------------------------
    // Validaci�n de usuarios
    //--------------------------------------------------------------------------
    Logueado:=False;
    //--------------------------------------------------------------------------
    //Se Verifica si la base de datos esta creada
    //--------------------------------------------------------------------------
    tmpStrSQL01:='';
    tmpStrSQL01:='SELECT  LoginName FROM  ICGDO_TB_AC_Users AS A WITH (NOLOCK)  ' +
      ' WHERE LoginName=  ' + QuotedStr(LoginName) ;
    CoInitialize(nil) ;
    //Creamos el Objeto de Conexi�n a la base de datos Master
    us02_Cn001  := TADOConnection.Create(nil);
    //--------------------------------------------------------------------------
    //Conexi�n a la Base de datos master
    //--------------------------------------------------------------------------
    Logueado:=Open_DB (us02_Cn001 , 12 , InfoConexion.ServerDB,InfoConexion.NameDB, InfoConexion.UserDB, InfoConexion.PasswordDB, '',MensajeError,False );
    //---------------------------------------------------------------------------
    //si no hay conexi�n con el la base de datos, se interrumpe el proceso
    //---------------------------------------------------------------------------
    if Logueado =False then
    begin
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  :' + MensajeError,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
      us02_Rs001.Close;
      us02_Rs001.Free;
      CoUninitialize;
      Exit;
    end;

    //------------------------------------------------------------------------
    //Se Verfica si existe la base de datos del Servicio Rest.
    //------------------------------------------------------------------------
    us02_Rs001  := TADOQuery.Create(nil);
    Logueado:= Open_ADO_Qry(us02_Cn001,us02_rs001,tmpStrSQL01,MensajeError,True,False);
    if Not us02_Rs001.Eof then
    begin
      tmpStrSQL01:='UPDATE ICGDO_TB_AC_Users  WITH (ROWLOCK) SET  ' +
      ' loginName =' + QuotedStr(LoginName) +  ',' +
      ' FullName =' +  QuotedStr(FullName) + ',' +
      ' Password =' +  QuotedStr(encriptar(LoginPassword,10)) + ',' +
      ' Enabled =' +  BoolToStr(Enabled) +
      ' WHERE LoginName=  ' + QuotedStr(LoginName) ;
    end
    else
    begin
       tmpStrSQL01:='INSERT INTO ICGDO_TB_AC_Users WITH (ROWLOCK) (loginName,FullName,Password)' +
      ' VALUES (' + QuotedStr(LoginName) +  ',' + QuotedStr(FullName) + ',' + QuotedStr(encriptar(LoginPassword,10)) + ')' ;
    end;
    //------------------------------------------------------------------------
    //Se ejecuta el SQL para Verificar o crear la Tabla
    //------------------------------------------------------------------------
    Logueado:= DB_Function(us02_Cn001,tmpStrSQL01,MensajeError,4,False);
    //------------------------------------------------------------------------
    if Logueado =False then
    begin
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  :' + MensajeError,Infoconexion.GeneraLogErrores,InfoConexion.GeneraLogData);
    end;
    //--------------------------------------------------------------------------
    //Cerramos los objetos ado y el hilo de ejecuci�n.
    //--------------------------------------------------------------------------
    us02_Rs001.Close;
    us02_Rs001.Free;
    us02_Cn001.Close;
    us02_Cn001.Free;
    CoUninitialize;
  Except On E:Exception
  do
     Begin
      Logueado:=False;
      // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,infoconexion.GeneraLogErrores,InfoConexion.GeneraLogData);
    End;
  End;
  //Retorna la informaci�n.
  Result:=Logueado;
end;


//----------------------------------------------------------------------------
//Funci�n que carga en memoeria lo datos del usuario
//----------------------------------------------------------------------------
function DataUsers (LoginName:String;  InfoConexion:TParametrosApps): TUserData;
//----------------------------------------------------------------------------
var
  Logueado:Boolean;
  //Objetos de Base de datos
  u03_Cn001: TADOConnection; //Conexi�n a la Base de datos
  u03_Rs001: TADOQuery;
  StrSql01, NameDBLocal,  RutaDBLocal:string;
  UsersInfo:TUserData ;

  u03_FDCn01: TFDConnection;
  u03_FDRs01: TFDQuery;


begin
  Try

    //--------------------------------------------------------------------------
    // Validaci�n de usuarios
    //--------------------------------------------------------------------------
    Logueado:=False;

    //Se verifica el tipo de conexi�n de la base de datos local
    if InfoConexion.TConexDBLocal <> 2 then  //Ado
    Begin
      //--------------------------------------------------------------------------
      //Se Verifica si la existe el usuario
      //--------------------------------------------------------------------------
      StrSQL01:='';
      StrSQL01:='SELECT  UserID, LoginName, FullName, Password, LastAccess, Enabled, CreatedDate FROM ICGDO_TB_AC_Users AS A WITH (NOLOCK) ' +
        ' WHERE LoginName=  ' + QuotedStr(LoginName) ;
      CoInitialize(nil) ;
      //Creamos el Objeto de Conexi�n a la base de datos Master
      u03_Cn001  := TADOConnection.Create(nil);
      //--------------------------------------------------------------------------
      //Conexi�n a la Base de datos master
      //--------------------------------------------------------------------------
      Logueado:=Open_DB (u03_Cn001 , 12 , InfoConexion.ServerDB,InfoConexion.NameDB, InfoConexion.UserDB, InfoConexion.PasswordDB, '',MensajeError,False );
      //---------------------------------------------------------------------------
      //si no hay conexi�n con el la base de datos, se interrumpe el proceso
      //---------------------------------------------------------------------------
      if Logueado =False then
      begin
        u03_Cn001.Close;
        u03_Cn001.Free;
        CoUninitialize;
        Result:=UsersInfo;;
        WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + MensajeError,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
        Exit;
      end;

      //------------------------------------------------------------------------
      //Se Verfica si existe la base de datos del Servicio Rest.
      //------------------------------------------------------------------------
      u03_Rs001  := TADOQuery.Create(nil);
      Logueado:= Open_ADO_Qry(u03_Cn001,u03_rs001,StrSQL01,MensajeError,True,False);
      if (not u03_rs001.Eof) and (u03_Rs001.Fields[1].AsString=LoginName) then
      Begin
        with u03_rs001 do
        Begin
          UsersInfo.UserID:=Fields[0].AsInteger;
          UsersInfo.LoginName:=Fields[1].AsString;
          UsersInfo.FullName:=Fields[2].AsString;
          UsersInfo.Password :=Fields[3].AsString;
          UsersInfo.LastAccess :=Fields[4].AsDateTime;
          UsersInfo.Enabled :=Fields[5].AsBoolean;
          UsersInfo.CreatedDate :=Fields[6].AsDateTime;
        End;
      End;
      //--------------------------------------------------------------------------
      //Cerramos los objetos ado y el hilo de ejecuci�n.
      //--------------------------------------------------------------------------
      u03_Rs001.Close;
      u03_Rs001.Free;
      u03_Cn001.Close;
      u03_Cn001.Free;
      CoUninitialize;
    End
    Else
    begin
      //----------------------------------------------------------------------------
      //Se verifica que la base de datos este creada
      //----------------------------------------------------------------------------
      u03_FDCn01:=TFDConnection.Create(nil);
      u03_FDRs01:=TFDQuery.Create(nil);
      u03_FDCn01.DriverName:='SQLite';
      NameDBLocal:= InfoConexion.NameDB;
      RutaDBLocal :=InfoConexion.PathDB;
      if FileExists(RutaDBLocal+NameDBLocal)then
      begin
        try
          //------------------------------------------------------------------------
          // Parametros de Conexion a la base de datos
          //------------------------------------------------------------------------
          u03_FDCn01.Params.Add('Database=' +RutaDBLocal+NameDBLocal );
          u03_FDCn01.Open();
          u03_FDCn01.Connected:=True;
        except
        on E: EDatabaseError do
          Begin
            // Se guarda el error en el log
            Mensaje:=E.Message;
            //------------------------------------------------------------------------
            // Si hay un error
            //------------------------------------------------------------------------
            //LogErrores(Mensaje,True);
            //------------------------------------------------------------------------
          End;
        end;
      end;
      //Se prepara el script del usuario
      StrSQL01:='';
      StrSQL01:='SELECT  UserID, LoginName, FullName, Password, LastAccess, Enabled, CreatedDate FROM ICGDO_TB_AC_Users ' +
        ' WHERE LoginName=  ' + QuotedStr(LoginName) ;
      StrSQL10:=TStringList.Create;
      StrSQL10.Add(StrSQL01);
      u03_FDRs01.Connection:=u03_FDCn01;
      u03_FDRs01.SQL:=StrSQL10;
      //Se Abre el DataSet
      u03_FDRs01.Open;

      if (not u03_FDRs01.Eof) and (u03_FDRs01.Fields[1].AsString=LoginName) then
      Begin
        with u03_FDRs01 do
        Begin
          UsersInfo.UserID:=Fields[0].AsInteger;
          UsersInfo.LoginName:=Fields[1].AsString;
          UsersInfo.FullName:=Fields[2].AsString;
          UsersInfo.Password :=Fields[3].AsString;
          UsersInfo.LastAccess :=Fields[4].AsDateTime;
          UsersInfo.Enabled :=Fields[5].AsBoolean;
          UsersInfo.CreatedDate :=Fields[6].AsDateTime;
        End;
      End;
      //Se cierran los objetos de base de datos
      StrSQL10.Free;
      u03_FDRs01.Close();
      u03_FDCn01.close;
      u03_FDCn01.Free;
      u03_FDRs01.Free;
    end;




  Except On E:Exception
  do
    Begin
      Logueado:=False;
      // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
    End;
  End;
  Result:=UsersInfo;
end;


//------------------------------------------------------------------------------
//Se Ubica los mienbros del grupo de usuarios
//------------------------------------------------------------------------------
function GroupMembershipLocate(const KeyValues: Variant;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Try
    Result := False;
    FUserNameField:='UserName' ;
    FKeyField:='GroupName';
    if DataSource = nil then Exit;
  Except On E:Exception
  do
    Begin
      // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,Infoconexion.GeneraLogErrores,Infoconexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
    End;
  End;
  Result := DataSource.DataSet.Locate(FUserNameField + ';' + FKeyField, KeyValues, [loCaseInsensitive]);



end;

//------------------------------------------------------------------------------
//Funci�n para ubicar registros en dataSourdce
//------------------------------------------------------------------------------
function LocateValue(const KeyValues: Variant;FKeyField:string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Result := False;
  if DataSource = nil then Exit;
  try
    Result := DataSource.DataSet.Locate(FKeyField, KeyValues, [loCaseInsensitive]);
  Except On E:Exception
  do
    Begin
      // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
      Result := false;
    End;
  End;
end;


//------------------------------------------------------------------------------
//Remueve los accesos del grupo
//------------------------------------------------------------------------------
function RemoveGroupAccess(Group,Permission,GroupNameField: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Try
    Result := false;
    if HasGroupAccess(Group,Permission,GroupNameField,DataSource,True,InfoConexion) then
    begin
      DataSource.DataSet.Edit;
      DataSource.DataSet.Delete;
      Result := true;
    end;
 Except On E:Exception
  do
    Begin
      // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
      Result := false;
    End;
  End;
end;


//------------------------------------------------------------------------------
//Agrego los permisos de los grupos
//------------------------------------------------------------------------------
function AddGroupAccess(Group,Permission,GroupNameField: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Try
    Result := false;
    FKeyField:='GroupName';
    if HasGroupAccess(Group,Permission,GroupNameField,DataSource,True,InfoConexion) then
    begin
      DataSource.DataSet.Append;
      DataSource.DataSet.FieldByName(FKeyField).AsString := Permission;
      DataSource.DataSet.FieldByName(GroupNameField).AsString := Group;
      DataSource.DataSet.Post;
      Result := true;
    end; // with
 Except On E:Exception
  do
    Begin                       // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoConexion.GeneraLogErrores,Infoconexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
      Result := false;
    End;
  End;
end;


//------------------------------------------------------------------------------
//Busca en la tabla de Accesos de los grupos
//------------------------------------------------------------------------------
function HasGroupAccess(Group,Permission, GroupNameField: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Result := False;
  GroupNameField:='';
  if DataSource = nil then Exit;
  try
    Result := DataSource.DataSet.Locate(FKeyField+';'+GroupNameField, VarArrayOf([Permission,Group]), [loCaseInsensitive]);
  Except On E:Exception
  do
    Begin                       // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
      Result := false;
    End;
  End;
end;

//------------------------------------------------------------------------------
//Busca en la tabla de Accesos de los grupos
//------------------------------------------------------------------------------
function AddUserToGroup(Username, GroupName: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Try
    Result := false;
    FUserNameField:='UserName' ;
    FKeyField:='GroupName';
    //Si existe no se adiciona el usuario al grupo
    if LocateValue(VarArrayOf([Username, GroupName]),FUserNameField + ';' + FKeyField,DataSource,True,InfoConexion) then Exit;
    DataSource.DataSet.Append;
    DataSource.DataSet.FieldByName(FUserNameField).AsString := UserName;
    DataSource.DataSet.FieldByName(FKeyField).AsString := GroupName;
    DataSource.DataSet.Post;
    Result := true;
  Except On E:Exception
  do
    Begin                       // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,Infoconexion.GeneraLogErrores,InfoConexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
      Result := false;
    End;
  End;
end;

//------------------------------------------------------------------------------
//Se remueve el usuario del grupo
//------------------------------------------------------------------------------
function RemoveUserFromGroup(Username, GroupName: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Try
    Result := false;
    FUserNameField:='UserName' ;
    FKeyField:='GroupName';
    //Si existe no se adiciona el usuario al grupo
    if LocateValue(VarArrayOf([Username, GroupName]),FUserNameField + ';' + FKeyField,DataSource,True,InfoConexion) then
    begin
      DataSource.DataSet.Edit;
      DataSource.DataSet.Delete;
      Result := true;
    end;
  Except On E:Exception  do
    Begin                       // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
      Result := false;
    End;
  End;
end;



//------------------------------------------------------------------------------
//Se borran los accesos en la tabla de permisos de usuarios
//------------------------------------------------------------------------------
function RemoveUserAccess(UserName,Permission: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Try
    Result := false;
    if HasUserAccess(UserName,Permission,DataSource,True,InfoConexion) then
    begin
      DataSource.DataSet.Edit;
      DataSource.DataSet.Delete;
      Result := true;
    end;

  Except On E:Exception  do
    Begin                       // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,Infoconexion.GeneraLogErrores,InfoConexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
      Result := false;
    End;
  End;
end;


//------------------------------------------------------------------------------
//Se agrega en la tabla de permisos de usuarios
//------------------------------------------------------------------------------
function AddUserAccess(UserName, Permission: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Try
    Result := false;
    if not HasUserAccess(UserName, Permission,DataSource,True,InfoConexion) then
      with DataSource do
      begin
        FUserNameField:='UserName' ;
        FKeyField:='Permission';
        DataSource.DataSet.Append;
        DataSource.DataSet.FieldByName(FKeyField).AsString := Permission;
        DataSource.DataSet.FieldByName(FUserNameField).AsString := UserName;
        DataSource.DataSet.Post;
        Result := true;
      end; // with
  Except On E:Exception  do
    Begin                       // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoConexion.GeneraLogErrores,InfoConexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
      Result := false;
    End;
  End;
end;


//------------------------------------------------------------------------------
//Se busca en la tabla de permisos de usuarios
//------------------------------------------------------------------------------
function HasUserAccess(UserName,Permission: string;DataSource:TDataSource;MostrarMSJError:Boolean;InfoConexion:TParametrosApps): boolean;
begin
  Result := False;
  FUserNameField:='UserName' ;
  FKeyField:='Permission';
  if DataSource = nil then Exit;
  try
    Result := DataSource.DataSet.Locate(FKeyField+';'+FUserNameField, VarArrayOf([Permission,UserName]), [loCaseInsensitive]);
  Except On E:Exception  do
    Begin                       // Se guarda el error en el log
      Mensaje:=E.Message;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,Infoconexion.GeneraLogErrores,InfoConexion.GeneraLogData);
      if MostrarMSJError then  MessageDlg(E.Message, mterror, [mbOK], 0);
      Result := false;
    End;
  End;
end;
end.
