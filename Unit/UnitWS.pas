//******************************************************************************
// Unit que contiene los procedimientos y funciones de uso Comun dentro del
// Web Services
// Empresas: Consultoria & Sistemas ICG, C.A.
// Autor: Rafael Rangel
// Fecha: 20/08/2014
// Ultima Modificacion: 20/08/2014
//******************************************************************************
unit UnitWS;

interface
Uses
Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics,  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls, Vcl.Buttons, ADODB,IniFiles, Data.DB,WinInet, WinSock,Registry,
  UnitAppsComunes

  ;

Var
  //Objetos de Base de datos
  Cn001: TADOConnection;
  Cn002: TADOConnection;
  Cn003: TADOConnection;
  Rs001: TADOQuery;
  Rs002: TADOQuery;
  Rs003: TADOQuery;

  //Variables  a ser utilizadas en el Unt
  ServerDB,
  NameDB,
  UsersDB,
  PasswordDB,
  StrSQL,
  StrSQL02,
  Mensaje:String;
  Conectado:Boolean;
  Registros:Integer;
  //----------------------------------------------------------------------------
  //Funciones
  //----------------------------------------------------------------------------
  //Funcion que verifica si existen la base de datos y las tablas que requiere el WS
  Function Verificar_ConfigWS (Servidor, BD, Usuario, Clave, Ruta: String ): Boolean;
  //----------------------------------------------------------------------------
  //Procedimientos
  //----------------------------------------------------------------------------




implementation

//------------------------------------------------------------------------------
//Funcion Para Abrir la Base de datos utilizando Ado y varios Provedores de base de datos
//------------------------------------------------------------------------------
Function Verificar_ConfigWS (Servidor, BD, Usuario, Clave, Ruta: String ): Boolean;
//Variables de la Funcion
Var
  ConStr: String; // Cadena de Conexion

Begin

    //Se prepara el Sql que crea la base de datos si no existe
    StrSQL:='';
    StrSQL:='IF NOT EXISTS(SELECT * FROM DBO.SYSDATABASES WHERE NAME = ' + Chr(39) + BD + Chr(39)+') ' +
    ' BEGIN ' +
    '   USE MASTER ' +
    '   CREATE DATABASE ' + Chr(39) + BD + Chr(39)+';' +
    ' END';
    //Conexion a la Base de datos
    Conectado:=Open_DB (Cn001 , 2 , Servidor, 'Master', Usuario, Clave, '' );
    //si hay conexion continue el programa
    if Conectado = False then
    Begin
      Result:=False;
      Abort;
    End;

    //Se ejecuta el Comando para verificar si existe la base de datos
    Conectado:=DB_Function(cn002,StrSQL,4);
    Result:=True;

    //Cerrar los Objetos de la base de datos
    Cn001.Close;
    Cn001.Free;

End;
//------------------------------------------------------------------------------
end.
