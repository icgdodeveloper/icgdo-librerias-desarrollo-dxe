//------------------------------------------------------------------------------
//Unit Creada para el Manejo de Archivos XML
//------------------------------------------------------------------------------
//Empresa: Consultoria & Sistemas ICG, C.A
//Rafael Rangel: 08/11/2013
//Ultima Modificacion: 08/11/2013
//------------------------------------------------------------------------------
unit UnitXmlFile;

interface

Uses
Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
System.Classes, Vcl.Graphics,Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Xml.xmldom,
Xml.XMLIntf,Xml.Win.msxmldom, Xml.XMLDoc;





Var

 //-----------------------------------------------------------------------------
 FileXML01 : TXMLDocument;
 //Definicion de Procedimientos del Unit
 Procedure Leer_XMLFile (ArchivoXML:String);



//-----------------------------------------------------------------------------
implementation
//------------------------------------------------------------------------------
//Procedimiento para Leer el Archivo XML
//------------------------------------------------------------------------------
procedure Leer_XMLFile (ArchivoXML:String)  ;
var
  // Variables del Procedimiento
   XMLDoc :  IxmlDocument;
   ChildNode : IXMLNode;
   i,j,
   Elementos   : Integer;
begin

  // Se crea el Objeto de XML;
  XMLDoc := TXMLDocument.Create(Nil) ;
  try
    XMLDoc.Active := True;
    // Se asigna el nombre del archivo
    XMLDoc.LoadFromFile((ArchivoXML));
    // Lee todos los Nodos o Registros
    Elementos:=  XMLDoc.DocumentElement.ChildNodes.Count ;
    for i:= 0 to Elementos - 1 do
    begin
      // Lee un Nodo o Registro
      ChildNode := XMLDoc.DocumentElement.ChildNodes[i];
      ShowMessage(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[i].NodeValue)
      //for j:= 1 to 3 do
        // Lee cada Nodo Hijo o Campo del registro
        // ListBox1.Items.Add(ChildNode.ChildNodes['Child_Node-' + IntToStr(j)].Text);
     //   ShowMessage(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[i].NodeValue)

    end;
    XMLDoc.Active := False;
  finally
    XMLDoc := nil;
  end;



end;
//------------------------------------------------------------------------------







end.
