//******************************************************************************
// Unit que contiene los procedimientos y funciones de uso Comun dentro del
// Programa
// Empresas: Consultoria & Sistemas ICG, C.A. / Geinfor Venezuela, C.A.
// Autor: Rafael Rangel
// Fecha: 31/03/2013
// Ultima Modificacion: 09-06-2015
//******************************************************************************
unit UnitAppsComunes;

interface


Uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics,  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls, Vcl.Buttons,IniFiles, ADODB ,Data.DB,
  IdRawBase, IdIcmpClient,WinInet, WinSock,Registry,
  Xml.xmldom, Xml.Win.msxmldom, XMLDoc,  Printers, Winspool,
  System.UITypes, Datasnap.DBClient,SeAES256, SeBase64,SeEasyAES, WinSvc,System.JSON ,

  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Phys.SQLite, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.Phys.MSSQLDef, FireDAC.Phys.ODBCBase,
  FireDAC.Phys.MSSQL


    ;




//Tipos de Registros
type TDatosPC   = record
  Nombre, IP, Usuario :String;
end;



//Para la Orientaci�n del caracateres
type
   TOrientacion = (toLeft, toRight);


Var
  IniFile : TIniFile;
  //Funciones-------------------------------------------------------------------
  //Funcion Para Abrir una Conexion a una Base de datos
  function Open_DB (CnADO :TADOConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta:String;var MensajeError: String;MostrarError:Boolean ): Boolean;
  //Funcion para abrir un Data Set
  Function Open_ADO_Qry(CnADO:TADOConnection; RsDataSet:TADOQuery ;StrSql,MensajeError:String;Lectura,MostrarError:Boolean): Boolean;

  //Funci�n Para Abrir una Conexi�n a una Base de datos FireDac  (FD)
  Function Open_DB_FD (CnFD :TFDConnection ; OpcionBd:Integer ; ServerDB, NameDB, UserDB, PasswordDB, PathDB:String; var MsgError: String;VerMsgError:Boolean ): Boolean;


  //Funcion Para Crear una Base de datos
  Function DB_Function(CnADO:TADOConnection;SQL_Function:string;Opcion_Function:Integer): Boolean;
  //Funcion para Hacer in Ping a un PC
  Function Ping_PC(IdCliente:TIdIcmpClient;PC:string): Boolean ;
  //Funcion para Verificar si existe una Llave en el Registro
  Function ExisteKey_Registry (Root_Key:HKEY;App_Key :String):Boolean;
  //Funcion para tomar valores del registro de Tipo String o Integer
  function GetRegistryData(RootKey: HKEY; Key, Value: string): variant;
  //Funcion para Ejecutar programas externos
  function EjecutarPrograma(sPrograma: String; Visibilidad: Integer ): Integer;
  //Funcion para rellenar con ceros a la izquierda
  Function FillSpaces(cVar:String;nLen:Integer):String;
  //Funcion para convertir numeros a letras
  function NumToLetra(const mNum: Currency; const iIdioma, iModo: Smallint): String;
  //Funcion para utilizar la Impresora Fiscal
  function ImpFiscal_Bixolon (var pRuta:String;pInfoSalida:Pointer):Integer; stdcall external 'IF_Bixolon.dll'  name 'ImprimirVoucher';
  //Funcion para Validar el RIF o Cedula del cliente
  Function ValidarCIF (CIF :String):Boolean;
  //Funcion para verificar si el valor es numerico
  function esNumero (valor : string) : boolean;
  //Funcion para obtener informacion de acuerdo al tipo de front
  function InfoTipoFront (TipoFront,CualInfo:Integer) : String;
  //Funcion para Encriptar una cadena
  function Encrypt(tr: string): string;
  //function descifrar(cadena, Key: string): String;
  function Decrypt(pr: string): string;
  //Funcion para Obtener la version de la aplicacion
  function Version_Info: String;
  //Funcion Para Crear la estruct del XML
  function MakeXmlStr (node, value: string): string  ;
  //Funcion Para Crear los Campos del XML
  function FieldsToXml (rootName: string; data: TADOQuery): string;
  //Funcion Para obtener las fechas de un archivo
  //function GetFileTimes(FileName : string;var Created : TDateTime;var Modified : TDateTime;  var Accessed : TDateTime) : boolean;
  //Funcion Para Imprimir en la impresora de Texto
  function WriteRawDataToPrinter(PrinterName: String; Str: String): Boolean;
  //Funcion Para Utilizar un Case con valores de Tipo String
  function StringToCaseSelect (Selector : string; CaseList: array of string): Integer;
  //Funcion para Redondear numeros
  function RoundN(X: Extended): Extended;
  //Funcion para Devolver el Nombre del mes
  function NombreMes(mes: Integer): string;

  //Funcion para convertir de Entero a Boleano
  function IntToBoolean(Num: Integer):Boolean;

  //Funci�n para Obtener el ID del CPU
  function GetEnvVarValue(const VarName: string): string;

  //Funci�n para Cifrar texto 256
  function Cifrar_Descifrar_Texto_256(Opcion,Texto, Clave: String): String;

  //Funci�n generar el xml de Plin de las impressoras fiscales con ICG
  function Generar_Xml_plin(CnADO:TADOConnection;SerieAlb,CamposOrdenar,ArchivoXML: String;TipoFront,TipoXml,NumeroAlb:Integer): String;

  //Funci�n indica el estado de un servicio de windows
   function EstadoServicioWindos(sPC, sServicio : string;TipoInfo:Integer ) : string;
  //Funci�n que inicia un servicio de windos.
   function IniciarServicioWindows(sMachine, sService: String): Boolean;

  //Funci�n que detiene un servicio de windos.
   function DetenerServicioWindows(sMachine, sService: String): Boolean;

  //Funci�n que genera transforma un data set en un Objeto JSon
  function DataRowToJSONObject(const AValue : TDataSet): TJSONObject;

  //Funci�n que genera transforma un Objeto Json en DataSet
  function JSONObjectToDataRow(const AJson : TJSONObject; const AValue : TDataSet): Boolean;

  //Funci�n para agregar caracteros a la izquierda o a la derecha
  Function PadString(cVar :String; Caracter :Char; nLen:Integer; Orientacion :TOrientacion):String;

  //Funci�n para crear un String List
  function SepararCadena(const Cadena: string; const Delim: Char): TStringList;

  //Funci�n para leer un archivo texto y convertirlo en una variable string
  function LeerTXT_A_Variable(Archivo:string): WideString;

  //Se ejecuta un adoqry desde un archivo
  function EjecutarTADOQuery(Archivo:string;adoQry:TAdoQuery;var MensajeError: String;MostrarError:Boolean ):Boolean;


  //Que toma de la tabla de Parametros interna el valor requerido
  Function ValorTablaParametros(CnADO:TADOConnection; Pclave,Psubclave,PUsuario,MensajeError:String;MostrarError,Encriptado:Boolean): String;

  //Genera la informaci�n de un archivo
  function GetFileTimes(FileName : string; var Created : TDateTime;
    var Modified : TDateTime; var Accessed : TDateTime) : boolean;

  //Funci�n realizada que permite obtener a partir de una fecha gregoriana (dd/mm/aaaa) la fecha juliana
  //(n�mero de d�as transcurridos desde inicio de a�o).
  function fechaJuliana (fechaGregoriana : TDateTime) : Integer;

  //----------------------------------------------------------------------------
  //Funci�n que retorna el digito verificador de los documentos de identidad en Rep Dominicana
  //----------------------------------------------------------------------------
  function GenDV_Para_DO(strRNCCedsinD:string):Integer;


  //----------------------------------------------------------------------------
  //Procedimientos--------------------------------------------------------------
  //----------------------------------------------------------------------------
  //Procedimiento Para Cerra la Conexion a la base de datos
  Procedure CloseConnection(CnADO:TADOConnection; RsDataSet:TADOQuery);
  //Procedimiento para cerrar las aplicaciones
  Procedure Close_EXE (Apps:String);
  //Procedimiento para Obtener informcion del equipo
  procedure ObtenerDatosPC (var Datos:TDatosPC );
  //Procedimiento para escribir valores en el Registro de Windows---------------
  procedure SetRegistryData(RootKey: HKEY; Key, Value: string; RegDataType: Integer; Data: variant);
  //Definicion de Procedimiento que Cierra la aplicacion activa
  Procedure CloseApp;
  //Definicion del procedimiento para borrar Archivos
  procedure EliminarArchivo (sArchivo: String);
  //Procedimiento Para Obtener la Version de la Aplicacion
  procedure GetBuild_Info(var V1, V2, V3, V4: Word);
  // Generar un arhivo texto que funcione como un archivo log
  procedure BitacoraTXT(Archivo,Contenido:String);

  //Genera el log con informaci�n de la aplicaci�n
  procedure WriteLogApps(TipoLog:Integer;Data:String;GeneraLogErrores,GeneraLogDataRecibida:Integer);


implementation
//******************************************************************************
//Funciones
//******************************************************************************

//------------------------------------------------------------------------------
//Funcion Para Abrir la Base de datos utilizando Ado y varios Provedores de base de datos
//------------------------------------------------------------------------------
Function Open_DB (CnADO :TADOConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta:String; var MensajeError: String;MostrarError:Boolean ): Boolean;

//Variables de la Funcion
Var
  ConStr: String; // Cadena de Conexion

Begin

  //Cerramos la Conexion
  if CnADO.Connected then CnADO.close ;
  Case OpcionBd  of
    1 :  //'Excel
      Begin
        //Se Arma la cadena de Conexion
        ConStr := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + Ruta + BD + ';Persist Security Info=False; Extended Properties=Excel 8.0;';

      End;

    2 : //SQL Server
      Begin
        // Se Arma La Cadena de Conexion
        ConStr:=  'Provider=sqloledb;' +
                  'Data Source=' + Servidor + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'User id=' + Usuario + ';' +
                  'Password=' + Clave + ';' +
                  'Trusted_Connection=False;' ;



      End;

      3: // Access
      Begin
        ConStr:= 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                  'Data Source=' + Ruta + Bd + '.mdb' +
                  ';Persist Security Info=False ' +  ';' +
                  'Jet OLEDB:Database Password=' + Clave ;

      End;

      4: //Oracle
      Begin
        ConStr:= 'Provider=msdaora;' +
                  'Data Source=' + Servidor + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'User id=' + Usuario + ';' +
                  'Password=' + Clave + ';' ;
      End;
      5: // ODBC  sin Indicar el usuario y Password
      Begin
        ConStr:= 'DSN=' + Bd +  ';pwd=' + Clave ;



      End;
      6:// Texto
      Begin
        ConStr:='DRIVER={Microsoft Text Driver (*.txt; *.csv)};' +
                'DBQ=' +  Ruta + ';';
      End;

      7:// MySql
      Begin

      End;

      8: // dBase
      Begin
        ConStr:= 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                  'Data Source=' + Ruta + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'Extended Properties=dBASE IV;'

      End;
      9: // Paradox
      Begin
        ConStr:= 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                  'Data Source=' + Ruta + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'Extended Properties=Paradox 5.x;'
      End;

      10: // SQL Lite
      Begin
       ConStr:= 'Provider=SQLITEDB;' +
                  'Data Source=' + Ruta + Bd + '.db' ;
                 // ';Persist Security Info=False ' +  ';' +
                 // 'Jet OLEDB:Database Password=' + Clave ;

       End;
       11: // ODBC  indicando el usuario y Password
      Begin
        ConStr:= 'DSN=' + Bd +  ';Uid=' + Usuario + ';pwd=' + Clave ;



      End;

       12 : //SQL Server  con Trusted Connection
      Begin
        // Se Arma La Cadena de Conexion
        ConStr:=  'Provider=sqloledb;' +
                  'Data Source=' + Servidor + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'User id=' + Usuario + ';' +
                  'Password=' + Clave + ';' +
                  'Trusted_Connection=False;' ;
                //  'integrated security=False;'    ;

      End;


  end;

  Result:=false;
  CnADO.Close ;
  CnADO.ConnectionString:=ConStr  ;
  CnADO.LoginPrompt:=false;
  CnADO.Mode:=cmReadWrite;


  if (NOT CnADO.Connected) Then
  Try
    //Se Abre la Conexion a la Base de datos
    //CnADO.Mode:=cmRead;
    CnADO.Open;
    Result:=True;
  // Si no se puede conectar se devuelve falso y el mensaje de error
  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      if MostrarError=True then
      Begin
        MessageDlg('No se Puede Conectar a ' + Servidor + ':' + BD + ' El error es : ' + #13#10 + E.Message,
        mterror, [mbok],0);
      End;
      MensajeError:=E.Message;
      Result:=False;
    End;
  End;

End;
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//Funci�n Para Abrir la Base de datos utilizando FireDac y varios Provedores de base de datos
//------------------------------------------------------------------------------
Function Open_DB_FD (CnFD :TFDConnection ; OpcionBd:Integer ; ServerDB, NameDB, UserDB, PasswordDB, PathDB:String; var MsgError: String;VerMsgError:Boolean ): Boolean;
//Variables de la Funci�n
Var
  ConStr: String; // Cadena de Conexi�n


Begin

  try
    //Cerramos la Conexi�n
    ConStr:='';
    CnFD.Params.Clear;
    if CnFD.Connected then CnFD.close ;
    Case OpcionBd  of
      1 :  //'Excel
      Begin

      End;

      2 : //SQL Server
      Begin

      End;

      3: // Access
      Begin

      End;

      4: //Oracle
      Begin

      End;
      5: // ODBC  sin Indicar el usuario y Password
      Begin

      End;
      6:// Texto
      Begin

      End;

      7:// MySql
      Begin

      End;

      8: // dBase
      Begin

      End;
      9: // Paradox
      Begin

      End;

      10: // SQL Lite
      Begin
        ConStr:= '';
        CnFD.DriverName:='SQLite';
       End;

      11: // ODBC  indicando el usuario y Password
      Begin

      End;

      12 : //SQL Server  con Trusted Connection
      Begin
        CnFD.DriverName := 'MSSQL';
        CnFD.Params.Values['Server'] := Trim(ServerDB);
        CnFD.Params.Values['Database'] := Trim(NameDB);
        CnFD.Params.Values['OSAuthent'] := 'No'; // 'Yes' for Windows user, 'No' for SQL user name
        CnFD.Params.Values['User_Name'] := Trim(UserDB);
        CnFD.Params.Values['Password'] := Trim(PasswordDB);
        CnFD.Params.Values['Trusted_Connection'] := 'False';
      End;
    end;
    CnFD.Params.Values['ApplicationName'] := ExtractFileName(ParamStr(0));
    CnFD.LoginPrompt := False; // Suppresses dialogs
    CnFD.Connected := True;
    Result:=True;
  Except On E:Exception do
    Begin

      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      if verMsgError then
      Begin
        MessageDlg('No se Puede Abrir la base de datos, el error es :' + E.Message,
        mterror, [mbok],0);
      End;
      MsgError:=E.Message;
      Result:=False;
    End;
  end;
End;







//Funcion que Abre un DataSet
Function Open_ADO_Qry(CnADO:TADOConnection; RsDataSet:TADOQuery ;StrSql,MensajeError:String;Lectura,MostrarError:Boolean): Boolean;
Begin
  Result:=False;

  if CnADO.Connected Then
  Begin
    with RsDataSet do begin
      Close;
      Connection:=CnADO;
      SQL.Clear;
      SQL.Text:=StrSQL;
      //LockType:=ltReadOnly;

      // Verificamos que se pueda abrir la Consulta
      try
        Open;
        Result:=True;

      Except On E:Exception do
        Begin

          //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
          if MostrarError=True then
          Begin
            MessageDlg('No se Puede Consultar la Informacion Solicitada, el Error es :' + E.Message,
            mterror, [mbok],0);
          End;
          MensajeError:=E.Message;
          Result:=False;
        End;
      end;
    End;
  End;
End ;


//Funcion para realizar diferentes acciones en la base de datos-----------------
Function DB_Function(CnADO:TADOConnection;SQL_Function:string;Opcion_Function:Integer): Boolean;
Var
  LaAccion: String; // Cadena de Conexion
begin

  Result:=False;
  if CnADO.Connected Then ; // Si la conexion esta Abierta
  Begin
    try

      LaAccion:=' ';
      Case Opcion_Function of
        1 :  //Crear una Base de datos
        Begin
          LaAccion:='CREATE DATABASE IF NOT EXISTS ';
        End;
        2 :  //Crear una Tabla
        Begin
          LaAccion:='CREATE TABLE IF NOT EXISTS ';
        End;

        3 :  //Borrar una Tabla
        Begin
          LaAccion:='DROP TABLE IF EXISTS ';
        End;

        4 :  //Ejecutar SQL
        Begin
          LaAccion:=' '
        End;
      End;

      CnADO.Execute(LaAccion + SQL_Function,cmdText);
      Result:=True;
      Except On E:Exception do
      Begin
        MessageDlg('No se Puede Ejecutar la acci�n :'  +  LaAccion + SQL_Function + ' , el Error es :' + E.Message,
        mterror, [mbok],0);
      End;
    end
  End  ;
end;
//------------------------------------------------------------------------------


//Funcion Para realizarle un Ping a un PC y saber si esta en la red-------------
Function Ping_PC(IdCliente:TIdIcmpClient;PC:string): Boolean ;
begin
  IdCliente.Host:=PC;
  IdCliente.ReceiveTimeout:=2000;
  Result:=False;
  // Se Ejecuta el PINg
  try
    IdCliente.Ping('Prueba de IP correcta',0);
    Result:=True;
    Except On E:Exception do
    Begin
        MessageDlg('El Equipo : '  +  PC + ' No esta en la red o no esta Activo, el Error es :' + E.Message,
        mterror, [mbok],0);
         Result:=False;
    End;
  end;
end;
//------------------------------------------------------------------------------

//Funci�n para Verificar si existe una clave en el registro---------------------
Function ExisteKey_Registry (Root_Key:HKEY;App_Key :String):Boolean;
var
  reg : TRegistry;
begin
  reg := TRegistry.Create(KEY_READ);
  Result:=False;
  reg.RootKey := Root_Key;
  if (not reg.KeyExists(App_Key)) then
    begin
      MessageDlg('No se encontro en el Registro la llave: ' + App_Key,mterror, [mbok],0);
      Result:=False
    end
    Else
    Begin
      Result:=True;
    End;
  //Cerramos el Registro
  reg.CloseKey();
  reg.Free;
end;
//------------------------------------------------------------------------------


//Funcion para Leer Valores del Registro----------------------------------------
function GetRegistryData(RootKey: HKEY; Key, Value: string): variant;
var
  Reg: TRegistry;
  RegDataType: TRegDataType;
  DataSize, Len: integer;
  s: string;
label cantread;
begin
  Reg := nil;
  try
    Reg := TRegistry.Create(KEY_QUERY_VALUE);
    Reg.RootKey := RootKey;
    if Reg.OpenKeyReadOnly(Key) then begin
      try
        RegDataType := Reg.GetDataType(Value);
        if (RegDataType = rdString) or
           (RegDataType = rdExpandString) then
          Result := Reg.ReadString(Value)
        else if RegDataType = rdInteger then
          Result := Reg.ReadInteger(Value)
        else if RegDataType = rdBinary then begin
          DataSize := Reg.GetDataSize(Value);
          if DataSize = -1 then goto cantread;
          SetLength(s, DataSize);
          Len := Reg.ReadBinaryData(Value, PChar(s)^, DataSize);
          if Len <> DataSize then goto cantread;
          Result := s;
        end else
cantread:
      raise Exception.Create(SysErrorMessage(ERROR_CANTREAD));
      except
        s := ''; // Deallocates memory if allocated
        Reg.CloseKey;
        raise;
      end;
      Reg.CloseKey;
    end else
    //    raise Exception.Create(SysErrorMessage(GetLastError));
    // Si se ubica el valor en el registro se detiene la ejecucion del programa
    raise Exception.Create('No se ubico el Valor en el Registro ' + Key + ' No se puede CONTINUAR con la Ejecucion del Programa');

  except
    Reg.Free;
    raise;
  end;
  Reg.Free;
end;
//------------------------------------------------------------------------------


//Funcion Para Ejecutar programa Externo y esperar que termine
function EjecutarPrograma( sPrograma: String; Visibilidad: Integer ): Integer;
var
  sAplicacion: array[0..512] of char;
  DirectorioActual: array[0..255] of char;
  DirectorioTrabajo: String;
  InformacionInicial: TStartupInfo;
  InformacionProceso: TProcessInformation;
  iResultado, iCodigoSalida: DWord;
begin
  StrPCopy( sAplicacion, sPrograma );
  GetDir( 0, DirectorioTrabajo );
  StrPCopy( DirectorioActual, DirectorioTrabajo );
  FillChar( InformacionInicial, Sizeof( InformacionInicial ), #0 );
  InformacionInicial.cb := Sizeof( InformacionInicial );

  InformacionInicial.dwFlags := STARTF_USESHOWWINDOW;
  InformacionInicial.wShowWindow := Visibilidad;
  CreateProcess( nil, sAplicacion, nil, nil, False,
                 CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS,
                 nil, nil, InformacionInicial, InformacionProceso );

  // Espera hasta que termina la ejecuci�n
  repeat
    iCodigoSalida := WaitForSingleObject( InformacionProceso.hProcess, 1000 );
    Application.ProcessMessages;
  until ( iCodigoSalida <> WAIT_TIMEOUT );

  GetExitCodeProcess( InformacionProceso.hProcess, iResultado );
  MessageBeep( 0 );
  CloseHandle( InformacionProceso.hProcess );
  Result := iResultado;
end;
//------------------------------------------------------------------------------

//Funcion para rellenar con ceros a la izquierda campos string
Function FillSpaces(cVar:String;nLen:Integer):String;
 begin
 Result:=StringOfChar('0',nLen - Length(cVar))+cVar;
 end;
//------------------------------------------------------------------------------



// Funcion para convertir numeros a Letras
function NumToLetra(const mNum: Currency; const iIdioma, iModo: Smallint): String;
 const
   iTopFil: Smallint = 6;
   iTopCol: Smallint = 10;
   aCastellano: array[0..5, 0..9] of PChar =
   ( ('UNA ','DOS ','TRES ','CUATRO ','CINCO ',
     'SEIS ','SIETE ','OCHO ','NUEVE ','UN '),
     ('ONCE ','DOCE ','TRECE ','CATORCE ','QUINCE ',
     'DIECISEIS ','DIECISIETE ','DIECIOCHO ','DIECINUEVE ',''),
     ('DIEZ ','VEINTE ','TREINTA ','CUARENTA ','CINCUENTA ',
     'SESENTA ','SETENTA ','OCHENTA ','NOVENTA ','VEINTI'),
     ('CIEN ','DOSCIENTAS ','TRESCIENTAS ','CUATROCIENTAS ','QUINIENTAS ',
     'SEISCIENTAS ','SETECIENTAS ','OCHOCIENTAS ','NOVECIENTAS ','CIENTO '),
     ('CIEN ','DOSCIENTOS ','TRESCIENTOS ','CUATROCIENTOS ','QUINIENTOS ',
     'SEISCIENTOS ','SETECIENTOS ','OCHOCIENTOS ','NOVECIENTOS ','CIENTO '),
     ('MIL ','MILLON ','MILLONES ','CERO ','Y ',
     'UNO ','DOS ','CON ','','') );
   aCatalan: array[0..5, 0..9] of PChar =
   ( ( 'UNA ','DUES ','TRES ','QUATRE ','CINC ',
     'SIS ','SET ','VUIT ','NOU ','UN '),
     ( 'ONZE ','DOTZE ','TRETZE ','CATORZE ','QUINZE ',
     'SETZE ','DISSET ','DIVUIT ','DINOU ',''),
     ( 'DEU ','VINT ','TRENTA ','QUARANTA ','CINQUANTA ',
     'SEIXANTA ','SETANTA ','VUITANTA ','NORANTA ','VINT-I-'),
     ( 'CENT ','DOS-CENTES ','TRES-CENTES ','QUATRE-CENTES ','CINC-CENTES ',
     'SIS-CENTES ','SET-CENTES ','VUIT-CENTES ','NOU-CENTES ','CENT '),
     ( 'CENT ','DOS-CENTS ','TRES-CENTS ','QUATRE-CENTS ','CINC-CENTS ',
     'SIS-CENTS ','SET-CENTS ','VUIT-CENTS ','NOU-CENTS ','CENT '),
     ( 'MIL ','MILIO ','MILIONS ','ZERO ','-',
     'UN ','DOS ','AMB ','','') );
 var
   aTexto: array[0..5, 0..9] of PChar;
   cTexto, cNumero: String;
   iCentimos, iPos: Smallint;
   bHayCentimos, bHaySigni: Boolean;

   (*************************************)
   (* Cargar Textos seg�n Idioma / Modo *)
   (*************************************)

   procedure NumLetra_CarTxt;
   var
     i, j: Smallint;
   begin
     (* Asignaci�n seg�n Idioma *)

     for i := 0 to iTopFil - 1 do
       for j := 0 to iTopCol - 1 do
         case iIdioma of
           1: aTexto[i, j] := aCastellano[i, j];
           2: aTexto[i, j] := aCatalan[i, j];
         else
           aTexto[i, j] := aCastellano[i, j];
         end;

     (* Asignaci�n si Modo Masculino *)

     if (iModo = 1) then
     begin
       for j := 0 to 1 do
         aTexto[0, j] := aTexto[5, j + 5];

       for j := 0 to 9 do
         aTexto[3, j] := aTexto[4, j];
     end;
   end;

   (****************************)
   (* Traducir D��gito -Unidad- *)
   (****************************)

   procedure NumLetra_Unidad;
   begin
     if not( (cNumero[iPos] = '0') or (cNumero[iPos - 1] = '1')
      or ((Copy(cNumero, iPos - 2, 3) = '001') and ((iPos = 3) or (iPos = 9))) ) then
       if (cNumero[iPos] = '1') and (iPos <= 6) then
         cTexto := cTexto + aTexto[0, 9]
       else
         cTexto := cTexto + aTexto[0, StrToInt(cNumero[iPos]) - 1];

     if ((iPos = 3) or (iPos = 9)) and (Copy(cNumero, iPos - 2, 3) <> '000') then
       cTexto := cTexto + aTexto[5, 0];

     if (iPos = 6) then
       if (Copy(cNumero, 1, 6) = '000001') then
         cTexto := cTexto + aTexto[5, 1]
       else
         cTexto := cTexto + aTexto[5, 2];
   end;

   (****************************)
   (* Traducir D��gito -Decena- *)
   (****************************)

   procedure NumLetra_Decena;
   begin
     if (cNumero[iPos] = '0') then
       Exit
     else if (cNumero[iPos + 1] = '0') then
       cTexto := cTexto + aTexto[2, StrToInt(cNumero[iPos]) - 1]
     else if (cNumero[iPos] = '1') then
       cTexto := cTexto + aTexto[1, StrToInt(cNumero[iPos + 1]) - 1]
     else if (cNumero[iPos] = '2') then
       cTexto := cTexto + aTexto[2, 9]
     else
       cTexto := cTexto + aTexto[2, StrToInt(cNumero[iPos]) - 1]
         + aTexto[5, 4];
   end;

   (*****************************)
   (* Traducir D��gito -Centena- *)
   (*****************************)

   procedure NumLetra_Centena;
   var
     iPos2: Smallint;
   begin
     if (cNumero[iPos] = '0') then
       Exit;

     iPos2 := 4 - Ord(iPos > 6);

     if (cNumero[iPos] = '1') and (Copy(cNumero, iPos + 1, 2) <> '00') then
       cTexto := cTexto + aTexto[iPos2, 9]
     else
       cTexto := cTexto + aTexto[iPos2, StrToInt(cNumero[iPos]) - 1];
   end;

   (**************************************)
   (* Eliminar Blancos previos a guiones *)
   (**************************************)

   procedure NumLetra_BorBla;
   var
     i: Smallint;
   begin
     i := Pos(' -', cTexto);

     while (i > 0) do
     begin
       Delete(cTexto, i, 1);
       i := Pos(' -', cTexto);
     end;
   end;

 begin
   (* Control de Argumentos *)

   if (mNum < 0.00) or (mNum > 999999999999.99) or (iIdioma < 1) or (iIdioma > 2)
     or (iModo < 1) or (iModo > 2) then
   begin
     Result := 'ERROR EN ARGUMENTOS';
     Abort;
   end;

   (* Cargar Textos seg�n Idioma / Modo *)

   NumLetra_CarTxt;

   (* Bucle Exterior -Tratamiento C�ntimos-     *)
   (* NOTA: Se redondea a dos d��gitos decimales *)

   cNumero := Trim(Format('%12.0f', [Int(mNum)]));
   cNumero := StringOfChar('0', 12 - Length(cNumero)) + cNumero;
   iCentimos := Trunc((Frac(mNum) * 100) + 0.5);

   repeat
     (* Detectar existencia de C�ntimos *)

     if (iCentimos <> 0) then
       bHayCentimos := True
     else
       bHayCentimos := False;

     (* Bucle Interior -Traducci�n- *)

     bHaySigni := False;

     for iPos := 1 to 12 do
     begin
       (* Control existencia D��gito significativo *)

       if not(bHaySigni) and (cNumero[iPos] = '0') then
         Continue
       else
         bHaySigni := True;

       (* Detectar Tipo de D��gito *)

       case ((iPos - 1) mod 3) of
         0: NumLetra_Centena;
         1: NumLetra_Decena;
         2: NumLetra_Unidad;
       end;
     end;

     (* Detectar caso 0 *)

     if (cTexto = '') then
       cTexto := aTexto[5, 3];

     (* Traducir C�ntimos -si procede- *)

     if (iCentimos <> 0) then
     begin
       cTexto := cTexto + aTexto[5, 7];
       cNumero := Trim(Format('%.12d', [iCentimos]));
       iCentimos := 0;
     end;
   until not (bHayCentimos);

   (* Eliminar Blancos innecesarios -s�lo Catal�n- *)

   if (iIdioma = 2) then
     NumLetra_BorBla;

   (* Retornar Resultado *)

   Result := Trim(cTexto);
 end;

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Funcion para Validar el RIF o la Cedula de Identidad

//Funcion para Verificar si existe una clave en el registro---------------------
Function ValidarCIF (CIF :String):Boolean;
Var

LargoCIF:Integer;
TipoCIF,
RestoCIF,
Mensaje:String;
ElResultado:Boolean;
begin

  LargoCIF:=Length(CIF);
  ElResultado:=True;
  if LargoCIF > 1 then
  Begin
    TipoCIF:=(Copy(CIF,1,1));
    RestoCIF:=(Copy(CIF,2,LargoCIF));
    //Se Compara el primer Digito de la cedula o RIF del cliente
    if (TipoCIF <> 'V') And (TipoCIF <> 'E') And (TipoCIF <> 'J')And (TipoCIF <> 'G') then
    Begin
      Mensaje:='El RIF/CI no cumple con el formato Requerido, Solo se Aceptan V,J,G,E Usted Indico: ' + CIF;
      ElResultado:=False ;
    End
    else
    Begin
      ElResultado:=esNumero (RestoCIF);
      Mensaje:='El RIF/CI no cumple con el formato Requerido, Solo se Aceptan Numeros, Usted Indico: ' + RestoCIF;
     // ElResultado:=True ;
    End;
  End
  Else
  Begin
    Mensaje:='El RIF/CI no cumple con el formato Requerido, la longitud es muy corta: ' + CIF;
    ElResultado:=False ;
  End;

  //Si no cumple con las condiciones se muestra el Mensaje
  if ElResultado=False then MessageDlg(Mensaje,mterror, [mbok],0);
  Result:=ElResultado;

end;
//------------------------------------------------------------------------------


//Comprueba si un valor es num�rico
function esNumero (valor : string) : boolean;
var
  numero : Int64;
begin
  try
    numero := StrToInt64(valor);
    result := true;
  except
    result := false;
  end;
end;
//------------------------------------------------------------------------------

//Devuelve informacion de acuerdo al tipo de Front
function InfoTipoFront (TipoFront,CualInfo:Integer) : String;
var
  Info : String;
begin
  Info:='';
  //Segun el tipo de Front la aplicacion se ajusta a la estructura de datos
  Case TipoFront  of
    1: //FrontRest
    Begin

      Case CualInfo  of
        1: // SQL para Buscar un cliente
        Begin
          Info:='';
          Info:='SELECT CODCLIENTE,CIF, CODCONTABLE, NOMBRECOMERCIAL, DIRECCION1, TELEFONO1, E_MAIL, '  +
          ' NOMBRECLIENTE, DESCATALOGADO' +
          ' FROM CLIENTES WHERE DESCATALOGADO <> ''T''';
        End;

        2: // SQL para Buscar los Rangos de Clientes
        Begin
          Info:='';
          Info:='SELECT  PuedeCrear, Minimo, Maximo FROM REM_RANGOS' +
              ' WHERE (IDFront = 0) AND (Tipo = 2)' ;
        End;

        3: // SQL para Buscar la informacion de los pluggins
        Begin
          Info:='';
          Info:='SELECT FILENAMEXML FROM PLUGGINS ' ;
        End;
        4: // SQL para Buscar la informacion del UltimoCliente
        Begin
          Info:='';
          Info:='SELECT MAX(CODCLIENTE) AS ULTIMOCLIENTE FROM CLIENTES ';
        End;

      End;
    End;
    2 : //FronRetail
    Begin
      //Se Define que se Buscara en el registro de Windows
      Info:='';
      Case CualInfo  of
        1: // SQL para Buscar un cliente
        Begin
          Info:='';
          Info:='SELECT CODCLIENTE,CIF, CODCONTABLE, NOMBRECOMERCIAL, DIRECCION1, TELEFONO1, E_MAIL, '  +
          ' NOMBRECLIENTE, DESCATALOGADO' +
          ' FROM CLIENTES WHERE DESCATALOGADO <> ''T''';
        End;

        2: // SQL para Buscar los Rangos de Clientes
        Begin
          Info:='';
          Info:='SELECT  PuedeCrear, Minimo, Maximo FROM REM_RANGOS' +
              ' WHERE (IDFront = 0) AND (Tipo = 2)' ;
        End;

        3: // SQL para Buscar la informacion de los pluggins
        Begin
          Info:='';
          Info:='SELECT FILENAMEXML FROM PLUGGINS ' ;
        End;
        4: // SQL para Buscar la informacion del UltimoCliente
        Begin
          Info:='';
          Info:='SELECT MAX(CODCLIENTE) AS ULTIMOCLIENTE FROM CLIENTES ';
        End;

      End;

    End;
    3 : //Front Hotel
    Begin
      //Se Define que se Buscara en el registro de Windows
      Info:='';
    End;

     4 : //Manager
    Begin
      Case CualInfo  of
        1: // SQL para Buscar un cliente
        Begin
          Info:='';
          Info:='SELECT CODCLIENTE,CIF, CODCONTABLE, NOMBRECOMERCIAL, DIRECCION1, TELEFONO1, E_MAIL, '  +
          ' NOMBRECLIENTE, DESCATALOGADO' +
          ' FROM CLIENTES WHERE DESCATALOGADO <> ''T''';
        End;

        2: // SQL para Buscar los Rangos de Clientes
        Begin
          Info:='';
          Info:='SELECT  PuedeCrear, Minimo, Maximo FROM REM_RANGOS' +
              ' WHERE (IDFront = 0) AND (Tipo = 2)' ;
        End;

        3: // SQL para Buscar la informacion de los pluggins
        Begin
          Info:='';
          Info:='SELECT FILENAMEXML FROM PLUGGINS ' ;
        End;
        4: // SQL para Buscar la informacion del UltimoCliente
        Begin
          Info:='';
          Info:='SELECT MAX(CODCLIENTE) AS ULTIMOCLIENTE FROM CLIENTES ';
        End;

      End;
    End;
  End;


  Result:= Info;
end;

//----------------------------------------------------------------------------
// Cifrar una cadena
//----------------------------------------------------------------------------

//Encrypt
function Encrypt(tr: string): string;
var
  i: Integer;
  Temp: string;

begin
  for i := 1 to Length(tr) do
  begin
    Temp := Temp + Chr(Ord(tr[i]) + 1);
  end;
  Encrypt := Temp;
end;
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//Descifrar una Cadena
//----------------------------------------------------------------------------

//Decrypt
function Decrypt(pr: string): string;
var
  i: Integer;
  Temp: string;
  begin
  for i := 1 to Length(pr) do
  begin
    Temp := Temp + Chr(Ord(pr[i]) - 1);
  end;
  Decrypt := Temp;
end;
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//Funcion para Obtener la version de la aplicacion
//------------------------------------------------------------------------------
function Version_Info: String;
var
  V1,       // Major Version
  V2,       // Minor Version
  V3,       // Release
  V4: Word; // Build Number
begin
  GetBuild_Info(V1, V2, V3, V4);
  Result := IntToStr(V1) + '.'
            + IntToStr(V2) + '.'
            + IntToStr(V3) + '.'
            + IntToStr(V4);
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Funcion para Crear la Estructura de un XML
//------------------------------------------------------------------------------
function MakeXmlStr (node, value: string): string;
begin
  Result := '<' + node + '>' + value + '</' + node + '>';
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Funcion para convertir un recordet en un una estructura XML
//------------------------------------------------------------------------------
function FieldsToXml (rootName: string; data: TADOQuery): string;
var
  i: Integer;
begin
  Result := '<' + rootName + '>' + sLineBreak;;
  for i := 0 to data.FieldCount - 1 do
      Result := Result + '  ' + MakeXmlStr (
                LowerCase (data.Fields[i].FieldName),
                data.Fields[i].AsString) + sLineBreak;
  Result := Result + '</' + rootName + '>' + sLineBreak;;
end;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Funcion para Imprimir en la impresora Directamente
//------------------------------------------------------------------------------
function WriteRawDataToPrinter(PrinterName: String; Str: String): Boolean;
var
  PrinterHandle: THandle;
  DocInfo: TDocInfo1;
  i: Integer;
  B: Byte;
  Escritos: DWORD;
begin
  Result:= FALSE;
  if OpenPrinter(PChar(PrinterName), PrinterHandle, nil) then
  try
    FillChar(DocInfo,Sizeof(DocInfo),#0);
    with DocInfo do
    begin
      pDocName:= PChar('Printer Test');
      pOutputFile:= nil;
      pDataType:= 'RAW';
    end;
    if StartDocPrinter(PrinterHandle, 1, @DocInfo) <> 0 then
    try
      if StartPagePrinter(PrinterHandle) then
      try
        while Length(Str) > 0 do
        begin
          if Copy(Str, 1, 1) = '\' then
          begin
            if Uppercase(Copy(Str, 2, 1)) = 'X' then
              Str[2]:= '$';
            if not TryStrToInt(Copy(Str, 2, 3),i) then
              Exit;
            B:= Byte(i);
            Delete(Str, 1, 3);
          end else B:= Byte(Str[1]);
          Delete(Str,1,1);
          WritePrinter(PrinterHandle, @B, 1, Escritos);
        end;
        Result:= TRUE;
      finally
        EndPagePrinter(PrinterHandle);
      end;
    finally
      EndDocPrinter(PrinterHandle);
    end;
  finally
    ClosePrinter(PrinterHandle);
  end;
end;



//------------------------------------------------------------------------------
//Funcion para utilizar el CASE con Variables de Tipo Texto
//------------------------------------------------------------------------------
function StringToCaseSelect (Selector : string;  CaseList: array of string): Integer;
var
cnt: integer;
begin
  Result:=-1;
  for cnt:=0 to Length(CaseList)-1 do
  begin
    if CompareText(Selector, CaseList[cnt]) = 0 then
    begin
       Result:=cnt;
       Break;
    end;
  end;
end;

//------------------------------------------------------------------------------
//Funcion para Redondear numeros
//------------------------------------------------------------------------------

function RoundN(X: Extended): Extended;
// Redondea un numero "normalmente": si la parte decimal
// es >= 0.5 el n�mero se redondea para arriba (ver RoundUp).
// En caso contrario, si la parte decimal es < 0.5, el
// n�mero se redondea para abajo (ver RoundDn).
// RoundN(3.5) = 4 RoundN(-3.5) = -4
// RoundN(3.1) = 3 RoundN(-3.1) = -3
begin
(*
if Abs(Frac(X)) >= 0.5 then
Result := RoundUp(X)
else
Result := RoundDn(X);
*)
Result := Int(X) + Int(Frac(X) * 2);
end;



//------------------------------------------------------------------------------
//Funcion para Devolver el nombre del mes
//------------------------------------------------------------------------------
 function NombreMes(mes: Integer): string;
const
  MESES: array[1..12] of string = ('Enero','Febrero','Marzo','Abril','Mayo',
        'Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
begin
  Result:= MESES[mes];
end;


//------------------------------------------------------------------------------
//Funcion para Convertir de Entero a Booleano
//------------------------------------------------------------------------------

function IntToBoolean(Num: Integer):Boolean;
begin
   if Num = 1 then
       Result := True
   else
       Result := False;
end;



//------------------------------------------------------------------------------
//Funci�n para Obtener el las variables de entorno
//------------------------------------------------------------------------------

function GetEnvVarValue(const VarName: string): string;
var
  BufSize: Integer;  // buffer size required for value
begin
  // Get required buffer size (inc. terminal #0)
  BufSize := GetEnvironmentVariable(PChar(VarName), nil, 0);
  if BufSize > 0 then
  begin
    // Read env var value into result string
    SetLength(Result, BufSize - 1);
    GetEnvironmentVariable(PChar(VarName),
      PChar(Result), BufSize);
  end
  else
    // No such environment variable
    Result := '';
end;


//Funci�n para Cifrar texto Mas completo
function Cifrar_Descifrar_Texto_256(Opcion,Texto, Clave: String): String;
var
  Src, Cifrado: WideString;
begin
  try
    Src:=Texto;
    if Opcion ='C' then Cifrado:= EasyAESEnc(Src,'Clave')
    Else Cifrado:= EasyAESDec(Src,'Clave');
    Result := Cifrado;
  except
    On E: Exception do
      Writeln(E.Message);
  end;
  Readln;
end;



//------------------------------------------------------------------------------
{Funci�n que genera las lineas que se requieren para la impresi�n de las l�neas
en la Impresora Fiscal - Retail, Hotel, Manager
 }
//------------------------------------------------------------------------------
function Generar_Xml_plin(CnADO:TADOConnection;SerieAlb,CamposOrdenar,ArchivoXML: String;TipoFront,TipoXml,NumeroAlb:Integer): String;
var
  DataFile,
  StrSQL01,
  RutaApps,
  VarXML,
  MensajeError :String;
  CDS: TClientDataSet;
  rs001 :TADOQuery;  //Recordset01 de las lineas del albaran
  Conectado:Boolean;

  plinXML : TXMLDocument;
Begin

  DataFile:=ArchivoXML;
  RutaApps:= ExtractFilePath(ParamStr(0));
  //Vamos a definir el data set para trabajarlo
  Try  //usamos los componentes

    //Si no esta abierta la conexi�n a la base de datos se retorna
    if CnADO.Connected=False then Exit;

    //Se crea los objetos ado y data set requeridos
    rs001:= TADOQuery.Create(nil);
    CDS := TClientDataSet.Create(nil);

    CDS.FieldDefs.Clear;
    CDS.FieldDefs.Add('NUMSERIE',ftWideString, 4);
    CDS.FieldDefs.Add('NUMALBARAN',ftInteger, 0);
    CDS.FieldDefs.Add('NUMLIN',ftInteger, 0);
    CDS.FieldDefs.Add('REFERENCIA',ftWideString, 15);
    CDS.FieldDefs.Add('SUPEDIDO',ftWideString, 15);
    CDS.FieldDefs.Add('TALLA',ftWideString, 10);
    CDS.FieldDefs.Add('COLOR',ftWideString, 10);
    CDS.FieldDefs.Add('DESCRIPCION',ftWideString, 46);
    CDS.FieldDefs.Add('UNIDADES',ftFloat, 0);
    CDS.FieldDefs.Add('PRECIO',ftFloat, 0);
    CDS.FieldDefs.Add('IVA',ftFloat, 0);
    CDS.FieldDefs.Add('TOTAL',ftFloat, 0);
    CDS.FieldDefs.Add('DTO',ftFloat, 0);
    CDS.FieldDefs.Add('TIPOIMPUESTO',ftSmallint, 0);
    CDS.FieldDefs.Add('PRECIOIVA',ftFloat, 0);
    CDS.FieldDefs.Add('TOTALEXPANSION',ftFloat, 0);
    CDS.FieldDefs.Add('UNID1',ftFloat, 0);
    CDS.FieldDefs.Add('UNID2',ftFloat, 0);
    CDS.FieldDefs.Add('UNID3',ftFloat, 0);
    CDS.FieldDefs.Add('UNID4',ftFloat, 0);
    CDS.FieldDefs.Add('CODARTICULO',ftInteger, 0);
    CDS.FieldDefs.Add('DPTO',ftInteger, 0);
    CDS.FieldDefs.Add('SECCION',ftInteger, 0);
    CDS.FieldDefs.Add('PORPESO',ftWideString,2);
    CDS.FieldDefs.Add('DESCSECCION',ftWideString, 25);
    CDS.FieldDefs.Add('CODBARRAS',ftWideString, 25);
    CDS.FieldDefs.Add('LINEAOCULTA',ftWideString, 2);
    CDS.FieldDefs.Add('MODIFICADORES',ftWideString, 120);
    CDS.FieldDefs.Add('CODALMACEN',ftWideString, 3);
    CDS.FieldDefs.Add('NOMBREALMACEN',ftWideString, 25);
    CDS.FieldDefs.Add('DESCRIPADIC',ftWideString, 25);
    CDS.FieldDefs.Add('CODVENDEDOR',ftInteger, 0);
    CDS.FieldDefs.Add('NOMVENDEDOR',ftWideString, 20);

    //Se crea el dataset
    CDS.CreateDataSet;

    //De arma el sql se acuerdo al tipo de Front requerido
    Case TipoFront  of
      1: // FrontRest
      Begin
        StrSQL01:='';
        StrSQL01:='';
        StrSQL01:=' SELECT AVL.SERIE AS  NUMSERIE, AVL.NUMERO AS NUMALBARAN, MAX(AVL.NUMLINEA) AS NUMLIN, AVL.REFERENCIA, '''' AS SUPEDIDO, ''''  AS TALLA, '''' AS COLOR, ' +
          '  AVL.DESCRIPCION,SUM(AVL.UNIDADES) AS UNIDADES,AVL.PRECIO,AVL.IVA, SUM(AVL.TOTAL) AS TOTAL,AVL.DTO,AVL.TIPOIVA AS TIPOIMPUESTO,AVL.PRECIOIVA,SUM(AVL.TOTAL) AS TOTALEXPANSION,' +
          ' SUM(AVL.UNIDADES) AS UNID1,  SUM(AVL.UNIDADES) AS UNID2, SUM(AVL.UNIDADES) AS UNID3, SUM(AVL.UNIDADES) AS UNID4, AVL.CODARTICULO,' +
          ' ART.DPTO, ART.SECCION,ART.PORPESO,' +
          ' ISNULL((SELECT TOP(1) A.DESCRIPCION FROM SECCIONES AS A WITH (NOLOCK) WHERE A.DPTO=ART.DPTO AND A.SECCION=ART.SECCION),'''') AS DESCSECCION ,' +
          ' ISNULL((SELECT TOP (1) CODBARRAS FROM ARTICULOSDETALLE AS B WITH(NOLOCK) WHERE AVL.CODARTICULO=B.CODARTICULO ),'''') AS CODBARRAS,' +

          QuotedSTR('F') + ' AS LINEAOCULTA, ' ;

       StrSQL01:=StrSQL01+ ' ' + '  ISNULL((' + Quotedstr(chr(124))  + ' + (SELECT DISTINCT  + DESCRIPCION  + ' + Quotedstr(chr(124))  + '   AS [text()] ' +
          'FROM  TIQUETSMODIF AS F WITH (NOLOCK)'+
          'WHERE F.SERIE=AVL.SERIE AND F.NUMERO=AVL.NUMERO AND ESARTICULO=1' +
          'AND NUMLINEA=MAX(AVL.NUMLINEA)'+
          'FOR XML PATH('''') )),'''') AS MODIFICADORES, ';
       StrSQL01:=StrSQL01+ ' ISNULL((SELECT TOP(1) C.CODALMACEN FROM TIQUETSCAB AS C WITH (NOLOCK) WHERE ' +
        ' AVL.SERIE=C.SERIE AND AVL.FO=C.FO AND AVL.NUMERO=C.NUMERO),'''') AS CODALMACEN,' +
        ' ISNULL((SELECT TOP(1) E.NOMBREALMACEN FROM TIQUETSCAB AS D WITH (NOLOCK) ' +
        ' LEFT JOIN ALMACEN AS E WITH (NOLOCK) ON D.CODALMACEN=E.CODALMACEN COLLATE Latin1_General_CS_AI WHERE AVL.SERIE=D.SERIE AND AVL.FO=D.FO AND AVL.NUMERO=D.NUMERO),'''') AS NOMBREALMACEN, '  +
        ' ISNULL(ART.DESCRIPADICIONAL,'+ Quotedstr('') + ') AS DESCRIPADIC, ' +
        ' AVC.CODVENDEDOR, SUBSTRING(VEN.NOMBREVENDEDOR,1,20) AS NOMVENDEDOR ' ;

        StrSQL01:=StrSQL01+
        ' FROM  TIQUETSLIN AS AVL WITH (NOLOCK)' +
        ' LEFT JOIN ARTICULOS AS ART WITH (NOLOCK) ON AVL.CODARTICULO=ART.CODARTICULO' +
        ' INNER JOIN TIQUETSCAB AS AVC WITH (NOLOCK) ON AVL.FO=AVL.FO AND AVL.NUMERO=AVC.NUMERO AND AVL.SERIE=AVC.SERIE' +
        ' LEFT JOIN VENDEDORES AS VEN WITH (NOLOCK) ON VEN.CODVENDEDOR=AVC.CODVENDEDOR ' +
        ' WHERE AVL.SERIE=' +  QuotedSTR(SerieAlb) +' AND AVL.NUMERO= ' +  IntToStr(NumeroAlb)  +
        ' GROUP BY AVL.FO,  AVL.SERIE , AVL.NUMERO , AVL.CODARTICULO,AVL.REFERENCIA, AVL.DESCRIPCION,ART.DESCRIPADICIONAL, ' +
        ' AVC.CODVENDEDOR,VEN.NOMBREVENDEDOR,AVL.PRECIO, AVL.IVA,AVL.DTO,AVL.TIPOIVA,AVL.PRECIOIVA,  ' +
        ' AVL.CODARTICULO, ART.DPTO, ART.SECCION,ART.PORPESO ' +
		    ' HAVING  (AVL.CODARTICULO > 0 AND SUM(AVL.UNIDADES) <> 0)  ';



        //Si se envio para ordenar los campos
       // if CamposOrdenar <> '' then   StrSQL01:= StrSQL01+ ' ORDER BY ' + CamposOrdenar    ;

      End;

      2: // FrontRetail, hotel o manager
      Begin
        StrSQL01:='';
        StrSQL01:='SELECT  AVL.NUMSERIE, AVL.NUMALBARAN, AVL.NUMLIN,  AVL.REFERENCIA,  AVL.SUPEDIDO,  AVL.TALLA, AVL.COLOR,AVL.DESCRIPCION,' +
            ' AVL.UNIDADESTOTAL AS UNIDADES, AVL.PRECIO, AVL.IVA,AVL.TOTAL,AVL.DTO,AVL.TIPOIMPUESTO,' +
            ' AVL.PRECIOIVA,AVL.TOTALEXPANSION, AVL.UNID1, AVL.UNID2, AVL.UNID3, AVL.UNID4,AVL.CODARTICULO,' +
            ' ART.DPTO, ART.SECCION,ART.PORPESO,' +
            ' ISNULL((SELECT TOP(1) A.DESCRIPCION FROM SECCIONES AS A WITH (NOLOCK) WHERE A.NUMDPTO=ART.DPTO AND A.NUMSECCION=ART.SECCION),'''') AS DESCSECCION,   ' +
            ' ISNULL((SELECT TOP (1) CODBARRAS FROM ARTICULOSLIN AS B WITH(NOLOCK) WHERE AVL.CODARTICULO=B.CODARTICULO AND AVL.TALLA=B.TALLA AND AVL.COLOR=B.COLOR),'''') AS CODBARRAS,' +
            ' AVL.LINEAOCULTA, '''' AS MODIFICADORES, AVL.CODALMACEN,  ' +
            ' ISNULL((SELECT NOMBREALMACEN FROM ALMACEN AS C WITH (NOLOCK) WHERE AVL.CODALMACEN=C.CODALMACEN),'''') AS NOMBREALMACEN,    ' +
            ' ISNULL(ART.DESCRIPADIC,'''') AS DESCRIPADIC, AVL.CODVENDEDOR, SUBSTRING(VEN.NOMVENDEDOR,1,20) AS NOMVENDEDOR' +
            ' FROM ALBVENTALIN AS AVL WITH (NOLOCK)   ' +
            ' LEFT JOIN ARTICULOS AS ART WITH (NOLOCK) ON AVL.CODARTICULO=ART.CODARTICULO  ' +
            ' LEFT JOIN VENDEDORES AS VEN WITH (NOLOCK) ON VEN.CODVENDEDOR=AVL.CODVENDEDOR ' +
            ' WHERE AVL.NUMSERIE=' + QuotedSTR(SerieAlb) + ' AND AVL.NUMALBARAN= ' + IntToStr(NumeroAlb) +  '' ;

        //Si se envio para ordenar los campos
        if CamposOrdenar <> '' then   StrSQL01:= StrSQL01+ 'ORDER BY ' + CamposOrdenar    ;
      End;
    End;


    //----------------------------------------------------------------------
    //Se Abre el DataSet  con los calculos del impuesto FPTE
    //----------------------------------------------------------------------
    Conectado:= Open_ADO_Qry(CnADO, rs001,StrSQL01,MensajeError,false,False);
    //----------------------------------------------------------------------

    //Si no hay conexi�n se interrumpe la ejecuci�n de la aplicaci�n
    if Conectado=False then Exit;

    //Se va analizar el objeto recordset
    With rs001 do
    Begin
      // Se verifica que se tengan registros
      if Not eof then
      Begin
        //Se mueve al primer registro del recordset
        First;
        //Se recorre los registros de las lineas
        while Not EOF do
        begin
          //Adicionamos al data set los registros
          CDS.Append;
          CDS.FieldByName('NUMSERIE').Value := FieldByName('NUMSERIE').AsString;
          CDS.FieldByName('NUMALBARAN').Value := FieldByName('NUMALBARAN').AsInteger;
          CDS.FieldByName('NUMLIN').Value := FieldByName('NUMLIN').AsInteger;
          CDS.FieldByName('REFERENCIA').Value := FieldByName('REFERENCIA').AsString;
          CDS.FieldByName('SUPEDIDO').Value := FieldByName('SUPEDIDO').AsString;
          CDS.FieldByName('TALLA').Value := FieldByName('TALLA').AsString;
          CDS.FieldByName('COLOR').Value := FieldByName('COLOR').AsString;
          CDS.FieldByName('DESCRIPCION').Value := FieldByName('DESCRIPCION').AsString;
          CDS.FieldByName('UNIDADES').Value := FieldByName('UNIDADES').AsFloat;
          CDS.FieldByName('PRECIO').Value := FieldByName('PRECIO').AsFloat ;
          CDS.FieldByName('IVA').Value := FieldByName('IVA').AsFloat;
          CDS.FieldByName('TOTAL').Value := FieldByName('TOTAL').AsFloat;
          CDS.FieldByName('DTO').Value := FieldByName('DTO').AsFloat;
          CDS.FieldByName('TIPOIMPUESTO').Value := FieldByName('TIPOIMPUESTO').AsInteger;
          CDS.FieldByName('PRECIOIVA').Value := FieldByName('PRECIOIVA').AsFloat;
          CDS.FieldByName('TOTALEXPANSION').Value := FieldByName('TOTALEXPANSION').AsFloat;
          CDS.FieldByName('UNID1').Value := FieldByName('UNID1').AsFloat;
          CDS.FieldByName('UNID2').Value := FieldByName('UNID2').AsFloat;
          CDS.FieldByName('UNID3').Value := FieldByName('UNID3').AsFloat;
          CDS.FieldByName('UNID4').Value := FieldByName('UNID4').AsFloat;
          CDS.FieldByName('CODARTICULO').Value := FieldByName('CODARTICULO').AsInteger;
          CDS.FieldByName('DPTO').Value := FieldByName('DPTO').AsInteger;
          CDS.FieldByName('SECCION').Value := FieldByName('SECCION').AsInteger;
          CDS.FieldByName('PORPESO').Value := FieldByName('PORPESO').AsString;
          CDS.FieldByName('DESCSECCION').Value := FieldByName('DESCSECCION').AsString;
          CDS.FieldByName('CODBARRAS').Value := FieldByName('CODBARRAS').AsString;
          CDS.FieldByName('LINEAOCULTA').Value := FieldByName('LINEAOCULTA').AsString;
          CDS.FieldByName('MODIFICADORES').Value := FieldByName('MODIFICADORES').AsString;
          CDS.FieldByName('CODALMACEN').Value := FieldByName('CODALMACEN').AsString;
          CDS.FieldByName('NOMBREALMACEN').Value := FieldByName('NOMBREALMACEN').AsString;
          CDS.FieldByName('DESCRIPADIC').Value := FieldByName('DESCRIPADIC').AsString;
          CDS.FieldByName('CODVENDEDOR').Value := FieldByName('CODVENDEDOR').AsInteger;
          CDS.FieldByName('NOMVENDEDOR').Value := FieldByName('NOMVENDEDOR').AsString;
          //Siguiente registro
          Next;
        end;
      End;
    End;

    //------------------------------------------------------------------------
    //Se Verifica si el archivo del subtotal existe, para borrarlo
    //------------------------------------------------------------------------

    if Length(DataFile)=0 then  DataFile:='ICGDO_plin.xml';

    if FileExists(RutaApps+DataFile)then
    begin
      EliminarArchivo (RutaApps+DataFile)
    end;

    CDS.SaveToFile(RutaApps+DataFile, dfXMLUTF8);
    plinXML := TXMLDocument.Create(nil);
    plinXML.XML.Text:= xmlDoc.FormatXMLData(plinXML.XML.Text);
    plinXML.Active := true;
    plinXML.LoadFromFile(RutaApps+DataFile);
    // Se Guarda el archivo creado
    VarXML:=plinXML.XML.Text;
    //XML.SaveToFile(rutaFichero);
    plinXML.Active := False;

  Finally
     CDS.Close;
     CDS.Free;
     rs001.Close;
     rs001.free;
     plinXML.Free;
  End;
    Result := VarXML;

End;


//------------------------------------------------------------------------------
// Funcion indica el estado de un servicio
//------------------------------------------------------------------------------
{Este proceso se encarga de indicar cual es el estado de un servicio de windows,
tiene dos opciones,
TipoInfo=0 devuelve el nombre del estado del servicio;
TipoInfo=1 devuelve el Id del estado del servicio
}
//------------------------------------------------------------------------------
function EstadoServicioWindos(sPC, sServicio : string;TipoInfo:Integer ) : string;
var
  schm, schs   : SC_Handle;
  ss     : TServiceStatus;
  dwStat : DWord;
begin
  dwStat := 0;
  schm := OpenSCManager(PChar(sPC), Nil, SC_MANAGER_CONNECT);
  if (schm > 0) then
  begin
    schs := OpenService(schm, PChar(sServicio), SERVICE_QUERY_STATUS);
    if (schs > 0) then
    begin
      if (QueryServiceStatus(schs, ss)) then
      begin
        dwStat := ss.dwCurrentState;
      end;
      CloseServiceHandle(schs);
    end;
    CloseServiceHandle(schm);
  end;
  case dwStat of
    0 : Result := 'No disponible';
    1 : Result := 'Detenido';
    2 : Result := 'Iniciando';
    3 : Result := 'Deteniendo';
    4 : Result := 'Iniciado';
    7 : Result := 'Pausado';
  else
    result := inttostr(dwstat);
  end;

  //Se verifica si se envia la descripci�n o el codigo del estado del servicio
  if TipoInfo=1 then Result :=inttostr(dwstat);
end;



//------------------------------------------------------------------------------
// Funcion que inicia un servicio de windows
//------------------------------------------------------------------------------
{Este proceso se encarga de iniciar un servcio de windows , se verifica si el servicio
esta instalado
}
//------------------------------------------------------------------------------
function IniciarServicioWindows(sMachine, sService: String): Boolean;
var
  schm,
  schs: SC_Handle;
  ss: TServiceStatus;
  psTemp: PChar;
  dwChkP: DWord;
begin
//  ss.dwCurrentState := -1;

  schm := OpenSCManager(PChar(sMachine), nil, SC_MANAGER_CONNECT);
  if (schm>0) then
  begin
    schs := OpenService(schm, PChar(sService), SERVICE_START or
      SERVICE_QUERY_STATUS);
    if (schs>0) then
    begin
      psTemp := nil;
      if (StartService(schs, 0, psTemp)) then
        if (QueryServiceStatus(schs, ss)) then
          while (SERVICE_RUNNING<>ss.dwCurrentState) do
          begin
            dwChkP := ss.dwCheckPoint;
            Sleep(ss.dwWaitHint);
            if (not QueryServiceStatus(schs, ss)) then
              Break;
            if (ss.dwCheckPoint < dwChkP) then
              Break;
          end;
      CloseServiceHandle(schs);
    end;
    CloseServiceHandle(schm);
  end;
  Result := SERVICE_RUNNING=ss.dwCurrentState;
end;


//------------------------------------------------------------------------------
// Funcion que detiene un servicio de windows
//------------------------------------------------------------------------------
{Este proceso se encarga de parar la ejecuci�n de un servcio de windows , se verifica si el servicio
esta instalado
}
//------------------------------------------------------------------------------

function DetenerServicioWindows(sMachine, sService: String): Boolean;
var
  schm,
  schs: SC_Handle;
  ss: TServiceStatus;
  dwChkP: DWord;
begin
  schm := OpenSCManager(PChar(sMachine), nil, SC_MANAGER_CONNECT);
  if (schm>0) then
  begin
    schs := OpenService(schm, PChar(sService), SERVICE_STOP or
      SERVICE_QUERY_STATUS);
    if (schs>0) then
    begin
      if (ControlService(schs, SERVICE_CONTROL_STOP, ss)) then
        if (QueryServiceStatus(schs, ss)) then
          while (SERVICE_STOPPED<>ss.dwCurrentState) do
          begin
            dwChkP := ss.dwCheckPoint;
            Sleep(ss.dwWaitHint);
            if (not QueryServiceStatus(schs, ss)) then
              Break;
            if (ss.dwCheckPoint < dwChkP) then
              Break;
          end;
      CloseServiceHandle(schs);
    end;
    CloseServiceHandle(schm);
  end;
  Result := SERVICE_STOPPED=ss.dwCurrentState;
end;

//------------------------------------------------------------------------------
{Funci�n que convierte un Objeto del tipo DataSet en un Objeto del Tipo JSon
}
//------------------------------------------------------------------------------
function DataRowToJSONObject(const AValue : TDataSet): TJSONObject;
var
  I: Integer;
  AString : String;
begin
  Result := TJSONObject.Create();
  for I := 0 to AValue.FieldDefs.Count-1 do
  begin
    case AValue.FieldDefs[I].DataType of
      ftString, ftWideString, ftMemo :
        begin
          if AValue.FieldByName(AValue.FieldDefs[I].Name).AsString <> '' then
            Result.AddPair(AValue.FieldDefs[I].Name, AValue.FieldByName(AValue.FieldDefs[I].Name).AsString)
          else
            Result.AddPair(AValue.FieldDefs[I].Name, ' ');
        end;
      ftSmallint, ftInteger, ftWord, ftLongWord, ftShortint :
        begin
          Result.AddPair(AValue.FieldDefs[I].Name, TJSONNumber.Create(AValue.FieldByName(AValue.FieldDefs[I].Name).AsInteger));
        end;
      ftFloat, ftCurrency :
        begin
          Result.AddPair(AValue.FieldDefs[I].Name, TJSONNumber.Create(AValue.FieldByName(AValue.FieldDefs[I].Name).AsFloat));
        end;
      ftBoolean :
        begin
          Result.AddPair(AValue.FieldDefs[I].Name, AValue.FieldByName(AValue.FieldDefs[I].Name).AsString)
        end;
    end;
  end;
end;


//------------------------------------------------------------------------------
{Funci�n que convierte un Objeto del tipo Objeto en un Objeto del Tipo DataSet
}
//------------------------------------------------------------------------------
function JSONObjectToDataRow(const AJson : TJSONObject; const AValue : TDataSet): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to AValue.FieldDefs.Count - 1 do
  begin
    case AValue.FieldDefs[I].DataType of
      ftString, ftWideString, ftMemo :
        begin
          AValue.FieldByName(AValue.FieldDefs[I].Name).AsString :=
            AJson.Get(AValue.FieldDefs[I].Name).JsonValue.Value;
        end;
      ftSmallint, ftInteger, ftWord, ftLongWord, ftShortint :
        begin
          AValue.FieldByName(AValue.FieldDefs[I].Name).AsInteger :=
            (AJson.Get(AValue.FieldDefs[I].Name).JsonValue as TJsonNumber).AsInt;
        end;
      ftFloat, ftCurrency :
        begin
          AValue.FieldByName(AValue.FieldDefs[I].Name).AsFloat :=
            (AJson.Get(AValue.FieldDefs[I].Name).JsonValue as TJsonNumber).AsDouble;
        end;
      ftBoolean :
        begin
          AValue.FieldByName(AValue.FieldDefs[I].Name).AsBoolean :=
            StrToBool(AJson.Get(AValue.FieldDefs[I].Name).JsonValue.Value);
        end;
    end;
  end;
end;



//------------------------------------------------------------------------------
//Funci�n para agregar caracteros a la izquierda o la derecha
//------------------------------------------------------------------------------
Function PadString(cVar :String; Caracter :Char; nLen:Integer; Orientacion :TOrientacion):String;
begin
  if Orientacion = toLeft then
    Result:=StringOfChar(Caracter, nLen - Length(cVar))+cVar
  else
    Result:=cVar+StringOfChar(Caracter, nLen - Length(cVar));
end;



//------------------------------------------------------------------------------
//Funci�n para Crear un string list de una cadena
//------------------------------------------------------------------------------
function SepararCadena(const Cadena: string; const Delim: Char): TStringList;
begin
  Result:= TStringList.Create;
  Result.Delimiter:= Delim;
  Result.DelimitedText:= Cadena;
end;



//-----------------------------------------------------------------------------
//Se lee el archivo texto y se convierte en una variable
//------------------------------------------------------------------------------
function LeerTXT_A_Variable(Archivo:string): WideString;
var F: TextFile;
    sLinea:String;
    VariableStr :  WideString;
begin
  //
  VariableStr:='';
  if FileExists(ExtractFilePath( Application.ExeName ) + Archivo)then
  begin

    AssignFile( F, ExtractFilePath( Application.ExeName ) + Archivo );
    Reset( F );
    sLinea:='';
    while not Eof( F ) do
    begin
      ReadLn( F, sLinea );
      VariableStr:= VariableStr+( sLinea )+   sLineBreak ;
    end;
    CloseFile( F );
  end;
  Result:=VariableStr;

end;


//-----------------------------------------------------------------------------
//Ejecutar TQry desde un archivo
//------------------------------------------------------------------------------
function EjecutarTADOQuery(Archivo:string;adoQry:TAdoQuery;var MensajeError: String;MostrarError:Boolean ):Boolean;
var
ScriptsToExecute:Tstringlist;
begin
try
 if FileExists(ExtractFilePath( Application.ExeName ) + Archivo)then
  begin
  ScriptsToExecute:=TStringList.Create;
  adoQry.Close;
  adoQry.SQL.Clear;
  ScriptsToExecute.LoadFromFile(Archivo);
  adoQry.SQL.Add(ScriptsToExecute.Text);
  adoQry.active:=true;
  Result:=True;

  end;
except
  On E:Exception do
    begin
     //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      if MostrarError=True then
      Begin
         MessageDlg(E.Message, mterror, [mbOK], 0);
      End;
      MensajeError:=E.Message;
      Result:=False;
      exit;
    end  ;
  end;
end;




//------------------------------------------------------------------------------
//Funcion que busca un valor en la tapla de parametros
//------------------------------------------------------------------------------
Function ValorTablaParametros(CnADO:TADOConnection; Pclave,Psubclave,PUsuario,MensajeError:String;MostrarError,Encriptado:Boolean): String;
Var
  rsParametro: TADOQuery; //Recordset tabla de Parametros
  ValorParametro,
  StrSQL:String;
Begin

  ValorParametro:='';
  StrSQL:='';
  try
    //si hay conexi�n a la base de datos
    if CnADO.Connected Then
    Begin
      //Se crea el objeto de base de datos AdoQry
      rsParametro:= TADOQuery.Create(nil);
      with rsParametro do
      begin
        Close;
        Connection:=CnADO;
        SQL.Clear;
        //se verifica si deben encriptarse los valores
        if Encriptado  then
        Begin
            Psubclave:= (Encrypt(PSubClave)) ;
            Pusuario:= (Encrypt(PUsuario)) ;
        End;

        StrSQL:='SELECT  VALOR FROM ICGDO_TB_PARAMETROS WITH (NOLOCK) ' +
          ' WHERE (CLAVE =  ' + QuotedSTR(Pclave) + ')' +
          ' AND (SUBCLAVE = ' + QuotedSTR(Psubclave) + ')' +
          ' AND (USUARIO = ' + QuotedSTR(Pusuario) + ')';
        SQL.Text:=StrSQL;
        Open;

        //Si existe el registro se toma el valor
        if not eof then
        Begin
            if Encriptado  then ValorParametro:= Decrypt(Fields[0].AsString)
            Else
              ValorParametro:= Fields[0].AsString;
        End;
        Close;
      End;
      rsParametro.Free;
    End;

  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      if MostrarError=True then
      Begin
        MessageDlg('No se Puede Consultar la Informacion Solicitada, el Error es :' + E.Message,
        mterror, [mbok],0);
      End;
      MensajeError:=E.Message;
    End;
  End;

  //Retornar el valor del parametro
  Result:=ValorParametro;

End ;



// ================================================================
// Return the three dates (Created,Modified,Accessed)
// of a given filename. Returns FALSE if file cannot
// be found or permissions denied. Results are returned
// in TdateTime VAR parameters
// ================================================================
// ================================================================
// Devuelve las tres fechas (Creaci�n, modificaci�n y �ltimo acceso)
// de un fichero que se pasa como par�metro.
// Devuelve FALSO si el fichero no se ha podido acceder, sea porque
// no existe o porque no se tienen permisos. Las fechas se devuelven
// en tres par�metros de ipo DateTime
// ================================================================
 // Correcto?
{  if GetFileTimes('c:\autoexec.bat', CDate, MDate, ADate) then begin
    Label1.Caption := FormatDateTime('dd/mm/yyyy hh:nn',CDate);
    Label2.Caption := FormatDateTime('dd/mm/yyyy hh:nn',MDate);
    Label3.Caption := FormatDateTime('dd/mm/yyyy hh:nn',ADate);
  end;
 }
function GetFileTimes(FileName : string; var Created : TDateTime;
    var Modified : TDateTime; var Accessed : TDateTime) : boolean;
var
   FileHandle : integer;
   Retvar : boolean;
   FTimeC,FTimeA,FTimeM : TFileTime;
   LTime : TFileTime;
   STime : TSystemTime;
begin
  // Abrir el fichero
  FileHandle := FileOpen(FileName,fmShareDenyNone);
  // inicializar
  Created := 0.0;
  Modified := 0.0;
  Accessed := 0.0;
  // Ha tenido acceso al fichero?
  if FileHandle < 0 then
    RetVar := false
  else begin

    // Obtener las fechas
    RetVar := true;
    GetFileTime(FileHandle,@FTimeC,@FTimeA,@FTimeM);
    // Cerrar
    FileClose(FileHandle);
    // Creado
    FileTimeToLocalFileTime(FTimeC,LTime);

    if FileTimeToSystemTime(LTime,STime) then begin
      Created := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
      Created := Created + EncodeTime(STime.wHour,STime.wMinute,
              STime.wSecond, STime.wMilliSeconds);
    end;

    // Accedido
    FileTimeToLocalFileTime(FTimeA,LTime);

    if FileTimeToSystemTime(LTime,STime) then begin
      Accessed := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
      Accessed := Accessed + EncodeTime(STime.wHour,STime.wMinute,
              STime.wSecond, STime.wMilliSeconds);
    end;

    // Modificado
    FileTimeToLocalFileTime(FTimeM,LTime);

    if FileTimeToSystemTime(LTime,STime) then begin
      Modified := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
      Modified := Modified + EncodeTime(STime.wHour,STime.wMinute,
                     STime.wSecond, STime.wMilliSeconds);
    end;
  end;
  Result := RetVar;
end;

//------------------------------------------------------------------------------
//Funci�n realizada que permite obtener a partir de una fecha gregoriana (dd/mm/aaaa) la fecha juliana (n�mero de d�as transcurridos desde inicio de a�o).
//-----------------------------------------------------------------------------
function fechaJuliana (fechaGregoriana : TDateTime) : Integer;
var
  dia, mes, ano : Word;
begin
  fechaGregoriana := Trunc(fechaGregoriana );
  DecodeDate(fechaGregoriana, ano, mes, dia);
  Result := Trunc(fechaGregoriana - EncodeDate(ano , 1 , 1)+1);
end;


//------------------------------------------------------------------------------
//Funci�n que retorna el digito verificador de la cedula o RNC Rep Dominicana
//------------------------------------------------------------------------------
function GenDV_Para_DO(strRNCCedsinD:string):Integer;
var
  DigitoV,Resta,A,Suma:Integer;
  Numero_Base:String;
  Division:Double;
begin
  Try
    If Length(strRNCCedsinD) = 8 Then
    begin
      {'If Rs!RRC_TIPO_PERSONA = PersonaJuridica Then
        'Si entra aqui es porque el tipo de persona
        'es Juridica, por lo tanto se calcula el digito
        'para un RNC }
        Numero_Base:= '79865432';
        //Se recorre cada dig�to del documento
        //RNC
        for A:=1 to 8 do
        begin
          Suma := Suma + StrToInt(Copy(strRNCCedsinD, A, 1)) * StrToInt((Copy(Numero_Base, A, 1)));
        end;
        //Se
        Division := Suma / 11;
        Resta := Suma - (Trunc(Division) * 11) ;
        If Resta = 0
          Then
            DigitoV:= 2
          Else
            If Resta= 1
            Then
              DigitoV := 1
            Else
              DigitoV:= 11 - Resta ;
      end
      Else
      Begin
        If Length(strRNCCedsinD) = 10 Then
        Begin
          //'ElseIf Rs!RRC_TIPO_PERSONA = PersonaFisica Then
          //'Si entra aqui es porque el tipo de persona
          //'no es Juridica, por lo tanto se calcula el digito
          //'para una C�dula
          //C�dula
          strRNCCedsinD := formatfloat('0000000000',StrToFloat(strRNCCedsinD));
          Numero_Base := '1212121212';
          //Se recorre cada digito del documento
          for A:=1 to 10 do
          begin
            Resta := StrToInt(Copy(strRNCCedsinD, A, 1)) * StrToInt(Copy(Numero_Base, A, 1)) ;
            if (Resta > 9)
              then
                 Suma := Suma + StrToInt(Copy(IntToStr(Resta), 1, 1)) +  StrToInt(Copy(IntToStr(Resta), 2, 1))
              else
               Suma := Suma + StrToInt(Copy(IntToStr(Resta), 1, 1)) ;
          end;
          Division := Trunc((Suma / 10)) * 10;
          If Division < Suma  Then
          begin
            Division := Division + 10;
            DigitoV := Trunc(Division) - Suma;
          end
          Else
            DigitoV := 0  ;
        end;
      End;
  except
  On E:Exception do
  begin
      MessageDlg(E.Message, mterror, [mbOK], 0);
      exit;
    end;
  end;

  //Se retorna el d�gito verificador
  Result := DigitoV;

end;







//------------------------------------------------------------------------------
//Procedimientos
//------------------------------------------------------------------------------

//Procedimiento para Cerrar la Conexion a la Base de datos
Procedure CloseConnection  (CnADO:TADOConnection; RsDataSet:TADOQuery);
var
Mensaje:string;
Begin
  Try
    //Si la Conexion a la Base de datos esta Abierta
    if CnADO.Connected Then
    Begin
      with RsDataSet do begin
        if  RsDataSet.Active then
        Begin
          Close;
          Free;
        End;
        CnADO.Close;
        CnADO.Free;
      end;
    End;
  except
    on E:Exception do
      Begin
        if Mensaje='' then Mensaje:=E.Message;
        //------------------------------------------------------------------------
        // Si hay un error
        //--------------------------------------------------------------------------
        // Se guarda el error en el log
        WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + ' ' + Mensaje, 1,0);
      End
    end;


End;



//Procedimiento que Cierra una aplicacion si ya se esta ejecutando
Procedure Close_EXE (Apps:String);
var
  Cerrar_Ejecutable:THandle;
  LaApps:PWideChar;
Begin
  LaApps:= PChar(Apps);
  Cerrar_Ejecutable:=FindWindow(LaApps,nil);
 // DefWindowProc(Cerrar_Ejecutable, WM_SETTEXT, 0, 0);
  if Cerrar_Ejecutable<> 0 Then ;
  Begin
    PostMessage(FindWindow(Nil, Pchar(LaApps)), WM_QUIT, 0, 0);
  end;
End;
//------------------------------------------------------------------------------

//Procedimiento Para Saber el Nombre del Pc, IP y el Usuario en Windows
Procedure ObtenerDatosPC (var Datos:TDatosPC );
const LARGO_MAXIMO = 50;
var
  buffer:Array [0..LARGO_MAXIMO+1] of char;
  Largo:Cardinal;
  PuntHost: PHostEnt;
  PuntIP: PAnsichar;
  wVersionRequested: WORD;
  wsaData: TWSAData;
begin
  Largo := LARGO_MAXIMO +1;
  Datos.Nombre := '';
  If GetComputerName (buffer, Largo) then Datos.Nombre := buffer;
  Datos.Usuario := '';
  if GetUserName(buffer, largo) then Datos.Usuario := buffer;
  wVersionRequested := MAKEWORD( 1, 1 );
  WSAStartup( wVersionRequested, wsaData );
  GetHostName( @buffer, LARGO_MAXIMO );
  PuntHost := GetHostByName( @buffer );
  PuntIP := iNet_ntoa( PInAddr( PuntHost^.h_addr_list^ )^ );
  Datos.IP := PuntIP ;
  WSACleanup;
end;
//------------------------------------------------------------------------------

//Procedimiento para escribir valores en el Registro de Windows-----------------
procedure SetRegistryData(RootKey: HKEY; Key, Value: string; RegDataType: Integer; Data: variant);
var
  Reg: TRegistry;
  s: string;
begin
  Reg := TRegistry.Create(KEY_WRITE);
  try
    Reg.RootKey := RootKey;
    if Reg.OpenKey(Key, True) then begin
      try
        if RegDataType = 0  then
          Reg.WriteString(Value, Data);
        if RegDataType = 1 then
          Reg.WriteString(Value, Data)
        else if RegDataType = 2 then
          Reg.WriteExpandString(Value, Data)
        else if RegDataType = 3 then
          Reg.WriteInteger(Value, Data)
        else if RegDataType = 4 then begin
          s := Data;
          Reg.WriteBinaryData(Value, PChar(s)^, Length(s));
        end else
          raise Exception.Create(SysErrorMessage(ERROR_CANTWRITE));
      except
        Reg.CloseKey;
        raise;
      end;
      Reg.CloseKey;
    end else
      raise Exception.Create(SysErrorMessage(GetLastError));
  finally
    Reg.Free;
  end;
end;
//------------------------------------------------------------------------------


// Procedimiento que cierra la Aplicaci�n
Procedure CloseApp;
Begin
  //TFrmMain.conexionADO.Close;
//    conexionADO: TADOConnection;
//    ADOQry_Clientes: TADOQuery;

  Application.Terminate;

End;
//--------------------------------------------


//Procedimiento para Borrar Archivos
//------------------------------------------------------------------------------
procedure EliminarArchivo ( sArchivo: String);
begin
 // sArchivo := ExtractFilePath( Application.ExeName ) + 'prueba.txt';
  if FileExists( sArchivo ) then
    DeleteFile( sArchivo );
end;

//------------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// Construir la informacion con la Version de la aplicacion
//------------------------------------------------------------------------------
procedure GetBuild_Info(var V1, V2, V3, V4: Word);
var
   VerInfoSize, VerValueSize, Dummy : DWORD;
   VerInfo : Pointer;
   VerValue : PVSFixedFileInfo;
begin
VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
GetMem(VerInfo, VerInfoSize);
GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
With VerValue^ do
begin
  V1 := dwFileVersionMS shr 16;
  V2 := dwFileVersionMS and $FFFF;
  V3 := dwFileVersionLS shr 16;
  V4 := dwFileVersionLS and $FFFF;
end;
FreeMem(VerInfo, VerInfoSize);
end;
//------------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Generar un arhivo texto que funcione como un archivo log
//------------------------------------------------------------------------------
procedure BitacoraTXT(Archivo,Contenido:String);
var
  Fichero: TextFile;
begin
  AssignFile(Fichero, Archivo);
  if fileexists(Archivo) then
  begin
    Append(Fichero);
  end else
  begin
    Rewrite(Fichero);
  end;
  writeLn(Fichero,Contenido);
  CloseFile(Fichero);
end;


//----------------------------------------------------------------------------
//Se Genera el Archivo Log de la aplicaci�n
//-----------------------------------------------------------------------------
procedure WriteLogApps(TipoLog:Integer;Data:String;GeneraLogErrores,GeneraLogDataRecibida:Integer);
Var
  fichero : TStringList;
  FicheroLog,NombreFichero,Carpeta,RutaLog,Mensaje:String;
  Conectado:Boolean ;
begin

  //----------------------------------------------------------------------------
  //Se Verifica el tipo de Log de la aplicaci�n
  //----------------------------------------------------------------------------
  NombreFichero:='Fichero.txt';
  Carpeta:='';
  RutaLog:= ExtractFilePath(ParamStr(0));
  Case TipoLog  of
    1 :  //Log de la Aplicaci�n
      Begin
        NombreFichero:='Log_Apps_';
        Carpeta:='Log';

      End;
    2 :  //Log de Errores
      Begin
         //Si la aplicaci�n no esta configurada para generar LOG de errores  se retorna
        if (GeneraLogErrores=0) then Exit();
        NombreFichero:='Log_Error_';
        Carpeta:='Log';

      End;

    3 :  //Log de la data recibida
      Begin
          //Si la aplicaci�n no esta configurada para generar LOG de la data recibida
        //if (Parametros.GeneraLogDataRecibida=0) then Exit();
        NombreFichero:='Log_Data';
        Carpeta:='Log';
      End;
  End;


   //Se Verifica si existe la carpeta
  Conectado:=DirectoryExists(RutaLog+Carpeta+'\');
  if Conectado = False then
  Begin
    Try
      //Se crea La carpeta
      Conectado:=CreateDir(RutaLog+Carpeta);
      // Si no se puede crear se controla el error
    Except On E:Exception do
      Begin
        // Se guarda el error en el log
        Mensaje:='No se Puede Crear la Carpeta  ' + RutaLog+ Carpeta + ' El error es : ' + #13#10 + E.Message;
      End;
    End;
  End;
  //Genera Archivo texto con data enviada
  Try
    FicheroLog := RutaLog+ Carpeta+ '\' + NombreFichero + formatdatetime('yyyymmdd', now)+ '.txt';
    fichero := TStringList.Create;
    //Si Existe el archivo se carga
    if FileExists(FicheroLog) then fichero.LoadFromFile(FicheroLog);
    fichero.Add(Data);
    fichero.SaveToFile(FicheroLog);
    //Se Cierra el archivo
    Fichero.Free;
    // Si no se puede crear se controla el error
  Except On E:Exception do
    Begin
      // Se guarda el error en el log
      Mensaje:='No se Puede Crear El Archivo El error es : ' + #13#10 + E.Message;
    End;
  End;

end;





end.
