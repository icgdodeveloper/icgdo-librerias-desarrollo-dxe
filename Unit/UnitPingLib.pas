//******************************************************************************
// Unit que contiene los procedimientos y funciones que permite realizar
// Un Ping para saber si un equipo esta en la red y nos responde
// Empresas: Consultoria & Sistemas ICG, C.A. / Geinfor Venezuela, C.A.
// Autor: Rafael Rangel
// Fecha: 14/04/2013
// Ult Modificacion: 14/04/2013
//******************************************************************************
unit UnitPingLib;

interface

uses
    SysUtils, Variants, Classes, Graphics,
    StdCtrls, IdBaseComponent,
    IdComponent, IdRawBase, IdRawClient, IdIcmpClient, IdException, ExtCtrls;

type
    TPingThread = class(TThread)
    protected
        procedure Execute; override;
    private
        Line: string;
        ThisDelay: Integer;
    public
        IdIcmp: TIdIcmpClient;
        IP: string;
        Delay: integer;
        Memo: TMemo;
        Timer: TTimer;
        Cyclic: boolean;
        procedure IdIcmpReplay(ASender: TComponent; const AReplyStatus: TReplyStatus);
        procedure Write;
        procedure CyclicExecution;
        procedure SetParametres(IdICMpClient : TIdIcmpClient; Ip : String; Delay : integer; Cyclic : Boolean; Memo : TMemo; Timer : TTimer; Lbl, LblAvg : TLabel);
    end;

implementation

procedure TPingThread.SetParametres(IdICMpClient: TIdIcmpClient; Ip: String; Delay: integer; Cyclic: Boolean; Memo: TMemo; Timer: TTimer; Lbl, LblAvg: TLabel);
begin
    IdIcmp := IdICMpClient;
    Self.Ip := IP;
    Self.Delay := Delay;
    Self.Cyclic := Cyclic;
    Self.Memo := Memo;
    Self.Timer := Timer;

    Self.Lbl := Lbl;
    Self.LblAvg := LblAvg;
    FreeOnTerminate := true;
end;

procedure TPingThread.Write;
begin
    Memo.Lines.Add(Line);
end;

procedure TPingThread.CyclicExecution;
begin
    Timer.Enabled := true;
end;

procedure TPingThread.Execute;
begin
    inherited;
    IdIcmp.Host := IP;
    IdIcmp.ReceiveTimeout := Delay;
    IdIcmp.OnReply := IdIcmpReplay;
    Line := 'Pinging ' + IdIcmp.Host;
    Synchronize(LblCaption);
    try
        IdIcmp.Ping;
    except
        on A: EIdIcmpException do
        begin
            Line := Timetostr(Time) + ' - ' + A.Message;
            Synchronize(Write);
            Line := 'Ready!';
            if Cyclic then
                Synchronize(CyclicExecution);
        end;
        on A: EIdSocketError do
        begin
            if A.LastError = 11001 then
                Line := TimeToStr(Time) + ' - ' + 'Host Not Found or Invalid'
            else
                Line := TimeToStr(Time) + ' - ' + A.Message + ' - ' + IntToStr(A.LastError);
            Synchronize(Write);
            Line := 'Ready!';
            if Cyclic then
                Synchronize(CyclicExecution);
        end;
    end;
end;

procedure TPingThread.IdIcmpReplay(ASender: TComponent; const AReplyStatus: TReplyStatus);
var
    Reply, Status: string;
    Fail: boolean;
begin
    Reply := 'Reply from '+ IP +' time<' + InttoStr(AReplyStatus.MsRoundTripTime) + 'ms';
    Fail := true;
    case AReplyStatus.ReplyStatusType of
        rsEcho:
            begin
                Status := '';
                ThisDelay := AReplyStatus.MsRoundTripTime;
                Fail := false;
            end;
        rsError: Status := ' - Error Ocurred.';
        rsTimeOut: Status := ' - TimeOut Exceeded';
        rsErrorUnreachable: Status := ' - Unreachable IP';
        rsErrorTTLExceeded: Status := ' - TTL Exceeded';
    end;
    Line := TimeToStr(Time) + ' - ' + Reply + Status;
    Synchronize(Write);
    if Cyclic then
        Synchronize(CyclicExecution);
    Line := 'Ready! ' + IdIcmp.Host;
end;

end.
