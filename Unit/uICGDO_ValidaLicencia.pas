//------------------------------------------------------------------------------
{Unit para validar si la licencia esta activa, devuleve la informaci�n en formato
JSON.
ICG Dominicana
Rafael Rangel.

}
//------------------------------------------------------------------------------
unit uICGDO_ValidaLicencia;

interface

uses
  Winapi.Windows, Winapi.Messages,Winapi.ShellApi, System.SysUtils, System.Variants, System.Classes,
  ComCtrls,  Vcl.ImgList, IniFiles,  UnitComunesRest,  DateUtils,   OnGuard, OgUtil,
  UIDLicenciasICGDO,  REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope,System.JSON;

const
  CKey : TKey = ($54,$1F,$7E,$20,$87,$59,$4E,$E2,$4D,$F7,$14,$CE,$3C,$5F,$D0,$BA);
  EncryptionKey : TKey = ($54,$1F,$7E,$20,$87,$59,$4E,$E2, $4D,$F7,$14,$CE,$3C,$5F,$D0,$BA);




var

    IniLicencia : TIniFile;
    TheDir     : string;
    L          : integer;
    IdMachine  : STring; //Id del equipo
    NroSerial  : string;  // C�digo del Nro del Serial generado para esta licencia
    NroLicencia: string;  // Serial (semilla de la licencia)
    resultado  : String;  // Mensaje de autorizaci�n o rechazo de la licencia
    Valida   : Boolean;   // Flag sobre el proceso de ejecuci�n.
    DatosPC: TDatosPc;
    IPPC,IDActivacion,
    IdMachineReal,IdMachineNew,Msg01,Msg02, Msg03,Msg04,Msg05,NameFileLicencia:String;
    PWMsg01,PWMsg02,PWMsg03,PWMsg04,PWMsg05, PWMsg06:PWideChar;
    Conectado:Boolean ;


    //------------------------------------------------------------------------------
    //Procedimiento que se encarga de validar si la licencia esta activa
    //------------------------------------------------------------------------------
    function ValidarLicencia (NameIniLicencia:string; CodApps,CodVersion:Integer;Activarla:Boolean):Boolean;
    //------------------------------------------------------------------------------
    //Funci�n que genera el Numeto de activaci�n en funci�n del Identificador + Numero de Licencia
    //-------------------------------------------------------------------------------
    function GetSerialNumber(const serialNumero : string; const codigo : string) : String;

    //------------------------------------------------------------------------------
    //Funci�n que genera la autentifcaci�n basado en el Id de la Maqu�na
    //------------------------------------------------------------------------------
    function AuthByMachine(const serialNumero : string; const codigo : string) : Boolean;

    //------------------------------------------------------------------------------
    //Funci�n que genera la autentifcaci�n basado en la fecha de la Licencia
    //------------------------------------------------------------------------------
    function AuthByDate(const serialNumero : string; const codigo : string) : TDateTime;

    //--------------------------------------------------------------------------
    //Se obtiene la clave de la licencia via web
    //--------------------------------------------------------------------------
    function ActivarWeb(edtIDLicencia,edtIDPC,edtIDSerial,NameIniLicencia:String):Boolean;

    //--------------------------------------------------------------------------
    //Se Registra la licencia
    //--------------------------------------------------------------------------
    function RegistrarLicencia(edtIDLicencia,edtIDPC,edtIDSerial,NameIniLicencia:string):Boolean;


implementation




//------------------------------------------------------------------------------
//Procedimiento que se encarga de validar si la licencia esta activa
//------------------------------------------------------------------------------
function ValidarLicencia(NameIniLicencia:string; CodApps,CodVersion:Integer;Activarla:Boolean):Boolean;
var
  Modifier:longint;
  Modkey:Tkey;
  FExpira,MDate,FVerifica:Tdate;
  ValorLicencia,VersionActual,VersionApps:string;
  releaseCode : TCode;
  largoID:Integer;
  edtIDLicencia,
  edtIDSerial,
  edtIDPC:string;

begin

  Try
    Valida:=False;
    NroSerial := '';
    NroLicencia:='';
    IdMachine:='';
    VersionActual:='';
    VersionApps:='';
    largoID:=0;
    Valida:=False;
    //Mensajes de la aplicaci�n
    Msg01:='Activaci�n Satisfactoria de la Licenai.';
    Msg02:='Licencia Expirada o Inv�lida.';
    Msg03:='La Licencia no esta Registrada';
    Msg04:='Proceda a registra la licencia, la fecha de expiraci�n es el :';
    Msg05:='Serial o licencia incorrecto, no se pudo registrar el producto';

    PWMsg01:='Nro. de Licencia no tiene la longitud correcta';
    PWMsg02:='Nro. de Serial no tiene la longitud correcta';
    PWMsg03:='Registro Realizado Correctamente,Gracias por Comprar el Producto';
    PWMsg04:='Licencia Expirada';
    PWMsg05:='Licencia Inv�lida o no corresponde a la clave de activaci�n.';
    PWMsg06:='C�digo de Activaci�n y/o la Licencia son Incorrectos, por favor verifique estos Valores!';
    //se Toma la informaci�n de la ruta donde esta corriendo la aplicaci�n

    TheDir :=  ExtractFilePath(ParamStr(0));
    L := Length(TheDir);
    if (L > 3) and (TheDir[L] <> '\') then
      TheDir := TheDir + '\';

    //----------------------------------------------------------------------------
    //Obtener los datos del Pc (Nombre, IP, Usuario)
    //----------------------------------------------------------------------------
    ObtenerDatosPC(DatosPC)   ;
    if Length(DatosPC.Nombre)<6
      then
      DatosPC.Nombre:=(StringOfChar('0',6 - Length(DatosPC.Nombre))+DatosPC.Nombre) ;
    //----------------------------------------------------------------------------

    //Validar se llego la informaci�n requerida
    //Si existe el archivo, se toman los Valores
    if (FileExists(TheDir + NameIniLicencia)) then
    begin
      {open Ini File}
      IniLicencia := TIniFile.Create(TheDir + NameIniLicencia);
      {try to read release code}
      NroSerial := IniLicencia.ReadString('Codes', 'SNCode', '');
      NroLicencia := IniLicencia.ReadString('Codes', 'SN','');
      IdMachine:= IniLicencia.ReadString('Codes', 'ID','');

      //------------------------------------------------------------------------
      //Se verifica la versi�n registrada con la versi�n indicada al llamar la dll
      //------------------------------------------------------------------------
      VersionApps:=Encrypt(FormatFloat('0000', CodVersion));
      VersionActual:=Copy(IdMachine,15,4);
      //si la versi�n del aplicaci�n es diferente al regsitrado en la versi�n, se
      //cambia la informaci�n en el archivo ini para olbligar que pida la licencia
      if VersionApps <> VersionActual  then
      begin
        largoID:=Length(IdMachine);
        IdMachine:=Copy(IdMachine,1,14) + VersionApps +  Copy(IdMachine,19,largoID-18);
        //Se guarda el nuevo valor del id de la maquina, con la actualizaci�n de la versi�n
        IniLicencia.WriteString('Codes', 'ID', IdMachine);
        IdMachine:='';
        //Se toma nuevamente el valor del Id Machine
        IdMachine:= IniLicencia.ReadString('Codes', 'ID','');
      end;
      //------------------------------------------------------------------------
      NameFileLicencia:= NameIniLicencia;
      FVerifica:= StrToDate(IniLicencia.ReadString('Codes', 'Fecha',DateToStr(IncDay(Date, -1))));
      // Liberamos el InitFile
      IniLicencia.Free;
      //se coloca la informaci�n el lo Edit
      edtIDLicencia:=NroLicencia;
      edtIDSerial:=NroSerial;
      edtIDPC:=IdMachine;

    end
    Else //No existe el archivo INI
    Begin

      // Se genera Identificador �nica de la m�quina generado por la libreria onguard
      Modifier := GenerateMachineModifierPrim;
      IdMachine:= BufferToHex(Modifier, SizeOf(longint));
      IdMachine:= ICGDO_GenerarIDMachine(IdMachine,DatosPC.IP,DatosPC.Nombre,CodApps,CodVersion);
      //Se devuelve el ID real del equipo
      edtIDPC:=IdMachine;
    End;

    //se obtiene el del indetificador real de la maquina
    IdMachineReal:=ICGDO_IDMachineReal(Now,IdMachine);
    //--------------------------------------------------------------------------
    // Validacion del numero de la licencia
    //--------------------------------------------------------------------------
    ValorLicencia := GetSerialNumber(edtIDLicencia, edtIDSerial) ;
    //Se verifica si el calculo de la licencia coincide con el valor registrado
    if  edtIDLicencia = ValorLicencia  Then
    begin
      // Verificamos que se trate de una licencia valida.
      if (AuthByMachine(edtIDLicencia, edtIDSerial))   Then
      begin
        // Verificamos que el c�digo de serial ingresado este vigente.
        FExpira:=(AuthByDate(edtIDLicencia, edtIDSerial));
        if (FExpira >= Date) then
        begin
          //------------------------------------------------------------------
          //se verifica si faltan 8 dias para que el sistema  expire, se da un mensaje
          //que el sistema esta por expirar
          //-------------------------------------------------------------------
          MDate:=IncDay(FExpira, -8);
          Valida := True;
          resultado :=Msg01;
          //se compara la fecha actual con la de vigencia de la licencia
          if MDate <= Date then
          Begin
            //se verifica si ya fue mostrado el dia de hoy el  mensaje para que registren la licencia
            if FVerifica<Date Then
            Begin
              WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Msg04 + DateToStr(FExpira) + ' ',1,0);
              Valida := False;
              Result:=Valida;
              resultado :=Msg04+ '   '  + DateToStr(FExpira);
              //se actualiza el INI para que no de el mensaje de validaci�n de la licencia
              IniLicencia := TIniFile.Create(TheDir + NameIniLicencia);
              try
                {write SN to IniFile se escribe que ya fue indicado el mensaje en el sistema }
                IniLicencia.WriteString('Codes', 'Fecha', DateToStr(Date));
              finally
                IniLicencia.Free;
              end;
            End;
          End
        Else;
        end
        else //No esta vigente la licencia
          Begin
           Valida := false;
           resultado :=Msg02 ;
           WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (resultado) + ' ',1,0)
          end
        end
      else //No es una licencia valida para la maquina y/o licencia
      begin
        Valida := false;
        resultado :=Msg02;
        WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (resultado) + ' ',1,0)
      end;
    end
    else //La licencia indicada con la calculada no coinciden
    begin
      Valida := false;
      Resultado := Msg03;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (resultado) + ' ',1,0)
    end;

    //se Verifica si la licencia no esta validad y debe activarse
    if (Valida=False)  and  (Activarla=True) then
    begin
      Valida:=ActivarWeb(edtIDLicencia,edtIDPC,edtIDSerial,NameIniLicencia);
      if Valida
        then
          WriteLogApps(3, 'Log ====> : ' + DateTimeToStr(now)  + '  , ' + (PWMsg03) + ' ',1,1) ;
    end;

  except
    On E:Exception do
    begin
      //Log De error
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (E.Message) + ' ',1,0) ;
      Valida := false;
      Exit;
    end;
  end;
  Result :=Valida;
end;

//------------------------------------------------------------------------------
//Funci�n que genera el Numeto de activaci�n en funci�n del Identificador + Numero de Licencia
//-------------------------------------------------------------------------------
function GetSerialNumber(const serialNumero : string; const codigo : string) : String;
var
  releaseCode : TCode;
  Modifier   : longint;
  ModKey: TKey;
  Idequipo:string;
begin
  ModKey:=Ckey;
  // Obtenemos el identificador �nico de la m�quina.
  Modifier := StringHashELF(IdMachineReal);
  HexToBuffer(serialNumero, Modifier, SizeOf(Modifier));
  HexToBuffer(codigo, releaseCode, SizeOf(TCode));
  ApplyModifierToKeyPrim(Modifier, ModKey, SizeOf(TKey));
  // Devolvemos el resultado de la validaci�n (Booleano)
  Result := IntToStr(GetSerialNumberCodeValue(Modkey, releaseCode));
end;

//------------------------------------------------------------------------------
//Funci�n que genera la autentifcaci�n basado en el Id de la Maqu�na
//------------------------------------------------------------------------------
function AuthByMachine(const serialNumero : string; const codigo : string) : Boolean;
var
  releaseCode : TCode;
  Modifier   : longint;
  Key : TKey;
begin
  Key := CKey;
  // Obtenemos el identificador �nico de la m�quina.
  Modifier := StringHashELF(IdMachineReal);
  HexToBuffer(serialNumero, Modifier, SizeOf(Modifier));
  HexToBuffer(codigo, releaseCode, SizeOf(TCode));
  ApplyModifierToKeyPrim(Modifier, Key, SizeOf(TKey));

  // Devolvemos el resultado de la validaci�n (Booleano)
  Result := IsSerialNumberCodeValid(Key, releaseCode);
end;

//------------------------------------------------------------------------------
//Funci�n que genera la autentifcaci�n basado en la fecha de la Licencia
//------------------------------------------------------------------------------
function AuthByDate(const serialNumero : string; const codigo : string) : TDateTime;
var
  releaseCode : TCode;
  Modifier   : longint;
  Key : TKey;
begin
  Key := CKey;
  // Obtenemos el identificador �nico de la m�quina.
  Modifier := StringHashELF(IdMachineReal);
  HexToBuffer(serialNumero, Modifier, SizeOf(Modifier));
  HexToBuffer(codigo, releaseCode, SizeOf(TCode));
  ApplyModifierToKeyPrim(Modifier, Key, SizeOf(TKey));
  // Devolvemos el resultado de la validaci�n (Booleano)
  Result := GetExpirationDate(Key, releaseCode);

end;


 //-----------------------------------------------------------------------------
//Se obtiene la clave de la licencia via web
//------------------------------------------------------------------------------
function ActivarWeb(edtIDLicencia,edtIDPC,edtIDSerial,NameIniLicencia:String):Boolean;
Var
  InfoJSON: TJSONObject;
  RespuestaJSON: TJSONObject;
  InfoJSONValue: TJSONValue;
  InfoJSONArray: TJSONArray;
  InfoRespuesta,IdSerial:String;
  InfoError:Integer;
  RESTClient_ActivarLicencia: TRESTClient;
  RESTRequestIdActivacion: TRESTRequest;
  RESTResponseID: TRESTResponse;

  Ini: TIniFile;
  ValorIni,FileINI, UrlMetodo:String;

begin
  Try


    //-----------------------------------------------------------------------------
    //Se Verifica si el archivo INI esta creado
    //-----------------------------------------------------------------------------
    FileINI:='ICGDO_DllAppsConfig.ini';
    Ini := TIniFile.Create(TheDir+FileINI);
    //Se Verifica si existe la Secci�n de Parametros
    Conectado:=Ini.SectionExists('Parametros');
    //Si No existe la seccion se crea
    if Conectado=False then
    Begin
      //------------------------------------------------------------------------
      //Se Crea el archivo INI
      //------------------------------------------------------------------------
      Ini.WriteString ('Parametros','Metodo','http://keys.icgdominicana.com:8800/datasnap/rest/TSM/ClaveActivacion');
      Ini.WriteString ('Parametros','Activacion','http://keys.icgdominicana.com:8800/Activacion');
    End;

    //-----------------------------------------------------------------------------
    //Se Verifica si el archivo INI existe
    //-----------------------------------------------------------------------------
    if FileExists(TheDir + FileINI)then
    begin
      //--------------------------------------------------------------------------
      //Se toman los datos del Archivo Ini y se tienen en el tipo de registro
      //Aplicaci�n
      //--------------------------------------------------------------------------
       UrlMetodo:= ini.ReadString('Parametros','Metodo','http://keys.icgdominicana.com:8800/datasnap/rest/TSM/ClaveActivacion');
    end;
    //Se libera el achivo INI
    FreeAndNil(Ini);

    //Se crea el objeto Json
    InfoJSON:=TJSONObject.Create;
    RespuestaJSON:=TJSONObject.Create;
    InfoJSONArray:=TJSONArray.Create;

    //Se prepara la petici�n al servicio Rest
    RESTClient_ActivarLicencia.BaseURL:='';
    RESTClient_ActivarLicencia.BaseURL:=UrlMetodo +'/' + edtIDLicencia + '/' + edtIDPC;

    //se ejecuta la instrucci�n
    RESTRequestIdActivacion.Execute;
    IDserial  := '';
    InfoError  := 1;
    InfoRespuesta  := 'No se obtuvo respuesta del sitio web';
    Valida:=False;
    //si la respuesta es satisfactoria
    if RESTResponseID.StatusCode = 200 then
    Begin
      //Se Toma la informaci�n del Objeto Json
      InfoRespuesta  := '';
      InfoRespuesta:=RESTResponseID.JSONText;
      InfoJSONValue:=RESTResponseID.JSONValue;
      InfoJSON := TJSONObject.ParseJSONValue(InfoRespuesta) as TJSONObject;
      //se toman los valores del Json
      Try
        InfoJSONArray:=   InfoJSON.Get(0).JsonValue as TJSONArray;
        RespuestaJson:=InfoJsonArray.Get(0) as TJSONOBject;
        //get the pair to evaluate in this case the index is 1
        IDserial  := RespuestaJson.Get(0).JsonValue.Value;
        InfoError  := StrToInt(RespuestaJson.Get(1).JsonValue.Value);
        InfoRespuesta  := RespuestaJson.Get(2).JsonValue.Value;
      finally
        InfoJSON.Free;
      end;
    End;

    //--------------------------------------------------------------------------
    //Verificamos si la licencia se pudo activar
    //--------------------------------------------------------------------------
     if (InfoError <> 0) then
     Begin
      MensajeError:='No se pudo activar la licencia ' + InfoRespuesta ;
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (MensajeError) + ' ',1,0) ;
      Valida := false;
     End
     Else
     Begin
      Valida:=RegistrarLicencia(edtIDLicencia,edtIDPC,edtIDSerial,NameIniLicencia);
     End;
 except
    On E:Exception do
    begin
      //Log De error
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (E.Message) + ' ',1,0) ;
      Valida := false;
      Exit;
    end;
  end;

Result:=Valida;
end;


//------------------------------------------------------------------------------
//Registrar la licencia
//------------------------------------------------------------------------------
function RegistrarLicencia(edtIDLicencia,edtIDPC,edtIDSerial,NameIniLicencia:string):Boolean;
Var
  ValorLicencia:string;
  Registrada:Boolean;
begin

  Try
    Registrada:=False;
    //Se verifica que la licencia tenga la longitud correcta
    if Length(edtIDLicencia) <10 Then
    Begin
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (PWMsg01) + ' ',1,0) ;
      Result:=Registrada;
      exit;
    End;

    //se Verifica si se introdujo el C�digo de activaci�n
    if Length(edtIDSerial)=0 Then
    Begin
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (PWMsg02) + ' ',1,0) ;
      Result:=Registrada;
      Exit
    End;

    //--------------------------------------------------------------------------
    //Se verifica si la licencia introducida es correcta
    //--------------------------------------------------------------------------
    //se obtiene el nombre el Id Real del equipo
    IdMachineReal:=ICGDO_IDMachineReal(Now,edtIDPC);
    ValorLicencia := GetSerialNumber(edtIDLicencia, edtIDSerial) ;

    //Se verifica si el calculo de la licencia coincide con el valor registrado
    if  edtIDLicencia = ValorLicencia  Then
    begin
      if (AuthByMachine(edtIDLicencia, edtIDSerial)) Then
      begin
        if (AuthByDate(edtIDLicencia, edtIDSerial) >= Date) then
        begin
          IniLicencia := TIniFile.Create(TheDir + NameFileLicencia);
          try
            {write SN to IniFile }
            IniLicencia.WriteInteger('Codes', 'SN', StrToInt(edtIDLicencia));
            IniLicencia.WriteString('Codes', 'SNCode', edtIDSerial);
            IniLicencia.WriteString('Codes', 'ID', edtIDPC);
            IniLicencia.WriteString('Codes', 'Fecha', DateToStr(IncDay(Date, -1)));
            WriteLogApps(3, 'Log ====> : ' + DateTimeToStr(now)  + '  , ' + (PWMsg03) + ' ',1,1) ;
            Registrada:=True;
          finally
            IniLicencia.Free;
          end;
          Registrada:=True;
          result:=Registrada;
          Exit
        end
        else
          WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (PWMsg04) + ' ',1,0) ;
        end
      else
        WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (PWMsg05) + ' ',1,0) ;
      end
    else
    Begin
      //Se Verifica el ID del Equipo
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (PWMsg06) + ' ',1,0) ;
    End;
    Registrada:=True;
   except
    On E:Exception do
    begin
      //Log De error
      WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + (E.Message) + ' ',1,0) ;
      Registrada:=False;
      result:=Registrada;
      Exit;
    end;
  end;
  result:=Registrada;
end;

end.
