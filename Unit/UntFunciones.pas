unit UntFunciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, DBCtrls, Grids, DBGrids, StdCtrls, Consts, Math, AdvGlowButton,
  ImgList, ToolWin, Menus, Db, ADODB, ExtActns, {Networks,} ShellAPI, ShlObj,
  Registry, DCPCast256, DCPcrypt2, DCPblockciphers, cxDBData, DCPblowfish, ppTypes, {AscrLib,}
  Printers, WinSpool, cxPC, cxControls, cxCheckBox, Types, dxDockPanel, dxDockControl,
  dxNavBar, dxNavBarCollns, dxBarExtItems, cxLookAndFeels, ActnList, cxGraphics,
  DCPmd5, AVLocks6{, uRegistrationForm};

const
  TESPSDataType : array[TppDataType] of string = ('dtBoolean', 'dtDate', 'dtTime', 'dtDateTime', 'dtInteger', 'dtSingle',
                 'dtDouble', 'dtExtended', 'dtCurrency', 'dtChar', 'dtString', 'dtVariant',
                 'dtLongint', 'dtBLOB', 'dtMemo', 'dtGraphic', 'dtNotKnown', 'dtLargeInt', 'dtGUID');

  TDBFieldType : array[TFieldType] of string = ('ftUnknown', 'ftString', 'ftSmallint', 'ftInteger', 'ftWord',
                'ftBoolean', 'ftFloat', 'ftCurrency', 'ftBCD', 'ftDate', 'ftTime', 'ftDateTime',
                'ftBytes', 'ftVarBytes', 'ftAutoInc', 'ftBlob', 'ftMemo', 'ftGraphic', 'ftFmtMemo',
                'ftParadoxOle', 'ftDBaseOle', 'ftTypedBinary', 'ftCursor', 'ftFixedChar', 'ftWideString',
                'ftLargeint', 'ftADT', 'ftArray', 'ftReference', 'ftDataSet', 'ftOraBlob', 'ftOraClob',
                'ftVariant', 'ftInterface', 'ftIDispatch', 'ftGuid', 'ftTimeStamp', 'ftFMTBcd', 'ftFixedWideChar', 'ftWideMemo', 'ftOraTimeStamp', 'ftOraInterval',
                'ftLongWord', 'ftShortint', 'ftByte', 'ftExtended', 'ftConnection', 'ftParams', 'ftStream', //42..48
    'ftTimeStampOffset', 'ftObject', 'ftSingle');

  eSDashBoardKey = '\SOFTWARE\eSoft Technologies\eSoft DashBoard';
  eSDashBoardConn = '\SOFTWARE\eSoft Technologies\eSoft DashBoard\Connection';
  eSPrintingSKey = '\SOFTWARE\eSoft Technologies\eSoft Price Printing Suite';
  eSPrintingSKeyConn = '\SOFTWARE\eSoft Technologies\eSoft Price Printing Suite\Connection';
  eSPrintingSKeyOptions = '\SOFTWARE\eSoft Technologies\eSoft Price Printing Suite\Options';

  eSPInquiryKey = '\SOFTWARE\eSoft Technologies\eSoft Price Inquiry\Connection';

  eSPOSAdminKey = '\SOFTWARE\eSoft Technologies\eSoft POS Administrator\Connection';
  eSPOSRegKey   = '\SOFTWARE\eSoft Technologies\eSoft POS Register\Connection';

  eSPMobileKeyOptions = '\SOFTWARE\eSoft Technologies\eSoft Mobile\Options';
  eSPMobileKey  = '\SOFTWARE\eSoft Technologies\eSoft Mobile\Connection';
  eSPMobileDestKey  = '\SOFTWARE\eSoft Technologies\eSoft Mobile\DestConnection';

  eSPReplication   = '\SOFTWARE\eSoft Technologies\eSoft Replication';
  eSRepConnection  = '\SOFTWARE\eSoft Technologies\eSoft Replication\Connection';
  eSRepTarget      = '\SOFTWARE\eSoft Technologies\eSoft Replication\Target';
  eSRepOptions     = '\SOFTWARE\eSoft Technologies\eSoft Replication\Options';
  eSoftTechRootKey = '\SOFTWARE\eSoft Technologies';

  eSoftSZCfgConnection = '\SOFTWARE\eSoft Technologies\Configuration\Connection';
  eSoftSZConnection = '\SOFTWARE\eSoft Technologies\Connection';
  eSPayrollDDConnection = '\SOFTWARE\eSoft Technologies\Microsoft Dynamics Payroll Direct Deposits\Connection';

  eSoftGPFAInvConnections = '\SOFTWARE\eSoft Technologies\Great Plains - Fixed Assets Inventory\Configuration\Connections';
  //InfoSolutions Constants
  ISPOSConnection = '\SOFTWARE\InfoSolutions\Connection';

  MSSQLSrvr     = '\SOFTWARE\Microsoft\MSSQLSERVER';
  MSSQLSrvrPath = '\SOFTWARE\Microsoft\MSSQLSERVER\Setup';

  Prompt = True;
  DonotPrompt = False;

type
//eSoft Replication Client/Server Types
  TSyncType = (stFull, stPartial);
  TLogType = (ltLog, ltLogDetail);
  TTaskType = (ttLogin, ttQuery, ttDownload, ttLogout);
  TUpdateOption = (uoUpdate, uoCancel);
  TUserStatus = (usActive, usInactive, usOnline, usOffline);
//eSoft Printing Suite Types
  TReportDeviceType = (rdtScreen, rdtPrinter, rdtOther);
  TeSoftRepOptions = (roConnection, roOptions, roSource, roTarget, roAll);
  TImportFromSourceType = (ifstADO, ifstABS, ifstDB);
  TVPNAction = (vaConnect, vaDisconnect);
  TExitType = (etLogOff, etShutDown, etRestart);
  TWindowHook = function(var Message : TMessage) : Boolean of object;

type
  TDataReplication = class(TObject)
  public
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;
//    procedure ExportFromDataSet(FromDataSet : TDataSet; FieldsList : TStringList; ToFile : String);
end;

type
  TAppInfo = record
  AppID : Word;
  AppName : string;
  AppVersion : string;
  UserName : string;
  CompanyName : string;
  Email : string;
  EncryptionKey : WideString;
  EncryptionKey2 : WideString;
  end;

  TAppSecurity=class(TObject)
  private
    FAVLock : TAVLockS6;
  public
//    KeyData : TKeyData;
    Status : TRegStatus;
    function GetAppLicenseStatus(AppInfo : TAppInfo) : TKeyData;
    function InstallCode : string;
    procedure RegisterApp;
  published
    constructor Create(AOwner:TComponent);
    destructor Destroy;

  end;

var
//  InkCollector : TInkCollector;
  FSideDockingSite : TdxSideContainerDockSite;
  FMainDockSite : TdxDockSite;
  FDockPanel : TdxDockPanel;
  FNavBar : TdxNavBar;
  FListView : TListView;
  FNavBarGroup : TdxNavBarGroup;
  FPageControl : TcxPageControl;
  FNavBarItem : TdxNavBarItem;
  FTabSheet : TcxTabSheet;

function GetDatabaseName(Connection: TADOConnection): string;
function GetDefaultConnection(Key : String; SetValueInRegistry : Boolean = False; AEntry : string = ''; APrompt : Boolean = DonotPrompt): WideString;
procedure SetDefaultConnection(Key : WideString; Value : String = ''; AKeyEntry : WideString = '');
procedure FormClose(Sender: TObject; var Action: TCloseAction);
procedure KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
function ConnectDataBase(Database : TCustomConnection) : Boolean;
procedure ConnectDataSet(DataSet : TDataSet; const AReopen : Boolean = False);
procedure ConnectDataSets(DataSets: Array of TDataSet);
procedure AddListViewItem(ListView : TListView; ItemName : String; ImageIndex : Integer; const SubItems : TStringList);
function ListSQLServers : TStrings;
function ListSQLDatabases : TStringList;
function GetRegKeyValue(KeyEntry : String) : Variant;

procedure FetchProgress(DataSet: TCustomADODataSet; Progress, MaxProgress: Integer; var EventStatus: TEventStatus);
function JpegStartsInBlob (PicField:TBlobField):integer;
procedure SaveJpegToTable(Table: TCustomADODataSet; PicField:TBlobField; sPicPath: string);
procedure CreateInkInstance(Handle : Integer);
procedure FreeInkInstance;
//procedure ExportFromDataSet(FromDataSet : TDataSet; FieldsList : TStringList; ToFile : String);
procedure ExportDataFromTable(TableName : String; const SystemTable : Boolean = False);
function Percentage(Amount : Double; TotalAmount : Double) : Integer;
procedure GetFieldsList(DataSet : TDataSet; List : TStrings);
function EditConnectionString(Value : string) : string;
function ProcessError(TxtMessage : string) : Integer;
procedure CloseAllDataSets(Connection : TADOConnection);
function CreateForm(Sender : TForm) : Boolean;
function SecondsIdle: DWord;
function GetDefaultPrinter : Integer;
procedure GetSystemPrinters(AList: TStrings);
procedure ChangeDefaultPrinter(const Name: string);
function GetCurrentUserName : string;
function ComputerName : string;
procedure PrintFile(const sFileName : string);
procedure QuotedStringList(ASourceList : TStrings; ATargetList : TStrings);
procedure ExitWindows(ExitType : TExitType);
function CreateMsgDialog(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; ShowCheckBox : Boolean = False; CheckBoxCaption : TCaption = ''): TForm;
function FilledString(AString : string; ALength : Integer; AChar : Char) : string;
//procedure HookMainWindow(Hook : TWindowHook);
function AppHook(var msg: TMessage): boolean;
procedure CreateSideDockSite(APartner : TdxDockPanel);
procedure CreateDockPanel(AOwner : TCustomForm;  AName : string; var APanel : TdxDockPanel; AParent : TdxDockSite; AAlign : TdxDockingType; const AShowCaption : Boolean = True; ACaption : string = ''; AAutohide : Boolean = False);
procedure CreateNavBar(AOwner : TWinControl; AParent : TWinControl; AName : string; const AImages : TcxImageList = nil);
//procedure CreateNavBarGroup(AName : string; AParent : TdxNavBar; ACaption : string);
procedure CreateNavBarGroup(AOwner : TdxNavBar; AName : string; ACaption : string = '');
procedure CreatePageControl(AOwner : TCustomForm; AParent : TWinControl; AName : string);
procedure LoadSystemPrinters(AImageComboBox : TdxBarImageCombo; AImageIndex : Integer);
procedure CreateNavBarItem(AOwner : TdxNavBar; AGroup : TdxNavBarGroup; AAction : TAction);
Procedure HideStartButton(AHide : Boolean);
procedure HideWindowsTasBar(AHide : Boolean);
function GetNodeByText(ATree : TdxBarTreeView; AValue:String; AVisible: Boolean): TTreeNode;
{function ShowMessageDlg(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons) : Integer;;}

type
  TDockedForm = class(TComponent)
  private
//    AFrame : TCustomForm;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
//    procedure CreateFrame(Form : TFormClass; AOwner : TdxDockPanel);
//    procedure CreateTabbedForm(AOwner : TCustomForm; Form : TFormClass; DockSite : TdxDockSite; const AllowClose : Boolean = False; AllowHide : Boolean = False);
//    procedure OnDockPanelClose(Sender : TdxCustomDockControl);
  end;

type
  TLoginForm = class(TComponent)
  private

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    class function Execute(var AForm : TForm) : Boolean;
  end;

type
  TISCryptMD5 = class(TComponent)
  private
    FMD5Object : TDCP_md5;
  protected

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    function Encrypt(AValue : WideString) : WideString;
    function Decrypt(AValue : WideString) : WideString;
  published

  end;

//procedure AddListViewItem(ListView : TListView; ItemName : String; ImageIndex : Integer);

//Find Computers Functions & Procedures;

function GetShellFolder(CompName: String): IShellFolder;
function GetComputerNames : TStringList;
function GetTaskBarSize: TRect;

implementation

//{$R MessageBoxResources.res}

function GetTaskBarSize: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Result, 0);
end;

function GetNodeByText(ATree : TdxBarTreeView; AValue:String; AVisible: Boolean): TTreeNode;
var
    Node: TTreeNode;
begin
  Result := nil;
  if ATree.Items.Count = 0 then Exit;
  Node := ATree.Items[0];
  while Node <> nil do
  begin
    if UpperCase(Node.Text) = UpperCase(AValue) then
    begin
      Result := Node;
      if AVisible then
        Result.MakeVisible;
      Break;
    end;
    Node := Node.GetNext;
  end;
end;

constructor TISCryptMD5.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMD5Object := TDCP_md5.Create(AOwner);
  with FMD5Object do
  begin
    Algorithm := 'MD5';
    HashSize := 128;
    Id := 16;
  end;
end;

destructor TISCryptMD5.Destroy;
begin
  inherited Destroy;
  FreeAndNil(FMD5Object);
end;

function TISCryptMD5.Encrypt(AValue : WideString) : WideString;
begin
  Result := '';
end;

function TISCryptMD5.Decrypt(AValue : WideString) : WideString;
begin
  Result := '';
end;    

procedure HideWindowsTasBar(AHide : Boolean);
begin
  case AHide of
  True : ShowWindow(FindWindow('Shell_TrayWnd',nil), SW_HIDE);
  False: ShowWindow(FindWindow('Shell_TrayWnd',nil), SW_SHOWNA);
  end;
end;  

Procedure HideStartButton(AHide : Boolean);
Var
  taskbarhandle,
  buttonhandle : HWND;
begin
  taskbarhandle := FindWindow('Shell_TrayWnd', nil);
  buttonhandle := GetWindow(taskbarhandle, GW_CHILD);
  case AHide of
  True : ShowWindow(buttonhandle, SW_HIDE);
  False: ShowWindow(buttonhandle, SW_RESTORE);
  end;
end;

procedure CreateNavBarItem(AOwner : TdxNavBar; AGroup : TdxNavBarGroup; AAction : TAction);
begin
  FNavBarItem := AOwner.Items.Add;
  with FNavBarItem do
  begin
    Action := AAction;
  end;
  AGroup.CreateLink(FNavBarItem);
end;  

procedure CreatePageControl(AOwner : TCustomForm; AParent : TWinControl; AName : string);
begin
  FPageControl := TcxPageControl.Create(AOwner);
  with FPageControl do
  begin
    Name := AName;
    Align := alClient;
    Parent := AParent;
    LookAndFeel.Kind := lfOffice11;             
  end;
end;  

procedure CreateNavBarGroup(AOwner : TdxNavBar; AName : string; ACaption : string = '');
begin
//  FNavBarGroup := TdxNavBarGroup.Create(AOwner);
  FNavBarGroup := AOwner.Groups.Add;
  with FNavBarGroup do
  begin
    Name := AName;
    Caption := ACaption;
    Expanded := False;
    LinksUseSmallImages := True;
  end;
end;

{procedure CreateNavBarGroup(AName : string; AParent : TdxNavBar; ACaption : string);
begin
  try
    with AParent.Groups.Add do
    begin
      Name := AName;
      Caption := ACaption;
    end;
  except
    raise Exception.Create('Could not find a NavBar component');
  end;

end;}

constructor TLoginForm.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
end;

destructor TLoginForm.Destroy;
begin
  inherited Destroy;
end;

class function TLoginForm.Execute(var AForm : TForm) : Boolean;
begin
  with AForm.Create(nil) do
  begin
    try
      Result := ShowModal = mrOk;
    finally
      Release;
    end;
  end;  
end;  

procedure LoadSystemPrinters(AImageComboBox : TdxBarImageCombo; AImageIndex : Integer);
Var Item : Integer;
begin
  try
    GetSystemPrinters(AImageComboBox.Items);
    for Item := 0 to AImageComboBox.Items.Count - 1 do
    begin
      AImageComboBox.ImageIndexes[Item] := AImageIndex;
    end;
    AImageComboBox.ItemIndex := GetDefaultPrinter;
  except
    on E: Exception do
      raise Exception.Create('Could not load System Printers.');
  end;
end;

procedure CreateNavBar(AOwner : TWinControl; AParent : TWinControl; AName : string; const AImages : TcxImageList = nil);
begin
  FNavBar := TdxNavBar.Create(AOwner);
  with FNavBar do
  begin
    Name := AName;
    Align := alClient;
    Parent := AParent;
    View := 11;
    SmallImages := AImages;
{    CreateNavBarGroup(FNavBar, AName + 'TransGroup', 'Transactions');
    CreateNavBarGroup(FNavBar, AName + 'JourGroup', 'Journals');
    CreateNavBarGroup(FNavBar, AName + 'InqGroup', 'Inquiries');}
    {with Groups.Add do
    begin
      Caption := 'Test Group';
      UseControl := True;
//      AutoSize := True;
      FListView := TListView.Create(Application);
      FListView.Align := alClient;
      FListView.Parent := Control;
      Control.Height := FNavBar.ClientHeight div 2;
      ShowControl := True;
//      InsertControl(FListView);
    end;}
    Visible := True;
  end;
end;

procedure CreateSideDockSite(APartner : TdxDockPanel);
begin
  FSideDockingSite := APartner.SideContainer;
  if FSideDockingSite <> nil then
  begin
    with FSideDockingSite do
    begin
      Name := 'SideDockingSite';
      DockTo(FMainDockSite, dtClient, 0);
    end;
  end;
end;

procedure CreateDockPanel(AOwner : TCustomForm; AName : string; var APanel : TdxDockPanel; AParent : TdxDockSite; AAlign : TdxDockingType; const AShowCaption : Boolean = True; ACaption : string = ''; AAutohide : Boolean = False);
var PanelIndex : Integer;
begin
  Inc(PanelIndex);
  APanel := TdxDockPanel.Create(AOwner);
  with APanel do
  begin
//    Name := Format('DockPanel%s', [IntToStr(PanelIndex)]);
    Name := AName;

    CaptionButtons := [cbHide];
    Caption := ACaption;
    ShowCaption := AShowCaption;
    {if APanel = FDockPanel then}
      DockTo(AParent, AAlign, 0);
    {else
      DockTo(AParent, AAlign, 1);}
    Visible := True;
    AutoHide := AAutohide;
  end;
end;

function AppHook(var msg: TMessage): boolean;
begin
  result := false;
  if (msg.Msg = WM_SYSCOMMAND) and (msg.WParam = SC_RESTORE) then
    result := MessageDlg('Are you an Admin?', mtWarning, mbYesNoCancel, 0) = mrNo;
end;

function FilledString(AString : string; ALength : Integer; AChar : Char) : string;
begin
  result := Copy(Trim(AString), 1, ALength) + StringOfChar(AChar, ALength - Length(Trim(AString)));
end;
  
procedure ExitWindows(ExitType : TExitType);
begin
  case ExitType of
  etLogOff : ExitWindowsEx(EWX_LOGOFF, 0);
  etShutDown : ExitWindowsEx(EWX_SHUTDOWN, 0);
  etRestart : ExitWindowsEx(EWX_REBOOT, 0);
  end;
end;  

procedure QuotedStringList(ASourceList : TStrings; ATargetList : TStrings);
var AListIndex : Integer;
begin
  ATargetList.Clear;
  For AListIndex := 0 to ASourceList.Count - 1 do
    ATargetList.Add(QuotedStr(ASourceList[AListIndex]));
end;  

procedure PrintFile(const sFileName: string);
const 
  iBufferSize = 32768; 
var 
  Count, BytesWritten: Cardinal; 
  hPrinter, hDeviceMode: THandle; 
  sDevice : array[0..255] of char; 
  sDriver : array[0..255] of char; 
  sPort   : array[0..255] of char; 
  DocInfo: TDocInfo1A; 
  f: File; 
  pBuffer: Pointer; 
begin
//  Printer.PrinterIndex := ComboBox1.ItemIndex;
  Printer.GetPrinter(sDevice, sDriver, sPort, hDeviceMode); 

  if not WinSpool.OpenPrinter(@sDevice, hPrinter, nil) then 
     exit; 

  DocInfo.pDocName := PAnsiChar(ExtractFileName(sFileName));
  DocInfo.pDatatype := 'RAW'; 
  DocInfo.pOutputFile := nil; 
   
  if StartDocPrinter(hPrinter, 1, @DocInfo) = 0 then 
  begin 
    WinSpool.ClosePrinter(hPrinter); 
    exit; 
  end; 

  if not StartPagePrinter(hPrinter) then 
  begin 
    EndDocPrinter(hPrinter); 
    WinSpool.ClosePrinter(hPrinter); 
    exit; 
  end; 

  System.Assign(f, sFileName); 
  try 
    Reset(f, 1); 
    GetMem(pBuffer, iBufferSize); 
    while not eof(f) do 
    begin 
      Blockread(f, pBuffer^, iBufferSize, Count); 
      if Count > 0 then 
      begin 
        if not WritePrinter(hPrinter, pBuffer, Count, BytesWritten) then 
        begin 
          EndPagePrinter(hPrinter); 
          EndDocPrinter(hPrinter); 
          WinSpool.ClosePrinter(hPrinter); 
          FreeMem(pBuffer, iBufferSize); 
          exit; 
        end; 
      end; 
    end; 
    FreeMem(pBuffer, iBufferSize); 
    EndDocPrinter(hPrinter); 
    WinSpool.ClosePrinter(hPrinter); 
  finally 
    System.Closefile(f); 
  end; 
end;

function ComputerName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

function GetCurrentUserName : string;
const
  cnMaxUserNameLen = 254;
var
  sUserName     : string;
  dwUserNameLen : DWord;
begin
  dwUserNameLen := cnMaxUserNameLen-1;
  SetLength( sUserName, cnMaxUserNameLen );
  GetUserName(
    PChar( sUserName ),
    dwUserNameLen );
  SetLength( sUserName, dwUserNameLen );
  Result := sUserName;
end;

{function CreateMessageDialog(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): TForm;
const
  mcHorzMargin = 8;
  mcVertMargin = 8;
  mcHorzSpacing = 10;
  mcVertSpacing = 10;
  mcButtonWidth = 50;
  mcButtonHeight = 14;
  mcButtonSpacing = 4;
var
  DialogUnits: TPoint;
  HorzMargin, VertMargin, HorzSpacing, VertSpacing, ButtonWidth,
  ButtonHeight, ButtonSpacing, ButtonCount, ButtonGroupWidth,
  IconTextWidth, IconTextHeight, X, ALeft: Integer;
  B, DefaultButton, CancelButton: TMsgDlgBtn;
  IconID: PChar;
  TextRect: TRect;
begin
  Result := TMessageForm.CreateNew(Application);
  with Result do
  begin
    BiDiMode := Application.BiDiMode;
    BorderStyle := bsDialog;
    Canvas.Font := Font;
    KeyPreview := True;
    OnKeyDown := TMessageForm(Result).CustomKeyDown;
    DialogUnits := GetAveCharSize(Canvas);
    HorzMargin := MulDiv(mcHorzMargin, DialogUnits.X, 4);
    VertMargin := MulDiv(mcVertMargin, DialogUnits.Y, 8);
    HorzSpacing := MulDiv(mcHorzSpacing, DialogUnits.X, 4);
    VertSpacing := MulDiv(mcVertSpacing, DialogUnits.Y, 8);
    ButtonWidth := MulDiv(mcButtonWidth, DialogUnits.X, 4);
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
    begin
      if B in Buttons then
      begin
        if ButtonWidths[B] = 0 then
        begin
          TextRect := Rect(0,0,0,0);
          Windows.DrawText( canvas.handle,
            PChar(LoadResString(ButtonCaptions[B])), -1,
            TextRect, DT_CALCRECT or DT_LEFT or DT_SINGLELINE or
            DrawTextBiDiModeFlagsReadingOnly);
          with TextRect do ButtonWidths[B] := Right - Left + 8;
        end;
        if ButtonWidths[B] > ButtonWidth then
          ButtonWidth := ButtonWidths[B];
      end;
    end;
    ButtonHeight := MulDiv(mcButtonHeight, DialogUnits.Y, 8);
    ButtonSpacing := MulDiv(mcButtonSpacing, DialogUnits.X, 4);
    SetRect(TextRect, 0, 0, Screen.Width div 2, 0);
    DrawText(Canvas.Handle, PChar(Msg), Length(Msg)+1, TextRect,
      DT_EXPANDTABS or DT_CALCRECT or DT_WORDBREAK or
      DrawTextBiDiModeFlagsReadingOnly);
    IconID := IconIDs[DlgType];
    IconTextWidth := TextRect.Right;
    IconTextHeight := TextRect.Bottom;
    if IconID <> nil then
    begin
      Inc(IconTextWidth, 32 + HorzSpacing);
      if IconTextHeight < 32 then IconTextHeight := 32;
    end;
    ButtonCount := 0;
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
      if B in Buttons then Inc(ButtonCount);
    ButtonGroupWidth := 0;
    if ButtonCount <> 0 then
      ButtonGroupWidth := ButtonWidth * ButtonCount +
        ButtonSpacing * (ButtonCount - 1);
    ClientWidth := Max(IconTextWidth, ButtonGroupWidth) + HorzMargin * 2;
    ClientHeight := IconTextHeight + ButtonHeight + VertSpacing +
      VertMargin * 2;
    Left := (Screen.Width div 2) - (Width div 2);
    Top := (Screen.Height div 2) - (Height div 2);
    if DlgType <> mtCustom then
      Caption := LoadResString(Captions[DlgType]) else
      Caption := Application.Title;
    if IconID <> nil then
      with TImage.Create(Result) do
      begin
        Name := 'Image';
        Parent := Result;
        Picture.Icon.Handle := LoadIcon(0, IconID);
        SetBounds(HorzMargin, VertMargin, 32, 32);
      end;
    TMessageForm(Result).Message := TLabel.Create(Result);
    with TMessageForm(Result).Message do
    begin
      Name := 'Message';
      Parent := Result;
      WordWrap := True;
      Caption := Msg;
      BoundsRect := TextRect;
      BiDiMode := Result.BiDiMode;
      ALeft := IconTextWidth - TextRect.Right + HorzMargin;
      if UseRightToLeftAlignment then
        ALeft := Result.ClientWidth - ALeft - Width;
      SetBounds(ALeft, VertMargin,
        TextRect.Right, TextRect.Bottom);
    end;
    if mbOk in Buttons then DefaultButton := mbOk else
      if mbYes in Buttons then DefaultButton := mbYes else
        DefaultButton := mbRetry;
    if mbCancel in Buttons then CancelButton := mbCancel else
      if mbNo in Buttons then CancelButton := mbNo else
        CancelButton := mbOk;
    X := (ClientWidth - ButtonGroupWidth) div 2;
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
      if B in Buttons then
        with TButton.Create(Result) do
        begin
          Name := ButtonNames[B];
          Parent := Result;
          Caption := LoadResString(ButtonCaptions[B]);
          ModalResult := ModalResults[B];
          if B = DefaultButton then Default := True;
          if B = CancelButton then Cancel := True;
          SetBounds(X, IconTextHeight + VertMargin + VertSpacing,
            ButtonWidth, ButtonHeight);
          Inc(X, ButtonWidth + ButtonSpacing);
          if B = mbHelp then
            OnClick := TMessageForm(Result).HelpButtonClick;
        end;
  end;
end;}

procedure ChangeDefaultPrinter(const Name: string) ;
var
    W2KSDP: function(pszPrinter: PChar): Boolean; stdcall;
    H: THandle;
    Size, Dummy: Cardinal;
    PI: PPrinterInfo2;
begin
    if (Win32Platform = VER_PLATFORM_WIN32_NT) and (Win32MajorVersion >= 5) then
    begin
      @W2KSDP := GetProcAddress(GetModuleHandle(winspl), 'SetDefaultPrinterA') ;
      if @W2KSDP = nil then RaiseLastOSError;
      if not W2KSDP(PChar(Name)) then RaiseLastOSError;
    end
    else
    begin
      if not OpenPrinter(PChar(Name), H, nil) then RaiseLastOSError;
      try
        GetPrinter(H, 2, nil, 0, @Size) ;
        if GetLastError <> ERROR_INSUFFICIENT_BUFFER then RaiseLastOSError;
        GetMem(PI, Size) ;
        try
          if not GetPrinter(H, 2, PI, Size, @Dummy) then RaiseLastOSError;
          PI^.Attributes := PI^.Attributes or PRINTER_ATTRIBUTE_DEFAULT;
          if not SetPrinter(H, 2, PI, PRINTER_CONTROL_SET_STATUS) then RaiseLastOSError;
        finally
          FreeMem(PI) ;
        end;
      finally
        ClosePrinter(H) ;
      end;
    end;
end;

function GetDefaultPrinter : Integer;
begin
  Result := Printer.PrinterIndex;
end;
  
procedure GetSystemPrinters(AList: TStrings);
begin
  AList.Clear;
  AList.Assign(Printer.Printers);
end;  

function SecondsIdle: DWord;
var
   liInfo: TLastInputInfo;
begin
   liInfo.cbSize := SizeOf(TLastInputInfo) ;
   GetLastInputInfo(liInfo) ;
   Result := (GetTickCount - liInfo.dwTime) DIV 1000;
end;

function CreateForm(Sender : TForm) : Boolean;
begin
  If Sender.FormStyle = fsMDIChild then
    Sender := TForm.Create(Application);
end;  

constructor TDockedForm.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
end;

destructor TDockedForm.Destroy;
begin
  inherited;
end;

{procedure TDockedForm.OnDockPanelClose(Sender: TdxCustomDockControl);
begin
//  FreeAndNil(Sender);
end;
}
{
procedure TDockedForm.CreateTabbedForm(AOwner : TCustomForm; Form : TFormClass; DockSite : TdxDockSite; const AllowClose : Boolean = False; AllowHide : Boolean = False);
var DockPanel : TdxDockPanel;
//    TabSheet  : Tcx
begin
  DockPanel := TdxDockPanel.Create(AOwner);
  DockPanel.DockTo(DockSite, dtClient, 0);
  DockPanel.CaptionButtons := [];
//  DockPanel.OnClose := OnDockPanelClose;

  CreateFrame(Form, DockPanel);
{  If AFrame <> nil then
    DockPanel.Caption := AFrame.Caption;
end;  

procedure TDockedForm.CreateFrame(Form : TFormClass; AOwner : TdxDockPanel);
var AFrame : TCustomForm;
begin
  LockWindowUpdate(AFrame.Handle);
  AFrame := Form.Create(AOwner);
  AFrame.Parent := AOwner;
  AFrame.Align := alClient;
  AFrame.BorderStyle := bsNone;
  LockWindowUpdate(0);
end;
}

function GetAveCharSize(Canvas: TCanvas): TPoint;
var
  I: Integer;
  Buffer: array[0..51] of Char;
begin
  for I := 0 to 25 do Buffer[I] := Chr(I + Ord('A'));
  for I := 0 to 25 do Buffer[I + 26] := Chr(I + Ord('a'));
  GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(Result));
  Result.X := Result.X div 52;
end;

type
  TMessageForm = class(TForm)
  private
    Message: TLabel;
    procedure HelpButtonClick(Sender: TObject);
  protected
    procedure CustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure WriteToClipBoard(Text: String);
    function GetFormText: String;
  public
    constructor CreateNew(AOwner: TComponent); reintroduce;
  end;

constructor TMessageForm.CreateNew(AOwner: TComponent);
var
  NonClientMetrics: TNonClientMetrics;
begin
  inherited CreateNew(AOwner);
  NonClientMetrics.cbSize := sizeof(NonClientMetrics);
  if SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, @NonClientMetrics, 0) then
    Font.Handle := CreateFontIndirect(NonClientMetrics.lfMessageFont);
end;

procedure TMessageForm.HelpButtonClick(Sender: TObject);
begin
  Application.HelpContext(HelpContext);
end;

procedure TMessageForm.CustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and (Key = Word('C')) then
  begin
    Beep;
    WriteToClipBoard(GetFormText);
  end;
end;

procedure TMessageForm.WriteToClipBoard(Text: String);
var
  Data: THandle;
  DataPtr: Pointer;
begin
  if OpenClipBoard(0) then
  begin
    try
      Data := GlobalAlloc(GMEM_MOVEABLE+GMEM_DDESHARE, Length(Text) + 1);
      try
        DataPtr := GlobalLock(Data);
        try
          Move(PChar(Text)^, DataPtr^, Length(Text) + 1);
          EmptyClipBoard;
          SetClipboardData(CF_TEXT, Data);
        finally
          GlobalUnlock(Data);
        end;
      except
        GlobalFree(Data);
        raise;
      end;
    finally
      CloseClipBoard;
    end;
  end
  else
    raise Exception.CreateRes(@SCannotOpenClipboard);
end;

function TMessageForm.GetFormText: String;
var
  DividerLine, ButtonCaptions: string;
  I: integer;
begin
  DividerLine := StringOfChar('-', 27) + sLineBreak;
  for I := 0 to ComponentCount - 1 do
    if Components[I] is TButton then
      ButtonCaptions := ButtonCaptions + TButton(Components[I]).Caption +
        StringOfChar(' ', 3);
  ButtonCaptions := StringReplace(ButtonCaptions,'&','', [rfReplaceAll]);
  Result := Format('%s%s%s%s%s%s%s%s%s%s', [DividerLine, Caption, sLineBreak,
    DividerLine, Message.Caption, sLineBreak, DividerLine, ButtonCaptions,
    sLineBreak, DividerLine]);
end;

var
  Captions: array[TMsgDlgType] of Pointer = (@SMsgDlgWarning, @SMsgDlgError,
    @SMsgDlgInformation, @SMsgDlgConfirm, nil);
  IconIDs: array[TMsgDlgType] of PChar = (IDI_EXCLAMATION, IDI_HAND,
    IDI_ASTERISK, IDI_QUESTION, nil);
{  IconResIDs : array[TMsgDlgType] of PChar = (EXCLAMATION_1, ERROR_2,
    INFO_1, CONFIRMATION_1, nil);}
  ButtonNames: array[TMsgDlgBtn] of string = (
    'mbYes', 'mbNo', 'mbOK', 'mbCancel', 'mbAbort', 'mbRetry', 'mbIgnore',
    'mbAll', 'mbNoToAll', 'mbYesToAll', 'mbHelp', 'mbClose');
  ButtonCaptions: array[TMsgDlgBtn] of Pointer = (
    @SMsgDlgYes, @SMsgDlgNo, @SMsgDlgOK, @SMsgDlgCancel, @SMsgDlgAbort,
    @SMsgDlgRetry, @SMsgDlgIgnore, @SMsgDlgAll, @SMsgDlgNoToAll, @SMsgDlgYesToAll,
    @SMsgDlgHelp, @SMsgDlgClose);
  ModalResults: array[TMsgDlgBtn] of Integer = (
    mrYes, mrNo, mrOk, mrCancel, mrAbort, mrRetry, mrIgnore, mrAll, mrNoToAll,
    mrYesToAll, mrClose, 0);
var
  ButtonWidths : array[TMsgDlgBtn] of integer;  // initialized to zero
  
function CreateMsgDialog(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; ShowCheckBox : Boolean = False; CheckBoxCaption : TCaption = ''): TForm;
const
  mcHorzMargin = 8;
  mcVertMargin = 8;
  mcHorzSpacing = 10;
  mcVertSpacing = 10;
  mcButtonWidth = 50;
  mcButtonHeight = 14;
  mcButtonSpacing = 4;
var
  DialogUnits: TPoint;
  HorzMargin, VertMargin, HorzSpacing, VertSpacing, ButtonWidth,
  ButtonHeight, ButtonSpacing, ButtonCount, ButtonGroupWidth,
  IconTextWidth, IconTextHeight, X, ALeft: Integer;
  B, DefaultButton, CancelButton: TMsgDlgBtn;
  IconID: PChar;
  TextRect: TRect;
  CheckBox : TcxCheckBox;
begin
  Result := TMessageForm.CreateNew(Application);
  with Result do
  begin
    if ShowCheckBox then
    begin
      CheckBox := TcxCheckBox.Create(Result);
      with CheckBox do
      begin
        Parent := Result;
        Caption := CheckBoxCaption;
        Top := Result.ClientHeight - Height - 10;
        Left := 8;
        Visible := True;
      end;
    end;  
    BiDiMode := Application.BiDiMode;
    BorderStyle := bsDialog;
    Canvas.Font := Font;
    KeyPreview := True;
    OnKeyDown := TMessageForm(Result).CustomKeyDown;
    DialogUnits := GetAveCharSize(Canvas);
    HorzMargin := MulDiv(mcHorzMargin, DialogUnits.X, 4);
    VertMargin := MulDiv(mcVertMargin, DialogUnits.Y, 8);
    HorzSpacing := MulDiv(mcHorzSpacing, DialogUnits.X, 4);
    VertSpacing := MulDiv(mcVertSpacing, DialogUnits.Y, 8);
    ButtonWidth := MulDiv(mcButtonWidth, DialogUnits.X, 4);
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
    begin
      if B in Buttons then
      begin
        if ButtonWidths[B] = 0 then
        begin
          TextRect := Rect(0,0,0,0);
          Windows.DrawText( canvas.handle,
            PChar(LoadResString(ButtonCaptions[B])), -1,
            TextRect, DT_CALCRECT or DT_LEFT or DT_SINGLELINE or
            DrawTextBiDiModeFlagsReadingOnly);
          with TextRect do ButtonWidths[B] := Right - Left + 8;
        end;
        if ButtonWidths[B] > ButtonWidth then
          ButtonWidth := ButtonWidths[B];
      end;
    end;
    ButtonHeight := MulDiv(mcButtonHeight, DialogUnits.Y, 8);
    ButtonSpacing := MulDiv(mcButtonSpacing, DialogUnits.X, 4);
//    SetRect(TextRect, 0, 0, Screen.Width div 2, 0);
    SetRect(TextRect, 0, 0, 64, 64);
    DrawText(Canvas.Handle, PChar(Msg), Length(Msg)+1, TextRect,
      DT_EXPANDTABS or DT_CALCRECT or DT_WORDBREAK or
      DrawTextBiDiModeFlagsReadingOnly);
//    IconID := IconResIDs[DlgType];
    IconID := IconIDs[DlgType];
    IconTextWidth := 64;//TextRect.Right;
    IconTextHeight := 64;//TextRect.Bottom;
    if IconID <> nil then
    begin
      Inc(IconTextWidth, 32 + HorzSpacing);
      if IconTextHeight < 32 then IconTextHeight := 32;
    end;
    ButtonCount := 0;
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
      if B in Buttons then Inc(ButtonCount);
    ButtonGroupWidth := 0;
    if ButtonCount <> 0 then
      ButtonGroupWidth := ButtonWidth * ButtonCount +
        ButtonSpacing * (ButtonCount - 1);
    if ShowCheckBox then
      ClientWidth := Max(IconTextWidth, ButtonGroupWidth) + HorzMargin * 2 + CheckBox.Width
    else
      ClientWidth := Max(IconTextWidth, ButtonGroupWidth) + HorzMargin * 2;
    ClientHeight := IconTextHeight + ButtonHeight + VertSpacing +
      VertMargin * 2;
    Left := (Screen.Width div 2) - (Width div 2);
    Top := (Screen.Height div 2) - (Height div 2);
    if DlgType <> mtCustom then
      Caption := LoadResString(Captions[DlgType]) else
      Caption := Application.Title;
    if IconID <> nil then
      with TImage.Create(Result) do
      begin
        Name := 'Image';
        Parent := Result;
//        Stretch := True;
        AutoSize := True;
        Transparent := True;
        if IconID = IDI_INFORMATION then
          Picture.Bitmap.LoadFromResourceName(HInstance, 'INFO_2');
        //          Picture.Icon.Handle := LoadIcon(0, IconID);
        SetBounds(HorzMargin, VertMargin, 32, 32);
      end;
    TMessageForm(Result).Message := TLabel.Create(Result);
    with TMessageForm(Result).Message do
    begin
      Name := 'Message';
      Parent := Result;
      WordWrap := True;
      Caption := Msg;
      BoundsRect := TextRect;
      BiDiMode := Result.BiDiMode;
      ALeft := IconTextWidth - TextRect.Right + HorzMargin;
      if UseRightToLeftAlignment then
        ALeft := Result.ClientWidth - ALeft - Width;
      SetBounds(ALeft, VertMargin,
        TextRect.Right, TextRect.Bottom);
    end;
    if mbOk in Buttons then DefaultButton := mbOk else
      if mbYes in Buttons then DefaultButton := mbYes else
        DefaultButton := mbRetry;
    if mbCancel in Buttons then CancelButton := mbCancel else
      if mbNo in Buttons then CancelButton := mbNo else
        CancelButton := mbOk;
    X := (ClientWidth - ButtonGroupWidth) div 2;
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
      if B in Buttons then
        with TAdvGlowButton.Create(Result) do
        begin
          Name := ButtonNames[B];
          Parent := Result;
          Caption := LoadResString(ButtonCaptions[B]);
          ModalResult := ModalResults[B];
          if B = DefaultButton then Default := True;
          if B = CancelButton then Cancel := True;
          SetBounds(X, IconTextHeight + VertMargin + VertSpacing,
            ButtonWidth, ButtonHeight);
          Inc(X, ButtonWidth + ButtonSpacing);
          if B = mbHelp then
            OnClick := TMessageForm(Result).HelpButtonClick;
        end;
  end;
end;
procedure CloseAllDataSets(Connection : TADOConnection);
var DataSetIndex : Integer;
begin
  with Connection do
  begin
    for DataSetIndex := 0 to DataSetCount - 1 do
    begin
      DataSets[DataSetIndex].Close;
    end;
  end;
end;  

function ProcessError(TxtMessage : string) : Integer;
begin
  Result := MessageDlg('The following error has been ocurred:'+#13+#10+#13+#10 + TxtMessage +#13+#10+#13+#10 + 'All actions are cancelled.', mtError, [mbOK], 0);
end;
  
function EditConnectionString(Value : string) : string;
begin
  Result := PromptDataSource(0, Value);
end;  

procedure GetFieldsList(DataSet : TDataSet; List : TStrings);
begin
  DataSet.GetFieldNames(List);
end;

function Percentage(Amount : Double; TotalAmount : Double) : Integer;
begin
  Result := Round((Amount / TotalAmount) * 100);
end;

constructor TDataReplication.Create(AOwner : TComponent);
begin
  inherited Create;
end;

destructor TDataReplication.Destroy;
begin
  inherited Destroy;
end;  

procedure ExportDataFromTable(TableName : String; const SystemTable : Boolean = False);
var ColumnIndex : Integer;
    vExportedFields, ResultQuery : TStringList;
    QryExport : TADOQuery;
//    QryABSSysExport : TABSQuery;
    vFileName : String;
    LogTableIndex : Integer;

begin
{
  vFileName := Trim(TableName) + '.TXT';

  case SystemTable of
  True : begin

           for LogTableIndex := 1 to 2 do
           begin

             try
               QryABSSysExport := TABSQuery.Create(Application);
               vExportedFields := TStringList.Create;

               With QryABSSysExport do
               begin
//                 DatabaseName := ABSDatabase1.DatabaseName;
                 ReadOnly := False;
                 RequestLive := True;
                 SQL.Clear;

                 If LogTableIndex = 1 then
                   SQL.Add('SELECT * FROM SYSLOG WHERE LOGID = (SELECT MAX(LOGID) FROM SYSLOG) AND STATUSRESULT = ' + QuotedStr('OK'))
                 else
                 begin
                   case SyncType of
                   stFull : SQL.Add('SELECT * FROM SYSLOGDETAIL WHERE LOGID = (SELECT MAX(LOGID) FROM SYSLOG)');
                   stPartial : SQL.Add('SELECT D.* FROM SYSLOGDETAIL D INNER JOIN SYSLOG L ON D.LOGID = L.LOGID WHERE L.LOGID = (SELECT MAX(LOGID) FROM SYSLOG) AND L.STATUSRESULT = ' + QuotedStr('OK'));
                   end;
                 end;

                 Open;

                 With vExportedFields do
                 begin
                  Clear;
                  For ColumnIndex := 0 to Fields.Count - 1 do
                    Add(Fields[ColumnIndex].DisplayName);
                 end;

                 if not (IsEmpty) then
                 begin
                   FDataUpdates := True;
                   try
                     If LogTableIndex = 1 then
                     begin
                       FObjectName := 'SYSLOG';
                       RegStartLogEvent('(SYSLOG) Data Export', ltLogDetail);
                       ExportFromDataSet(QryABSSysExport, vExportedFields, FFilesLocationDirectory + '\' + 'SYSLOG.TXT');
                       FObjectName := 'SYSLOG.TXT';
                       RegEndLogEvent('OK', ltLogDetail);
                     end
                     else
                     begin
                       FObjectName := 'SYSLOGDETAIL';
                       RegStartLogEvent('(SYSLOGDETAIL) Data Export', ltLogDetail);
                       ExportFromDataSet(QryABSSysExport, vExportedFields, FFilesLocationDirectory + '\' + 'SYSLOGDETAIL.TXT');
                       FObjectName := 'SYSLOGDETAIL.TXT';
                       RegEndLogEvent('OK', ltLogDetail);
                     end;


                   except
                     on E: Exception do
                       RegEndLogEvent(E.Message, ltLogDetail);
                   end;
                 end
                 else
                 begin
                   if FileExists(FFilesLocationDirectory + '\' + vFileName) then
                     DeleteFile(FFilesLocationDirectory + '\' + vFileName);
                   RegStartLogEvent('(' + Trim(TableName) + ') Data Export', ltLogDetail);
                   RegEndLogEvent('No data for updates.', ltLogDetail);
                 end;
               end;
             finally
               FreeAndNil(QryABSSysExport);
               FreeAndNil(vExportedFields);
             end;
//               end;
           end;
         end;
  False: begin
           ResultQuery := TStringList.Create;
           vExportedFields := TStringList.Create;
           QryExport := TADOQuery.Create(Application);
           QryExport.Connection := MainConnection;

           try
             If not QryColumns.Active then QryColumns.Open;
             QryColumns.First;

             vExportedFields.Clear;
             ResultQuery.Clear;

             ResultQuery.Add('SELECT ');

             for ColumnIndex := 1 to QryColumns.RecordCount do
             begin
               If ColumnIndex <> QryColumns.RecordCount then
                 ResultQuery.Add(Trim(QryColumns.FieldByName('COLUMNNAME').AsString) + ',')
               else
                 ResultQuery.Add(Trim(QryColumns.FieldByName('COLUMNNAME').AsString));

               vExportedFields.Add(QryColumns.FieldByName('COLUMNNAME').AsString);
               QryColumns.Next;

               Application.ProcessMessages;
             end;

             ResultQuery.Add('FROM ' + Trim(TableName));

             case SyncType of
             stPartial : ResultQuery.Add('WHERE (CREATEDDATE > ' + QuotedStr(DateTimeToStr(FLastSyncDateTime)) +
                                         ' AND CREATEDDATE IS NOT NULL) OR (MODIFIEDDATE > ' + QuotedStr(DateTimeToStr(FLastSyncDateTime)) +
                                         ' AND MODIFIEDDATE IS NOT NULL)');
             end;

             with QryExport do
             begin
               If Active then Close;
               SQL.Clear;
               SQL := ResultQuery;
               DisableControls;
               Open;

               if not (IsEmpty) then
               begin
                 FDataUpdates := True;
                 try
                   FObjectName := Trim(TableName);
                   RegStartLogEvent('(' + TableName + ') Data Export', ltLogDetail);
                   ExportFromDataSet(QryExport, vExportedFields, FFilesLocationDirectory + '\' + vFileName);
                   FObjectName := vFileName;
                   RegEndLogEvent('OK', ltLogDetail);
                 except
                 on E: Exception do
                   RegEndLogEvent(E.Message, ltLogDetail);
                 end;
               end
               else
               begin
                 if FileExists(FFilesLocationDirectory + '\' + vFileName) then
                   DeleteFile(FFilesLocationDirectory + '\' + vFileName);
                 RegStartLogEvent('(' + Trim(TableName) + ') Data Export', ltLogDetail);
                 RegEndLogEvent('No data for updates.', ltLogDetail);
               end;
             end;
           finally
             FreeAndNil(ResultQuery);
             FreeAndNil(vExportedFields);
             FreeAndNil(QryExport);
             vFileName := '';
             FObjectName := '';
           end;
//           end;
         end;
  end;
}
end;
{
procedure TDataReplication.ExportFromDataSet(FromDataSet : TDataSet; FieldsList : TStringList; ToFile : String);
Var QExport4ASCII1 : TQExport4ASCII;
begin
  QExport4ASCII1 := TQExport4ASCII.Create(Application);

  try
    With QExport4ASCII1 do
    begin

      DataSet := FromDataSet;
      ExportedFields.Clear;
      ExportedFields := FieldsList;
      FileName := ToFile;
      try
        Execute;
      except
        On E: Exception do
        begin
          MessageDlg('Unable to execute dataset data export due to the following error:'+#13+#10+''+#13+#10+E.Message+#13+#10+''+#13+#10+'Please contact your system administrator to solve this problem.', mtError, [mbOK], 0);
          Exit;
        end;
      end;
    end;
  finally
    FreeAndNil(QExport4ASCII1);
  end;
end;
}
function GetRegKeyValue(KeyEntry : String) : Variant;
begin
  Result := '';
end;

function ListSQLDatabases : TStringList;
//Var DatabasesList : Databases;
//    DatabaseName  : Database;
//    DBIndex : Integer;
//    SQLServerApp : TSQLServer;
//    List : TStringList;
begin
//try
//  try
//    SQLServerApp := TSQLServer.Create(Application);
//
//    SQLServerApp.LoginSecure := False;
//
//    SQLServerApp.Connect1('XPTABLETPC', 'sa', '123');
//  except
//    MessageDlg('No es posible conectar sql server', mtWarning, [mbOK], 0);
//    Exit;
//  end;
//
//  DatabasesList := SQLServerApp.Databases;
////  DatabasesList.
//
//  List.Create;
//  List.Clear;
//
//  for DBIndex := 1 to DatabasesList.Count do
//  begin
//    DatabaseName := DatabasesList.Item(DBIndex, 'dbo');
//    List.Add(DatabaseName.Name);
//  end;
//
//  Result := List;
//finally
//  FreeAndNil(SQLServerApp);
//  FreeAndNil(DatabasesList);
//  FreeAndNil(DatabaseName);
//  FreeAndNil(List);
//end;
end;

procedure FreeInkInstance;
begin
//  FreeAndNil(InkCollector);
end;

procedure CreateInkInstance(Handle : Integer);
begin
//  try
//    InkCollector.Create(Application);
//    InkCollector.hWnd := Handle;
//    InkCollector.Enabled := True;
//  except
//    on E: Exception do
//    begin
//      ShowMessage(E.Message);
//      FreeAndNil(InkCollector);
//    end;
//  end;
end;

procedure SaveJpegToTable(Table: TCustomADODataSet; PicField:TBlobField; sPicPath: string);
{
Usage:

  SPicFileName := 'C:\!gajba\cdcovers\cdcover1.jpg';
  SaveJpegToTable(ADOTable1, ADOTable1Picture, SPicFileName);
}
var
  fS  : TFileStream;
begin
  fs:=TFileStream.Create(sPicPath, fmOpenRead);
  try
   Table.Edit;
   PicField.LoadFromStream(fs);
   Table.Post;
  finally
   fs.Free;
  end;
end;

function JpegStartsInBlob (PicField:TBlobField):integer;
var
 bS     : TADOBlobStream;
 buffer : Word;
 hx     : string;
begin
 Result := -1;
 bS := TADOBlobStream.Create(PicField, bmRead);
 try
  while (Result = -1) and (bS.Position + 1 < bS.Size) do
  begin
   bS.ReadBuffer(buffer, 1);
   hx:=IntToHex(buffer, 2);
   if hx = 'FF' then begin
     bS.ReadBuffer(buffer, 1);
     hx:=IntToHex(buffer, 2);
     if hx = 'D8' then Result := bS.Position - 2
     else if hx = 'FF' then bS.Position := bS.Position-1;
   end; //if
  end; //while
 finally
  bS.Free
 end;  //try
end;

procedure FetchProgress(DataSet: TCustomADODataSet; Progress, MaxProgress: Integer; var EventStatus: TEventStatus);
begin

  Application.ProcessMessages;
end;


function ListSQLServers : TStrings;
//Var Servers : NameList;
//    ServerIndex : Integer;
//    SQLServerApp : TSQLServer;
//    List : TStringList;
begin
//try
//  SQLServerApp := TSQLServer.Create(Application);
//
//  Servers := SQLServerApp.SDMOApplication.ListAvailableSQLServers;
//
//  List := TStringList.Create;
//  List.Clear;
//
//  for ServerIndex := 1 to Servers.Count do
//  begin
//    List.Add(Servers.Item(ServerIndex));
//  end;
//
//  Result := List;
//finally
//  SQLServerApp.Free;
//end;

end;


function GetShellFolder(CompName: String): IShellFolder;
var
 S: String;
 W: WideString;
 P: PWideChar;
 Desktop: IShellFolder;
 Len, Flags: LongWord;
 Machine: PItemIDList;
begin
 S:=CompName;
 if Pos('\\', S) <> 1 then S:='\\'+S;
 Len:=Length(S);
 W:=S;
 P:=@W[1];
 SHGetDesktopFolder(Desktop);
 Desktop.ParseDisplayName(0, nil, P, Len, Machine, Flags);
 Desktop.BindToObject(Machine, nil, IShellFolder, Pointer(Result));
end;

function GetComputerNames : TStringList;
var
// N: TNetworkNeighborhood;
 List: TStringList;
 ListComputers, IPAddresses : TStringList;
 i, j: Integer;
 ListViewItem: TListItem;
 WorkgroupNode, ComputerNode: TTreeNode;
 WorkGroupNodes, ComputerNodes : TTreeNodes;
begin
 Screen.Cursor:=crHourGlass;
 try
  // Initializing the objects and scanning a network
//  N:=TNetworkNeighborhood.Create;

  try
   // Obtaining the list of all the computers in a local area network
//   Memo1.Lines.Clear;
   ListComputers := TStringList.Create;
   IPAddresses := TStringList.Create;
//   N.ListComputers(ListComputers);

   // Obtaining the alphbetically sorted list of all the workgroups
   // and computers in a network
//   List:=TStringList.Create;
//   ListView1.Items.Clear;
//   try
//    N.ListNetwork(List);
{    for i:=0 to List.Count - 1 do begin
     ListViewItem:=ListView1.Items.Add;
     ListViewItem.Caption:=List[i];
     ListViewItem.ImageIndex:=Integer(List.Objects[i]);
    end;
}
//   finally
//    List.Free;
//   end;

   // Obtaining the tree view of the workgroups and computers in a network
//   TreeView1.Items.Clear;
{   for i:=0 to N.Count - 1 do begin
    WorkgroupNode:= TreeView1.Items.Add(nil, N[i]);
    WorkgroupNode.ImageIndex:=1;
    WorkgroupNode.SelectedIndex:=1;
    for j:=0 to (N.Objects[i] as TStrings).Count - 1 do begin
     ComputerNode:=TreeView1.Items.AddChild(WorkgroupNode, (N.Objects[i] as TStrings).Strings[j]);
     ComputerNode.ImageIndex:=0;
    end;
   end;
}
  // Obtaining the IP addresses of computers
//  GetIPAddresses(N, IPAddresses);

  finally
//   N.Free;
  end;
//  TreeView1.FullExpand;

 finally
  Screen.Cursor:=crDefault;
 end;
end;


procedure FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

function GetDatabaseName(Connection: TADOConnection): string;
begin
{  Result := GetConnectionParam(Connection, cpHost) + '.' +
            GetConnectionParam(Connection, cpDatabase) + '.' +
            GetConnectionParam(Connection, cpUser);}
end;

function GetDefaultConnection(Key : String; SetValueInRegistry : Boolean = False; AEntry : string = ''; APrompt : Boolean = DonotPrompt): WideString;
Var Registry : TRegistry;
    ConnectionString : String;
    Encripter : TDCP_blowfish;
    KeyEntry : WideString;
begin

  Try
{    Encripter := TDCP_blowfish.Create(Application);
    Encripter.Algorithm := 'Blowfish';
    Encripter.BlockSize := 64;
    Encripter.CipherMode := cmCBC;
    Encripter.Id := 5;
    Encripter.MaxKeySize := 448;
}
    Registry := TRegistry.Create;

    Registry.RootKey := HKEY_LOCAL_MACHINE;

    If Registry.OpenKey(Key, False) then
      if AEntry = '' then
        ConnectionString := Registry.ReadString('ConnectionString')
      else
        ConnectionString := Registry.ReadString(AEntry);

    if (ConnectionString <= '') and (APrompt) then
    begin
      if MessageDlg('Connection for ' + Application.Title + ' has not been set up.' + #10+#13 + 'Would you like to set the connection now?'+ #10+#13 + #10+#13 +
                    'NOTE: If you don''''t set it up now the application will be terminated.', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ConnectionString := EditConnectionString(ConnectionString);
        If ConnectionString > '' then
        begin
          if SetValueInRegistry then
          begin
            if AEntry = '' then
              SetDefaultConnection(Key, ConnectionString, AEntry)
            else
              SetDefaultConnection(Key, ConnectionString);
          end;
        end
        else
        begin
          MessageDlg('I''m sorry the connection could not be created.'+#13+#10+''+#13+#10+'This application will close now.', mtError, [mbOK], 0);
          Application.Terminate;
        end;
      end;
    end;

  Finally
    Registry.CloseKey;
    FreeAndNil(Registry);
    Result := ConnectionString;
  end;
end;

procedure SetDefaultConnection(Key : WideString; Value : String = ''; AKeyEntry : WideString = '');
Var Registry : TRegistry;
    Encripter : TDCP_blowfish;
begin

  Try
{    Encripter := TDCP_blowfish.Create(Application);
    Encripter.Init('!@#$%^&*()_+', 64, ^string);
    Encripter.Algorithm := 'Blowfish';
    Encripter.BlockSize := 64;
    Encripter.CipherMode := cmCBC;
    Encripter.Id := 5;
    Encripter.MaxKeySize := 448;
}
    Registry := TRegistry.Create;
    with Registry do
    begin
      RootKey := HKEY_LOCAL_MACHINE;
      If OpenKey(Key, True) then
      begin
        try
          if AKeyEntry = '' then
            WriteString('ConnectionString', Value)
          else
            WriteString(AKeyEntry, Value);
        except
          MessageDlg('Unable to write the connection setup to registry,'+#13+#10+'please verify that the logged user has administrative privileges.', mtError, [mbOK], 0);
        end;
      end;
    end;

  finally
    Registry.CloseKey;
    Registry.Free;
//    Encripter.Free;
  end;

end;

procedure KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
{
  If Key =  VK_RETURN then
    Perform(WM_NextDlgCtl, 0, 0);
}
end;

function ConnectDataBase(Database : TCustomConnection) : Boolean;
begin
With Database do
begin
  If not Connected then
  begin
    try
      if LoginPrompt then LoginPrompt := False;
      Open;
      Result := True;
    except
      MessageDlg('There has been an error connecting to the database.'+#13+#10+''+#13+#10+'Please contact your system Manager.', mtError, [mbOK], 0);
      Result:= False;
      Application.Terminate;
    end;
  end;
end;
end;

procedure ConnectDataSet(DataSet : TDataSet; const AReopen : Boolean = False);
begin
  if AReopen then
    if DataSet.Active then DataSet.Close;
  if not DataSet.Active then
    DataSet.Open;
end;

procedure ConnectDataSets(DataSets: Array of TDataSet);
var
  I: Integer;
begin
  for I := Low(DataSets) to High(DataSets) do
    ConnectDataset(DataSets[I], True);
end;

procedure AddListViewItem(ListView : TListView; ItemName : String; ImageIndex : Integer; const SubItems : TStringList);
Var Item : TListItem;
begin
  Item := ListView.Items.Add;
  Item.Caption := Trim(ItemName);
  Item.ImageIndex := ImageIndex;
  Item.SubItems := SubItems;
end;
{ TAppSecurity }

constructor TAppSecurity.Create(AOwner: TComponent);
begin
  FAVLock := TAVLockS6.Create(nil);
  FAVLock.Active:=True;
end;

destructor TAppSecurity.Destroy;
begin
  FAVLock.Active:=False;
  FAVLock.Destroy;
  FAVLock := nil;
end;

function TAppSecurity.GetAppLicenseStatus(AppInfo : TAppInfo) : TKeyData;
//var KeyData : TKeyData;
begin
  with FAVLock do
  begin
    AppName := AppInfo.AppName;
    AppID := AppInfo.AppID;
    AppVersion := AppInfo.AppVersion;
    RegPath := TRegPath.ExeDir;
    EncryptionKey := AppInfo.EncryptionKey;
    EncryptionKey2 := AppInfo.EncryptionKey2;
    //
    UserName := AppInfo.UserName;
    company := AppInfo.CompanyName;
    email := AppInfo.Email;
    //
//    WriteAppData;
    GetKeyData(0,Result);

    Status := Result.Status;
//    Result := KeyData;
  end;
end;


function TAppSecurity.InstallCode: string;
begin
  Result:=FAVLock.InstallCode;
end;

procedure TAppSecurity.RegisterApp;
begin
{
  frmRegistration :=  TfrmRegistration.Create(nil);
  try
    frmRegistration.ShowModal;
    if frmRegistration.ModalResult=mrOk then
    begin
      FAVLock.WriteAppData;
      FAVLock.restart;
    end
    else
    begin
      MessageDlg('Proceso de registro no completado, reinicie la aplicación para intentar nuevamente.', mtWarning, [mbOk],0);
      Application.Terminate;
    end;
  finally
    FreeAndNil(frmRegistration);
  end;
  }
end;

end.
