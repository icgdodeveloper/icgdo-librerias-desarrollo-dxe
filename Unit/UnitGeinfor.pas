//------------------------------------------------------------------------------
//Unit de Librerias o rutinas a ser utilizadas para trabajar con la aplicacion
//Geinfor
//Realizado por : Geinfor Venezuela
//
//
//
//------------------------------------------------------------------------------


unit UnitGeinfor;

interface

Uses  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
   DateUtils,ADODB,IniFiles, Data.DB,UnitAppsComunes,UnitINI, Math;


Var
  IniFile : TIniFile;
  Cn001: TADOConnection;
  Cn002: TADOConnection;
  Cn003: TADOConnection;
  Rs001: TADOQuery;
  Rs002: TADOQuery;
  Rs003: TADOQuery;
  Rs004: TADOQuery;
  Rs005: TADOQuery;
  Rs006: TADOQuery;

  //Variables
  ServerDB,
  NameDB,
  UsersDB,
  PasswordDB,
  StrSQL,
  StrSQL02,
  Mensaje:String;
  Conectado:Boolean;
  Registros:Integer;


  //----------------------------------------------------------------------------
  //Procedimientos--------------------------------------------------------------
  //----------------------------------------------------------------------------
  //Procedimiento Para Trasladar Asientos de Bs a Dolares
  Procedure AsientosDiarios_MonedaUSD(ODBC_Origen,ODBC_Destino,PasswordODBCOrigen,PasswordODBCDestino:String;Fecha_INI,Fecha_FIN:TDate;Actualiza_Presupuesto:Boolean);




implementation






 //Procedimiento Para Trasladar Asientos de Bs a Dolares
 Procedure AsientosDiarios_MonedaUSD(ODBC_Origen,ODBC_Destino,PasswordODBCOrigen,PasswordODBCDestino:String;Fecha_INI,Fecha_FIN:Tdate;Actualiza_Presupuesto:Boolean);
 Var

 //Cabecera del Asiento
 cEmpresa, cSerie:String;
 nAsiento,nTipoasiento,
 nClase,nNumero:Integer;
 dFecha:TDate;


 //Lineas del Asiento
 nApunte:Integer;
 nFactor:Integer;
 cCuenta:String;
 bImporte:Double;
 nCodigo_Contable:Integer;
 cComentario:String;
 cMarca:String;
 nOrigen:Integer;
 cSerieenlace:String;
 ncuentareparto :Integer;
 nNumremesa :Integer;
 cMarcapunteo:String;
 nProyecto :Integer;
 nCreadopor:Integer;
 dFechacreacion:TDate;
 nModificadopor:Integer;
 dFechamodificacion:TDate;
 tHoramodificacion:TTime;

 //Plan de cuentas
 nPC_Plancontable:Integer;
 cPC_Codigo_Contable:String;
 nPC_Nivel:Integer;
 cPC_Denominacion:String;
 nPC_Origen:Integer;
 nPC_Enlace:Integer;
 nPC_Cuentareparto:Integer;
 nPC_Seleccionable:Integer;
 nPC_Bloqueototal:Integer;
 dPC_Fechainibloqueo:Tdate;
 dPC_Fechafinbloqueo:Tdate;



 //Cuentas de Reparto
 nCR_Codigo:Integer;
 cCR_Descripcion:String;
 nCR_Seleccionable:Integer;
 //Relacion Cuentas de Reparto Centro de Costos
 nRC_CentroCoste:Integer;
 bRC_Porcentaje:Double;
 cRC_Descripcion_CC:String;


 //presupuesto Contable
 cPR_Codigo_Contable:String;
 nPR_CuentaReparto: Integer;
 nPR_Proyecto : Integer;
 bPR_IMPORTE_ANUAL:Double;
 b_PR_Monto_Mes:Double;
 n_RP_Tipo_Cuenta: Integer;
 cPR_DETALLE_TEXTO1:String;
 b_PR_DETALLE_IMPORTE1:Double;
 cPR_DETALLE_TEXTO2:String;
 b_PR_DETALLE_IMPORTE2:Double;
 cPR_DETALLE_TEXTO3:String;
 b_PR_DETALLE_IMPORTE3:Double;
 cPR_DETALLE_TEXTO4:String;
 b_PR_DETALLE_IMPORTE4:Double;
 cPR_DETALLE_TEXTO5:String;
 b_PR_DETALLE_IMPORTE5:Double;
 cPR_DETALLE_TEXTO6:String;
 b_PR_DETALLE_IMPORTE6:Double;
 cPR_DETALLE_TEXTO7:String;
 b_PR_DETALLE_IMPORTE7:Double;
 cPR_DETALLE_TEXTO8:String;
 b_PR_DETALLE_IMPORTE8:Double;
 cPR_DETALLE_TEXTO9:String;
 b_PR_DETALLE_IMPORTE9:Double;
 cPR_DETALLE_TEXTO10:String;
 b_PR_DETALLE_IMPORTE10:Double;
 cPR_DETALLE_TEXTO11:String;
 b_PR_DETALLE_IMPORTE11:Double;
 cPR_DETALLE_TEXTO12:String;
 b_PR_DETALLE_IMPORTE12:Double;


 //Variables del Procedimiento
 ArchivoINI,
 cImporte:String;
 cImporte01:String;
 nCodMonedaPM:Integer;
 nCodMonedaPNM:Integer;
 bTasaCambioPM,
 bTasaCambioPNM:Double;
 nEjercicio_Contable_Anio:Integer; //A�o del ejercicio Contable del Calandario de la Empresa
 nEjercicio_Contable_Mes:Integer; //Mes del ejercicio Contable del Calandario de la Empresa
 dEjercicio_Contable_Inicio:Tdate;//Fecha de Inicio del mes del ejercicio Contable del Calandario de la Empresa
 dEjercicio_Contable_Fin:Tdate;//Fecha de Fin del mes del ejercicio Contable del Calandario de la Empresa
 wAnyo, wMes, wDia: Word; // Manejo de Fechas
 cCtaDescuadreAsiento:String;
 bTotalDebe:Double; //Monto total de los debidos del asiento
 bTotalHaber:Double;//Monto Total de los credios del asinto
 bDescuadreAsiento:Double; // Monto del Descuadre del asiento
 nUltApunte:Integer; //Ultima linea del asiento



 //Formulario en tiempo de ejecucion
  Frm:TForm;
  PBar: TProgressBar  ;
  Lbl01,
  Lbl02,
  Lbl03:TLAbel;
  i:Integer;

 Begin

    //
    ArchivoINI:=(ExtractFilePath(application.ExeName)+'GeinforUsGaap.ini') ;
    // Se Verifica si el archivo de Configuracion existe
    if fileexists(ArchivoINI) then
    begin
      //Leemos la Configuracion del .INI
      nCodMonedaPNM:= ReadEntINI ('Config', 'CODMONEDAPNM' , ArchivoINI,0) ;
      nCodMonedaPM:= ReadEntINI ('Config', 'CODMONEDAPM' , ArchivoINI,0) ;
      cCtaDescuadreAsiento:=ReadCadINI ('Config', 'CUENTACUADRE' , ArchivoINI,'810302001') ;
    end;



    //Se arma el sql de los asientos Origen
    StrSQL:='';
    StrSQL:='SELECT Empresa, Serie, Asiento, Fecha,Clase,Numero,Tipoasiento FROM CABECERA_DIARIO' +
      ' WHERE Fecha>=' + Chr(39) + DateToStr(Fecha_INI) + Chr(39) +
      ' AND Fecha<=' + Chr(39) + DateToStr(Fecha_FIN) + Chr(39) ;


    //Define los Objetos ADO del Origen de datos
    ServerDB:='';
    NameDB:=ODBC_Origen;
    UsersDB:='';
    PasswordDB:=PasswordODBCOrigen;
    Cn001  := TADOConnection.Create(nil);
    Rs001    := TADOQuery.Create(nil); //Cabecera de Asientos
    Rs003    := TADOQuery.Create(nil); //Lineas de Asientos
    Rs004    := TADOQuery.Create(nil); //Usos Varios dentro de la Aplicacion
    Rs005    := TADOQuery.Create(nil); //Usos Varios dentro de la Aplicacion
    Rs006    := TADOQuery.Create(nil); //Usos Varios dentro de la Aplicacion

    //Conexion a la Base de datos
    Conectado:=Open_DB (Cn001 , 5 , ServerDB, NameDB, UsersDB, PasswordDB, '' );
    //si hay conexion continue el programa
    if Conectado then
    Begin
      //Se Abre el DataSet de los clientes
      Conectado:= Open_ADO_Qry(Cn001, Rs001,StrSQL,false);
      // Verifica si se Abrio el DataSet
      if Conectado then
      Begin
        with Rs001 do begin
          if not eof then
          Begin
            Registros:=RecordCount;
            //Se crean los Objetos en tiempo de Ejecucion
            //Formulario
            Frm := TForm.Create(Application);
            FRM.Name := 'AsientosDiarios_MonedaUSD_Form01';
            Frm.Caption := 'Proceso de Envio Asientos';
            Frm.Position := poDesktopCenter;
            Frm.Width := 600;
            Frm.Height := 300;


            //Labels
            Lbl01 := TLabel.Create(Frm);
            Lbl01.Parent:=Frm;
            Lbl01.Name := 'AsientosDiarios_MonedaUSD_Label01';
            Lbl01.Top := 180;
            Lbl01.Left := 200;
            Lbl01.Width := 100;
            Lbl01.Caption := 'Asiento Nro :   de:';
            Lbl01.Visible:=True;
            Lbl01.Font.Style:= [fsBold];


            //Progres Bar
            PBar:= TProgressBar.Create(Frm);
            PBar.Parent:=Frm;
            PBar.Name := 'AsientosDiarios_MonedaUSD_Pbar01';
            PBar.Top := 150;
            PBar.Left := 10;
            PBar.Width := 560;
            PBar.Min:=0;
            PBar.Max:=Registros;
            PBar.MarqueeInterval:=10;
            PBar.Visible:=True;

            //Se activa el formulario
            Frm.Show;

            //------------------------------------------------------------------
            //Abrir la Conexion a la Base de datos Destino
            //------------------------------------------------------------------
            ServerDB:='';
            NameDB:=ODBC_Destino;
            UsersDB:='';
            PasswordDB:=PasswordODBCOrigen;
            Cn002  := TADOConnection.Create(nil);
            Rs002    := TADOQuery.Create(nil);
            //Conexion a la Base de datos
            Conectado:=Open_DB (Cn002 , 5 , ServerDB, NameDB, UsersDB, PasswordDB, '' );
            //si Np hay conexion a la base de datos Destino, se aborta el proceso
            if Conectado=False then
            Begin
              // Cerrar la Conexion a la Base de datos
                CloseConnection  (cn001, rs001);
              // Cerrar la Conexion a la Base de datos
              CloseConnection  (cn002, rs002);
              Exit()
            End;
            //------------------------------------------------------------------

            //------------------------------------------------------------------
            //Ciclo para recorrer los asientos de la Empresa Origen
            //------------------------------------------------------------------
            i:=1;
            While not Eof do
            Begin
              //Recorrido del RecordSet

              cEmpresa:=Fields[0].AsString;
              cSerie:=Fields[1].AsString;
              nAsiento:=Fields[2].AsInteger;
              dFecha:=Fields[3].AsDateTime;
              nClase:=Fields[4].AsInteger;
              nNumero:= Fields[5].AsInteger;
              nTipoasiento:=Fields[6].AsInteger;

              //----------------------------------------------------------------
              Lbl01.Caption := 'Asiento Nro : ' + IntTOStr(i) + ' de: ' + IntToStr(Registros);
              Lbl01.Refresh;
              PBar.Position:=i;
              //----------------------------------------------------------------

              //----------------------------------------------------------------
              // Buscar la tasa de Cambio Partidas no Monetarias
              //----------------------------------------------------------------
              StrSQL:='';
              StrSQL:='SELECT TasaCambio FROM GEINVZLA_TB_MONEDAS ' +
              ' WHERE Codmoneda =' + IntToStr(nCodMonedaPNM) +
              ' AND ' + Chr(39) + DateToStr(Fecha_INI) + Chr(39) + '>=Fechainicial'  +
              ' AND ' + Chr(39) + DateToStr(Fecha_FIN) + Chr(39) + '<=Fechafinal' ;
              //Se Abre el DataSet
              Conectado:= Open_ADO_Qry(Cn002, Rs004,StrSQL,false);
              // Verifica si se Abrio el DataSet
              if Conectado then
              Begin
                with Rs004 do
                begin
                  if not eof then
                  Begin
                     bTasaCambioPNM:=Fields[0].AsFloat;
                  End;
                end;
              End;
              //Cierra el RecordSet
              Rs004.Close;
              //----------------------------------------------------------------
              // Buscar la tasa de Cambio Partidas Monetarias
              //----------------------------------------------------------------
              StrSQL:='';
              StrSQL:='SELECT TasaCambio FROM GEINVZLA_TB_MONEDAS ' +
              ' WHERE Codmoneda =' + IntToStr(nCodMonedaPM) +
              ' AND ' + Chr(39) + DateToStr(Fecha_INI) + Chr(39) + '>=Fechainicial'  +
              ' AND ' + Chr(39) + DateToStr(Fecha_FIN) + Chr(39) + '<=Fechafinal' ;

              //Se Abre el DataSet
              Conectado:= Open_ADO_Qry(Cn002, Rs004,StrSQL,false);
              // Verifica si se Abrio el DataSet
              if Conectado then
              Begin
                with Rs004 do
                begin
                  if not eof then
                  Begin
                     bTasaCambioPM:=Fields[0].AsFloat;
                  End;
                end;
              End;
              if bTasaCambioPM <= 0 then bTasaCambioPM:=1;
              if bTasaCambioPNM <= 0 then bTasaCambioPNM:=1;
              Rs004.Close;
              //----------------------------------------------------------------


              //----------------------------------------------------------------
              // Buscar la Fecha del Calendario Coorporativo
              //----------------------------------------------------------------
              nEjercicio_Contable_Anio:=0;
              nEjercicio_Contable_Mes:=0;
              // Se construye los rangos de fecha
              DecodeDate(dFecha , wAnyo, wMes, wDia );
              nEjercicio_Contable_Anio:=wAnyo;
              nEjercicio_Contable_Mes:=wMes;
              StrSQL:='';
              StrSQL:='SELECT  FECHAINICIAL, FECHAFINAL FROM GEINVZLA_TB_CALENDARIO_CONTABLE ' +
              ' WHERE Ejercicio =' + IntToStr(nEjercicio_Contable_Anio) +
              ' AND Mes=' + IntToStr(nEjercicio_Contable_Mes) ;
              //Se Abre el DataSet
              Conectado:= Open_ADO_Qry(Cn002, Rs004,StrSQL,false);
              // Verifica si se Abrio el DataSet
              if Conectado then
              Begin
                with Rs004 do
                begin
                  if not eof then
                  Begin
                    //Movemos los valores a las variables de memoria
                     dEjercicio_Contable_Inicio:=Fields[0].AsDateTime;
                     dEjercicio_Contable_Fin:=Fields[1].AsDateTime;
                     //Verificamos que la fecha del asiento sea la correcta
                     If dFecha > dEjercicio_Contable_Fin Then dFecha:=  dEjercicio_Contable_Fin
                  End;
                end;
              End;
              Rs004.Close;
              //----------------------------------------------------------------

              //----------------------------------------------------------------
              //--Borrar el asiento en la base de datos destino
              //----------------------------------------------------------------
              //Se arma el sql de las lineas del Asiento a Borrar
              StrSQL:='';
              StrSQL:='Delete FROM LINEAS_DIARIO_APUNT ' +
              ' WHERE Empresa =' + Chr(39)  + cEmpresa  + Chr(39) +
              ' AND Serie = ' + Chr(39)  + cSerie  + Chr(39) +
              ' AND Asiento = ' + Chr(39)  + IntToStr(nAsiento)  + Chr(39);
              //Se ejecuta el Comando para Borrar la Cabecera el Asiento
              Conectado:=DB_Function(cn002,StrSQL,4);
              //Se arma el sql de los asientos Origen
              StrSQL:='';
              StrSQL:='Delete FROM CABECERA_DIARIO' +
              ' WHERE Empresa =' + Chr(39)  + cEmpresa  + Chr(39) +
              ' AND Serie = ' + Chr(39)  + cSerie  + Chr(39) +
              ' AND Asiento = ' + Chr(39)  + IntToStr(nAsiento)  + Chr(39);
              //Se ejecuta el Comando para Borrar la Cabecera el Asiento
              Conectado:=DB_Function(cn002,StrSQL,4);
              //----------------------------------------------------------------

              //---------------------------------------------------------------
              //Se Inserta la Cabecera del asiento en la base de datos Destino
              //----------------------------------------------------------------
              StrSQL:='';
              StrSQL:='Insert Into CABECERA_DIARIO (Empresa, Serie, Asiento, Fecha,Clase,Numero,Tipoasiento)' +
                ' Values (' + Chr(39) +  cEmpresa + Chr(39) + ',' +
                Chr(39) + cSerie + Chr(39) + ',' +
                IntToStr(nAsiento) + ',' +
                 Chr(39) + DateToStr(dFecha)  + Chr(39) + ',' +
                IntToStr(nClase)  + ',' +
                IntToStr(nNumero) + ',' +
                IntToStr(nTipoasiento)  + ')' ;
                //Se ejecuta el Comando para Insertar el Asiento en la base de datos Destino
                Conectado:=DB_Function(cn002,StrSQL,4);


              //----------------------------------------------------------------
              //Se abre la conexion de las lineas del asiento Origen
              //----------------------------------------------------------------
              //Se arma el sql de las lineas del Asiento Origen
              StrSQL:='';
              StrSQL:='SELECT Apunte,Factor,Cuenta,Importe, ' +
                'Codigo_Contable,Comentario,Marca,Origen,Serieenlace,cuentareparto, ' +
                'Numremesa,Marcapunteo,Proyecto,Creadopor,Fechacreacion,Modificadopor,' +
                'Fechamodificacion,Horamodificacion FROM LINEAS_DIARIO_APUNT ' +
                'WHERE Empresa =' + Chr(39)  + cEmpresa  + Chr(39) +
                ' AND Serie = ' + Chr(39)  + cSerie  + Chr(39) +
                ' AND Asiento = ' + Chr(39)  + IntToStr(nAsiento)  + Chr(39) +
                ' Order By Apunte';

                //Se Abre el DataSet
                Conectado:= Open_ADO_Qry(Cn001, Rs003,StrSQL,false);
                // Verifica si se Abrio el DataSet
                if Conectado then
                Begin
                  with Rs003 do
                  begin
                    if not eof then
                    Begin
                      //------------------------------------------------------------------
                      //Ciclo para recorrer las lineas de los asientos de la Empresa Origen
                      //------------------------------------------------------------------
                      While not Eof do
                      Begin
                        //Recorrido del RecordSet de las lineas de los asientos

                        nApunte:=Fields[0].AsInteger;
                        nFactor:=Fields[1].AsInteger;
                        cCuenta:=Fields[2].AsString;
                        bImporte:=Fields[3].AsFloat;
                        nCodigo_Contable:=Fields[4].AsInteger;
                        cComentario:=Fields[5].AsString;
                        cMarca:=Fields[6].AsString;
                        nOrigen:=Fields[7].AsInteger;
                        cSerieenlace:=Fields[8].AsString;
                        ncuentareparto :=Fields[9].AsInteger;
                        nNumremesa:=Fields[10].AsInteger;
                        cMarcapunteo:=Fields[11].AsString;
                        nProyecto :=Fields[12].AsInteger;
                        nCreadopor:=Fields[13].AsInteger;
                        dFechacreacion:=Fields[14].AsDateTime;
                        nModificadopor:=Fields[15].AsInteger;
                        dFechamodificacion:=Fields[16].AsDateTime;
                        tHoramodificacion:=Fields[17].AsDateTime;

                        dFechacreacion:=Now;
                        dFechamodificacion:=Now;
                        tHoramodificacion:=Time;

                        //Se aplica la conversion Monetaria
                        bImporte:=RoundTo((bImporte/bTasaCambioPM)/1000,0);

                        //Se convierte a Cadena el importa para poderlo insertar
                        cImporte:=FloattoStr(bImporte);
                        cImporte:=  StringReplace( cImporte, ',', '.', [rfReplaceAll] ) ;

                        //  se quintan los caracteres especiales de la descripcion
                        cComentario:=  StringReplace(cComentario, ',', ' ', [rfReplaceAll] ) ;
                        cComentario:=  StringReplace(cComentario, Chr(39), ' ', [rfReplaceAll] ) ;
                        cComentario:=  StringReplace(cComentario, '"', ' ', [rfReplaceAll] ) ;
                        //Si el asiento trabaja con cuenta de reparto se realizan
                        //las Siguientes acciones
                        if ncuentareparto > 0then
                        Begin
                          //----------------------------------------------------------------
                          // 1. Se busca la cuenta de Reparto en la base de datos Origen
                          //----------------------------------------------------------------
                          StrSQL:='';
                          StrSQL:='SELECT Descripcion, Seleccionable FROM REPARTOS' +
                          ' WHERE Codigo =' + IntToStr(ncuentareparto)    ;
                          //Cerramos el DataSet
                          Rs005.Close;
                          //Se Abre el DataSet
                          Conectado:= Open_ADO_Qry(Cn001, Rs005,StrSQL,false);
                          // Verifica si se Abrio el DataSet
                          if Conectado then
                          Begin
                            with Rs005 do
                            begin
                              if not eof then
                              Begin
                                //Movemos los valores a las variables de memoria
                                cCR_Descripcion:=Fields[0].AsString;
                                nCR_Seleccionable:=Fields[1].AsInteger;
                              End;
                            end;
                          End;
                          //Cerramos el DataSet
                          Rs005.Close;
                          //----------------------------------------------------------------



                          //----------------------------------------------------------------
                          // 2. Se busca la cuenta de Reparto en la base de datos Destino
                          //----------------------------------------------------------------
                          StrSQL:='';
                          StrSQL:='SELECT Descripcion, Seleccionable FROM REPARTOS' +
                          ' WHERE Codigo =' + IntToStr(ncuentareparto)    ;
                          //Cerramos el DataSet
                          Rs005.Close;
                          //Se Abre el DataSet
                          Conectado:= Open_ADO_Qry(Cn002, Rs005,StrSQL,false);
                          // Verifica si se Abrio el DataSet
                          if Conectado then
                          Begin
                            with Rs005 do
                            begin
                              if eof then
                              Begin
                                //----------------------------------------------
                                //3. Insertamos la cuenta de reparto
                                //----------------------------------------------
                                StrSQL:='';
                                StrSQL:='Insert Into REPARTOS (Codigo,Descripcion, Seleccionable)' +
                                ' Values (' +  IntToStr(ncuentareparto) + ',' +
                                Chr(39) + cCR_Descripcion + Chr(39) + ',' +
                                IntToStr(nCR_Seleccionable) + ')' ;
                                //Se ejecuta el Comando para Insertar la cuenta de reparto
                                Conectado:=DB_Function(cn002,StrSQL,4);

                                //----------------------------------------------
                                //4. Borramos la relacion centros de costos en la empresa Destino
                                //----------------------------------------------
                                StrSQL:='';
                                StrSQL:='Delete FROM REPARTOS_CCOSTE' +
                                ' WHERE Codigo =' + IntToStr(ncuentareparto)    ;
                                //Se ejecuta el Comando para Borrar la relacion Centros de Costos Repartos
                                Conectado:=DB_Function(cn002,StrSQL,4);

                                //----------------------------------------------
                                //5. Se lee la relacion cuentas repartos en la base de datos Origen
                                //----------------------------------------------
                                StrSQL:='';
                                StrSQL:='SELECT A.Codigo,A.CentroCoste,A.Porcentaje,B.Descripcion ' +
                                ' FROM REPARTOS_CCOSTE AS A LEFT JOIN CENTROS_COSTE AS B ' +
                                ' ON A.CentroCoste=B.Codigo WHERE A.Codigo =' + IntToStr(ncuentareparto);
                                //Cerramos el DataSet
                                Rs006.Close;
                                //Se Abre el DataSet
                                Conectado:= Open_ADO_Qry(Cn001, Rs006,StrSQL,false);
                                // Verifica si se Abrio el DataSet
                                if Conectado then
                                Begin
                                  with Rs006 do
                                  begin
                                    if not eof then
                                    Begin
                                      //Se recorre la tabla de cuentas de repartos centros de costos Origen
                                       While not Eof do
                                       Begin
                                         //Movemos los valores a las variables de memoria
                                          nRC_CentroCoste:=Fields[1].AsInteger;
                                          bRC_Porcentaje:=Fields[2].AsFloat;
                                          cRC_Descripcion_CC:=Fields[3].AsString;
                                          //------------------------------------
                                          //6. Se inserta en la tabla destino de relacion Centros de costos
                                          //------------------------------------
                                          cImporte01:=FloattoStr(bRC_Porcentaje);
                                          cImporte01:=  StringReplace( cImporte01, ',', '.', [rfReplaceAll] ) ;
                                          //  se quintan los caracteres especiales de la descripcion

                                          StrSQL:='';

                                          StrSQL:='Insert Into REPARTOS_CCOSTE (Codigo,CentroCoste,Porcentaje)' +
                                            ' Values (' +  IntToStr(ncuentareparto) + ',' +
                                            IntToStr(nRC_CentroCoste) + ',' +
                                            cImporte01 + ')' ;
                                          //Se ejecuta el Comando para Insertar la cuenta de reparto por cenrtro de costo
                                          Conectado:=DB_Function(cn002,StrSQL,4);
                                          cImporte01:='';
                                          //------------------------------------
                                          //Siguiente Registro
                                          Next;
                                       End;
                                       //---------------------------------------
                                       //7. Borrar el centro de costo en la empresa Destino
                                       //---------------------------------------
                                        StrSQL:='';
                                        StrSQL:='DELETE FROM CENTROS_COSTE' +
                                          ' WHERE Codigo =' + IntToStr(nRC_CentroCoste)    ;
                                        //Se ejecuta el Comando para Borrar el centro de costo
                                        Conectado:=DB_Function(cn002,StrSQL,4);
                                       //---------------------------------------
                                       //8. Insertar el centro de costo en la empresa Destino
                                       //---------------------------------------
                                        StrSQL:='';
                                        StrSQL:='INSERT INTO CENTROS_COSTE (Codigo,Descripcion) ' +
                                          ' Values (' +  IntToStr(nRC_CentroCoste) + ',' +
                                          Chr(39) +  cRC_Descripcion_CC + Chr(39) + ')' ;
                                        //Se ejecuta el Comando para Borrar el centro de costo
                                        Conectado:=DB_Function(cn002,StrSQL,4);
                                    End;
                                  end;
                                End;
                                //Cerramos el DataSet
                                Rs006.Close;
                              End;
                            end;
                          End;
                          //Cerramos el DataSet
                          Rs004.Close;
                          Rs005.Close;
                        End;
                        //----------------------------------------------------------------

                        //------------------------------------------------------
                        //Verificar si existe la cuenta en la base de datos Destino
                        //------------------------------------------------------
                        //Se arma el sql para buscar en la base de datos Origen
                        StrSQL:='';
                        StrSQL:='SELECT Plancontable,Codigo_Contable,Nivel,Denominacion,Origen,Enlace,Cuentareparto, ' +
                         ' Seleccionable,Bloqueototal,Fechainibloqueo,Fechafinbloqueo FROM PLAN_CONTABLE ' +
                         ' WHERE Plancontable=0' +
                         ' AND Codigo_Contable=' + Chr(39) + cCuenta + Chr(39) ;
                        //Se Abre el DataSet
                        Conectado:= Open_ADO_Qry(Cn001, Rs004,StrSQL,false);
                        // Verifica si se Abrio el DataSet
                        if Conectado then
                        Begin
                          with Rs004 do
                          begin
                            //Si no es fin de registro
                            if not eof then
                            Begin
                              nPC_Plancontable:=Fields[0].AsInteger;
                              cPC_Codigo_Contable:=Fields[1].AsString;
                              nPC_Nivel:=Fields[2].AsInteger;
                              cPC_Denominacion:=Fields[3].AsString;
                              nPC_Origen:=Fields[4].AsInteger;
                              nPC_Enlace:=Fields[5].AsInteger;
                              nPC_Cuentareparto:=Fields[6].AsInteger;
                              nPC_Seleccionable:=Fields[7].AsInteger;
                              nPC_Bloqueototal:=Fields[8].AsInteger;
                              dPC_Fechainibloqueo:=Fields[9].AsDateTime;
                              dPC_Fechafinbloqueo:=Fields[10].AsDateTime;
                            End;
                          end;
                        End;
                        //Cerrar el data Set
                        Rs004.Close;

                        //Se Busca en la empresa Destino
                        //Se Abre el DataSet
                        Conectado:= Open_ADO_Qry(Cn002, Rs004,StrSQL,false);
                        // Verifica si se Abrio el DataSet
                        if Conectado then
                        Begin
                          with Rs004 do
                          begin
                            //Si no existe la cuenta se inserta
                            if eof then
                            Begin
                              StrSQL:='';
                              StrSQL:='Insert Into PLAN_CONTABLE (Plancontable,Codigo_Contable,Nivel,Denominacion,Origen,Enlace,Cuentareparto, ' +
                              ' Seleccionable,Bloqueototal,Fechainibloqueo,Fechafinbloqueo) ' +
                              ' Values (' + IntToStr(nPC_Plancontable) +',' +
                                 Chr(39) + (cPC_Codigo_Contable) +Chr(39)   +',' +
                                IntToStr(nPC_Nivel)+',' +
                                Chr(39) + cPC_Denominacion  + Chr(39) +',' +
                                IntToStr(nPC_Origen)+',' +
                                IntToStr(nPC_Enlace) +',' +
                                IntToStr(nPC_Cuentareparto) +',' +
                                IntToStr(nPC_Seleccionable) +',' +
                                IntToStr(nPC_Bloqueototal) +',' +
                                Chr(39) + DateToStr(dPC_Fechainibloqueo) +Chr(39)   +',' +
                                Chr(39) + DateToStr(dPC_Fechafinbloqueo)+ Chr(39) + ')' ;
                              //Se ejecuta el Comando para Insertar la cuenta contable
                              Conectado:=DB_Function(cn002,StrSQL,4);
                              //Se Inserta en la tabla de de Cuentas GEINVZLA_TB_CUENTAS  en la base de datos destino
                              StrSQL:='';
                              StrSQL:='Insert Into GEINVZLA_TB_CUENTAS (CODIGOCONTABLE) ' +
                              ' Values (' + Chr(39) + (cPC_Codigo_Contable) +Chr(39)   +')' ;
                              //Se ejecuta el Comando para Insertar la cuenta contable
                              Conectado:=DB_Function(cn002,StrSQL,4);
                            End;
                          end;
                        End;
                        //Cerrar el data Set
                        Rs004.Close;
                        //------------------------------------------------------
                        //Insertar el asiento en la Base de datos Destino
                        //------------------------------------------------------
                        StrSQL:='';
                        StrSQL:='Insert Into LINEAS_DIARIO_APUNT (Empresa, Serie, Asiento,Apunte,Factor,Cuenta,Importe, ' +
                            'Codigo_Contable,Comentario,Marca,Origen,Serieenlace,cuentareparto, ' +
                            'Numremesa,Marcapunteo,Proyecto,Creadopor,Fechacreacion,Modificadopor,' +
                            ' Fechamodificacion )' +
                            'Values (' + Chr(39) + cEmpresa + Chr(39) +',' +
                            Chr(39) + cSerie + Chr(39) +',' +
                            IntToStr(nAsiento) +',' +
                            IntToStr(nApunte)  +',' +
                            IntToStr(nFactor)  +',' +
                            Chr(39) +cCuenta + Chr(39) +',' +
                            (cImporte) +',' +
                            IntToStr(nCodigo_Contable)  +',' +
                            Chr(39) +cComentario + Chr(39) +',' +
                            Chr(39) +cMarca + Chr(39) +',' +
                            IntToStr(nOrigen)  +',' +
                            Chr(39) +cSerieenlace  + Chr(39) +',' +
                            IntToStr(ncuentareparto)   +',' +
                            IntToStr(nNumremesa)  +',' +
                            Chr(39) +cMarcapunteo + Chr(39) +',' +
                            IntToStr(nProyecto)   +',' +
                            IntToStr(nCreadopor)  +',' +
                            Chr(39) +DateToStr(dFechacreacion) + Chr(39) +',' +
                            IntToStr(nModificadopor)  +',' +
                            Chr(39) +DateToStr(dFechamodificacion)  + Chr(39) +')' ;
                          //  Chr(39) +DateToStr(tHoramodificacion) + Chr(39) +')' ;

                            //Se ejecuta el Comando para Insertar el Asiento en la base de datos Destino
                            Conectado:=DB_Function(cn002,StrSQL,4);
                        //Siguiente Registro
                        //----------------------------------------------------------------
                        Next;
                      End;
                    End;
                  end;
                End;




              //----------------------------------------------------------------
              // Se verifica si el asiento esta descuadrado en la base de datos Destino
              //----------------------------------------------------------------
              //cCtaDescuadreAsiento:String;
              //bTotalDebe:Double; //Monto total de los debidos del asiento
              //bTotalHaber:Double;//Monto Total de los credios del asinto
              //bDescuadreAsiento:Double; // Monto del Descuadre del asiento
              //nUltApunte:Integer; //Ultima linea del asiento
              StrSQL:='';
              StrSQL:='SELECT A.Empresa, A.Serie, A.Asiento,' +
                ' (SELECT SUM (Importe) FROM LINEAS_DIARIO_APUNT AS B' +
                ' WHERE (B.Empresa=A.Empresa) AND (B.Serie=A.Serie) AND (B.Asiento=A.Asiento) AND (B.Factor=1)) AS Debe,' +
                ' (SELECT SUM (Importe) FROM LINEAS_DIARIO_APUNT AS B' +
                ' WHERE (B.Empresa=A.Empresa) AND (B.Serie=A.Serie) AND (B.Asiento=A.Asiento) AND (B.Factor=-1)) AS Haber,' +
                ' (SELECT Max (Apunte)+1 FROM LINEAS_DIARIO_APUNT AS B' +
                ' WHERE (B.Empresa=A.Empresa) AND (B.Serie=A.Serie) AND (B.Asiento=A.Asiento)) AS Apunte ' +
                ' FROM CABECERA_DIARIO AS A' +
                ' WHERE A.Empresa=' +   Chr(39)  + cEmpresa + Chr(39) +
                ' AND A.Serie='     +  Chr(39) + cSerie + Chr(39) +
                ' AND A.Asiento=' + InttoStr(nAsiento) ;

              //Cerramos el data Set
              Rs006.Close;
              //Se Abre el DataSet
              Conectado:= Open_ADO_Qry(Cn002, Rs006,StrSQL,false);
              // Verifica si se Abrio el DataSet
              if Conectado then
              Begin
                with Rs006 do
                begin
                  //Si no es fin de registro
                  if not eof then
                  Begin
                    bTotalDebe:=Fields[3].AsFloat;
                    bTotalHaber:=Fields[4].AsFloat;
                    nUltApunte:=Fields[5].AsInteger;
                  End;
                end;
              End;
              //Cerramos el data Set
              Rs006.Close;

              bDescuadreAsiento:=0;
              nApunte:=nUltApunte;
              cCuenta:=cCtaDescuadreAsiento;
              ncuentareparto :=0;
              //Si los debitos son mayores se debe registrar un Credito para cuadrar el asiento
              if bTotalDebe > bTotalHaber then
              Begin
                bDescuadreAsiento:=  bTotalDebe -bTotalHaber;
                 nFactor:=-1;
              End  ;
              //Si los debitos son mayores se debe registrar un Credito para cuadrar el asiento
              if bTotalDebe < bTotalHaber then
              Begin
                bDescuadreAsiento:=  bTotalHaber-bTotalDebe;
                nFactor:=1;
              End ;

              //Si el asiento esta descuadrado
              if bDescuadreAsiento <> 0 then
              Begin
                //------------------------------------------------------
                //Insertar el asiento en la Base de datos Destino
                //------------------------------------------------------
                //Se convierte a Cadena el importa para poderlo insertar
                cImporte:=FloattoStr(bDescuadreAsiento);
                cImporte:=  StringReplace( cImporte, ',', '.', [rfReplaceAll] ) ;
                StrSQL:='';
                StrSQL:='Insert Into LINEAS_DIARIO_APUNT (Empresa, Serie, Asiento,Apunte,Factor,Cuenta,Importe, ' +
                            'Codigo_Contable,Comentario,Marca,Origen,Serieenlace,cuentareparto, ' +
                            'Numremesa,Marcapunteo,Proyecto,Creadopor,Fechacreacion,Modificadopor,' +
                            ' Fechamodificacion )' +
                            'Values (' + Chr(39) + cEmpresa + Chr(39) +',' +
                            Chr(39) + cSerie + Chr(39) +',' +
                            IntToStr(nAsiento) +',' +
                            IntToStr(nApunte)  +',' +
                            IntToStr(nFactor)  +',' +
                            Chr(39) +cCuenta + Chr(39) +',' +
                            (cImporte) +',' +
                            IntToStr(nCodigo_Contable)  +',' +
                            Chr(39) +cComentario + Chr(39) +',' +
                            Chr(39) +cMarca + Chr(39) +',' +
                            IntToStr(nOrigen)  +',' +
                            Chr(39) +cSerieenlace  + Chr(39) +',' +
                            IntToStr(ncuentareparto)   +',' +
                            IntToStr(nNumremesa)  +',' +
                            Chr(39) +cMarcapunteo + Chr(39) +',' +
                            IntToStr(nProyecto)   +',' +
                            IntToStr(nCreadopor)  +',' +
                            Chr(39) +DateToStr(dFechacreacion) + Chr(39) +',' +
                            IntToStr(nModificadopor)  +',' +
                            Chr(39) +DateToStr(dFechamodificacion)  + Chr(39) +')' ;
                //Se ejecuta el Comando para Insertar el Asiento en la base de datos Destino
                Conectado:=DB_Function(cn002,StrSQL,4);

              End;
              //----------------------------------------------------------------

              //----------------------------------------------------------------
              //Siguiente Registro de la Cabecera de Asientos
              //----------------------------------------------------------------
              Next;
              i:=i+1
              //----------------------------------------------------------------
            end;

            //------------------------------------------------------------------
            //Si el presupuesto sera actualizado
            //------------------------------------------------------------------
            if Actualiza_Presupuesto then
            Begin
              // Se construye los rangos de fecha
              nEjercicio_Contable_Anio:=0;
              nEjercicio_Contable_Mes:=0;
              DecodeDate(dFecha , wAnyo, wMes, wDia );
              nEjercicio_Contable_Anio:=wAnyo;
              nEjercicio_Contable_Mes:=wMes;
              //Se prepara el SQL actualizar
              StrSQL:='';
              StrSQL02:='';
              StrSQL:='SELECT CODIGO_CONTABLE,CUENTAREPARTO,PROYECTO,IMPORTE_ANUAL, ';
              Case nEjercicio_Contable_Mes  of
                1:
                Begin
                  StrSQL02:= ' MES_1, ';
                End;

                2:
                Begin
                  StrSQL02:= ' MES_2, ' ;
                End;

                3:
                Begin
                  StrSQL02:= ' MES_3, ';
                End;


                4:
                Begin
                  StrSQL02:= ' MES_4, ';
                End;


                5:
                Begin
                  StrSQL02:= ' MES_5, ';
                End;

                6:
                Begin
                  StrSQL02:= ' MES_6, ' ;
                End;

                7:
                Begin
                  StrSQL02:= ' MES_7, ' ;
                End;

                8:
                Begin
                  StrSQL02:= ' MES_8, ' ;
                End;

                9:
                Begin
                  StrSQL02:= ' MES_9, ' ;
                End;

                10:
                Begin
                  StrSQL02:= ' MES_10, ' ;
                End;

                11:
                Begin
                  StrSQL02:= ' MES_11, ' ;
                End;

                12:
                Begin
                  StrSQL02:= ' MES_12, ' ;
                End;
              End;
              StrSQL:=StrSQL+ StrSQL02 +' TIPO_CUENTA, ' +
                ' DETALLE_TEXTO1,DETALLE_IMPORTE1,' +
                ' DETALLE_TEXTO2,DETALLE_IMPORTE2,' +
                ' DETALLE_TEXTO3,DETALLE_IMPORTE3,' +
                ' DETALLE_TEXTO4,DETALLE_IMPORTE4, ' +
                ' DETALLE_TEXTO5,DETALLE_IMPORTE5,' +
                ' DETALLE_TEXTO6,DETALLE_IMPORTE6,' +
                ' DETALLE_TEXTO7,DETALLE_IMPORTE7,' +
                ' DETALLE_TEXTO8,DETALLE_IMPORTE8,' +
                ' DETALLE_TEXTO9,DETALLE_IMPORTE9,' +
                ' DETALLE_TEXTO10,DETALLE_IMPORTE10,' +
                ' DETALLE_TEXTO11,DETALLE_IMPORTE11,' +
                ' DETALLE_TEXTO12,DETALLE_IMPORTE12 ' +
                ' FROM PREVISION_CONTABLE ' +
                ' WHERE EMPRESA =' + Chr(39) + 'A' + cHR(39) +
                ' AND SERIE=' + Chr(39) + IntToStr(nEjercicio_Contable_Anio)  + Chr(39) ;
              //Cerramos el DataSet
              Rs003.Close;
              //Se Abre el DataSet
              Conectado:= Open_ADO_Qry(Cn001, Rs003,StrSQL,false);
              // Verifica si se Abrio el DataSet
              if Conectado then
              Begin
                with Rs003 do
                begin
                  if not eof then
                  Begin
                    //Se recorre la tabla de Previsicones Contables
                    While not Eof do
                    Begin
                    //Movemos los valores a las variables de memoria
                      cPR_Codigo_Contable:=Fields[0].AsString;
                      nPR_CuentaReparto:=Fields[1].AsInteger;
                      nPR_Proyecto:=Fields[2].AsInteger;
                      bPR_Importe_Anual:=Fields[3].AsFloat;
                      b_PR_Monto_Mes:=Fields[4].AsFloat;
                      n_RP_Tipo_Cuenta:=Fields[5].AsInteger;
                     //----------------------------------------------------------
                     //Conversionde Numero a caractar del monto,para poderlo insertar
                     //----------------------------------------------------------
                     cImporte01:='';
                     cImporte01:=FloattoStr(b_PR_Monto_Mes);
                     cImporte01:=StringReplace( cImporte01, ',', '.', [rfReplaceAll] ) ;
                     //---------------------------------------------------------

                    //----------------------------------------------------------
                    //Se Busca la cuenta contable en la tabla de presupuesto de la base de datos destino
                    //----------------------------------------------------------
                    StrSQL:='SELECT CODIGO_CONTABLE, ' +
                      ' FROM PREVISION_CONTABLE' +
                      ' WHERE EMPRESA =' + Chr(39) + 'A' + cHR(39) +
                      IntToStr(nEjercicio_Contable_Anio) +
                      ' AND SERIE=' + IntToStr(nEjercicio_Contable_Anio) +
                      ' AND CODIGO_CONTABLE= '+ Chr(39) + cPR_Codigo_Contable + cHR(39) +
                      ' AND CUENTAREPARTO=' + IntToStr(nPR_CuentaReparto) +
                      ' AND PROYECTO=' + IntToStr(nPR_Proyecto) ;
                    Rs004.Close;
                    //Se Abre el DataSet
                    Conectado:= Open_ADO_Qry(Cn002, Rs004,StrSQL,false);
                    // Verifica si se Abrio el DataSet
                    if Conectado then
                    Begin
                      with Rs004 do
                      begin
                        StrSQL:='';
                        StrSQL:='Update PREVISION_CONTABLE ' +
                        ' SET ' + StrSQL02 + '=' + StringReplace(FloattoStr(bPR_Importe_Anual), ',', '.', [rfReplaceAll] )  +
                        ' WHERE EMPRESA =' + Chr(39) + 'A' + cHR(39) +
                         IntToStr(nEjercicio_Contable_Anio) +
                        ' AND SERIE=' + IntToStr(nEjercicio_Contable_Anio) +
                        ' AND CODIGO_CONTABLE= '+ Chr(39) + cPR_Codigo_Contable + cHR(39) +
                        ' AND CUENTAREPARTO=' + IntToStr(nPR_CuentaReparto) +
                        ' AND PROYECTO=' + IntToStr(nPR_Proyecto) ;
                        //Si no existe la cuenta se crea
                        if eof then
                        Begin
                          StrSQL:='';
                          StrSQL:='Inser to PREVISION_CONTABLE (EMPRESA,CODIGO_CONTABLE,CUENTAREPARTO,PROYECTO,IMPORTE_ANUAL,TIPO_CUENTA,' + StrSQL02 +   ') ' +
                          ' Values (' + 'A' + ','  +
                          Chr(39) + cPR_Codigo_Contable + cHR(39) + ',' +
                           IntToStr(nPR_CuentaReparto) + ',' +
                           IntToStr(nPR_Proyecto) + ',' +
                           StringReplace(FloattoStr(bPR_Importe_Anual), ',', '.', [rfReplaceAll] ) + ',' +
                           IntToStr(n_RP_Tipo_Cuenta) + ',' +
                           cImporte01 + ')' ;
                        End;
                      end;
                    End;

                    //Se ejecuta el Comando para actualizar o ingresar el presupuesto
                    Conectado:=DB_Function(cn002,StrSQL,4);


                    //Cerramos el Data Set
                    Rs004.Close;





                    //----------------------------------------------------------





                      //Siguiente Registro
                      Next;
                    End;
                  End;
                end;
              End;
            End;

            //Cerramos los objetos ADO
            Rs003.Close;
            Rs003.Free;
            Rs004.Close;
            Rs004.Free;
            Rs005.Close;
            Rs005.Free;
            Rs006.Close;
            Rs006.Free;

            // Cerrar la Conexiona la Base de datos
            CloseConnection  (cn001, rs001);
            // Cerrar la Conexion a la Base de datos
            CloseConnection  (cn002, rs002);
            //Cerramos los Objetos creados en tiempo de ejecucion
            Frm.Destroy;
            MessageDlg('Proceso Ejecutado.', mtInformation,
              [mbOk], 0, mbOk);
          End
          Else
          Begin
            // Colocamos los datos del archivo en las variables  de Memoria
            MessageDlg('No se econtraron Registros en la fecha indicada'  ,  mterror, [mbok],0);
          End;
        end;
      End;
    End;
 End;

end.
