//------------------------------------------------------------------------------
//Unit que genera los ID de las aplicación creadas por ICG Dominicana
//Se asigna un ID unico por aplicación y Versión, con la finalidad de
//Poder controlar el licenciamiento de la aplicación.
//ICG Dominicana, SRL.
//Rafael Rangel
//Creada:20170115
//Modificada:
//------------------------------------------------------------------------------

unit UnitAppsID;

interface

Uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics,  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls, Vcl.Buttons,IniFiles, ADODB ,Data.DB,WinInet, WinSock,Registry,
  System.UITypes, SeAES256, SeBase64,SeEasyAES, SeSHA256, SeStreams,
  ActiveX  ,ComObj ,JclSysInfo ;










Var

   IniFile : TIniFile;
  //----------------------------------------------------------------------------
  //Función que genera un ID al equipo donde se ejecuta la aplicación
  //----------------------------------------------------------------------------
  function GetIDMachine (TipoID:String):String;
  //Funcion que inddica si existe un formulario  function FrmWindowsExist(aForm: TForm): Boolean;





implementation



//------------------------------------------------------------------------------
//Función que devuelve el ID del Motherboard
//------------------------------------------------------------------------------

function GetIDMachine(TipoID:String):String;
var
  objWMIService : OLEVariant;
  colItems      : OLEVariant;
  colItem       : OLEVariant;
  oEnum         : IEnumvariant;
  iValue        : LongWord;
    Serial:String;
  function GetWMIObject(const objectName: String): IDispatch;
  var
    chEaten: Integer;
    BindCtx: IBindCtx;
    Moniker: IMoniker;

  begin
    OleCheck(CreateBindCtx(0, bindCtx));
    OleCheck(MkParseDisplayName(BindCtx, StringToOleStr(objectName), chEaten, Moniker));
    OleCheck(Moniker.BindToObject(BindCtx, nil, IDispatch, Result));
  end;

begin
  Result:='';
  objWMIService := GetWMIObject('winmgmts:\\localhost\root\cimv2');
 colItems      := objWMIService.ExecQuery('SELECT SerialNumber FROM Win32_BaseBoard','WQL',0);
 // colItems      := objWMIService.ExecQuery('SELECT SerialNumber FROM  ' + TipoID + 'Win32_DiskDrive','WQL',0);
 // colItems      := objWMIService.ExecQuery('SELECT SerialNumber FROM  ' + TipoID + '','WQL',0);

  Serial:= GetVolumeSerialNumber('C');


  oEnum         := IUnknown(colItems._NewEnum) as IEnumVariant;
  if oEnum.Next(1, colItem, iValue) = 0 then
  Result:=VarToStr(colItem.SerialNumber);
end;










end.
