//------------------------------------------------------------------------------
{ Unit que convierte el ID de la maquina encryptado en el ID Real del equipo
ICG Dominicana
Rafael Rangel
Creado: 2017-10-08

el ID del equipo se genera de la siguiente manera.
XXXXXXXX-XXXX-XXXX-XXXX-XXXX-XXXX-XXXX-177401100AEYF19-0281

Primera Cadena: tiene una longitud de 8 d�gitos generados por la libreria Onguard, como identificador del equipo
Segunda Cadena: tiene una longitud de 4 d�gitos que es el c�digo de la aplicaci�n llevada a 4 d�ditos y encryptado con la funci�n Encrypt de la libreria Comunes
Tercer  Cadena: tiene una longitud de 4 d�gitos que es la versi�n de la aplicaci�n llevada a 4 d�ditos y encryptado con la funci�n Encrypt de la libreria Comunes
Cuarta  Cadena: tiene una longitud de 4 d�gitos que es  el primer segmento de la IP del equipo con las siguientes particularidades
  1. Se encrypta la IP la funci�n Encrypt de la libreria Comunes
  2. Se sustituyen los valores ":" que es el cero luego de ecncyptar por el valor cero real.
  3. Se adiciona un numero a la izquierda para llavarlo a tres digitos, se calcula ubicando el dia de la semana + 1
Quinta  Cadena: tiene una longitud de 4 d�gitos que es  el primer segmento de la IP del equipo con las siguientes particularidades
  1. Se encrypta la IP la funci�n Encrypt de la libreria Comunes
  2. Se sustituyen los valores ":" que es el cero luego de ecncyptar por el valor cero real.
  3. Se adiciona un numero a la izquierda para llavarlo a tres digitos, se calcula ubicando el dia de la semana + 1
Sexta Cadena: tiene una longitud de 4 d�gitos que es  el primer segmento de la IP del equipo con las siguientes particularidades
  1. Se encrypta la IP la funci�n Encrypt de la libreria Comunes
  2. Se sustituyen los valores ":" que es el cero luego de ecncyptar por el valor cero real.
  3. Se adiciona un numero a la izquierda para llavarlo a tres digitos, se calcula ubicando el dia de la semana + 1
Septima Cadena: tiene una longitud de 4 d�gitos que es  el primer segmento de la IP del equipo con las siguientes particularidades
  1. Se encrypta la IP la funci�n Encrypt de la libreria Comunes
  2. Se sustituyen los valores ":" que es el cero luego de ecncyptar por el valor cero real.
  3. Se adiciona un numero a la izquierda para llavarlo a tres digitos, se calcula ubicando el dia de la semana + 1
Octava Cadena: tiene una longitud de 15 d�gitos que es  el nombre del equipo con las siguientes particularidades
  1. Se quitan los espacios a la derecha e izquierda al nombre del equipo
  2. Se adiciona al nombre del equipo el caracter '@' para saber donde inicia el nombre del equipo.
  3. Se adiciona ceros a la izquierda para llavarlo a tres digitos.
  4. Se encrypta el nombre del Equipo con funci�n Encrypt de la libreria Comunes
  5. Los d�gitos restantes para completar los 15 digitos es un numero random.

Novena Cadena: tiene una longitud de  4 d�gitos que es los dias del a�o transcurridos hasta el dia de hoy, se rellena con ceros a la izquierda y no ss ecnrypta
--------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
unit UIDLicenciasICGDO;


interface
Uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
 Vcl.Controls,Vcl.Dialogs, DateUtils, System.Math , ADODB, Vcl.Buttons ,
  OnGuard, OgUtil,
  UnitComunesRest,IniFiles,System.JSON,ActiveX;

const
//ICG Dominicana

  CKey : TKey =  ($54,$1F,$7E,$20,$87,$59,$4E,$E2,
                 $4D,$F7,$14,$CE,$3C,$5F,$D0,$BA);



  

      {TKey = ($54,$1F,$7E,$20,$87,$59,$4E,$E2,
                 $4D,$F7,$14,$CE,$3C,$5F,$D0,$BA);
//LCDS
{  CKey : TKey = ($D9,$C5,$00,$66,$7C,$4D,$16,$8B,$A5,$7E,$2F,$85,$26,$14,$C1,$6E); }

//-----------------------------------------------------------------------------
//Tipos de Registros  de Parametros Generales
//------------------------------------------------------------------------------
type TParametrosApps   = record
   IP,ServerDB,NameDB,UsersDB,PasswordDB,NameDBGestion:String;
   Puerto,GeneraLogErrores,GeneraLogDataRecibida,Servicio:Integer;
end;



Var
  IniFile : TIniFile;
  //Objetos de tipo ADO
  Cn001: TADOConnection; //Conexion a la Base de datos  de Gesti�n
  Rs001: TADOQuery; //Conexi�n a la base de datos

  //Variables Tipo String
  MensajeError,
  Mensaje,
  RutaApps,
  FileINI,
  StrSQL01,
  StrSQL02,
  StrSQL03,
  NombreApps :String;
  //Variables Tipo Boleanas
  Conectado,
  LogData,
  LogError:Boolean;
  //Tipos de registro
  InfoApps:TParametrosApps;


  //----------------------------------------------------------------------------
  //Funci�n que devuelve el ID real de la m�quina.
  //----------------------------------------------------------------------------
  function ICGDO_IDMachineReal(Fecha:TDateTime;IDMachine:String):String;

  //----------------------------------------------------------------------------
  //Funci�n que Genera el ID encryptado de la maquina
  //----------------------------------------------------------------------------
  function ICGDO_GenerarIDMachine(IDMachine,IPPC,NombrePC:String;CodApps,CodVersion:Integer):String;

  //----------------------------------------------------------------------------
  //Funci�n que Extrae del ID del equipo la IP o el Nombre del equipo
  //----------------------------------------------------------------------------
  function ICGDO_Extraer_Info_IDMachine(IDMachine:String;TipoRetorno:Integer):String;

  //----------------------------------------------------------------------------
  //Funci�n que Genera el C�digo de activaci�n de la licencia
  //----------------------------------------------------------------------------
  function ICGDO_ActivarLicencia(IDMachine:String;IdLicencia,IdUsers:Integer;InfoApps:TParametrosApps):TJSONObject;

  //----------------------------------------------------------------------------
  //Funci�n que Cierra la conexi�n a la base de datos
  //----------------------------------------------------------------------------
  function ICGDO_CloseDB:Boolean;



  //----------------------------------------------------------------------------
  //Funci�n que Genera el C�digo de activaci�n de la licencia
  //----------------------------------------------------------------------------
   procedure LogApps(TipoLog:Integer;Data:String;Parametros:TParametrosApps);



  implementation


//----------------------------------------------------------------------------
//Funci�n que devuelve el ID real de la m�quina.
//----------------------------------------------------------------------------
function ICGDO_IDMachineReal(Fecha:TDateTime;IDMachine:String):String;
Var

  ValorIDMachineReal:String;
  StrListIDMachine:TStringList;
  LI,NEquipo:longint;
  Relleno,
  EquipoNombre,
  TmpIP,
  Delim,
  Segmento,
  LimiteInferior,
  LimiteSuperior,
  DiasAnio:String;
  Caracter:Char;
  RangoIP: TStringList;
  i,InicioNombre,Largo,DifLargo:Integer;
  C01_Id,
  C02_Apps,
  C03_Version,
  C04_Ip01,
  C05_Ip02,
  C06_Ip03,
  C07_Ip04,
  C08_MachineName,
  C09_DiasANio:String;


Begin

  Try
    ValorIDMachineReal:='';
    C01_Id:='';
    C02_Apps:='';
    C03_Version:='';
    C04_Ip01:='';
    C05_Ip02:='';
    C06_Ip03:='';
    C07_Ip04:='';
    C08_MachineName:='';
    C09_DiasANio:='';

    //--------------------------------------------------------------------------
    //Se coloca el ID de la maquina en un String list para obtener cada campo
    //--------------------------------------------------------------------------
    StrListIDMachine:=TStringList.Create;
    IDMachine:=(stringreplace(IDMachine,' ','',  [rfReplaceAll, rfIgnoreCase]));
    StrListIDMachine.Delimiter := '-';        // Each list item will be blank separated
    StrListIDMachine.DelimitedText := IDMachine;//


    //Se verifica la longitud del ID de la Maquina.
    if (Length(IDMachine) <51) or  (StrListIDMachine.Count-1 <> 8)  Then
    Begin
      Result:=ValorIDMachineReal;
      Exit;
    End;
    C01_Id:=StrListIDMachine[0];
    C02_Apps:=Decrypt(StrListIDMachine[1]);
    C03_Version:=Decrypt(StrListIDMachine[2]);
    //Omitimos el primer Caracter
    C04_Ip01:=Copy(StrListIDMachine[3],2,3);
    C05_Ip02:=Copy(StrListIDMachine[4],2,3);
    C06_Ip03:=Copy(StrListIDMachine[5],2,3);
    C07_Ip04:=Copy(StrListIDMachine[6],2,3);
    Relleno:=Copy(StrListIDMachine[3],1,1);

    C08_MachineName:=StrListIDMachine[7];
    C08_MachineName:=stringreplace(C08_MachineName,'A','@',  [rfReplaceAll, rfIgnoreCase]);
    C09_DiasANio:=StrListIDMachine[8];

    //se libera de memoria el Objeto
    StrListIDMachine.Free;

    //Se onbtiene el dia de la semana cuando se esta generando la clave y se le suma un dia
    //Relleno:=IntToSTR(DayOfTheWeek(Fecha));
    //se Convierte a tipo Char
    Caracter:=Relleno[1];

    //Convertimos los ceros en ":" , le adicionamos el caracter de relleno y se desencrypta
    C04_Ip01:=(Caracter) + Decrypt(stringreplace(C04_Ip01,'0','1',  [rfReplaceAll, rfIgnoreCase]));
    C05_Ip02:=(Caracter) + Decrypt(stringreplace(C05_Ip02,'0','1',  [rfReplaceAll, rfIgnoreCase]));
    C06_Ip03:=(Caracter) + Decrypt(stringreplace(C06_Ip03,'0','1',  [rfReplaceAll, rfIgnoreCase]));
    C07_Ip04:=(Caracter) + Decrypt(stringreplace(C07_Ip04,'0','1',  [rfReplaceAll, rfIgnoreCase]));


    InicioNombre:=0;
    InicioNombre:=AnsiPos('@',C08_MachineName);
    //si no se ubica el caracter, el nombre de la maquina es completo
    if InicioNombre=0
      then
          C08_MachineName:=Decrypt(C08_MachineName)
      Else
      Begin
        C08_MachineName:=Copy(C08_MachineName,1,InicioNombre) + Decrypt(Copy(C08_MachineName,InicioNombre+1,15-InicioNombre+1))
      End;

    //se arma el ID de la Maquina Real
    ValorIDMachineReal:=C01_Id +'-' + C02_Apps  + '-'+ C03_Version + '-' + C04_Ip01 + '-' + C05_Ip02 + '-' + C06_Ip03 + '-' + C07_Ip04 + '-' + C08_MachineName  + '-' +C09_DiasANio;
    //Se retorna el ID real del equipo
    Result:=ValorIDMachineReal;
   except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
End;


//----------------------------------------------------------------------------
//Funci�n que Generael ID real de la m�quina.
//----------------------------------------------------------------------------
function  ICGDO_GenerarIDMachine(IDMachine,IPPC,NombrePC:String;CodApps,CodVersion:Integer):String;
var
  NEquipo:longint;
  ValorIDMachine,
  Relleno,
  EquipoNombre,
  Segmento,
  LimiteInferior,
  LimiteSuperior,
  DiasAnio:String;
  Caracter:Char;
  RangoIP: TStringList;
  i,Largo,DifLargo:Integer;
begin

  Try

    ValorIDMachine:='';
    //Se onbtiene el dia de la semana cuando se esta generando la clave y se le suma un dia
    Relleno:=IntToSTR(DayOfTheWeek(Date)+1);
    //se Convierte a tipo Char
    Caracter:=Relleno[1];

    //la IP del equipo se convierte en un string list para recorrerla
    RangoIP:=TStringList.Create;
    IPPC:=(stringreplace(IPPC,'.','|',  [rfReplaceAll, rfIgnoreCase]));
    IPPC:=(stringreplace(IPPC,' ','',  [rfReplaceAll, rfIgnoreCase]));
    RangoIP.Delimiter := '|';        // Each list item will be blank separated
    RangoIP.DelimitedText := IPPC;//

    IPPC:='';
    //Se recorre le String List para tomar cada segmento IP
    for i := 0 to RangoIP.Count-1 do
    Begin
      //Se encrypta el Segmento de la IP
      Segmento:=Encrypt (RangoIP[i]);
      //se remplaza el caracter ': 'por el cero
      Segmento:=(stringreplace(Segmento,':','0',  [rfReplaceAll, rfIgnoreCase]));
      //si es de menos de tres digitos, se le agrega cero a la izquierda
      if Length(RangoIP[i]) < 3 then
      Begin
        //se agrega un cero a la izquierda
        Segmento:=StringOfChar('0',3 - Length(Segmento))+Segmento;
      End;

      //se arma la IP
      IPPC:=IPPC+ (StringOfChar(Caracter,4 - Length(Segmento))+Segmento)+'-'  ;
    End;
    //Se libera la lista de caractares de la IP
    RangoIP.Free;
    //Se coloca el ID Generado por ON guard
    ValorIDMachine:=IDMachine;
    //se le agrega al Identificador el c�digo de la aplicaci�n y la versi�n
    ValorIDMachine := ValorIDMachine + '-' + Encrypt(FormatFloat('0000', CodApps)) + '-' + Encrypt(FormatFloat('0000', CodVersion));
    //Se le agregan al Identificador la IP del Equipo
    ValorIDMachine := ValorIDMachine + '-' + (IPPC);
     //se quitan lo espacios al nombre del equipo
    EquipoNombre:=Trim(NombrePC);
    Largo:=Length(EquipoNombre);
    if Length(EquipoNombre) <15 Then
    Begin
      //Se agrega el caracter que indica donde inicia el nombre del equipo
      EquipoNombre:=Encrypt('@' + EquipoNombre);
      Largo:=Length(EquipoNombre);
      DifLargo:=15-Largo;
      if DifLargo > 0 then
      Begin
        LimiteInferior:='1'+StringOfChar('0',DifLargo-1);
        LimiteSuperior:='9'+StringOfChar('9',DifLargo-1);
        Randomize;
        NEquipo:=(RandomRange(strToInt(LimiteInferior),StrToInt(LimiteSuperior)  )) ;
        EquipoNombre:=InTToSTR(NEquipo)+EquipoNombre;
        EquipoNombre:=(StringOfChar('0',DifLargo - Length(EquipoNombre))+EquipoNombre)
      End;
    End
    Else
    Begin
      EquipoNombre:=Encrypt(EquipoNombre);
    End;


    //Se calculan los dias Transcurridos del a�o
    DiasAnio:=FormatFloat('0000',(fechaJuliana (date)));
    //Se arma la variabla del ID de la maquina
    ValorIDMachine := ValorIDMachine  + EquipoNombre + '-' + DiasAnio;
    //Se retorna el ID del equipo Calculado
   // WriteLogApps(3, 'ID Machine Real (funcion 06) ====> : ' + DateTimeToStr(now)  + '  , ' + ValorIDMachine  ,1,1);


    Result:=ValorIDMachine;
  except
     Begin
      // Se guarda el error en el log
      Mensaje:=MensajeError;
      LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoApps);
      LogError:=True;
    End;
  end;
End;

//----------------------------------------------------------------------------
//Funci�n que Extrae del ID del equipo la IP
//----------------------------------------------------------------------------
function ICGDO_Extraer_Info_IDMachine(IDMachine:String;TipoRetorno:Integer):String;
Var
  ValorRetorno:String;
  StrListIDMachine:TStringList;
  LI,NEquipo:longint;
  Relleno,
  EquipoNombre,
  TmpIP,
  Delim,
  Segmento,
  RangoIP: TStringList;
  i,InicioNombre,Largo,DifLargo:Integer;

  C01_Id,
  C02_Apps,
  C03_Version,
  C04_Ip01,
  C05_Ip02,
  C06_Ip03,
  C07_Ip04,
  C08_MachineName,TmpIDMachine,TmpMachineName:String;
Begin

  Try
    ValorRetorno:='';
    C01_Id:='';
    C02_Apps:='';
    C03_Version:='';
    C04_Ip01:='';
    C05_Ip02:='';
    C06_Ip03:='';
    C07_Ip04:='';
    C08_MachineName:='';
    TmpIDMachine:='';
    TmpMachineName:='';
    //--------------------------------------------------------------------------
    //Se coloca el ID de la maquina en un String list para obtener cada campo
    //--------------------------------------------------------------------------
    TmpIDMachine:=IDMachine ;
    TmpMachineName:=Copy(IDMachine,40,15);
    TmpMachineName:=(stringreplace(TmpMachineName,'-','_',  [rfReplaceAll, rfIgnoreCase]));
    IDMachine:=Copy(TmpIDMachine,1,39)+ TmpMachineName +Copy(TmpIDMachine,55,4);
    StrListIDMachine:=TStringList.Create;
    IDMachine:=(stringreplace(IDMachine,' ','',  [rfReplaceAll, rfIgnoreCase]));
    StrListIDMachine.Delimiter := '-';        // Each list item will be blank separated
    StrListIDMachine.DelimitedText := IDMachine;//

    //Se verifica la longitud del ID de la Maquina.
    if (Length(IDMachine) <51) or  (StrListIDMachine.Count-1 <> 8)  Then
    Begin
      Result:=ValorRetorno;
      Exit;
    End;


    C01_Id:=StrListIDMachine[0];
    C02_Apps:=Decrypt(StrListIDMachine[1]);
    C03_Version:=Decrypt(StrListIDMachine[2]);
    //Omitimos el primer Caracter
    C04_Ip01:=Copy(StrListIDMachine[3],2,3);
    C05_Ip02:=Copy(StrListIDMachine[4],2,3);
    C06_Ip03:=Copy(StrListIDMachine[5],2,3);
    C07_Ip04:=Copy(StrListIDMachine[6],2,3);

    C08_MachineName:=StrListIDMachine[7];
    C08_MachineName:=stringreplace(C08_MachineName,'A','@',  [rfReplaceAll, rfIgnoreCase]);


    //se libera de memoria el Objeto
    StrListIDMachine.Free;
    //Convertimos los ceros en ":" , le adicionamos el caracter de relleno y se desencrypta
    C04_Ip01:= Decrypt(stringreplace(C04_Ip01,'0','1',  [rfReplaceAll, rfIgnoreCase]));
    C05_Ip02:= Decrypt(stringreplace(C05_Ip02,'0','1',  [rfReplaceAll, rfIgnoreCase]));
    C06_Ip03:= Decrypt(stringreplace(C06_Ip03,'0','1',  [rfReplaceAll, rfIgnoreCase]));
    C07_Ip04:= Decrypt(stringreplace(C07_Ip04,'0','1',  [rfReplaceAll, rfIgnoreCase]));
    InicioNombre:=0;
    InicioNombre:=AnsiPos('@',C08_MachineName);
    //si no se ubica el caracter, el nombre de la maquina es completo
    if InicioNombre=0
      then
          C08_MachineName:=Decrypt(C08_MachineName)
      Else
      Begin
        C08_MachineName:=Copy(C08_MachineName,1,InicioNombre) + Decrypt(Copy(C08_MachineName,InicioNombre+1,15-InicioNombre+1))
      End;

    //se arma la informaci�n de retorno
    Case TipoRetorno  of
      1 :  //Se retorna la IP del Equipo
      Begin
         ValorRetorno:= C04_Ip01 + '.' + C05_Ip02 + '.' + C06_Ip03 + '.' + C07_Ip04;
      End;
      2:   //Se retorna el nombre del equipo
      Begin
          ValorRetorno:=(Copy(C08_MachineName,InicioNombre+1,15-InicioNombre+1))
      End;
      3:   //Se retorna la Versi�n de la apps
      Begin
          ValorRetorno:=(C03_Version)  ;
      End;
      4:   //Se retorna el C�digo de la aplicaci�n
      Begin
          ValorRetorno:=(C02_Apps)
      End;
    End;

    //Se retorna la informaci�n solicitada
    Result:=ValorRetorno;
   except
     Begin
      // Se guarda el error en el log
      Mensaje:=MensajeError;
      LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoApps);
      LogError:=True;
    End;
  end;
End;


//------------------------------------------------------------------------------
//Activar la licencia
//------------------------------------------------------------------------------
function ICGDO_ActivarLicencia(IDMachine:String;IdLicencia,IdUsers:Integer;InfoApps:TParametrosApps):TJSONObject;
var
  Work : TCode;
  IdMachineReal,IdEquipo,IdSerial,IP,NombrePC,Resultado,
  IDActivacion,InfoRespuesta,StrVersionApps,StrIdApps,TmpIdMachine,
  TmpNombrePC,TmpHardware:String;
  Modifier: longint;
  ModKey: TKey;
  FExpiraLicenciaAnt,FExpiraLicencia:TDateTime;
  MDate:Tdate;
  LicActiva,fueActivada:Boolean;
  JSON: TJSONObject;
  DiasLicencia,InfoError,IDApps,VersionApps,HardwareLiberado:Integer;

begin

  Try

    //Se crea el objeto Json
    JSON:=TJSONObject.Create;
    MensajeError:='';
    IdActivacion:='';
    InfoError:=0;

    //Se registra la petici�n
    LogApps(3, 'Solicitud de activaci�n de licencia ====> : ' + DateTimeToStr(now)  + '  , ' + IDMachine + ' Licencia: ' + IntTostr(IdLicencia) ,InfoApps);

    //Se verifica la longitud del ID de la Maquina.
    if (Length(IDMachine) <>59)  Then
    Begin
      InfoError:=400;
      InfoRespuesta:='El ID del equipo no cumple con la longitud requerida';
      //se construye el Objeto Json con los datos
      JSON.AddPair('IDActivacion',TJSONString.Create(IDActivacion));
      JSON.AddPair('InfoError',TJSONNumber.Create(InfoError));
      JSON.AddPair('InfoRespuesta',TJSONString.Create(InfoRespuesta));
      LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + InfoRespuesta  + ' ' + IDMachine,InfoApps);
      Result:=JSON;
      Exit;
    End;

    //Se obtiene la versi�n y c�digo de la aplicaci�n
    VersionApps:=0;
    IdApps:=0 ;
    StrVersionApps:=(ICGDO_Extraer_Info_IDMachine(IDMachine,3));
    StrIdApps:=(ICGDO_Extraer_Info_IDMachine(IDMachine,4)) ;

    if Length(StrVersionApps)=4 then VersionApps:=StrToInt(ICGDO_Extraer_Info_IDMachine(IDMachine,3));
    if Length( StrIdApps)= 4 Then IdApps:=StrToInt(ICGDO_Extraer_Info_IDMachine(IDMachine,4)) ;
    //--------------------------------------------------------------------------
    //Verificar si existe la Licencia
    //--------------------------------------------------------------------------
    //Define los Objetos ADO
    //--------------------------------------------------------------------------
    //Se abre el hilo de ejecuci�n
    CoInitialize(nil) ;
    Cn001  := TADOConnection.Create(nil);
    Rs001:= TADOQuery.Create(nil);
    //--------------------------------------------------------------------------
    //Conexi�n a la Base de datos
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    Conectado:=Open_DB (Cn001 , 2 , InfoApps.ServerDB,InfoApps.NameDB, InfoApps.UsersDB, InfoApps.PasswordDB, '', MensajeError ,False );
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    //si hay conexi�n con el la base de datos, se continua la ejecuci�n del programa
    //--------------------------------------------------------------------------
    if Conectado= False then
    //si no hay conexi�n a la base de datos
    Begin
      InfoError:=500;
      InfoRespuesta:='No hay conexi�n a la base de datos ' + MensajeError;
      //se construye el Objeto Json con los datos
      JSON.AddPair('IDActivacion',TJSONString.Create(IDActivacion));
      JSON.AddPair('InfoError',TJSONNumber.Create(InfoError));
      JSON.AddPair('InfoRespuesta',TJSONString.Create(InfoRespuesta));
      LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + MensajeError + ' ' +infoapps.PasswordDB ,InfoApps);
      Result:=JSON;
      //Se cierra el hilo de ejecuci�n
      CoUninitialize;
      //se cierra la conexi�n a la base de datos
      ICGDO_CloseDB;
      Exit;
    End;

    //------------------------------------------------------------------------
    //Se Genera el SQL para Verificar si existe la licencia
    //------------------------------------------------------------------------
    StrSQL01:='SELECT A.EXPIRACION, A.ACTIVO,(CASE WHEN (A.ESDEMO)=1 THEN 30 ELSE  B.DIAS_VALIDACION END) DIAS_VIGENCIA ' +
    ' FROM ICGDO_TB_LICENCIAS AS A WITH (NOLOCK) INNER JOIN ICGDO_TB_APLICACIONES AS B WITH (NOLOCK) ON ' +
    ' B.IDAPPS=A.IDAPPS AND B.VERSION=A.VERSION' +
    ' WHERE A.IDLICE=' +  IntTostr(IdLicencia) +
    ' AND A.IDAPPS=' + InTtoStr(Idapps) +
    ' AND A.VERSION=' + IntToStr(VersionApps);


    //------------------------------------------------------------------------
    //Se Verficca si existe la licencia y corresponde con la aplicaci�n y versi�n.
    //------------------------------------------------------------------------
    Conectado:= Open_ADO_Qry(Cn001,rs001,StrSQL01,MensajeError,True,False);
    //--------------------------------------------------------------------------
    //si hay conexion con el la base de datos, se continua la ejecucion del programa
    //--------------------------------------------------------------------------
    if Conectado= False then
    //si no hay conexi�n a la base de datos
    Begin
      InfoError:=500;
      InfoRespuesta:='No hay conexi�n con la tabla de las licencias';
      //se construye el Objeto Json con los datos
      JSON.AddPair('IDActivacion',TJSONString.Create(IDActivacion));
      JSON.AddPair('InfoError',TJSONNumber.Create(InfoError));
      JSON.AddPair('InfoRespuesta',TJSONString.Create(InfoRespuesta));
      LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + MensajeError ,InfoApps);
      Result:=JSON;
      CoUninitialize;
      //se cierra la conexi�n a la base de datos
      ICGDO_CloseDB;
      Exit;
    End;

    //Se verifica si existe la licencia para esta aplicaciones y versi�n
    if rs001.RecordCount = 0 then
    Begin
      InfoError:=400;
      InfoRespuesta:='No existe la licencia para la aplicaci�n y/0 versi�n indicada';
      //se construye el Objeto Json con los datos
      JSON.AddPair('IDActivacion',TJSONString.Create(IDActivacion));
      JSON.AddPair('InfoError',TJSONNumber.Create(InfoError));
      JSON.AddPair('InfoRespuesta',TJSONString.Create(InfoRespuesta));
      LogApps(3, 'Data Recibida ====> : ' + DateTimeToStr(now)  + '  , ' + MensajeError ,InfoApps);
      Result:=JSON;
      CoUninitialize;
      //se cierra la conexi�n a la base de datos
      ICGDO_CloseDB;
      Exit;
    End;

    //si la licencia existe
    LicActiva:=False;
    DiasLicencia:=0;


    If not rs001.eof then
    Begin
      //Tomamos los datos de la tabla de licencias
      FExpiraLicencia:=rs001.Fields[0].AsDateTime;
      LicActiva:=rs001.Fields[1].AsBoolean;
      DiasLicencia:=rs001.Fields[2].AsInteger;

    End;

    //Si la licencia no esta activa
    If LicActiva=False Then
    Begin
      InfoRespuesta:='La licencia esta bloqueada';
      InfoError:=400;
      //se construye el Objeto Json con los datos
      JSON.AddPair('IDActivacion',TJSONString.Create(IDActivacion));
      JSON.AddPair('InfoError',TJSONNumber.Create(InfoError));
      JSON.AddPair('InfoRespuesta',TJSONString.Create(InfoRespuesta));
      LogApps(3, 'Data Recibida ====> : ' + DateTimeToStr(now)  + '  , ' + MensajeError ,InfoApps);
      Result:=JSON;
      CoUninitialize;
      //se cierra la conexi�n a la base de datos
      ICGDO_CloseDB;
      Exit;
    End;

    //se verifica si la licencia esta vigente, en el caso de no estarlo y siempre y cuando no
    //este bloqueda se procede a colocarla nuevamente vigente.
     MDate:=IncDay(FExpiraLicencia, -8);
    if (FExpiraLicencia <= Now) or (Mdate <= Date) then
    Begin
      //se calcula la nueva fecha de expiraci�n
      FExpiraLicenciaAnt:=FExpiraLicencia;
      FExpiraLicencia:=IncDay(Now,DiasLicencia);
      //Actualiza la tabla de licencias
      StrSql01:='';
      StrSQL01:='UPDATE ICGDO_TB_LICENCIAS WITH (ROWLOCK) SET EXPIRACION=DATEADD (dd ,' + intToStr(DiasLicencia) + ' , GETDATE() )' +
        ' WHERE IDLICE=' +  IntTostr(IdLicencia) +
        ' AND IDAPPS=' + InTtoStr(Idapps) +
        ' AND VERSION=' + IntToStr(VersionApps);
      //------------------------------------------------------------------------
      //Se ejecuta el SQL
      //------------------------------------------------------------------------
      Conectado:= DB_Function(Cn001,StrSQL01,MensajeError,4,False);
      //------------------------------------------------------------------------


      //--------------------------------------------------------------------------
      // //Se registra en el seguimiento la licencia que se cambio la vigencia
      //--------------------------------------------------------------------------
      StrSQL01:='INSERT INTO ICGDO_TB_SEGUIMIENTO WITH (ROWLOCK) (IDLICE, IDAPPS, VERSION, IDINCI, DESCRIPCION, ID_USER,ID_HARDWARE, IP, PCNOMBRE,VALOR_ANTERIOR, VALOR_NUEVO)' +
      ' VALUES ('  +
      QuotedStr(IntTostr(IdLicencia)) +  ',' +
      IntToSTR(IdApps) + ',' +
      IntToSTR(VersionApps) + ',20,' + QuotedSTR('Licencia vigente hasta el : ' + DatetoStr(FExpiraLicencia)) + ',' +
      IntToStr(IdUsers) +  ',' +
      QuotedStr(IdMachineReal) +  ',' +
      QuotedStr(IP) +  ',' +
      QuotedStr(NombrePC) +  ',' +
      QuotedStr(DateToStr(FExpiraLicenciaAnt)) +  ',' +
      QuotedStr(DateToStr(FExpiraLicencia)) + ' ) ';
      //------------------------------------------------------------------------
      //Se ejecuta el SQL
      //------------------------------------------------------------------------
      Conectado:= DB_Function(Cn001,StrSQL01,MensajeError,4,False);
      //------------------------------------------------------------------------

      //Se verifica si la activaaci�n de la licencia fue satisfatorio
      if Conectado
        then
          LogApps(3, 'Vigencia de fecha de licencia guardada satisfactoriamente en la base de datos ====> : ' + DateTimeToStr(now)  + '  , ' + IdActivacion ,InfoApps)
        Else
          LogApps(2, 'Error al guardar en la base de datos la actualizaci�n de la fecha de vigencia de la licencia ====> : ' + DateTimeToStr(now)  + '  , ' + IdActivacion ,InfoApps);

    End;


    //---------------------------------------------------------------------------
    //Se obtiene los datos reales de IP y nombre del equipo
    //--------------------------------------------------------------------------
    TmpIdMachine:=IDMachine;
    Ip:=ICGDO_Extraer_Info_IDMachine(TmpIdMachine,1);
    NombrePC:=ICGDO_Extraer_Info_IDMachine(TmpIdMachine,2);
    Rs001.Close;
     //----------------------------------------------------------------------------
    //Se Genera el SQL para Verificar si La licencia se activo en otro equipo
    //------------------------------------------------------------------------
    StrSQL01:='SELECT TOP(1) PCNOMBRE, FECHA,' +
      ' (CASE WHEN A.FECHA < ISNULL((SELECT TOP(1) B.FECHA FROM ICGDO_TB_SEGUIMIENTO AS B WITH (NOLOCK) ' +
      ' WHERE B.IDINCI=80   ' +
      ' AND B.IDLICE=A.IDLICE     ' +
      ' ORDER BY FECHA DESC),CONVERT(DATETIME, ''1900-12-31 00:00:00'',102)) THEN 1 ELSE 0 END)AS HARDWARELIBERADO, A.ID_HARDWARE ' +
      ' FROM ICGDO_TB_SEGUIMIENTO AS A WITH (NOLOCK)     ' +
      ' WHERE IDINCI=40 ' +
      ' AND IDLICE=' +  IntTostr(IdLicencia) +
   //   ' AND PCNOMBRE <> ' + QuotedStr(NombrePC)  +
      ' ORDER BY FECHA DESC ' ;

    LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + StrSQL01,InfoApps);


    //------------------------------------------------------------------------
    //Se Verficca si existe la licencia y corresponde con la aplicaci�n y versi�n.
    //------------------------------------------------------------------------
    Conectado:= Open_ADO_Qry(Cn001,rs001,StrSQL01,MensajeError,True,False);
    //--------------------------------------------------------------------------
    //si hay conexion con el la base de datos, se continua la ejecucion del programa
    //--------------------------------------------------------------------------
    if Conectado= False then
    //si no hay conexi�n a la base de datos
    Begin
      InfoError:=500;
      InfoRespuesta:='No hay conexi�n con la tabla de seguimiento de licencias';
      //se construye el Objeto Json con los datos
      JSON.AddPair('IDActivacion',TJSONString.Create(IDActivacion));
      JSON.AddPair('InfoError',TJSONNumber.Create(InfoError));
      JSON.AddPair('InfoRespuesta',TJSONString.Create(InfoRespuesta));
      LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + MensajeError ,InfoApps);
      Result:=JSON;
      CoUninitialize;
      //se cierra la conexi�n a la base de datos
      ICGDO_CloseDB;
      Exit;
    End;

    //se verifica si la licencia fue activada en otro equipo
    fueActivada:=False;
    HardwareLiberado:=0;

    If not rs001.eof then
    Begin
      //Tomamos los datos de la tabla de seguimiento de licencias
      TmpNombrePC:=rs001.Fields[0].AsString;
      HardwareLiberado:=rs001.Fields[2].AsInteger;
      TmpHardware:=rs001.Fields[3].AsString;
      //se valida que no esta activado en otro equipo

      if (TmpNombrePC=NombrePC) and (Copy(TmpHardware,1,8)= Copy(IDMachine,1,8))
        then
          fueActivada:=False
        Else
          if (HardwareLiberado=0 ) then fueActivada:=True;
    End;


    LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' +  TmpNombrePC + ' ' +   TmpHardware ,InfoApps);


    //Si la licencia se activo anteriormente se devuelve un error
    If fueActivada=True Then
    Begin
      InfoError:=300;
      InfoRespuesta:=', ya fue activada anteriormente en otro equipo, contacte a su proveedor';
      //se construye el Objeto Json con los datos
      JSON.AddPair('IDActivacion',TJSONString.Create(IDActivacion));
      JSON.AddPair('InfoError',TJSONNumber.Create(InfoError));
      JSON.AddPair('InfoRespuesta',TJSONString.Create(InfoRespuesta));
      LogApps(3, 'Data Recibida ====> : ' + DateTimeToStr(now)  + '  , ' + MensajeError ,InfoApps);
      Result:=JSON;
      CoUninitialize;
      //se cierra la conexi�n a la base de datos
      ICGDO_CloseDB;
      Exit;
    End;




    //----------------------------------------------------------------------------
    //Se procede a la activaci�n de la licencia
    //----------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //se obtiene el nombre el Id Real del equipo
    //--------------------------------------------------------------------------
    IdMachineReal:=ICGDO_IDMachineReal(Now,IDMachine);


    {set default key to use}
    //Se asigna el valor para generar la llave
    ModKey:=Ckey;
    //se Obtiene un Long int en funci�n del IdMachine
    Modifier:=StringHashELF(IdMachineReal);
    ApplyModifierToKeyPrim(Modifier, ModKey, SizeOf(ModKey));
    Idequipo:=BufferToHex(Modifier, SizeOf(Modifier));
    InitSerialNumberCode(ModKey, (IdLicencia), FExpiraLicencia, Work);
    IdActivacion:=BufferToHex(Work, SizeOf(Work));

    //se guarda en el log la informaci�n recibida.
    LogApps(3, 'Modifier ====> : ' + DateTimeToStr(now)  + '  , ' +  Idequipo ,InfoApps);
    LogApps(3, 'Clave Generada ====> : ' + DateTimeToStr(now)  + '  , ' + IdActivacion ,InfoApps);
    LogApps(3, 'Fecha Expiraci�n ====> : ' + DateTimeToStr(now)  + '  , ' + DatetoSTR(FExpiraLicencia) ,InfoApps);

    //se arma el Json a Devolver

    InfoError:=0;
    InfoRespuesta:='Clave de activaci�n generada correctamente';
    //se construye el Objeto Json con los datos
    JSON.AddPair('IDActivacion',TJSONString.Create(IDActivacion));
    JSON.AddPair('InfoError',TJSONNumber.Create(InfoError));
    JSON.AddPair('InfoRespuesta',TJSONString.Create(InfoRespuesta));

    //Datos del equipo
    Ip:=ICGDO_Extraer_Info_IDMachine(IDMachine,1);
    NombrePC:=ICGDO_Extraer_Info_IDMachine(IDMachine,2);

    //--------------------------------------------------------------------------
    //Insertar en la tabla de seguimiento la activaci�n de la licencia.
    //--------------------------------------------------------------------------
    StrSQL01:='INSERT INTO ICGDO_TB_SEGUIMIENTO WITH (ROWLOCK) (IDLICE, IDAPPS, VERSION, IDINCI, DESCRIPCION, ID_USER,ID_HARDWARE, IP, PCNOMBRE)' +
    ' VALUES ('  +
    QuotedStr(IntTostr(IdLicencia)) +  ',' +
    IntToSTR(IdApps) + ',' +
    IntToSTR(VersionApps) + ',40,' + QuotedSTR('Licencia activada') + ',' +
    IntToStr(IdUsers) +  ',' +
    QuotedStr(IdMachineReal) +  ',' +
    QuotedStr(IP) +  ',' +
    QuotedStr(NombrePC) +  ' ) ';

    //------------------------------------------------------------------------
    //Se ejecuta el SQL
    //------------------------------------------------------------------------
    Conectado:= DB_Function(Cn001,StrSQL01,MensajeError,4,False);
    //------------------------------------------------------------------------

    //Se verifica si la activaaci�n de la licencia fue satisfatorio
    if Conectado
      then
        LogApps(3, 'Activaci�n de licencia guardada satisfactoriamente en la base de datos ====> : ' + DateTimeToStr(now)  + '  , ' + IdActivacion ,InfoApps)
      Else
        LogApps(2, 'Error al guardar en la base de datos la activaci�n de la licencia ====> : ' + DateTimeToStr(now)  + '  , ' + IdActivacion ,InfoApps);

    //----------------------------------------------------------------------------
    //Cierra el Objeto ADO  y libera la memoria
    //----------------------------------------------------------------------------
     CoUninitialize;
    //se cierra la conexi�n a la base de datos
    ICGDO_CloseDB;
    //Retorno de la informaci�n.
    Result:=JSON;
   Except On E:Exception do
     Begin
      // Se guarda el error en el log
      Mensaje:=E.Message;
      LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoApps);
      LogError:=True;
      JSON.AddPair('IDActivacion',TJSONString.Create(''));
      JSON.AddPair('InfoError',TJSONNumber.Create('500'));
      JSON.AddPair('InfoRespuesta',TJSONString.Create(Mensaje));
      Result:=JSON;
      Exit;
    End;
  end;



end;


//----------------------------------------------------------------------------
//Se Genera el Archivo Log de la aplicaci�n
//-----------------------------------------------------------------------------
procedure LogApps(TipoLog:Integer;Data:String;Parametros:TParametrosApps);
Var
  fichero : TStringList;
  FicheroLog,NombreFichero,Carpeta,RutaLog:String;
begin

  //----------------------------------------------------------------------------
  //Se Verifica el tipo de Log de la aplicaci�n
  //----------------------------------------------------------------------------
  NombreFichero:='Fichero.txt';
  Carpeta:='';
  RutaLog:= ExtractFilePath(ParamStr(0));
  Case TipoLog  of
    1 :  //Log de la Aplicaci�n
      Begin
        NombreFichero:='Log_Apps_';
        Carpeta:='Log';

      End;
    2 :  //Log de Errores
      Begin
         //Si la aplicaci�n no esta configurada para generar LOG de errores  se retorna
        if (Parametros.GeneraLogErrores=0) then Exit();
        NombreFichero:='Log_Error_';
        Carpeta:='Log';

      End;

    3 :  //Log de la data recibida
      Begin
          //Si la aplicaci�n no esta configurada para generar LOG de la data recibida
        //if (Parametros.GeneraLogDataRecibida=0) then Exit();
        NombreFichero:='Log_Data';
        Carpeta:='Log';
      End;
  End;


   //Se Verifica si existe la carpeta
  Conectado:=DirectoryExists(RutaLog+Carpeta+'\');
  if Conectado = False then
  Begin
    Try
      //Se crea La carpeta
      Conectado:=CreateDir(RutaLog+Carpeta);
      // Si no se puede crear se controla el error
    Except On E:Exception do
      Begin
        // Se guarda el error en el log
        Mensaje:='No se Puede Crear la Carpeta  ' + RutaLog+ Carpeta + ' El error es : ' + #13#10 + E.Message;
      End;
    End;
  End;
  //Genera Archivo texto con data enviada
  Try
    FicheroLog := RutaLog+ Carpeta+ '\' + NombreFichero + formatdatetime('yyyymmdd', now)+ '.txt';
    fichero := TStringList.Create;
    //Si Existe el archivo se carga
    if FileExists(FicheroLog) then fichero.LoadFromFile(FicheroLog);
    fichero.Add(Data);
    fichero.SaveToFile(FicheroLog);
    //Se Cierra el archivo
    Fichero.Free;
    // Si no se puede crear se controla el error
  Except On E:Exception do
    Begin
      // Se guarda el error en el log
      Mensaje:='No se Puede Crear El Archivo El error es : ' + #13#10 + E.Message;
    End;
  End;

end;

//----------------------------------------------------------------------------
//Funci�n que Cierra la conexi�n a la base de datos
//----------------------------------------------------------------------------
function ICGDO_CloseDB;
Begin
  Try
    rs001.Close;
    rs001.Free;
    Cn001.Close;
    Cn001.Free;
  except
    Begin
      // Se guarda el error en el log
      Mensaje:=MensajeError;
      LogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + Mensaje,InfoApps);
    End;
  End;
End;

end.
