//------------------------------------------------------------------------------
//Unit Para el Manejo de los Pluggins
//Empresa: Consultoria & Sistemas ICG, C.A.
//Rafael Rangel
//Creado: 08/11/2013
//Modificado: 08/11/2013
//------------------------------------------------------------------------------

unit UnitPluggins;

interface

Uses
Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
System.Classes, Vcl.Graphics,Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Xml.xmldom,
Xml.XMLIntf,Xml.Win.msxmldom, Xml.XMLDoc, UnitAppsComunes;

Type TICG_InfoPluggins   = record
  TipoFront,
  IdPluggin,
  NroEmpresa:Integer;
  Activo,
  Configurado:Boolean;
  IDTipoDoc,
  IdEstadoDoc01,
  IdEstadoDoc02,
  IdEstadoDoc03,
  IdEstadoDoc04,
  CodCliMaxInternos,
  OrigenCtaConsumo :Integer;
  UserDB,
  PassworDB:String;

end;


Type TICG_XMLPlugginFRT   = record
   Server,
   Database,
   Users,
   Serie,
   ModoDocumento,
   TipoDocumento:String;
   NroDocumento:Integer;
end;

Type TICG_XMLPlugginFRS   = record
   Server,
   Database,
   Users,
   TipoDocumento:String;
   Sala,
   Mesa,
   CodCliente,
   CodVendedor:Integer;
end;





Var

 //-----------------------------------------------------------------------------
 FileXML01 : TXMLDocument;
 //Definicion de Procedimientos del Unit

  // Procedimiento para leer la informacion que genera el XML del Pluggins de FrontRetail
  procedure LeerXMLPlugginFRT (ArchivoXML:String; var DatosXML:TICG_XMLPlugginFRT );
  // Procedimiento para leer la informacion que genera el XML del Pluggins de FrontRes
  procedure LeerXMLPlugginFRS_2012 (ArchivoXML:String; var DatosXML:TICG_XMLPlugginFRS );
  // Procedimiento para leer la informacion que genera el XML del Pluggins de FrontRes
  procedure LeerXMLPlugginFRS (ArchivoXML:String; var DatosXML:TICG_XMLPlugginFRS  );




//-----------------------------------------------------------------------------
implementation

//------------------------------------------------------------------------------
//Procedimiento para leer el archivo xml del Pluggins de FrontRetail, FrontHotel y Manager
procedure LeerXMLPlugginFRT (ArchivoXML:String; var DatosXML:TICG_XMLPlugginFRT );
var

XMLDoc :  IxmlDocument;
begin

  // Se crea el Objeto de XML;
  XMLDoc := TXMLDocument.Create(Nil) ;
  try
    XMLDoc.Active := True;
    // Se asigna el nombre del archivo
    XMLDoc.LoadFromFile((ArchivoXML));
    // Lee los valores del xml
    DatosXML.Server:=XMLDoc.DocumentElement.ChildNodes['bd'].ChildNodes['server'].NodeValue  ;
    DatosXML.Database:=XMLDoc.DocumentElement.ChildNodes['bd'].ChildNodes['database'].NodeValue ;
    DatosXML.Users:=XMLDoc.DocumentElement.ChildNodes['bd'].ChildNodes['user'].NodeValue  ;
    DatosXML.TipoDocumento:=XMLDoc.DocumentElement.ChildNodes['tipodoc'].NodeValue  ;
    DatosXML.Serie:=XMLDoc.DocumentElement.ChildNodes['serie'].NodeValue  ;
    DatosXML.NroDocumento:=XMLDoc.DocumentElement.ChildNodes['numero'].NodeValue  ;
    DatosXML.ModoDocumento:=XMLDoc.DocumentElement.ChildNodes['n'].NodeValue  ;
    XMLDoc.Active := False;
  finally
    XMLDoc := nil;
  end;



end;
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//Procedimiento para leer el archivo xml del Pluggins de FrontRetail, FrontHotel y Manager
procedure LeerXMLPlugginFRS (ArchivoXML:String; var DatosXML:TICG_XMLPlugginFRS );
var

XMLDoc :  IxmlDocument;
begin

  // Se crea el Objeto de XML;
  XMLDoc := TXMLDocument.Create(Nil) ;
  try
    XMLDoc.Active := True;
    // Se asigna el nombre del archivo
    XMLDoc.LoadFromFile((ArchivoXML));
    // Lee los valores del xml
    DatosXML.Server:=XMLDoc.DocumentElement.ChildNodes['bd'].ChildNodes['server'].NodeValue  ;
    DatosXML.Database:=XMLDoc.DocumentElement.ChildNodes['bd'].ChildNodes['database'].NodeValue ;
    DatosXML.Users:=XMLDoc.DocumentElement.ChildNodes['bd'].ChildNodes['user'].NodeValue  ;
    DatosXML.Sala:=XMLDoc.DocumentElement.ChildNodes['sala'].NodeValue  ;
    DatosXML.Mesa:=XMLDoc.DocumentElement.ChildNodes['mesa'].NodeValue  ;
    DatosXML.CodCliente:=XMLDoc.DocumentElement.ChildNodes['codcliente'].NodeValue  ;
    if XMLDoc.DocumentElement.ChildNodes['codvendedor'].NodeValue <> Null
    Then
      DatosXML.CodVendedor:=XMLDoc.DocumentElement.ChildNodes['codvendedor'].NodeValue
    Else
      DatosXML.CodVendedor:=0;

    DatosXML.TipoDocumento:='TIQUET';
    XMLDoc.Active := False;
  finally
    XMLDoc := nil;
  end;


end;
//------------------------------------------------------------------------------






//------------------------------------------------------------------------------
//Procedimiento para leer el archivo xml del Pluggins de FrontRetail
procedure LeerXMLPlugginFRS_2012 (ArchivoXML:String; var DatosXML:TICG_XMLPlugginFRS );
var F: TextFile;
    sLinea,
    Registro,
    TmpServer,
    TmpDatabase,
    TmpUsers,
    TmpSala,
    TmpMesa,
    TmpCodCliente:String;
    i : Integer;
    Linea01,
    Linea02,
    Linea03,
    Linea04,
    Linea05,
    Linea06,
    Linea07,
    Linea08,
    Linea09,
    Linea10,
    sArchivo:String;
begin
  AssignFile( F, ExtractFilePath( Application.ExeName ) + ArchivoXML );
  Reset( F );

  i:=1;
  Linea01:='';
  Linea02:='';
  Linea03:='';
  Linea04:='';
  Linea05:='';
  Linea06:='';
  Linea07:='';
  Linea08:='';
  Linea09:='';
  Linea10:='';
  sArchivo:='';
  while not Eof( F ) do
  begin
    ReadLn( F, sLinea );
    Registro:=Trim( sLinea );

    If i=1  Then
    Begin
      Linea01:= Registro  ;
    End;

    If i=2  Then
    Begin
      Linea02:= Registro  ;
    End;

    If i=3  Then
    Begin
      Linea03:= Registro  ;
    End;
    //Tomamos del XML el nombre del servidor
    If i=4  Then
    Begin
      TmpServer := Registro  ;
      Linea04:= Registro  ;
    End;
    //'Tomanos del XML el nombre de la Base de datos
    If i=5  Then
    Begin
      TmpDatabase := Registro;
      Linea05:= Registro  ;
    End;

    //'Tomanos del XML el Nombre del Usuarios de la Base de datos
    If i=6  Then
    Begin
      TmpUsers := Registro;
      Linea06:= Registro  ;
    End;

    If i=7  Then
    Begin
      Linea07:= Registro  ;
    End;
    //'Tomanos del XML el Tipo de Documento
    If i=8  Then
    Begin
      TmpSala := Registro;
      Linea08:= Registro  ;
    End;

    //Tomanos del XML el Tipo el numero de Serie
    If i=9  Then
    Begin
      TmpMesa := Registro;
      Linea09:= Registro  ;
    End;

    //'Tomaos del XML el Nro del documento
    If i=10 Then
    Begin
      TmpCodCliente := Registro;
      Linea10:= Registro  ;
    End;
    i:=i+1;
  end;
  CloseFile( F );
  TmpServer:=StringReplace(TmpServer, '<server>', '', [] );
  TmpServer:=StringReplace(TmpServer, '</server>', '', [] );
  TmpDatabase := StringReplace(TmpDatabase, '<database>', '',[]) ;
  TmpDatabase := StringReplace(TmpDatabase, '</database>', '',[]);
  TmpUsers := StringReplace(TmpUsers, '<user>', '',[]);
  TmpUsers := StringReplace(TmpUsers, '</user>', '',[]);
  TmpSala := StringReplace(TmpSala, '<sala>', '',[]);
  TmpSala := StringReplace(TmpSala, '</sala>', '',[]);
  TmpMesa := StringReplace(TmpMesa, '<mesa>', '',[]);
  TmpMesa := StringReplace(TmpMesa, '</mesa>', '',[]);
  TmpCodCliente := StringReplace(TmpCodCliente, '<codcliente>','',[]);
  TmpCodCliente := StringReplace(TmpCodCliente, '</codcliente>', '',[]);

  DatosXML.Server:=TmpServer;
  DatosXML.Database:=TmpDatabase;
  DatosXML.Users:= TmpUsers   ;
  if Length(TmpSala)  > 0 Then DatosXML.Sala:=StrToINT(TmpSala)   ;
  if Length(TmpMesa) > 0 Then DatosXML.Mesa:=StrToINT(TmpMesa) ;
  If Length(TmpCodCliente) > 0 Then DatosXML.CodCliente:=StrToINT(TmpCodcliente) ;



  //Borramos el archivo
  EliminarArchivo (ExtractFilePath( Application.ExeName ) + ArchivoXML);

  //Creamos el nuevo
  AssignFile( F, ExtractFilePath( Application.ExeName ) + ArchivoXML );
  sArchivo :=ExtractFilePath( Application.ExeName ) + ArchivoXML ;
  Rewrite( F );
  WriteLn( F,'<?xml version="1.0" encoding="ISO-8859-1" ?>');
  WriteLn( F,'<doc>');
  WriteLn( F,'   <bd>');
  WriteLn( F,'      '+Linea04);
  WriteLn( F,'      '+Linea05);
  WriteLn( F,'      '+Linea06);
  WriteLn( F,'   </bd>');
  WriteLn( F,'   '+Linea08);
  WriteLn( F,'   '+Linea09);
  WriteLn( F,'   '+Linea10);
  WriteLn( F,'</doc>');
  // Cerrar el archivo
  CloseFile( F );

  //DatosXML.NroDocumento:= StrToInt(TmpNroDocumento)   ;
end;
//------------------------------------------------------------------------------







end.
