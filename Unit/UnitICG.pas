//******************************************************************************
// Unit que contiene los procedimientos y funciones de uso Comun Relacionados
// Con las Aplicaciones ICG
// Empresas: Consultoria & Sistemas ICG, C.A. / Geinfor Venezuela, C.A.
// Autor: Rafael Rangel
// Fecha: 14/04/2013
// Ult Modificacion: 02/05/2014
//******************************************************************************

unit UnitICG;

interface


Uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics,  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls, Vcl.Buttons, ADODB,IniFiles,  Data.DB,WinInet, WinSock,
  Registry,UnitAppsComunes,DBClient ;

type TICG_DataRegistry   = record
  ServerNameGeneral,
  ServerNameGestion,
  ServerNameContable,
  DBGeneral,
  DBGestion,
  DBContable,
  CajaFront,
  DescripcionFront:String;
  NroEmpresaGestion,
  NroEmpresaContable,
  EjercioContable,
  TipoFront,
  Version,
  Nro_Z,
  CodVendedor : Integer;
  ICGInstalado:Boolean;
  Fecha_Trabajo:TDateTime;
  NomEmpresaGestion,
  NomEmpresaContable:String;
  Fecha_Bloqueo_Contable:TDateTime;
end;



Var

  //Conexiones a la Base de datos
  Cn001: TADOConnection;
  Cn002: TADOConnection;
  Cn003: TADOConnection;

  Rs001: TADOQuery;
  Rs002: TADOQuery;
  Rs003: TADOQuery;

  //Declaracion de Variables
  Continuar:Boolean;
  BDConectado:Boolean;
  SQL001,
  SQL002,
  SQL003,
  SQL004,
  SQL005,
  SQL006,
  MensajeError:String;
  //Declaracion de Funciones
  Function Extrar_DB_Server(StrCampo:string;Devolver:Integer): string;

  //Se verifica si el ejercicio contable esta cerrado
  Function Ejercicio_Contable_Cerrado(CnADO:TADOConnection): Boolean;

  //Se genera el asiento
  Function Insertar_Asiento_Contable(CnADOC,CnADOG,CnADOGestion:TADOConnection;ElDataSet:TClientDataSet;FechaAsiento:TDate;CodEmpresa,Ejercicio,CodEmpresaGestion,IDConciliacion:Integer;PNorma43Str,CuentaBanco:String;Insert_Diario,Insert_Conciliacion:Boolean): Boolean;

  //Declaracion de Procedimientos-----------------------------------------------
  //Procedimientos para tomar los valores de ICG en el Registro
  Procedure Datos_ICGRegistry (var Datos:TICG_DataRegistry;FrontICG:Integer;PCDatos:TDatosPC;UsersDB,PasswordDB:String);
  //Procedimientos para tomar los valores de ICG en el Registro
  Procedure Extraer_Empresas_Contables (var Datos:TICG_DataRegistry; Serie,UsersDB, PasswordDB:String);
  //----------------------------------------------------------------------------
  //Procedimientos para tomar los valores de la empresa de Gestion
  Procedure Extraer_Empresas_Gestion (var Datos:TICG_DataRegistry; UserDB,PasswordDB:String);
  //----------------------------------------------------------------------------


implementation

//******************************************************************************
// Funciones
//******************************************************************************



//------------------------------------------------------------------------------
//Función que se encarga de indicar si el ejercicio esta cerrado
//------------------------------------------------------------------------------
Function Ejercicio_Contable_Cerrado(CnADO:TADOConnection): Boolean;
Var
  StrSQL:String;
  rs001 :TADOQuery;  //Recordset01 de las lineas del albaran
  Conectado,
  Cerrado:Boolean;

Begin

  Try
    try
      Cerrado:=False;
      StrSQL:='';
      StrSQL:='SELECT TOP 1 Fecha FROM DiarioApuntes With (Nolock) WHERE (CONCEPTO=98 OR CONCEPTO=-98)';
      rs001:= TADOQuery.Create(nil);
      //----------------------------------------------------------------------
      //Se Abre el DataSet  con los calculos del impuesto FPTE
      //----------------------------------------------------------------------
      Conectado:= Open_ADO_Qry(CnADO, rs001,StrSQL,MensajeError,false,False);
      //----------------------------------------------------------------------
      //Si no hay conexión se interrumpe la ejecución de la aplicación
      if Conectado=False then
      Begin
        Result:= Cerrado;
        Exit;
      End;

      //Se va analizar el objeto recordset
      With rs001 do
      Begin
        //Se verifica si hay registros del cierre de ejercicio
        if RecordCount > 0  then Cerrado:=True;
      End;
    finally
      //Se cierra los objetos ADO
      rs001.Close;
      rs001.free;
      Result:= Cerrado;
    end;

 except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
End;

//------------------------------------------------------------------------------
//Se inserta el registro del asiento
//------------------------------------------------------------------------------
Function Insertar_Asiento_Contable(CnADOC,CnADOG,CnADOGestion:TADOConnection;ElDataSet:TClientDataSet;FechaAsiento:TDate;CodEmpresa,Ejercicio,CodEmpresaGestion,IDConciliacion:Integer;PNorma43Str,CuentaBanco:String;Insert_Diario,Insert_Conciliacion:Boolean): Boolean;
Var
  rs001 :TADOQuery;  //Recordset01 de las lineas del albaran
  Conectado,
  Generado,
  EsMonedaPrincipal:Boolean;
  SigAsiento,
  SigAsientoVisible,
  SigAsientoControl,
  CodMonedaPrincipal:Integer;
  TablaAsiento,
  EsNumerador:String;
  Debe1,
  Haber1,
  FactorMoneda:Double;


Begin
  Try

    SigAsiento:=0;
    SigAsientoVisible:=0;
    SigAsientoControl:=0;
    //------------------------------------------------------------------------
    //se busca el ultimo nro de asiento
    //------------------------------------------------------------------------
    Generado:=False;
    SQL001:='';
    SQL001:='SELECT MAX(ASIENTO)+1 AS ASIENTO, MAX(ASIENTOVISIBLE)+1 AS ASIENTOVISIBLE, ISNULL((SELECT MAX(ASIENTO) FROM CONTROLASIENTO),0)+1  AS ASIENTOCONTROL  FROM DIARIOAPUNTES';
    rs001:= TADOQuery.Create(nil);
    //------------------------------------------------------------------------
    //Se Abre el DataSet  con los calculos del impuesto FPTE
    //------------------------------------------------------------------------
    Conectado:= Open_ADO_Qry(CnADOC, rs001,SQL001,MensajeError,false,False);
    //------------------------------------------------------------------------
    //Si no hay conexión se interrumpe la ejecución de la aplicación
    //------------------------------------------------------------------------
    if Conectado=False then
    Begin
      Result:=False;
      Exit;
    End;
    //--------------------------------------------------------------------------
    //Se va analizar el objeto recordset
    //--------------------------------------------------------------------------
    With rs001 do
    Begin
      //si no hay registros se inicializa las variables
      if eof then
      Begin
        SigAsiento:=1;
        SigAsientoVisible:=1;
      End
      Else
      Begin
        //Se toma los datos del siguiente registro
        SigAsiento:=Fields[0].AsInteger;
        SigAsientoVisible:=Fields[1].AsInteger;
        SigAsientoControl:=Fields[2].AsInteger;
      End;
    End;

    //se Verifica si el siguiente asiento es menor a lo registrado en el control de asiento
    if SigAsientoControl > SigAsiento then SigAsiento:=SigAsientoControl ;
    //--------------------------------------------------------------------------
    //se actualiza el ultímo nro de asiento
    //--------------------------------------------------------------------------
    SQL001:='';
    SQL001:='UPDATE EMPRESASCONTABLES WITH (ROWLOCK) SET ASIENTO = ' + IntToStr(SigAsiento) +
    ' WHERE CODIGO = ' + IntToStr(CodEmpresa) +
    ' AND EJERCICIO = ' + IntTostr(Ejercicio);
    //--------------------------------------------------------------------------
    //Se ejecuta el SQL para insertar la cabecera
    //------------------------------------------------------------------------
    Conectado:= DB_Function(CnADOG,SQL001,4);
    if Conectado=False then
    Begin
      Result:=False;
      Exit;
    End;
    //--------------------------------------------------------------------------
    //se Inserta en la tabla de control de asientos
    //--------------------------------------------------------------------------
    SQL001:='';
    SQL001:='INSERT INTO CONTROLASIENTO  WITH (ROWLOCK) (Asiento) VALUES (' + IntToStr(SigAsiento)+ ')';
    //--------------------------------------------------------------------------
    //Se ejecuta el SQL
    //------------------------------------------------------------------------
    Conectado:= DB_Function(CnADOC,SQL001,4);
    if Conectado=False then
    Begin
     Result:=False; Exit;
    End;

    //se lee cada resgitro del data set
    with ElDataSet do
    begin
      First;
      //Se recorre el data set
      While not(EOF) do
      begin



        //Se toman los parametos de las monedas para realizar los calculos de los montos en divisas

        SQL001:='';
        SQL001:='SELECT [dbo].[F_ES_NUMERADOR](' + IntTostr(FieldByName('CodMoneda').AsInteger) +  ' ) AS ESNUMERODOR, [dbo].[F_GET_MONEDAPRINCIPAL]() AS MONEDAPRINCIPAL ';
        try
          rs001:= TADOQuery.Create(nil);
          //----------------------------------------------------------------------
          //Se Abre el DataSet  con los calculos del impuesto FPTE
          //----------------------------------------------------------------------
          Conectado:= Open_ADO_Qry(CnADOC, rs001,SQL001,MensajeError,false,False);
          //----------------------------------------------------------------------
          //Si no hay conexión se interrumpe la ejecución de la aplicación
          if Conectado=False then
          Begin
            Result:= False;
            Exit;
          End;

          //Se va analizar el objeto recordset
          With rs001 do
          Begin
            //Se toman los datos de la consulta
            if Not Eof then
            Begin
              EsNumerador:= Fields[0].AsString;
              CodMonedaPrincipal:= Fields[1].AsInteger;
            End;

          End;
          finally
          //Se cierra los objetos ADO
          rs001.Close;
          rs001.free;
        end;

        Debe1:=0;
        Haber1:=0;
        FactorMoneda:=0;

        Debe1:=FieldByName('Debe').AsFloat;
        Haber1:=FieldByName('Haber').AsFloat;
        FactorMoneda:=FieldByName('FactorMoneda').AsFloat;
        if Factormoneda=0  then FactorMoneda:=1;

        if Esnumerador='T' then
        Begin
          Debe1:=Debe1/FactorMoneda;
          Haber1:=Haber1/FactorMoneda;
        End
        else
        Begin
          Debe1:=Debe1*FactorMoneda;
          Haber1:=Haber1*FactorMoneda;
        End;


        //se inserta en el asiento
        SQL001:='';
        SQL002:='';
        SQL003:='';
        SQL004:='';
        SQL005:='';
        SQL006:='';

        SQL001:='INSERT INTO DiarioApuntes WITH (ROWLOCK) (  ' ;
        SQL002:='INSERT INTO ICGDO_TB_BCO_CONCILIACION_LIN WITH (ROWLOCK) (ID,  '  ;
        SQL006:='INSERT INTO ICGDO_TB_BCO_CONCILIACION_TRAN WITH (ROWLOCK) (ID,  '  ;

        SQL003:=' Usuario, Asiento, Apunte, Nivel, Fecha, Cuenta, Concepto, Comentario,SerieDocumento, NumeroDocumento, Debe, Haber, Costes,' +
        ' Punteo, CentroCoste, EmpresaGestion,CodMoneda,FactorMoneda,   ' +
        ' Debe1,Haber1,AsientoVisible,PNorma43Str, ' +
        ' TIPODOCUMENTO,SERIEFISCAL,NUMEROFISCAL,CODTERCERO,TERCEROESCLIENTE) VALUES ' ;

        SQL003:=SQL003 + '(' ;

        //SQL004:=  IntTostr(IDConciliacion) + ',' ;
        SQL004:=  IntTostr(0) + ',' ;

        SQL005:=QuotedStr('ICGDO') + ',' +
        IntToStr(SigAsiento)+ ',' +
        IntTostr(FieldByName('Apunte').AsInteger) + ',' +
        QuotedStr('A') + ',' +
        QuotedStr(FormatDateTime('YYYYMMDD',FechaAsiento)) + ',' +
        QuotedStr(FieldByName('CodCuenta').AsString) + ',' +
        IntToStr(FieldByName('CodConcepto').AsInteger) + ',' +
        QuotedStr(FieldByName('Comentarios').AsString) + ',' +
        QuotedStr('') + ',0,' +
        FloatToStr(FieldByName('Debe').AsFloat) + ',' +
        FloatToStr(FieldByName('Haber').AsFloat) + ',' +
        '0,0,' +  QuotedSTR('') + ',' +
        IntTostr(CodEmpresaGestion) + ',' +
        IntTostr(FieldByName('CodMoneda').AsInteger) + ',' +
        FloatToStr(FactorMoneda) + ',' +
        FloatToStr(Debe1) + ',' +
        FloatToStr(Haber1) + ',' +
        IntTostr(SigAsientoVisible) + ',' +
        QuotedSTR(PNorma43Str)+ ',' +
        IntTostr(-1) + ',' +
        QuotedSTR('')+ ',' +
        IntTostr(0) + ',' +
        IntTostr(-1) + ',' +
        IntTostr(0) + ')';
        //--------------------------------------------------------------------------
        //Se ejecuta el SQL
        //------------------------------------------------------------------------
        //Se ejecuta el SQL
        if FieldByName('CodCuenta').AsString <> '' Then
        Begin

          //Solo se registra en el concilición el movimiento del banco
          if FieldByName('CodCuenta').AsString =CuentaBanco
          Then
            If Insert_Conciliacion Then
            Begin
              //Lineas de Conciliación
             // Conectado:= DB_Function(CnADOGestion,SQL002+SQL003+SQL004+SQL005,4);
              //Conciliación en transito
              Conectado:= DB_Function(CnADOGestion,SQL006+SQL003+SQL004+SQL005,4);
            End;
          //se Inserta en la tabla de diario de apuntes.
          If Insert_Diario Then Conectado:= DB_Function(CnADOC,SQL001+SQL003+SQL005,4);


        End;
        Next;
      end;
    end;
    //se retorna lo valores del asiento
    Result:=True;
  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
End;



//------------------------------------------------------------------------------






//******************************************************************************
// Procedimientos
//******************************************************************************

//------------------------------------------------------------------------------
//Procedimiento para tomar los datos  de la aplicacion ICG que se este utilizando
//del registro de Windows y de los parametros de la aplicacion
//------------------------------------------------------------------------------
Procedure Datos_ICGRegistry (var Datos:TICG_DataRegistry;FrontICG:Integer;PCDatos:TDatosPC;UsersDB,PasswordDB:String);
Var
  Key_RegistryICG,
  Key_ServerName,
  Key_DBGeneral,
  Key_EmpresaGestion,
  StrTexto01,
  StrDelimitador:String;
  IntNumero01:Integer;
  DateFecha01:TDateTime;

Begin

  Datos.ICGInstalado:=False;
  //Se verifica si la aplicacion ICG esta Instalada en la Maquina
  Datos.ICGInstalado:=ExisteKey_Registry (HKEY_CURRENT_USER,'Software\ICG');
  // si Ubicamos en el registro los valores los tomamos
  if Datos.ICGInstalado then
  Begin
    //Asignamos Valores por Defecto
    Key_ServerName:='SQL_ServerName' ;
    Key_DBGeneral:='SQL_General' ;
    Key_EmpresaGestion := 'EMPGESTFRONT';

    Case FrontICG  of

      1: //FrontRest
        Begin
          //Se Define que se Buscara en el registro de Windows
          Key_RegistryICG:='\Software\ICG\FrontRest2007\';
          Datos.NroEmpresaGestion:=1;
          Key_ServerName:='ServerSQL5' ;
          Key_DBGeneral:='DataBaseSQL5' ;
          Datos.DescripcionFront:='FRONT REST';
        End;

      2 : //FronRetail
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='Software\ICG\RetailSQL2007\';
        Datos.DescripcionFront:='FRONT RETAIL';
        Datos.NroEmpresaGestion:=0;
      End;

      3 : //Front Hotel
      Begin
        //Se Define que se Buscara en el registro de Windows
      //HKEY_CURRENT_USER\Software\ICG\FrontHotel2007
        Key_RegistryICG:='\Software\ICG\FrontHotel2007\';
        Datos.DescripcionFront:='FRONT HOTEL';
        Datos.NroEmpresaGestion:=0;
      End;

      4: //Manager Advanced
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='\Software\ICG\ICGManagerSQL2007\';
        Datos.DescripcionFront:='MANAGER ADVANCED';

      End;
      5: //Manager CRM
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='\Software\ICG\ManagerCRM\';
        Datos.DescripcionFront:='MANAGER CRM';
      End;

      6: //Data Exchage
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='\Software\ICG\ICGDataExchange\';
        //Asignamos Valores por Defecto
        Key_ServerName:='SERVIDORNAME' ;
        Key_DBGeneral:='BASEDATOSGENERAL' ;
        Key_EmpresaGestion := 'CODEMPRESAGESTION';
        Datos.DescripcionFront:='DATAEXCHANGUE';
      End;

      7: //Organizer
      Begin
        //Se Define que se Buscara en el registro de Windows
        //HKEY_CURRENT_USER\Software\ICG\ICGOrganizer2007
        Key_RegistryICG:='\Software\ICG\ICGOrganizer2007\';
        Datos.DescripcionFront:='ORGANIZER';
      End;
    End;

    //Buscar en el registro
    Datos.ServerNameGeneral:= GetRegistryData(HKEY_CURRENT_USER,Key_RegistryICG, Key_ServerName);
    Datos.DBGeneral:= GetRegistryData(HKEY_CURRENT_USER,Key_RegistryICG, Key_DBGeneral);
    //'Si no hay por defecto una empresa de Gestion definida, se busca en el registro
    if Datos.NroEmpresaGestion=0 then Datos.NroEmpresaGestion:=GetRegistryData(HKEY_CURRENT_USER,Key_RegistryICG, Key_EmpresaGestion);

    //
    if FrontICG =1 then // Si es Restaurante lo tratamos diferente a las otras aplicaciones
      Begin
        Datos.ServerNameGestion:= Datos.ServerNameGeneral;
        Datos.DBGestion:= Datos.DBGeneral;


        //Obtener los datos de la Caja
        Cn001 := TADOConnection.Create(nil);
        Rs001 := TADOQuery.Create(nil);
        BDConectado:=False;
        BDConectado:=Open_DB (Cn001, 12 , Datos.ServerNameGestion, Datos.DBGestion,UsersDB, PasswordDB, '',MensajeError,True );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion de la caja
          SQL001:='SELECT  CAJA FROM TERMINALES WITH (NOLOCK) WHERE (TERMINAL=' + QuotedSTR(PcDatos.Nombre) + ')';
          //Se Abre el DataSet Parametros
          BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,'',false,true);
          // Verifica si se Abrio el DataSet
          if BDConectado then
          Begin
            //
            StrTexto01:='';
            with rs001 do begin
              if not eof then
              Begin
                // Extraemos los datos de la caja
                StrTexto01:=Fields[0].AsString  ;
              End;
              if StrTexto01='' then   StrTexto01:='1';
              //Numero de la caja
              Datos.CajaFront:=StrTexto01;
            end;
          End;
        End;
        // Cerrar la Base de datos
        CloseConnection  (cn001, rs001);

       //Obtener El Z Actual de la Caja
        //Obtener los datos de la Caja
        Cn001 := TADOConnection.Create(nil);
        Rs001 := TADOQuery.Create(nil);
        BDConectado:=False;
        BDConectado:=Open_DB (Cn001, 12 , Datos.ServerNameGestion, Datos.DBGestion,UsersDB, PasswordDB, '',MensajeError,True );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion del z actual de la caja
          SQL001:='SELECT MAX(NUMERO) + 1 FROM ARQUEOS WITH (NOLOCK) WHERE CAJA=' + QuotedSTR(Datos.CajaFront)+ ' AND ARQUEO=' + QuotedSTR('Z') ;
         //Se Abre el DataSet Parametros
          BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,'',false,true);
          // Verifica si se Abrio el DataSet
          if BDConectado then
          Begin
            // Leemos los valores de la tabla
            with rs001 do begin
              if not eof then
              Begin
                // Extraemos los datos del servidor y base de datos de Gestion
                IntNumero01:=Fields[0].AsInteger  ;
                if IntNumero01=0 then   IntNumero01:=1;
                //Servidor y Base de datos de Gestion
                Datos.Nro_Z:=IntNumero01;
              End;
            end;
          End;
        End;
        // Cerrar la Base de datos
        CloseConnection  (cn001, rs001);





      End
    Else
      Begin


        //----------------------------------------------------------------------
        //Obtener los datos del Servidor de Gestion
        //----------------------------------------------------------------------
        Extraer_Empresas_Gestion (Datos,UsersDB, PasswordDB);
        //----------------------------------------------------------------------


        //Obtener los datos de la Caja
        Cn001 := TADOConnection.Create(nil);
        Rs001 := TADOQuery.Create(nil);
        BDConectado:=False;
        BDConectado:=Open_DB (Cn001, 12 , Datos.ServerNameGestion, Datos.DBGestion,UsersDB, PasswordDB, '',MensajeError,True );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion de la caja
          SQL001:='SELECT VALOR FROM PARAMETROS WITH (NOLOCK) WHERE (CLAVE = ' + CHR(39) + 'SERIE' + Chr(39)
           + ') AND (SUBCLAVE = ' + Chr(39) + '.' + Chr(39)
           + ') AND (USUARIO = '+Chr(39)  + PcDatos.Nombre + Chr(39) + ')';
          //Se Abre el DataSet Parametros
          BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,'',false,True);
          // Verifica si se Abrio el DataSet
          if BDConectado then
          Begin
            //
            StrTexto01:='';
            with rs001 do begin
              if not eof then
              Begin
                // Extraemos los datos del servidor y base de datos de Gestion
                StrTexto01:=Fields[0].AsString  ;
                if StrTexto01='' then   StrTexto01:='AAA';
                //Servidor y Base de datos de Gestion
                Datos.CajaFront:=StrTexto01;
              End;
            end;
          End;
        End;
        // Cerrar la Base de datos
        CloseConnection  (cn001, rs001);

        //Obtener El Z Actual de la Caja
        //Obtener los datos de la Caja
        Cn001 := TADOConnection.Create(nil);
        Rs001 := TADOQuery.Create(nil);
        BDConectado:=False;
        BDConectado:=Open_DB (Cn001, 12 , Datos.ServerNameGestion, Datos.DBGestion,UsersDB, PasswordDB, '',MensajeError,True );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion del z actual de la caja
          SQL001:='SELECT MAX(NUMERO) + 1 FROM ARQUEOS WITH (NOLOCK) WHERE CAJA=' + Chr(39) + Datos.CajaFront + Chr(39)+ 'AND ARQUEO=' + Chr(39) + 'Z' + Chr(39) ;
         //Se Abre el DataSet Parametros
          BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,'',false,True);
          // Verifica si se Abrio el DataSet
          if BDConectado then
          Begin
            // Leemos los valores de la tabla
            with rs001 do begin
              if not eof then
              Begin
                // Extraemos los datos del servidor y base de datos de Gestion
                IntNumero01:=Fields[0].AsInteger  ;
                if IntNumero01=0 then   IntNumero01:=1;
                //Servidor y Base de datos de Gestion
                Datos.Nro_Z:=IntNumero01;
              End;
            end;
          End;
        End;
        // Cerrar la Base de datos
        CloseConnection  (cn001, rs001);



        //Obtener la fecha de trabajo del Front
        Cn001 := TADOConnection.Create(nil);
        Rs001 := TADOQuery.Create(nil);
        BDConectado:=False;
        BDConectado:=Open_DB (Cn001, 12 , Datos.ServerNameGestion, Datos.DBGestion,UsersDB, PasswordDB, '',MensajeError,True );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion de la fecha de trabajo
         SQL001:='SELECT CONVERT(VARCHAR(10),ISNULL(VALOR,GETDATE()), 112)      FROM PARAMETROS WITH (NOLOCK) WHERE (CLAVE = ' + CHR(39) + '$C' +Datos.CajaFront + Chr(39)
           + ') AND (SUBCLAVE = ' + Chr(39) + 'Date' + Chr(39)
           + ') AND (USUARIO = ' + Chr(39) + '.' + Chr(39) + ')';


        End;
        // Cerrar la Base de datos
        CloseConnection  (cn001, rs001);


        //----------------------------------------------------------------------
        //Obtener el Nombre de la Empresa de Gestion
        //----------------------------------------------------------------------
        Cn001 := TADOConnection.Create(nil);
        Rs001 := TADOQuery.Create(nil);
        BDConectado:=False;
        BDConectado:=Open_DB (Cn001, 12 , Datos.ServerNameGeneral, Datos.DBGeneral,UsersDB, PasswordDB, '',MensajeError,True );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion del z actual de la caja
          SQL001:='SELECT NOMBRECOMERCIAL FROM EMPRESAS WHERE  (CODEMPRESA = ' + IntToSTR(Datos.NroEmpresaGestion) + ')' ;
         //Se Abre el DataSet Parametros
          BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,'',false,True);
          // Verifica si se Abrio el DataSet
          if BDConectado then
          Begin
            // Leemos los valores de la tabla
            with rs001 do begin
              if not eof then
              Begin
                // Extraemos los datos del servidor y base de datos de Gestion
                StrTexto01:=Fields[0].AsString  ;
                if StrTexto01='' then   StrTexto01:='EMPRESA';
                //Servidor y Base de datos de Gestion
                Datos.NomEmpresaGestion:=StrTexto01;
              End;
            end;
          End;
        End;
        // Cerrar la Base de datos
        CloseConnection  (cn001, rs001);




    End;
    Datos.Version:=2012;
    Datos.TipoFront:= FrontICG;
    Datos.ICGInstalado:= True;
  End
  else
      MessageDlg('No se encontro Ningun Sistema ICG Instalado ',mterror, [mbok],0);
  end   ;

//------------------------------------------------------------------------------


//Funcion Para Extraer el Nombre del Servidor o de la Base de datos ------------
Function Extrar_DB_Server(StrCampo:string;Devolver:Integer): string ;
Var
  PosInicial,
  PosFinal,
  LargoCadena:Integer;
  Servidor:String;
  BasedeDatos:String;
  Resultado:String  ;
begin
  PosInicial:=1;
  PosFinal:=1;
  LargoCadena:=1;
  LargoCadena:=Length( StrCampo );
  PosInicial:=Pos( ':', StrCampo );
  PosFinal:=LargoCadena;
  Servidor:='';
  BasedeDatos:='';
  Servidor:=Copy(StrCampo,1,PosInicial-1);
  BasedeDatos:=Copy(StrCampo,PosInicial+1,(LargoCadena)-(PosInicial));
  Resultado:=Servidor;
  if Devolver=2 then Resultado:=BasedeDatos;
  Result:=Resultado;
end;






//------------------------------------------------------------------------------
//Funcion para Extraer los datos de las empresas Contables
//------------------------------------------------------------------------------
Procedure Extraer_Empresas_Contables (var Datos:TICG_DataRegistry; Serie:String;UsersDB, PasswordDB:string);
Var
  StrTexto01,
  StrDelimitador:String;
  EmpContable:Integer;
  EjeContable:Integer;
  FechaBloqueo:TDateTime;
Begin

  //Creamos los Objetos
  Cn003 := TADOConnection.Create(nil);
  Rs003 := TADOQuery.Create(nil);
  BDConectado:=False;

  //----------------------------------------------------------------------------
  //Si no se envia la serie la informacionde del ejercicio contable se obtiene de
  //----------------------------------------------------------------------------
  if Serie <> ''
  then
    Begin
      BDConectado:=Open_DB (Cn003, 12 , Datos.ServerNameGestion, Datos.DBGestion,UsersDB, PasswordDB, '',MensajeError,True );
      if BDConectado then
      Begin
        //Se Arma el Sql para la informacion del Servidor y empresa de Gestion
        SQL001:='SELECT CONTABILIDADB FROM SERIES WITH (NOLOCK) WHERE SERIE =' + Chr(39) + Serie + Chr(39);
        //Se Abre el DataSet de los clientes
        BDConectado:= Open_ADO_Qry(Cn003, rs003,SQL001,'',false,True);
        // Verifica si se Abrio el DataSet
        if BDConectado then
        Begin
        //
          StrTexto01:='';
          EmpContable:=1;
          EjeContable:=2012;
          with rs003 do
          begin
            if not eof then
            Begin
              // Extraemos los datos de la tabla de series
              StrTexto01:=Fields[0].AsString  ;
              if StrTexto01='' then   StrTexto01:='C2012010';
              //Ejercicio y Empresa Contable
              EjeContable:= StrToInt(Copy(StrTexto01,2,4));
              EmpContable:= StrToInt(Copy(StrTexto01,6,3));
            End;
          end;
        End;
      End
    End
  Else
    Begin
      if Serie = '' then
      Begin

        //Se busca la informacion de la empresa contable en la bse de datos de gestion
        BDConectado:=Open_DB (Cn003, 12 , Datos.ServerNameGeneral, Datos.DBGeneral,UsersDB, PasswordDB, '',MensajeError,true );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion del Servidor y empresa de Gestion
          SQL001:='SELECT TOP (1) CODEMPCONTA, EJERCICIO FROM RELEMPGESTIONCONTA  WITH (NOLOCK) ' +
            ' WHERE (CODEMPGESTION = ' + IntToSTR(Datos.NroEmpresaGestion) + ')  ORDER BY EJERCICIO DESC,CODEMPCONTA';
          //Se Abre el DataSet
          BDConectado:= Open_ADO_Qry(Cn003, rs003,SQL001,'',false,True);
          // Verifica si se Abrio el DataSet
          if BDConectado then
          Begin
            //
            EmpContable:=1;
            EjeContable:=2012;
            with rs003 do
            begin
              if not eof then
              Begin
                //Ejercicio y Empresa Contable
                EmpContable:= Fields[0].AsInteger;
                EjeContable:= Fields[1].AsInteger;
              End;
            end;
          End;
        End;
      End;
    End;

  // Cerrar la Base de datos
  CloseConnection  (cn003, rs003);


  //Se colocan los datos en la variable de ICG
  Datos.NroEmpresaContable:= EmpContable;
  Datos.EjercioContable:=EjeContable;

  //Buscar los valores en la base de datos General
  //Para Extraer los nombres del servidor y empresa Contable
  Cn002 := TADOConnection.Create(nil);
  Rs002 := TADOQuery.Create(nil);
  BDConectado:=False;
  BDConectado:=Open_DB (Cn002, 12 , Datos.ServerNameGeneral, Datos.DBGeneral,UsersDB, PasswordDB, '',MensajeError,True );
  if BDConectado then
  Begin
    //Se Arma el Sql para la informacion del Servidor y empresa de Gestion
    SQL001:='SELECT PATHBD,FECHABLOQUEO FROM EMPRESASCONTABLES WITH (NOLOCK) WHERE CODIGO =' +  IntToStr(Datos.NroEmpresaContable)
    + ' AND EJERCICIO = ' + IntToStr(Datos.EjercioContable);
    //Se Abre el DataSet de los clientes
    BDConectado:= Open_ADO_Qry(Cn002, rs002,SQL001,'',false,True);
    // Verifica si se Abrio el DataSet
    if BDConectado then
    Begin
      //
      StrTexto01:='';
      StrDelimitador:=':';

      with rs002 do begin
        if not eof then
        Begin
          // Extraemos los datos del servidor y base de datos de Gestion
          StrTexto01:=Fields[0].AsString  ;
          if StrTexto01='' then   StrTexto01:='Servidor:Gestion';
          //Servidor y Base de datos de Gestion
          Datos.ServerNameContable:=Extrar_DB_Server(StrTexto01,1);
          Datos.DBContable:=Extrar_DB_Server(StrTexto01,2);;
          Datos.Fecha_Bloqueo_Contable:= Fields[1].AsDateTime;
        End;
      end;
    End;
  End;
  // Cerrar la Base de datos
  CloseConnection  (cn002, rs002);
End;

//------------------------------------------------------------------------------
//Funcion para Extraer los datos de las empresas de Gestion
//------------------------------------------------------------------------------
Procedure Extraer_Empresas_Gestion (var Datos:TICG_DataRegistry;UserDB,PasswordDB:String);
Var
  StrTexto01,
  StrDelimitador:String;
Begin

  //Buscar los valores en la base de datos General
  //Para Extraer los nombres del servidor y empresa de Gestion
  Cn001 := TADOConnection.Create(nil);
  Rs001 := TADOQuery.Create(nil);
  BDConectado:=False;
  BDConectado:=Open_DB (Cn001, 12 , Datos.ServerNameGeneral, Datos.DBGeneral,UserDB,PasswordDB, '',MensajeError,True );
  if BDConectado then
  Begin
    //Se Arma el Sql para la informacion del Servidor y empresa de Gestion
    SQL001:='SELECT PATHBD FROM EMPRESAS WITH (NOLOCK) WHERE CODEMPRESA = ' + IntToStr( Datos.NroEmpresaGestion );
    //Se Abre el DataSet de los clientes
    BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,'',false,True);
    // Verifica si se Abrio el DataSet
    if BDConectado then
    Begin
      //
      StrTexto01:='';
      StrDelimitador:=':';
      with rs001 do
      begin
        if not eof then
        Begin
          // Extraemos los datos del servidor y base de datos de Gestion
          StrTexto01:=Fields[0].AsString  ;
          if StrTexto01='' then   StrTexto01:='Servidor:Gestion';
          //Servidor y Base de datos de Gestion
          Datos.ServerNameGestion:=Extrar_DB_Server(StrTexto01,1);
          Datos.DBGestion:=Extrar_DB_Server(StrTexto01,2);;
        End;
      end;
    End;
  End;
  // Cerrar la Base de datos
  CloseConnection  (cn001, rs001);
End;










end.
