//------------------------------------------------------------------------------
//Unidad Principal del formulario de busqueda de cuentas contables
//ICG Dominicana
//Rafael Rangel
//Elaborado: 20160724
//
//
//------------------------------------------------------------------------------
unit UnitMainBuscarCC;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids,
  Data.DB, Data.Win.ADODB, Vcl.StdCtrls, Vcl.DBCtrls, Vcl.Buttons;

type
  TFrmBusquedaCC = class(TForm)
    PnlTop: TPanel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    QDatos: TADOQuery;
    DataSDatos: TDataSource;
    adcCn01: TADOConnection;
    rgbOpciones: TRadioGroup;
    lblValor: TLabel;
    EdtValor: TEdit;
    DBNCuentas: TDBNavigator;
    SpeBAceptar: TSpeedButton;
    SpeBCancelar: TSpeedButton;

    Function fn_Open_ADO_Qry(CnADO:TADOConnection; RsDataSet:TADOQuery ;StrSql:String;Lectura:Boolean): Boolean;
    Function fn_Open_DB (CnADO :TADOConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta: String ): Boolean;
    procedure FormCreate(Sender: TObject);
    procedure SpeBCancelarClick(Sender: TObject);
    procedure SpeBAceptarClick(Sender: TObject);
    procedure rgbOpcionesClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdtValorKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }
  public
    { Public declarations }
     //Variables del Formulario
    f_ServerDB,
    f_NameDB,
    f_UsersDB,
    f_PasswordDB:String;

  end;

var
  FrmBusquedaCC: TFrmBusquedaCC;
  StrSQL,
  Mensaje:String;
  Conectado:Boolean;
  CodCuenta,NomCuenta:String;

implementation
{$R *.dfm}




//------------------------------------------------------------------------------
//Activacion del Formulario
//------------------------------------------------------------------------------

procedure TFrmBusquedaCC.FormActivate(Sender: TObject);
begin
  //----------------------------------------------------------------------------
  //Define los Objetos ADO
  //----------------------------------------------------------------------------

  rgbOpciones.ItemIndex:=0;
  rgbOpcionesClick(Sender);
  EdtValor.Setfocus;
end;

//------------------------------------------------------------------------------
//Creaci�n del Formulario
//------------------------------------------------------------------------------

procedure TFrmBusquedaCC.FormCreate(Sender: TObject);
begin
  Conectado:=False;
   MessageDlg(f_NameDB,
          mterror, [mbok],0);

  Conectado:=False;
  if (f_ServerDB='') or (f_NameDB='') or (f_UsersDB='') or (f_PasswordDB='') then    Exit;
  Conectado:=fn_Open_DB (adcCn01 , 2 , f_ServerDB,  f_NameDB, f_UsersDB,f_PasswordDB, '' );
  //-------------------------------------------------------------------------
  // Verifica si se Abrio la conexi�n a la base de datos
  //--------------------------------------------------------------------------
  if (Conectado=False)   then Exit  ;
  //----------------------------------------------------------------------------
  //si Arma el sql para leer la tabla de cuentas
  //----------------------------------------------------------------------------
  StrSQL:='' ;
  StrSQL:='SELECT  CODIGO AS [C�digo], TITULO as [Descripci�n] FROM CUENTAS WITH (NOLOCK)';
  //----------------------------------------------------------------------------
  //Se Abre el DataSet
  //----------------------------------------------------------------------------
  QDatos.Open;
  QDatos.Active:=True;


end;


//------------------------------------------------------------------------------
//Definir las busquedas de la ventana
//------------------------------------------------------------------------------
procedure TFrmBusquedaCC.rgbOpcionesClick(Sender: TObject);
var
  temp,
  ElFiltro : string;

begin

  ElFiltro:='';
  temp:='';
  With Qdatos do
  Begin

    temp := Stringreplace(EdtValor.Text, '''', '''''', [rfReplaceAll, rfIgnoreCase]);
    if temp =  '' then Exit;

    //--------------------------------------------------------------------------
    //Se Arma la busqueda del campo se acuerdo a los valores indicados
    //--------------------------------------------------------------------------

    Case rgbOpciones.ItemIndex of
      0:
      Begin
        lblValor.Caption:='C�digo de la Cuenta';
         ElFiltro := '(CODIGO LIKE ''%' + temp +'%'')' ;
      End;
      1:
      Begin
        lblValor.Caption:='Descripci�n de la Cuenta';
        ElFiltro := '(TITULO LIKE ''%' + temp + '%'')' ;
      End;
    End;
    //--------------------------------------------------------------------------
    // Se activa el Flitro
    //--------------------------------------------------------------------------
    Qdatos.Filter:=ElFiltro;
    Filtered:=True;
  End;
   //---------------------------------------------------------------------------
   EdtValor.Setfocus;
end;


//------------------------------------------------------------------------------
//Funcion Para Abrir la Base de datos utilizando Ado y varios Provedores de base de datos
//------------------------------------------------------------------------------
Function TFrmBusquedaCC.fn_Open_DB (CnADO :TADOConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta: String ): Boolean;

//Variables de la Funcion
Var
  ConStr: String; // Cadena de Conexion

Begin

  //Cerramos la Conexion
  if CnADO.Connected then CnADO.close ;
  Case OpcionBd  of

    2 : //SQL Server
      Begin
        // Se Arma La Cadena de Conexion
        ConStr:=  'Provider=sqloledb;' +
                  'Data Source=' + Servidor + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'User id=' + Usuario + ';' +
                  'Password=' + Clave + ';' ;


      End;


  end;

  Result:=false;
  CnADO.Close ;
  CnADO.ConnectionString:=ConStr  ;
  CnADO.LoginPrompt:=false;


  if (NOT CnADO.Connected) Then
  Try
    //Se Abre la Conexion a la Base de datos
    CnADO.Mode:=cmRead;
    CnADO.Open;

    Result:=True;

  // Si no se puede conectar se devuelve falso y el mensaje de error
  Except On E:Exception do
    Begin
        MessageDlg('No se Puede Conectar a ' + Servidor + ':' + BD + ' El error es : ' + #13#10 + E.Message,
        mterror, [mbok],0);
        Result:=False;
    End;
  End;

End;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Funcion que convierte el Enter como un tab
//------------------------------------------------------------------------------
procedure TFrmBusquedaCC.EdtValorKeyPress(Sender: TObject; var Key: Char);
begin
If Key = #13 Then Begin
    If HiWord(GetKeyState(VK_SHIFT)) <> 0 then
    Begin
     SelectNext(Sender as TWinControl,False,True)
    End
    else
    Begin
     SelectNext(Sender as TWinControl,True,True) ;
     Key := #0;
     SpeBAceptarClick(Sender);
    End;

   end;
end;
//------------------------------------------------------------------------------
//Funcion que Abre un DataSet
//------------------------------------------------------------------------------

Function TFrmBusquedaCC.fn_Open_ADO_Qry(CnADO:TADOConnection; RsDataSet:TADOQuery ;StrSql:String;Lectura:Boolean): Boolean;
Begin
  Result:=False;

  if CnADO.Connected Then
  Begin
    with RsDataSet do begin
      Close;
      Connection:=CnADO;
      SQL.Clear;
      SQL.Text:=StrSQL;
      //LockType:=ltReadOnly;

      // Verificamos que se pueda abrir la Consulta
      try
        Open;
        Result:=True;

      Except On E:Exception do
        Begin
          MessageDlg('No se Puede Consultar la Informacion Solicitada, el Error es :' + E.Message,
          mterror, [mbok],0);
          Result:=False;
        End;
      end;
    End;
  End;
End ;


//------------------------------------------------------------------------------
//  Se Pulso sobre el Boton de Aceptar
//-------------------------------------------------------------------------------
procedure TFrmBusquedaCC.SpeBAceptarClick(Sender: TObject);
begin
  CodCuenta:=Qdatos.Fields[0].AsString;
  NomCuenta:=Qdatos.Fields[1].AsString;
  Close;

end;

//------------------------------------------------------------------------------
//  Se Pulso sobre el Boton de Cancelar
//-------------------------------------------------------------------------------

procedure TFrmBusquedaCC.SpeBCancelarClick(Sender: TObject);
begin
  CodCuenta:='';
  NomCuenta:='';
  Close;
end;

end.
