//
{Formulario para el registro de los roles de usuarios
ICG Dominicana
Rafael Rangel


}

//
unit uFrmRolesUsers;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, System.UITypes,
  System.ImageList, Vcl.ImgList, cxImageList, cxGraphics,
  Data.Win.ADODB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Phys.SQLite, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.Phys.MSSQLDef, FireDAC.Phys.ODBCBase,
  FireDAC.Phys.MSSQL  ,


  uUsersV03,uDBManagement;

type
  TFrmRolesUsers = class(TForm)
    cxImgMain: TcxImageList;
    grpMain: TGridPanel;
    pnlTop: TPanel;
    lblIdGroup: TLabel;
    lblNombre: TLabel;
    edtIdRol: TEdit;
    edtNameRol: TEdit;
    pnlLeft: TPanel;
    btnOk: TButton;
    pnlRigth: TPanel;
    btnCancelar: TButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    TypeSecurity,IdRol:Integer;
    DatosConexionDB:TConexionDBInfo ;
    DeleteReg:boolean;

  end;

var
  FrmRolesUsers: TFrmRolesUsers;
  FDCnGestion:TFDConnection;
  FDLinkMSSQL : TFDPhysMSSQLDriverLink;
  FDLinkSQLite : TFDPhysSQLiteDriverLink;
  FDQryRoles: TFDQuery;
  MsgError:String;
  VerMsgError, NewRol:Boolean;




implementation

{$R *.dfm}

//Cierra el formulario
procedure TFrmRolesUsers.btnCancelarClick(Sender: TObject);
begin
  Close
end;


//------------------------------------------------------------------------------
//Guarda el registro
//------------------------------------------------------------------------------
procedure TFrmRolesUsers.btnOkClick(Sender: TObject);
Var
  Valor,MsgError:String;
  buttonSelected : Integer;
begin

  // Si se indico que debe borrarse el registro
  if DeleteReg
    then
      buttonSelected := messagedlg('�Esta Seguro de BORRAR la informaci�n?',mtWarning , [mbYes,mbCancel], 0)
    Else
       buttonSelected := messagedlg('�Esta Seguro de guardar la informaci�n?',mtInformation, [mbYes,mbCancel], 0);
 // se cancela el proceso de guardar o borrar la informaci�n
  if buttonSelected = mrNo
    Then
      Exit;
  try
    //--------------------------------------------------------------------------
    // Se verifica si los valores a guardar en el registro estan correctos
    //--------------------------------------------------------------------------
    if edtNameRol.Text='' then
    Begin
      MessageDlg('Indique por favor el nombre del Rol del usuario',mterror, [mbok],0);
      edtNameRol.SetFocus;
    End;

    //si se debe eliminar el registro
    if DeleteReg Then
    Begin
      FDQryRoles.Delete ;
      //Borrar las dependencias que se tienen con los roles.
      //Borrar los roles x grupo de usuarios
      FDCnGestion.ExecSQL('DELETE FROM ICGDO_TB_acGroupAccess WHERE IDROL=' + IntToStr(IdRol));
      //Borrar los roles x  usuarios
      FDCnGestion.ExecSQL('DELETE FROM  ICGDO_TB_acUsersAccess WHERE IDROL=' + IntToStr(IdRol));

    End
    Else
    Begin //se edita el registro
      FDQryRoles.FieldByName('NameRol').Value:=edtNameRol.Text;
      //se aplican los cambios en la base de datos
      FDQryRoles.Post;
    End;

    //cerramos el formulario
    Close;
  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      MessageDlg(E.Message,mterror, [mbok],0);
    End;
  End;
end;

//------------------------------------------------------------------------------
//Se crea el Formulario
//------------------------------------------------------------------------------
procedure TFrmRolesUsers.FormCreate(Sender: TObject);
begin
  try
    MsgError:='';
    VerMsgError:=True;
  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje

      MessageDlg(E.Message,mterror, [mbok],0);
    End;
  End;
end;

//------------------------------------------------------------------------------
// Se muestra el formulario
//------------------------------------------------------------------------------
procedure TFrmRolesUsers.FormShow(Sender: TObject);
begin
  Try
    //--------------------------------------------------------------------------
    //Se crean los objetos de base de datos
    //--------------------------------------------------------------------------
    FDCnGestion:=TFDConnection.Create(Self);
    FDQryRoles:= TFDQuery.Create(self);
    //--------------------------------------------------------------------------
    //Se abre la conexi�n a la base de datos
    //--------------------------------------------------------------------------
    with  FDCnGestion do
    Begin
      if DatosConexionDB.TConexDBLocal <=1 then  //Sql Server
      begin
        //asignamos el driver de conexi�n
        FDLinkMSSQL := TFDPhysMSSQLDriverLink.Create(Self);
        FDCnGestion.DriverName :=  FDLinkMSSQL.BaseDriverId;
        //se abre la base de datos de Gesti�n
        Conectado:=Open_DB_FD (FDCnGestion,12,DatosConexionDB.ServerDB, DatosConexionDB.NameDBGestion
        , DatosConexionDB.UserDB, DatosConexionDB.PasswordDB,DatosConexionDB.PathDB, MsgError,VerMsgError);

        if Conectado then
        Begin
          //Qry Roles de usuarios
          FDQryRoles.SQL.Text:='';
          FDQryRoles.SQL.Text:='SELECT  IDRol, NameRol FROM  ' +
          ' ICGDO_TB_acRoles AS A WITH (NOLOCK) ' +
          ' WHERE IDRol=' + IntToStr(IdRol);
          FDQryRoles.Connection:=FDCnGestion;
        end;
      end
      else //Sql Lite
      begin
        FDLinkSQLite := TFDPhysSqliteDriverLink.Create(Self);
        FDCnGestion.DriverName := FDLinkSQLite.BaseDriverId;

     {   if FileExists(usrRutaDBLocal+usrNameDBLocal)then
        begin
          try
            //------------------------------------------------------------------------
            // Parametros de Conexion a la base de datos
            //------------------------------------------------------------------------
            DriverName:='SQLite';
            Params.Add('Database=' +usrRutaDBLocal+usrNameDBLocal );
            Open();
            Connected:=True;
            Conectado:=Connected;
          except
          on E: EDatabaseError do
            Begin
              // Se guarda el error en el log
              Mensaje:=E.Message;


            End;
          end;
        end
        else
        begin
          MessageDlg('No se pudo conectar a la base de datos', mterror, [mbOK], 0);
          Exit;
        end;
      end;}
      End;
      //------------------------------------------------------------------------
      //Si no hay conexi�n se interrumpe la ejecuci�n de la aplicaci�n
      //------------------------------------------------------------------------
      if Conectado=False then
      Begin
        MessageDlg('No se pudo conectar a la base de datos: ' + MsgError, mterror, [mbOK], 0);
        PostMessage(Self.Handle,wm_close,0,0);
      End;
    end;
    //--------------------------------------------------------------------------
    //Abrir los data set
    //--------------------------------------------------------------------------
    //usuarios
    with FDQryRoles do
    Begin
      Connection:=FDCnGestion;
      Close;
      //se abre el dataset
      Active := true;
      NewRol:=True;
      //si esta abierto
      if Active then
      begin
        //si tiene registros
        if RecordCount>0 then
        begin
          //primer registro
          First;
          NewRol:=False;
        end;
      end;
      //------------------------------------------------------------------------
      //si es un nuevo rol
      //------------------------------------------------------------------------
      if NewRol then
      Begin
        edtIdRol.Text:='0';
        edtNameRol.Text:='';
        Append;
      End
      Else //ya existe
      Begin
        // se toman los valores de la taba
        edtIdRol.Text:=FieldByName('IdRol').AsString;
        edtNameRol.Text:=FieldByName('NameRol').AsString;

        //si es una edici�n se coloca el data set en modo edici�n.
        if DeleteReg=False
          then
            Edit
          Else
            edtNameRol.Enabled:=False; //se desectiva el edit del nombre
      End;
    End;



  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      MessageDlg(E.Message,mterror, [mbok],0);
    End;
  End;
end;

end.
