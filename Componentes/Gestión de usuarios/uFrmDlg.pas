//------------------------------------------------------------------------------
{Unit para la gesti�n de dialogos al usuatio

}
//------------------------------------------------------------------------------
unit uFrmDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, cxImageList, cxGraphics;

type
  TfrmDlg = class(TForm)
    GridPanel1: TGridPanel;
    pnlTop: TPanel;
    pnlLeft: TPanel;
    pnlRigth: TPanel;
    cxImgL01: TcxImageList;
    btnOk: TButton;
    btnCancel: TButton;
    Label1: TLabel;
    mmoInfo: TMemo;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
     OkButton:Boolean;
  end;

var
  frmDlg: TfrmDlg;

implementation

{$R *.dfm}

procedure TfrmDlg.btnCancelClick(Sender: TObject);
begin
  OkButton:=False;
end;

procedure TfrmDlg.btnOkClick(Sender: TObject);
begin
  OkButton:=True;
end;

end.
