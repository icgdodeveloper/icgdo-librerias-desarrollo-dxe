program ICGDO_GestionUsuarios;

uses
  Vcl.Forms,
  uFrmLogin in 'uFrmLogin.pas' {frmLogin},
  UUsersV03 in '..\..\Units\UUsersV03.pas',
  uLogApps in '..\..\Units\uLogApps.pas',
  uFileManagement in '..\..\Units\uFileManagement.pas',
  uDBManagement in '..\..\Units\uDBManagement.pas',
  uSecurityManagement in '..\..\Units\uSecurityManagement.pas',
  uFrmMain in 'uFrmMain.pas' {frmMain},
  uFrmCreateDB in 'uFrmCreateDB.pas' {frmCreateDB},
  uFrmDlg in 'uFrmDlg.pas' {frmDlg},
  uUserForm in 'E:\Unit\uUserForm.pas' {frmUser},
  uFrmUsers in 'uFrmUsers.pas' {FrmUsers},
  uFrmRolesUsers in 'uFrmRolesUsers.pas' {FrmRolesUsers},
  uFrmGroups in 'uFrmGroups.pas' {FrmGroups},
  uFrmMttoUsers in 'uFrmMttoUsers.pas' {FrmMttoUsers};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmCreateDB, frmCreateDB);
  Application.CreateForm(TfrmDlg, frmDlg);
  Application.CreateForm(TfrmUser, frmUser);
  Application.CreateForm(TFrmUsers, FrmUsers);
  Application.CreateForm(TFrmRolesUsers, FrmRolesUsers);
  Application.CreateForm(TFrmGroups, FrmGroups);
  Application.CreateForm(TFrmMttoUsers, FrmMttoUsers);
  Application.Run;
end.
