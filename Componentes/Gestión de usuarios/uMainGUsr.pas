//------------------------------------------------------------------------------
{Unit de Login de usuario
ICG Dominicana
Rafael Rangel
 }
//------------------------------------------------------------------------------
unit uMainGUsr;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons,
  System.ImageList, Vcl.ImgList, cxImageList, cxGraphics, Vcl.StdCtrls,

  uLogApps
  ;

type
  TfrmLogin = class(TForm)
    grpMain: TGridPanel;
    pnlTop: TPanel;
    pnlLeft: TPanel;
    pnlRigth: TPanel;
    cxImgMain: TcxImageList;
    btnOk: TButton;
    btnCancelar: TButton;
    lblUser: TLabel;
    lblPassword: TLabel;
    edtUser: TEdit;
    Edit1: TEdit;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

{$R *.dfm}

procedure TfrmLogin.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

//Cerramos el formulario
procedure TfrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//Se retorna si el login es satisfactorio
end;

end.
