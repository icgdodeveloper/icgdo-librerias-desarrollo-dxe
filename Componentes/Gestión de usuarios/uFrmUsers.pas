//------------------------------------------------------------------------------
{Unit para el manejo de los usuarios
ICG Dominicana,SRL
Rafael Rangel
}
//------------------------------------------------------------------------------
unit uFrmUsers;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  System.Uitypes,  CommCtrl, System.ImageList, Vcl.ImgList, Vcl.Controls,
  cxImageList, cxGraphics, Vcl.Menus, Vcl.Buttons, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.ExtCtrls,
   Vcl.Forms, Vcl.Dialogs,
  DB,   Data.Win.ADODB,
  uDBManagement, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Phys.SQLite, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.Phys.MSSQLDef, FireDAC.Phys.ODBCBase,
  FireDAC.Phys.MSSQL  ,uUsersV03, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL, cxTLdxBarBuiltInMenu,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxSkinsCore,
  dxSkinsDefaultPainters, cxInplaceContainer, cxTLData, cxDBTL,
  Datasnap.DBClient, cxMaskEdit, cxCheckBox, FireDAC.Stan.StorageBin ,uLogApps,

  dxBar

  {, dxRibbon, dxRibbonForm, dxRibbonSkins, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxClasses, dxRibbonBackstageView, cxBarEditItem,
  dxSkinsCore, dxSkinsDefaultPainters, dxRibbonCustomizationForm, cxTextEdit,
  dxSkinsForm, dxStatusBar, dxRibbonStatusBar
   }
  { dxBar, dxRibbon, dxRibbonForm, dxRibbonSkins, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxClasses, dxRibbonBackstageView, cxBarEditItem,
  dxSkinsCore, dxSkinsDefaultPainters, dxRibbonCustomizationForm, cxTextEdit,
  dxSkinsForm, dxStatusBar, dxRibbonStatusBar, System.ImageList, Vcl.ImgList,
  cxImageList, Vcl.ExtCtrls, Vcl.ComCtrls,


  }


  ;

type
  TFrmUsers = class(TForm)
    pcPermissions: TPageControl;
    tshUsers: TTabSheet;
    Splitter1: TSplitter;
    Panel1: TPanel;
    Label1: TLabel;
    tvUsersUsers: TTreeView;
    Panel2: TPanel;
    Label2: TLabel;
    tvUsersPermissions: TTreeView;
    Panel4: TPanel;
    btnAddUser: TBitBtn;
    btnEditUser: TBitBtn;
    btnDeleteUser: TBitBtn;
    btnOK: TBitBtn;
    Panel10: TPanel;
    btnPermissionToUser: TSpeedButton;
    btnDelUserPermission: TSpeedButton;
    tshGroups: TTabSheet;
    Splitter2: TSplitter;
    Panel3: TPanel;
    Label4: TLabel;
    tvGroupsGroups: TTreeView;
    Panel5: TPanel;
    btnAddGroup: TBitBtn;
    btnEditGroup: TBitBtn;
    btnDeleteGroup: TBitBtn;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    Label3: TLabel;
    tvGroupsPermissions: TTreeView;
    Panel11: TPanel;
    btnPermissionToGroup: TSpeedButton;
    btnDelGroupPermission: TSpeedButton;
    tssMembership: TTabSheet;
    Splitter3: TSplitter;
    Panel7: TPanel;
    BitBtn6: TBitBtn;
    btnRemoveMembership: TBitBtn;
    Panel8: TPanel;
    Label6: TLabel;
    tvMembershipGroups: TTreeView;
    Panel9: TPanel;
    Label5: TLabel;
    Panel12: TPanel;
    btnUserToGroup: TSpeedButton;
    btnDelUserFromGroup: TSpeedButton;
    popGroups: TPopupMenu;
    AddGroup1: TMenuItem;
    miEditGroup: TMenuItem;
    miRemoveGroup: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    popUsers: TPopupMenu;
    AddUser1: TMenuItem;
    EditUser1: TMenuItem;
    Remove1: TMenuItem;
    N1: TMenuItem;
    Cut1: TMenuItem;
    PastePermissions1: TMenuItem;
    tshRigths: TTabSheet;
    Splitter4: TSplitter;
    Panel13: TPanel;
    Label7: TLabel;
    trvRolesPermission: TTreeView;
    Panel14: TPanel;
    Label8: TLabel;
    Panel15: TPanel;
    BitBtn2: TBitBtn;
    BitBtn5: TBitBtn;
    Panel16: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    cxImgList01: TcxImageList;
    tshRoles: TTabSheet;
    Panel17: TPanel;
    Label9: TLabel;
    trvRoles: TTreeView;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    btnAddRoles: TBitBtn;
    btnEditRoles: TBitBtn;
    btnDelRoles: TBitBtn;
    BitBtn10: TBitBtn;
    Splitter5: TSplitter;
    cxImgList02: TcxImageList;
    tvMembershipUsers: TTreeView;
    tvPermissionsRoles: TTreeView;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    Function  OpenDataSet (Var MsgError :String; VerMsgError:Boolean) :Boolean;
    procedure btnPermissionToUserClick(Sender: TObject);
    procedure btnDelUserPermissionClick(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnAddRolesClick(Sender: TObject);
    procedure btnEditRolesClick(Sender: TObject);

    procedure pFrmRoles (IdRol:Integer;DeleteReg:Boolean);
    procedure pFrmGrupos (IdGrupo:Integer;DeleteReg:Boolean);
    procedure pFrmUsers (IdUser:Integer;DeleteReg:Boolean);

    procedure btnDelRolesClick(Sender: TObject);
    procedure btnPermissionToGroupClick(Sender: TObject);
    procedure btnDelGroupPermissionClick(Sender: TObject);
    procedure trvRolesDblClick(Sender: TObject);
    procedure tvUsersUsersDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tvUsersUsersDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure pcPermissionsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tvGroupsGroupsDragDrop(Sender, Source: TObject; X, Y: Integer);

    function LeerObjetosFrm(IdApps:integer;InfoConexion:TConexionDBInfo;
    var MsgError :String ; VerMsgError:Boolean;TypeSecurity:Integer) :Boolean;

    //Insertar objetos
    procedure InsertObjetos (FDCn:TFDConnection;Descripcion,Objeto,TipoObjeto:String;IdPadre:Integer; ObjetoValido,Encriptado:Boolean);

    procedure LeerComponentesRecursivo(FDCn:TFDConnection;const AControl: TControl; IdPadre:Integer);

    function BuscarIdPadre_Objeto (FDCn:TFDConnection;Objeto:String;Encriptado:Boolean): Integer;
    procedure trvRolesPermissionClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);

    //------------------------------------------------------------------------------
    //Se guardan los permisos de los usuarios en la aplicaci�n
    //------------------------------------------------------------------------------
    procedure Update_Accesos_Usuarios  ;
    procedure trvRolesPermissionKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure trvRolesPermissionKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    function OnOffFormLocal (InfoConexion:TConexionDBInfo;
  InfoUsr:TUserData; TypeSecurity,IdApps:Integer; var MsgError:string ;
  VerMsgError:Boolean): Boolean;
    procedure btnAddUserClick(Sender: TObject);
    procedure btnEditUserClick(Sender: TObject);
    procedure btnDeleteUserClick(Sender: TObject);
    procedure btnAddGroupClick(Sender: TObject);
    procedure btnEditGroupClick(Sender: TObject);
    procedure btnDeleteGroupClick(Sender: TObject);
    procedure btnUserToGroupClick(Sender: TObject);
    procedure btnDelUserFromGroupClick(Sender: TObject);



  private
    { Private declarations }
    CurrentCopy : TTreeNode;
    procedure AddChild(SenderTree : TTreeView; var Node : TTreeNode; AText : string; AImageNum : integer);

  public
    { Public declarations }
    TypeSecurity,IdApps:Integer;
    DatosConexionDB:TConexionDBInfo ;
    InfoUsr:TUserData;
    procedure RefreshUsers;
    procedure RefreshGroups;
    procedure RefreshMembership;
    procedure RefreshRoles;

  end;

const
  TreeNodeChecked = 2;
  TreeNodeUnChecked = 1;
  TVIS_CHECKED  = $2000;
  TVIS_UNCHECKED= $1000;

var
  FrmUsers: TFrmUsers;
  MsgError:String;
  Conectado,VerMsgError:Boolean;

  oFrmObjetos: TForm;
  //Tipo data Source
  dso_AC_Groups,
  dso_AC_Users,
  dso_AC_GroupAccess ,
  dso_AC_Permissions,
  dso_AC_UserAccess,
  dso_AC_GroupMembers,
  dso_AC_Roles:TDataSource;



  //----------------------------------------------------------------------------
  //FireDac
  //----------------------------------------------------------------------------
  FDCnGestion:TFDConnection;
  FDCnGeneral:TFDConnection;
  FDLinkMSSQL : TFDPhysMSSQLDriverLink;
  FDLinkSQLite : TFDPhysSQLiteDriverLink;
  FDQry01: TFDQuery;

  FDQryUsers,FDQryAccessUsers,FDQryGroups,
  FDQryGroupAccess ,FDQryGroupMembers,FDQryRoles,
  FDQryPermisosCab
  : TFDQuery;
  NameFields:TNameFieldsAC ;


  IDRol,IdUser:Integer;


implementation
Uses uFrmRolesUsers,uFrmGroups,uFrmMttoUsers;
{$R *.dfm}


function IsChecked(Node :TTreeNode):boolean;
var TvItem:TTVItem;
begin
  TvItem.Mask := TVIF_STATE;
  TvItem.hItem := Node.ItemId;
  TreeView_GetItem(Node.TreeView.Handle, TvItem);
  Result :=(TvItem.State and TVIS_CHECKED>0);
end;
//------------------------------------------------------------------------------
//Se borra  al usuario un rol
//------------------------------------------------------------------------------

procedure TFrmUsers.BitBtn10Click(Sender: TObject);
begin
  Close;
end;

procedure TFrmUsers.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFrmUsers.BitBtn2Click(Sender: TObject);
Var
buttonSelected : Integer;
begin
  //----------------------------------------------------------------------------
  // Show a confirmation dialog
  //----------------------------------------------------------------------------
  buttonSelected := messagedlg('�Esta Seguro de Guardar el Registro?',mtInformation, [mbYes,mbCancel], 0);
  // Show the button type selected
  if buttonSelected = mrYes
  then
    Begin
      //Guardar_Cambios_Accesos;
      Update_Accesos_Usuarios;
      //RefrescarDataset;
    End;

end;

procedure TFrmUsers.BitBtn5Click(Sender: TObject);
begin
  Close;
end;

procedure TFrmUsers.BitBtn6Click(Sender: TObject);
begin
  Close;
end;


//------------------------------------------------------------------------------
//se crea o edita el grupo de usuarios
//------------------------------------------------------------------------------
procedure TFrmUsers.btnAddGroupClick(Sender: TObject);
begin
  try
    //se activa el formulario de roles, para insertar un registro
    pFrmGrupos (-1,False);

  Except On E:Exception do
    Begin
     MessageDlg('No se puede crear el formulario el error es : ' + #13#10 + E.Message,
     mterror, [mbok],0);
    End;
  End;
end;

//------------------------------------------------------------------------------
//se crea o edita el rol del usuario
//------------------------------------------------------------------------------
procedure TFrmUsers.btnAddRolesClick(Sender: TObject);
begin
  try
    //se activa el formulario de roles, para insertar un registro
    pFrmRoles (-1,False);

  Except On E:Exception do
    Begin
     MessageDlg('No se puede crear el formulario el error es : ' + #13#10 + E.Message,
     mterror, [mbok],0);
    End;
  End;
end;


//------------------------------------------------------------------------------
//Formulario para Agregar usuarios
//------------------------------------------------------------------------------
procedure TFrmUsers.btnAddUserClick(Sender: TObject);
begin
  try
    //se activa el formulario de usuarios, para insertar un registro
    pFrmUsers (-1,False);

  Except On E:Exception do
    Begin
     MessageDlg('No se puede crear el formulario el error es : ' + #13#10 + E.Message,
     mterror, [mbok],0);
    End;
  End;
end;

//----------------------------------------------------------------
//Se editan los grupos usuarios
//----------------------------------------------------------------
procedure TFrmUsers.btnEditGroupClick(Sender: TObject);
Var

  Id,i:Integer;
begin

  //se recorre el tree para tomar el valor seleccionado, el ID
  for i := 0 to tvGroupsGroups.Items.Count - 1 do
  Begin
    if tvGroupsGroups.Items[i].Selected then
    Begin
      Id:=Integer(tvGroupsGroups.Items[i].data);
      Break;
    End;
  End ;
  //se llama al procediento para editar un grupo.
  pFrmGrupos (Id,False);
end;

//----------------------------------------------------------------
//Se editan los roles de usuarios
//----------------------------------------------------------------
procedure TFrmUsers.btnEditRolesClick(Sender: TObject);
Var
  Node: TTreeNode;
  Id,i:Integer;
begin

  //se recorre el tree para tomar el valor seleccionado, el ID
  for i := 0 to trvRoles.Items.Count - 1 do
  Begin
    if trvRoles.Items[i].Selected then
    Begin
      Id:=Integer(trvRoles.Items[i].data);
      Break;
    End;
  End ;
  //se llama al procediento para editar un rol.
  pFrmRoles (Id,False);
end;

//------------------------------------------------------------------------------
//Formulario para modificar el usuario
//------------------------------------------------------------------------------
procedure TFrmUsers.btnEditUserClick(Sender: TObject);
Var

  Id,i:Integer;
begin

  //se recorre el tree para tomar el valor seleccionado, el ID
  for i := 0 to tvUsersUsers.Items.Count - 1 do
  Begin
    if tvUsersUsers.Items[i].Selected then
    Begin
      Id:=Integer(tvUsersUsers.Items[i].data);
      Break;
    End;
  End ;
  //se llama al procediento para editar un grupo.
  pFrmUsers (Id,False);
//
end;

//Borrado del Nodo
procedure TFrmUsers.btnDelRolesClick(Sender: TObject);
Var
  Node: TTreeNode;
  Id,i:Integer;
begin

  //se recorre el tree para tomar el valor seleccionado, el ID
  for i := 0 to trvRoles.Items.Count - 1 do
  Begin
    if trvRoles.Items[i].Selected then
    Begin
      Id:=Integer(trvRoles.Items[i].data);
      Break;
    End;
  End ;
  //se llama al procediento para editar un rol.
  pFrmRoles (Id,True);

end;


//------------------------------------------------------------------------------
//Borra el usuario
//------------------------------------------------------------------------------
procedure TFrmUsers.btnDeleteGroupClick(Sender: TObject);
Var

  Id,i:Integer;
begin

  //se recorre el tree para tomar el valor seleccionado, el ID
  for i := 0 to tvGroupsGroups.Items.Count - 1 do
  Begin
    if tvGroupsGroups.Items[i].Selected then
    Begin
      Id:=Integer(tvGroupsGroups.Items[i].data);
      Break;
    End;
  End ;
  //se llama al procediento para editar un rol.
  pFrmGrupos (Id,True);
end;

procedure TFrmUsers.btnDeleteUserClick(Sender: TObject);
Var
  Id,i:Integer;
begin

  //se recorre el tree para tomar el valor seleccionado, el ID
  for i := 0 to tvUsersUsers.Items.Count - 1 do
  Begin
    if tvUsersUsers.Items[i].Selected then
    Begin
      Id:=Integer(tvUsersUsers.Items[i].data);
      Break;
    End;
  End ;
  //se llama al procediento para editar un rol.
  pFrmUsers (Id,True);
//
end;

//------------------------------------------------------------------------------
//Borra los roles de los grupos
//------------------------------------------------------------------------------

procedure TFrmUsers.btnDelGroupPermissionClick(Sender: TObject);
var
  t01SQL:string;
  tidRol,tidGroup,buttonSelected:Integer;
begin
  Try

    if (tvGroupsGroups.Selected = nil) then Exit;

    //si no esta seleccionado un rol
    if tvGroupsGroups.Selected.Parent = nil Then
    Begin
      messagedlg('Debe seleccionar un rol de usuario del grupo: ' + tvGroupsGroups.Selected.Text , mtInformation, [mbOk], 0);
      exit;
    End;



    //----------------------------------------------------------------------------
    // Show a confirmation dialog
    //----------------------------------------------------------------------------
    buttonSelected := messagedlg('Esta seguro de borrar los permisos del grupo: ' + tvGroupsGroups.Selected.Text + '?', mtConfirmation, [mbYes, mbNo], 0);

    // Show the button type selected
    if buttonSelected = mrNo
      then
        Exit;

    tidGroup:=0;
    tidRol:=Integer(tvGroupsGroups.Selected.data);
    if tvGroupsGroups.Selected.Parent <> nil
      then
        tidGroup:=Integer(tvGroupsGroups.Selected.Parent.data);

    t01SQL:='';
    t01SQL:= 'DELETE FROM ICGDO_TB_acGroupAccess WHERE  GroupID = ' + Inttostr(tidGroup) +
    ' AND Id=' + (IntTostr(tidRol)) ;

    //------------------------------------------------------------------------
    //Se ejecuta el SQL para eliminar los permisos del usuario
    //------------------------------------------------------------------------
    FDCnGestion.ExecSQL(t01SQL);

    //se actualiza el data set
    with FDQryGroupAccess do
    Begin
      Connection:=FDCnGestion;
      Close;
      //se abre el dataset
      Active := true;
      First;
    End;

    //refresca los objetos
    RefreshGroups;
    RefreshMembership;
    RefreshRoles;
    RefreshUsers;

   
  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;

end;


//------------------------------------------------------------------------------
//Borra el usuario del grupo
//------------------------------------------------------------------------------
procedure TFrmUsers.btnDelUserFromGroupClick(Sender: TObject);
begin
  Try
    //--------------------------------------------------------------------------
    //Se verifica que este posicionado en nivel correcto
    //--------------------------------------------------------------------------
    if not Assigned(tvMembershipGroups.Selected) or (tvMembershipGroups.Selected.Parent = nil )  then
    Begin
      MessageDlg('Debe seleccionar el usuario', mtError, [mbOK], 0)  ;
      Exit;
    End;

    if MessageDlg('Esta seguro de borrar el usuario: ' +
      tvMembershipGroups.Selected.Text  +

      ' del grupo: ' +  tvMembershipGroups.Selected.Parent.text  + ' ?', mtConfirmation, [mbYes, mbNo], 0) = idYes then
    begin

      //-------------------------------------------------------------------------
      //Buscar el id del usuario
      //-------------------------------------------------------------------------
      Conectado:=RemoveUserFromGroup(IntTostr(Integer(tvMembershipGroups.Selected.Data)),IntTostr(Integer(tvMembershipGroups.Selected.Parent.Data)), dso_AC_GroupMembers,DatosConexionDB,
        MsgError,  VerMsgError,TypeSecurity,NameFields)  ;
    end;

     //--------------------------------------------------------------------------
    //Se refresca los usuarios mienbros de cada grupo
    //--------------------------------------------------------------------------
   //refresca los objetos
   dso_AC_GroupMembers.DataSet.Close;
   dso_AC_GroupMembers.DataSet.open;
   RefreshMembership;


  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;

procedure TFrmUsers.btnDelUserPermissionClick(Sender: TObject);
begin
 Try
    if tvUsersUsers.Selected = nil then exit;

    if tvUsersUsers.Selected.Parent = nil then
    begin
      if MessageDlg('Esta seguro de borrar el Rol del usuario ' + tvUsersUsers.Selected.Text + '?', mtConfirmation, [mbYes, mbNo], 0) = idYes then
      begin
        if LocateValue(Integer(tvUsersUsers.Selected.Data), NameFields.UserIDField, dso_AC_UserAccess,
        DatosConexionDB,MsgError,  VerMsgError) then
        begin
          dso_AC_UserAccess.Dataset.Edit;
          dso_AC_UserAccess.Dataset.Delete;
        end;
         RefreshUsers;
      end;
    end
    else
    begin

      if LocateValue(intToStr(Integer(tvUsersUsers.Selected.Parent.Data)), NameFields.UserIDField, dso_AC_UserAccess,DatosConexionDB,
     MsgError,  VerMsgError) then
      begin
        Conectado:=RemoveUserAccess(intToStr((Integer(tvUsersUsers.Selected.Parent.Data))),IntTostr(Integer(tvUsersUsers.Selected.Data)), dso_AC_UserAccess,DatosConexionDB,
     MsgError,  VerMsgError,TypeSecurity,NameFields)  ;
        if Conectado then tvUsersUsers.Selected.Delete
      end;

    end;
  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;

procedure TFrmUsers.btnOKClick(Sender: TObject);
begin
   Close;
end;

//------------------------------------------------------------------------------
//Se a�ade al grupo los roles
//------------------------------------------------------------------------------
procedure TFrmUsers.btnPermissionToGroupClick(Sender: TObject);
var
   Node : TTreeNode;
begin
  try
    if not Assigned(tvGroupsPermissions.Selected) then Exit;
    Node := tvGroupsGroups.Selected;
    if not Assigned(Node) then Exit;
    //se recorre el nodo
    while Assigned(Node.Parent) do
      Node := Node.Parent;



      if LocateValue(Node.Text,NameFields.GroupNameField,dso_AC_Groups,
        DatosConexionDB,  MsgError,  VerMsgError) then
      Begin
        Conectado:=AddGroupAccess(IntToStr(Integer(Node.Data)),IntToStr(Integer(tvGroupsPermissions.Selected.Data)),
        dso_AC_GroupAccess,DatosConexionDB,  MsgError,  VerMsgError,TypeSecurity,NameFields)  ;

        if  Conectado then
          AddChild(tvGroupsGroups, Node, tvGroupsPermissions.Selected.Text, 7);

      end;


    Node.Selected := true;
    Node := tvGroupsPermissions.Selected.GetNextSibling;
    if Node <> nil then
      Node.Selected := true;
  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;


end;

//------------------------------------------------------------------------------
//Se a�ade al usuario un rol
//------------------------------------------------------------------------------

procedure TFrmUsers.btnPermissionToUserClick(Sender: TObject);
var
   Node : TTreeNode;
begin
  Try
    if not Assigned(tvUsersPermissions.Selected) then
     Exit;

    Node := tvUsersUsers.Selected;
    if not Assigned(Node) then
     Exit;

    while Assigned(Node.Parent) do
      Node := Node.Parent;

     if LocateValue(Node.Text, NameFields.UserNameField, dso_AC_Users,DatosConexionDB,
     MsgError,  VerMsgError ) then
     Begin
      Conectado:= AddUserAccess(IntToStr(Integer(Node.Data)), IntToStr(Integer(tvUsersPermissions.Selected.Data)),dso_AC_UserAccess
      ,DatosConexionDB,  MsgError,  VerMsgError,TypeSecurity,NameFields)  ;
      if Conectado
        then
         AddChild(tvUsersUsers, Node, tvUsersPermissions.Selected.Text, 7);
     End;
    Node.Selected := true;
    Node := tvUsersPermissions.Selected.GetNextSibling;
    if Node <> nil then
      Node.Selected := true;
 except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;

end;


//------------------------------------------------------------------------------
//Se agregan usuarios al grupo
//------------------------------------------------------------------------------
procedure TFrmUsers.btnUserToGroupClick(Sender: TObject);
var
  Node : TTreeNode;
begin
  try

    //Se verifica que esten seleccionados los tree
    if not Assigned(tvMembershipUsers.Selected) then
    Begin
     MessageDlg('Debe seleccionar el usuario, para poder agregar al grupo', mtError, [mbOK], 0)  ;
     Exit;
    End ;

    if not Assigned(tvMembershipGroups.Selected) then
    Begin
      MessageDlg('Debe seleccionar el grupo', mtError, [mbOK], 0)  ;
      Exit;
    End;


    Node := tvMembershipUsers.Selected;
    if not Assigned(Node) then
     Exit;

    while Assigned(Node.Parent) do
      Node := Node.Parent;

    tvMembershipUsers.Selected;
    tvMembershipGroups.Selected;

     Conectado:=AddUserToGroup( IntToStr(Integer(tvMembershipUsers.Selected.Data)),IntToStr(Integer(tvMembershipGroups.Selected.Data)),
        dso_AC_GroupMembers,DatosConexionDB,  MsgError,  VerMsgError);


     if  Conectado then
     Begin
      AddChild(tvMembershipGroups, Node, tvMembershipUsers.Selected.Text, 0);
      Node.Data := Pointer(Integer(tvMembershipUsers.Selected.Data))   ;

     End;

     Node.Selected := true;
     Node := tvMembershipUsers.Selected.GetNextSibling;

     if assigned(Node)
      then
        Node.Selected := true;

    //--------------------------------------------------------------------------
    //Se refresca los usuarios mienbros de cada grupo
    //--------------------------------------------------------------------------
    Conectado:=opendataset(MsgError, VerMsgError);
    RefreshMembership;




 except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;

//------------------------------------------------------------------------------
//Se crea el formulario
//------------------------------------------------------------------------------
procedure TFrmUsers.FormCreate(Sender: TObject);
begin
  //

  Try
    pcPermissions.ActivePage := tshUsers;
    VerMsgError:=True;
    //Nombre de los campos de los data Set

    NameFields.AccessUsersIDField:='UserID';
    NameFields.UserIDField:='UserID';
    NameFields.UserNameField:='LoginName';
    NameFields.RolIDField:='IdRol';
    NameFields.NameRolField:='NameRol';
    NameFields.GroupIDField:='GroupID';
    NameFields.GroupNameField:='GroupName';
    NameFields.GroupAccessIDField:='ID';



    NameFields.GroupDescriptionField:='Description';
    NameFields.GroupPermissionsField:='Permissions';
    NameFields.UsersPermissionsField:='Permissions';
    NameFields.FullNameField:='FullName';
    NameFields.PasswordField:='Password';
    NameFields.AccessCountField:='AccessCount';
    NameFields.CreatedDateField:='CreatedDate';
    NameFields.LastAccessField:='LastAccess';
    NameFields.EnabledField:='Enabled';

    //----------------------------------------------------------------------------
    //Se activa el check box para la gesti�n de usuarios
    //----------------------------------------------------------------------------
    SetWindowLong(tvPermissionsRoles.Handle, GWL_STYLE, GetWindowLong(tvPermissionsRoles.Handle, GWL_STYLE) or TVS_CHECKBOXES);

  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;


//------------------------------------------------------------------------------
//Se muestra la informaci�n del formulario
//------------------------------------------------------------------------------
procedure TFrmUsers.FormShow(Sender: TObject);
Var
  ObjActivos:boolean;
begin

  Try
    //Se activa el tab de usuarios
    pcPermissions.ActivePage := tshUsers;
    //De acuerdo al tipo de seguridad se activan o desactivan botones
    if TypeSecurity <= 1 Then
    Begin
      btnAddUser.Enabled:=False;
      btnEditUser.Enabled:=False;
      btnDeleteUser.Enabled:=False;
      btnAddGroup.Enabled:=False;
      btnEditGroup.Enabled:=False;
      btnDeleteGroup.Enabled:=False;
    end;

    //--------------------------------------------------------------------------
    //Se crean los objetos de base de datos
    //--------------------------------------------------------------------------
    FDCnGestion:=TFDConnection.Create(Self);
    FDCnGeneral:=TFDConnection.Create(Self);
    FDQryUsers:= TFDQuery.Create(self);
    FDQryAccessUsers:= TFDQuery.Create(self);
    FDQryGroups:= TFDQuery.Create(self);
    FDQryGroupAccess := TFDQuery.Create(self);
    FDQryGroupMembers:= TFDQuery.Create(self);
    FDQryPermisosCab:= TFDQuery.Create(self);
    FDQryRoles:= TFDQuery.Create(self);


    //--------------------------------------------------------------------------
    //Se abre la conexi�n a la base de datos
    //--------------------------------------------------------------------------

    with  FDCnGestion do
    Begin
      if DatosConexionDB.TConexDBLocal <=1 then  //Sql Server
      begin
        //asignamos el driver de conexi�n
        FDLinkMSSQL := TFDPhysMSSQLDriverLink.Create(Self);
        FDCnGestion.DriverName :=  FDLinkMSSQL.BaseDriverId;

        //se abre la base de datos de Gesti�n
        Conectado:=Open_DB_FD (FDCnGestion,12,DatosConexionDB.ServerDB, DatosConexionDB.NameDBGestion
        , DatosConexionDB.UserDB, DatosConexionDB.PasswordDB,DatosConexionDB.PathDB, MsgError,VerMsgError);

        //Si no hay conexi�n a la base de datos de gesti�n
        if Conectado=False then
        Begin
          MessageDlg('No se pudo conectar a la base de datos: ' + MsgError, mterror, [mbOK], 0);
          PostMessage(Self.Handle,wm_close,0,0);
        End;

        //abre la conexi�n a la base de datos General
        Conectado:=Open_DB_FD (FDCnGeneral,12,DatosConexionDB.ServerDB, DatosConexionDB.NameDBGeneral
          , DatosConexionDB.UserDB, DatosConexionDB.PasswordDB,DatosConexionDB.PathDB, MsgError,VerMsgError);



      end
      else //Sql Lite
      begin
        FDLinkSQLite := TFDPhysSqliteDriverLink.Create(Self);
        FDCnGestion.DriverName := FDLinkSQLite.BaseDriverId;

     {   if FileExists(usrRutaDBLocal+usrNameDBLocal)then
        begin
          try
            //------------------------------------------------------------------------
            // Parametros de Conexion a la base de datos
            //------------------------------------------------------------------------
            DriverName:='SQLite';
            Params.Add('Database=' +usrRutaDBLocal+usrNameDBLocal );
            Open();
            Connected:=True;
            Conectado:=Connected;
          except
          on E: EDatabaseError do
            Begin
              // Se guarda el error en el log
              Mensaje:=E.Message;


            End;
          end;
        end
        else
        begin
          MessageDlg('No se pudo conectar a la base de datos', mterror, [mbOK], 0);
          Exit;
        end;
      end;}
      End;
      //------------------------------------------------------------------------
      //Si no hay conexi�n se interrumpe la ejecuci�n de la aplicaci�n
      //------------------------------------------------------------------------
      if Conectado=False then
      Begin
        MessageDlg('No se pudo conectar a la base de datos: ' + MsgError, mterror, [mbOK], 0);
        PostMessage(Self.Handle,wm_close,0,0);
      End;
    End;

    //----------------------------------------------------------------------------
    //Se crean los objetos y abren los data set
    //----------------------------------------------------------------------------
    dso_AC_Users:=TDataSource.Create(Self);
    dso_AC_UserAccess:=TDataSource.Create(Self);
    dso_AC_Groups:=TDataSource.Create(Self);
    dso_AC_GroupAccess:=TDataSource.Create(Self);
    dso_AC_GroupMembers:=TDataSource.Create(Self);
    dso_AC_Roles:=TDataSource.Create(Self);


    //se abren los data set
    Conectado:=OpenDataSet (MsgError, VerMsgError);

    //se asignan los dataset a los data suource.
    dso_AC_Users.DataSet:=FDQryUsers;
    dso_AC_UserAccess.DataSet:=FDQryAccessUsers;
    dso_AC_Groups.DataSet:=FDQryGroups;
    dso_AC_GroupAccess.DataSet:=FDQryGroupAccess;
    dso_AC_GroupMembers.DataSet:=FDQryGroupMembers;
    dso_AC_Roles.DataSet:=FDQryRoles;

    RefreshUsers;
    RefreshGroups;
    RefreshMembership;
    RefreshRoles;
    
    //se mustran las opciones del sistema
    Conectado:= LeerObjetosFrm(IdApps,DatosConexionDB,
      MsgError,True,TypeSecurity);


    //se verifican las opciones del usuario
    ObjActivos:=  OnOffFormLocal ( DatosConexionDB,
    InfoUsr, TypeSecurity,IdApps,
    MsgError,VerMsgError);
    if ObjActivos=False
      then
        Close;




  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;

//------------------------------------------------------------------------------
//Funci�n para abrir los data set
//------------------------------------------------------------------------------
Function TFrmUsers.OpenDataSet (Var MsgError :String; VerMsgError:Boolean) :Boolean;
Var
  OpenDset:boolean;
begin

  Try
    OpenDset:=False;
    if DatosConexionDB.TConexDBLocal <=1 then  //Sql Server
    begin
      //Qry de usuarios

      If TypeSecurity=0 Then  //ICG Front
      Begin
        //Usuarios
        FDQryUsers.SQL.Text:='';
        FDQryUsers.SQL.Text:='SELECT CODVENDEDOR AS UserID,  NOMVENDEDOR AS LoginName, '+
        ' NOMVENDEDOR AS FullName, NEWPASSENTRADA AS Password , ' +
        ' GetDate() AS LastAccess, (CASE WHEN ISNULL(DESCATALOGADO,''F'') = ''T'' THEN ''false'' ELSE ''true'' END)  AS Enabled,'   +
        ' GetDate() AS CreatedDate, TIPOUSUARIO AS GroupID, ' +
        ' (CASE WHEN TIPOUSUARIO=0 THEN ''true'' ELSE ''false'' END) AS SysAdmin FROM VENDEDORES AS A WITH (NOLOCK) ' +
        ' WHERE ISNULL(DESCATALOGADO,''F'') =''F''' +
        ' AND ISNULL(BLOQUEADO,''F'') =''F''' ;
        FDQryUsers.Connection:=FDCnGestion;


        //Qry Usuarios x grupo
        FDQryGroupMembers.SQL.Text:='';
        FDQryGroupMembers.SQL.Text:='SELECT A.CODUSUARIO AS GroupID, ' +
        ' A.USUARIO AS GroupName ,CODVENDEDOR AS UserID, B.NOMVENDEDOR AS LoginName  ' +
        ' FROM ' + DatosConexionDB.NameDBGeneral  + '.dbo.USUARIOS AS A WITH (NOLOCK) ' +
        ' LEFT JOIN VENDEDORES AS B WITH (NOLOCK) ON A.CODUSUARIO=B.TIPOUSUARIO ';
        FDQryGroupMembers.Connection:=FDCnGestion;

        //Qry de Grupos
        FDQryGroups.SQL.Text:='';
        FDQryGroups.SQL.Text:='SELECT  CODUSUARIO AS GroupID,  USUARIO AS GroupName, '''' AS Description'+
          ' FROM USUARIOS AS A WITH (NOLOCK) WHERE ISNULL(BLOQUEADO,''F'') <> ''T''' ;
        FDQryGroups.Connection:=FDCnGeneral;


      end
      Else
      Begin
        If TypeSecurity=1 Then // ICG Manager
        Begin
          //Usuarios
          FDQryUsers.SQL.Text:='';
          FDQryUsers.SQL.Text:='SELECT CODUSUARIO AS UserID,  USUARIO AS LoginName,' +
		      ' USUARIO AS FullName, NEWPASS AS Password , ' +
          ' GetDate() AS LastAccess, (CASE WHEN ISNULL(DESCATALOGADO,''F'') = ''T'' THEN ''false'' ELSE ''true'' END)  AS Enabled,' +
          ' GetDate() AS CreatedDate, CODUSUARIO AS GroupID,' +
          ' (CASE WHEN CODUSUARIO=0 THEN ''true'' ELSE ''false'' END) AS SysAdmin FROM USUARIOS AS A WITH (NOLOCK)  ' +
          ' WHERE ISNULL(DESCATALOGADO,''F'') =''F'' ' +
          ' AND ISNULL(BLOQUEADO,''F'') =''F''' ;
          FDQryUsers.Connection:=FDCnGeneral;


          //Qry Usuarios x grupo
          FDQryGroupMembers.SQL.Text:='';
          FDQryGroupMembers.SQL.Text:='SELECT A.CODUSUARIO AS GroupID, ' +
          ' A.USUARIO AS GroupName ,A.CODUSUARIO AS UserID, A.USUARIO AS LoginName ' +
          ' FROM GENERAL.dbo.USUARIOS AS A WITH (NOLOCK) ';
          FDQryGroupMembers.Connection:=FDCnGeneral;

          //Qry de Grupos
          FDQryGroups.SQL.Text:='';
          FDQryGroups.SQL.Text:='SELECT  CODUSUARIO AS GroupID,  USUARIO AS GroupName, '''' AS Description'+
          ' FROM USUARIOS AS A WITH (NOLOCK) WHERE ISNULL(BLOQUEADO,''F'') <> ''T''' ;
          FDQryGroups.Connection:=FDCnGeneral;


        End
        Else   //Tablas propias del sistema
        Begin
          //Usuarios x Grupos
          FDQryUsers.SQL.Text:='';
          FDQryUsers.SQL.Text:='SELECT  UserID, LoginName, FullName, Password, LastAccess, Enabled, CreatedDate, ' +
          ' AccessCount AS GroupID, SysAdmin FROM  ICGDO_TB_acUsers WITH (NOLOCK)' ;
          FDQryUsers.Connection:=FDCnGestion;

          //Qry Usuarios x grupo
          FDQryGroupMembers.SQL.Text:='';
          FDQryGroupMembers.SQL.Text:='SELECT A.GroupID, B.GroupName, A.UserID, C.LoginName ' +
           ' FROM ICGDO_TB_acGroupMembers AS A WITH (NOLOCK) ' +
           ' LEFT JOIN ICGDO_TB_acGroups AS B WITH (NOLOCK) ON A.GroupID=B.GroupID ' +
           ' LEFT JOIN ICGDO_TB_acUsers AS C WITH (NOLOCK) ON A.UserID=C.UserID ' ;
          FDQryGroupMembers.Connection:=FDCnGestion;

          //Qry de Grupos
          FDQryGroups.SQL.Text:='';
          FDQryGroups.SQL.Text:='SELECT   GroupID, GroupName, Description'+
          ' FROM ICGDO_TB_acGroups AS A WITH (NOLOCK) ' ;
          FDQryGroups.Connection:=FDCnGestion;
        End;
      End;

      //Qry de accesos de usuarios
      FDQryAccessUsers.SQL.Text:='';
      FDQryAccessUsers.SQL.Text:='SELECT A.ID, A.UserID, A.IDRol, B.NameRol' +
        ' FROM ICGDO_TB_acUsersAccess AS A WITH (NOLOCK) LEFT OUTER JOIN  ' +
        ' ICGDO_TB_acRoles AS B WITH (NOLOCK) ON A.IDRol = B.IDRol' ;
      FDQryAccessUsers.Connection:=FDCnGestion;
      //Qry Accesso de los grupos
      FDQryGroupAccess.SQL.Text:='';
      FDQryGroupAccess.SQL.Text:='SELECT A.ID, A.GroupID, A.IDRol, B.NameRol ' +
        ' FROM ICGDO_TB_acGroupAccess AS A WITH (NOLOCK)' +
     //   ' LEFT OUTER JOIN ICGDO_TB_acRoles AS B WITH (NOLOCK) ON A.IDRol = B.IDRol';
        ' LEFT JOIN ICGDO_TB_acRoles AS B WITH (NOLOCK) ON A.IDRol = B.IDRol';

      FDQryGroupAccess.Connection:=FDCnGestion;

      //Qry Roles de usuarios
      FDQryRoles.SQL.Text:='';
      FDQryRoles.SQL.Text:='SELECT  IDRol, NameRol FROM  ' +
        ' ICGDO_TB_acRoles AS A WITH (NOLOCK) ';
      FDQryRoles.Connection:=FDCnGestion
    end
    Else
    Begin

    End;


    //--------------------------------------------------------------------------
    //Abrir los data set
    //--------------------------------------------------------------------------
    //usuarios
    with FDQryUsers do
    Begin
      Close;
      //se abre el dataset
      Active := true;
      //si esta abierto
      if Active then
      begin
        //si tiene registros
        if RecordCount>0 then
        begin
          //primer registro
          First;
        end;
      end;
    End;

    //Accesos por usuarios
    with FDQryAccessUsers do
    Begin
      Close;
      //se abre el dataset
      Active := true;
      //si esta abierto
      if Active then
      begin
        //si tiene registros
        if RecordCount>0 then
        begin
          //primer registro
          First;
        end;
      end;
    End;

    //Grupos de usuarios
    with FDQryGroups do
    Begin
      Close;
      //se abre el dataset
      Active := true;
      //si esta abierto
      if Active then
      begin
        //si tiene registros
        if RecordCount>0 then
        begin
          //primer registro
          First;
        end;
      end;
    End;

    //Acceso x Grupos
    with FDQryGroupAccess do
    Begin
      Close;
      //se abre el dataset
      Active := true;
      //si esta abierto
      if Active then
      begin
        //si tiene registros
        if RecordCount>0 then
        begin
          //primer registro
          First;
        end;
      end;
    End;

    //Usuarios x Grupo
    with FDQryGroupMembers do
    Begin
      Close;
      //se abre el dataset
      Active := true;
      //si esta abierto
      if Active then
      begin
        //si tiene registros
        if RecordCount>0 then
        begin
          //primer registro
          First;
        end;
      end;
    End;

    //Roles
    with FDQryRoles do
    Begin
      Close;
      //se abre el dataset
      Active := true;
      //si esta abierto
      if Active then
      begin
        //si tiene registros
        if RecordCount>0 then
        begin
          //primer registro
          First;
        end;
      end;
    End;

    OpenDset:=True;
  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
  Result:=OpenDset;
end;


//------------------------------------------------------------------------------
//Se leen los  usuarios
//------------------------------------------------------------------------------
procedure TFrmUsers.RefreshUsers;
var
   Node,
   SubNode : TTreeNode;
begin
  Try
    tvUsersUsers.Items.Clear;
    tvMembershipUsers.Items.Clear;

    with  dso_AC_Users.DataSet do
    begin
      First;
      while not EOF do
      begin
         Node := tvMembershipUsers.Items.Add(nil, FieldByName((NameFields.UserNameField)).AsString);
         Node.Data := Pointer(FieldByName((NameFields.UserIDField)).AsInteger);
         Node.ImageIndex := 0;
         Node.SelectedIndex := 0;

         Node := tvUsersUsers.Items.Add(nil, FieldByName((NameFields.UserNameField)).AsString);
         Node.Data := Pointer(FieldByName((NameFields.UserIDField)).AsInteger);
         Node.ImageIndex := 0;
         Node.SelectedIndex := 0;
         // Add Permissions...
         with dso_AC_UserAccess.DataSet do
         begin
            First;
            while not EOF do
            begin
              if FieldByName((NameFields.UserIDField)).AsInteger = Integer(Node.Data) then
              begin
                SubNode := tvUsersUsers.Items.AddChild(Node, FieldByName((NameFields.NameRolField)).AsString);
                SubNode.ImageIndex := 7;
                SubNode.SelectedIndex := 7;
                SubNode.Data := Pointer(FieldByName((NameFields.RolIDField)).AsInteger);
              end;
              Next;
            end;
         end;
         Next;
      end;      // while
   end;      // with
   tvUsersUsers.FullExpand;
  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;

//------------------------------------------------------------------------------
//Se leen los  usuarios
//------------------------------------------------------------------------------
procedure TFrmUsers.RefreshRoles;
var
   Node,
   SubNode : TTreeNode;
begin
  Try
    //Se inicializa el contenido de los grids
    trvRoles.Items.Clear;
    trvRolesPermission.Items.Clear;
    tvUsersPermissions.Items.Clear;
    tvGroupsPermissions.Items.Clear;

    // se recorre el data set de roles
    with  dso_AC_Roles.DataSet do
    begin
      First;
      while not EOF do
      begin
        //Se a�ade al treeview de Usuarios y permisos
        Node := tvUsersPermissions.Items.Add(nil, FieldByName((NameFields.NameRolField)).AsString);
        Node.Data := Pointer(FieldByName((NameFields.RolIDField)).AsInteger);
        Node.ImageIndex := 7;
        Node.SelectedIndex := 7;
        //Se a�ade al treeview de Grupos y permisos
        Node := tvGroupsPermissions.Items.Add(nil, FieldByName((NameFields.NameRolField)).AsString);
        Node.Data := Pointer(FieldByName((NameFields.RolIDField)).AsInteger);
        Node.ImageIndex := 7;
        Node.SelectedIndex := 7;

        //Se a�ade al Treview de roles x permisos
        Node := trvRolesPermission.Items.Add(nil, FieldByName((NameFields.NameRolField)).AsString);
        Node.Data := Pointer(FieldByName((NameFields.RolIDField)).AsInteger);
        Node.ImageIndex := 7;
        Node.SelectedIndex := 7;

        //se A�ada el treview de roles
        Node := trvRoles.Items.Add(nil, FieldByName((NameFields.NameRolField)).AsString);
        Node.Data := Pointer(FieldByName((NameFields.RolIDField)).AsInteger);
        Node.ImageIndex := 7;
        Node.SelectedIndex := 7;
        // Add Permissions...
       {  with dso_AC_UserAccess.DataSet do
         begin
            First;
            while not EOF do
            begin
              if FieldByName((NameFields.UserNameField)).AsString = Node.Text then
              begin
                SubNode := tvUsersUsers.Items.AddChild(Node, FieldByName((NameFields.NameRolField)).AsString);
                SubNode.ImageIndex := 2;
                SubNode.SelectedIndex := 2;
                SubNode.Data := Pointer(FieldByName((NameFields.RolIDField)).AsInteger);
              end;
              Next;
            end;
         end;}
         Next;
      end;      // while
   end;      // with
   tvUsersUsers.FullExpand;
  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;

procedure TFrmUsers.Splitter1Moved(Sender: TObject);
begin
   tvUsersPermissions.Refresh;
end;


procedure TFrmUsers.trvRolesDblClick(Sender: TObject);
begin
  btnEditRolesClick(Sender);
end;

procedure TFrmUsers.trvRolesPermissionClick(Sender: TObject);
Var
  NroRegistro,i, tIdRol,tCodigo,tPadre,IdNodo,ImgIndex:Integer;
  StrSQL,tDescripcion:String;
  tActivo:Boolean;
  Node,   SubNode,UserNode : TTreeNode  ;
begin

  Try
    //se recorre el tree para tomar el valor seleccionado, el ID del rol
    for i := 0 to trvRolesPermission.Items.Count - 1 do
    Begin
      if trvRolesPermission.Items[i].Selected then
      Begin
        IdRol:=Integer(trvRolesPermission.Items[i].data);
        Break;
      End;
    End;


    //------------------------------------------------------------------------
    //Abrir Conexi�n a tabla de permisos de usuarios
    //------------------------------------------------------------------------

    StrSQL:='';
    if DatosConexionDB.TConexDBLocal <=1 then  //Sql Server
    begin
      //Qry de usuarios
      FDQryPermisosCab.SQL.Text:='';
      FDQryPermisosCab.SQL.Text:=
        ' SELECT ' + IntTostr(IdRol) + '  AS IdRol, A.IDOBJMAIN AS Codigo, ' +
        ' A.DESCRIPCION AS Descripcion , A.IDPADRE AS Padre,  ' +
        ' A.ACTIVO AS Activo ' +
        ' FROM ICGDO_TB_acOBJMAIN_APPS AS A WITH (NOLOCK) ' +
        ' WHERE A.IDAPPS= ' + IntTostr(IdApps) + ' AND IDPADRE=0 '+
        ' ' +
        ' UNION ALL ' +
        ' SELECT  ' + IntTostr(IdRol) + ' AS IdRol, A.IDOBJMAIN AS Codigo, ' +
        ' A.DESCRIPCION AS Descripcion , A.IDPADRE AS Padre,   ' +
        ' (CASE WHEN (SELECT C.ACTIVO FROM ICGDO_TB_acObjetos_Rol ' +
        ' AS C WITH (NOLOCK)   ' +
        ' WHERE C.IDOBJETO= A.IDOBJMAIN AND C.IDAPPS= ' + IntTostr(IdApps) +
        '  AND C.IDRol= ' + IntTostr(IdRol) + ') IS NULL THEN ''False''  ' +
        ' ELSE (SELECT C.ACTIVO FROM ICGDO_TB_acObjetos_Rol  AS C WITH (NOLOCK) WHERE C.IDOBJETO= A.IDOBJMAIN ' +
        '  AND C.IDAPPS= '+ IntTostr(IdApps) +  ' AND C.IDRol= ' + IntTostr(IdRol) + ' ) END) AS Activo ' +
        ' FROM ICGDO_TB_acOBJMAIN_APPS AS A WITH (NOLOCK) ' +
        ' WHERE A.IDAPPS= ' + IntTostr(IdApps) + '  AND EXISTS (SELECT B.IDPADRE FROM ICGDO_TB_acOBJETOS_APPS AS B WHERE B.IDPADRE=A.IDPADRE)'+
        ' UNION ALL   '+
        ' SELECT  ' + IntTostr(IdRol) + ' AS IdRol,  B.IDOBJETO AS Codigo, B.DESCRIPCION As Descripcion,  B.IDPADRE AS Padre, '+
        ' (CASE WHEN (SELECT C.ACTIVO FROM ICGDO_TB_acObjetos_Rol  AS C WITH (NOLOCK)  '+
        ' WHERE C.IDOBJETO= B.IDOBJETO AND C.IDAPPS= ' + IntTostr(IdApps) + '  AND C.IDRol= ' + IntTostr(IdRol) + '  ) IS NULL THEN ''False''  '+
        '  ELSE (SELECT C.ACTIVO FROM ICGDO_TB_acObjetos_Rol  AS C WITH (NOLOCK)  '+
        ' WHERE C.IDOBJETO= B.IDOBJETO  AND C.IDAPPS= ' + IntTostr(IdApps) + ' AND C.IDRol=  ' + IntTostr(IdRol) + ' ) END) AS Activo  '+
        ' FROM  ICGDO_TB_acOBJETOS_APPS AS B WITH (NOLOCK)   ORDER BY 4 ;';
    end;


    //Open data set de acuerdo al tipo de rol
    With FDQryPermisosCab do
    Begin
      Close;
      Active:=False;
      Connection:=FDCnGestion;
      Active := true;
      //si esta abierto
      if Active then
      begin
        //si tiene registros
        if RecordCount>0 then
        begin
          //primer registro
          First;
          NroRegistro:=RecordCount;
        end;
      end;
    End;


    //se tree con los permisos del usuarop
    tvPermissionsRoles.Items.Clear;

    //--------------------------------------------------------------------------
    //se recorre la tabla
    //--------------------------------------------------------------------------
    while not FDQryPermisosCab.eof do
    Begin
      tActivo:=false;
      tPadre:= FDQryPermisosCab.FieldByName('Padre').Asinteger;
      tCodigo:=FDQryPermisosCab.FieldByName('Codigo').Asinteger;
      tDescripcion:=FDQryPermisosCab.FieldByName('Descripcion').AsString;
      tActivo:=FDQryPermisosCab.FieldByName('Activo').Asboolean;
      //------------------------------------------------------------------------
      //si es el formulario principal
      //------------------------------------------------------------------------
      if tPadre=0 then
      begin
        tActivo:=True;
        Node := tvPermissionsRoles.Items.Add(nil,tDescripcion );
        Node.Data := Pointer(tCodigo);
        ImgIndex:=0;
        Node.ImageIndex := ImgIndex;
        Node.SelectedIndex := ImgIndex;
        Node.StateIndex:=TreeNodeUnChecked ;
        //----------------------------------------------------------------------
        //se coloca como activo
        //----------------------------------------------------------------------
        if tActivo
          Then Node.StateIndex:=TreeNodeChecked ;


      end
      Else //son los nodos hijos del principal
      Begin

        // Recorrer los nodos del �rbol
        for i:=0 to (tvPermissionsRoles.Items.Count - 1) do
        begin
          // se busca por el c�digo del objeto
          if (Integer(tvPermissionsRoles.items[i].data) = tpadre) then
          begin
            node := tvPermissionsRoles.items[i];
            // Seleccionar
            node.Selected := True;
            //se adiciona el hijo al nodo
            SubNode := tvPermissionsRoles.Items.AddChild(Node,tDescripcion );
            ImgIndex:= SubNode.level;
            if Assigned(SubNode) then
            begin
              //----------------------------------------------------------------
              //se guarda el c�digo del objeto
              //----------------------------------------------------------------
              SubNode.Data := Pointer(tCodigo);
              //se coloca la imagen del Nodo
              SubNode.ImageIndex := ImgIndex;
              SubNode.SelectedIndex := ImgIndex;
              //se verifica si debe colocarse como checked o unchecked
              SubNode.StateIndex:=TreeNodeUnChecked ;
              if tActivo Then
                SubNode.StateIndex:=TreeNodeChecked ;
            end;


          end;
        end;

      End;
      //siguiente registro de los permisos
      FDQryPermisosCab.next;

    End;

    //se expande el tree
   // tvPermissionsRoles.Expand(True);
  except
    On E:Exception do
  begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      exit;
    end;
  end;

end;

procedure TFrmUsers.trvRolesPermissionKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  trvRolesPermissionClick(Sender);
end;

procedure TFrmUsers.trvRolesPermissionKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   trvRolesPermissionClick(Sender);
end;

//DragDrop
procedure TFrmUsers.tvGroupsGroupsDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
//
end;





procedure TFrmUsers.tvUsersUsersDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
   SenderTree, SourceTree : TTreeView;
   Node, SourceNode: TTreeNode;
   ImgIndex : Integer;
begin
   ImgIndex := -1;
   SourceTree := (Source as TTreeView);
   SenderTree := (Sender as TTreeView);

   if SenderTree = tvUsersUsers then
      ImgIndex := 7

   else if SenderTree = tvGroupsGroups then
      ImgIndex := 7

   else if SenderTree = tvMembershipGroups then
      ImgIndex := 3;


   SourceNode := SourceTree.Selected;
   Node := SenderTree.GetNodeAt(X,Y);
   if not Assigned(Node) then
     Exit;


   if Node.Level = 1 then
     Node := Node.Parent;

   if Node = nil then
     Exit;

   if SourceNode = nil then
     Exit;

   //Permisos por usuarios
   if Sender = tvUsersUsers then
   begin
     if LocateValue(Node.Text, NameFields.UserNameField, dso_AC_Users,DatosConexionDB,
     MsgError,  VerMsgError ) then
     begin
      Conectado:= AddUserAccess(IntToStr(Integer(Node.Data)), IntToStr(Integer(tvUsersPermissions.Selected.Data)),dso_AC_UserAccess
      ,DatosConexionDB,  MsgError,  VerMsgError,TypeSecurity,NameFields)  ;
      if Conectado
        then
         AddChild(tvUsersUsers, Node, tvUsersPermissions.Selected.Text, 7);

      if  Conectado
        then
          AddChild(SenderTree, Node, SourceNode.Text, ImgIndex);
     end;
   end;

   //Permisos por grupos
   if Sender = tvGroupsGroups then
   begin
    if LocateValue(Node.Text,NameFields.GroupNameField,dso_AC_Groups,
        DatosConexionDB,  MsgError,  VerMsgError) then
      Begin
        Conectado:=AddGroupAccess(IntToStr(Integer(Node.Data)),IntToStr(Integer(tvGroupsPermissions.Selected.Data)),
        dso_AC_GroupAccess,DatosConexionDB,  MsgError,  VerMsgError,TypeSecurity,NameFields)  ;

        if  Conectado then
          AddChild(SenderTree, Node, SourceNode.Text, ImgIndex);
     end;
   end;


   //Agregar usuarios al grupo
   if Sender = tvMembershipGroups then
   begin
    {if LocateValue(Node.Text,NameFields.GroupNameField,dso_AC_Groups,
        DatosConexionDB,  MsgError,  VerMsgError) then
      Begin
        Conectado:=AddGroupAccess(IntToStr(Integer(Node.Data)),IntToStr(Integer(tvGroupsPermissions.Selected.Data)),
        dso_AC_GroupAccess,DatosConexionDB,  MsgError,  VerMsgError,TypeSecurity,NameFields)  ;

        if  Conectado then
          AddChild(SenderTree, Node, SourceNode.Text, ImgIndex);
     end;  }

     Conectado:=AddUserToGroup( IntToStr(Integer(tvMembershipUsers.Selected.Data)),IntToStr(Integer(tvMembershipUsers.Selected.Data)),
        dso_AC_GroupMembers,DatosConexionDB,  MsgError,  VerMsgError);


     if  Conectado then
     Begin
      AddChild(tvMembershipUsers, Node, tvMembershipUsers.Selected.Text, 0);
      Node.Data := Pointer(Integer(tvMembershipUsers.Selected.Data))   ;

     End;
    //--------------------------------------------------------------------------
    //Se refresca los usuarios mienbros de cada grupo
    //--------------------------------------------------------------------------
    Conectado:=opendataset(MsgError, VerMsgError);
    RefreshMembership;

   end;



   SenderTree.FullExpand;

end;

procedure TFrmUsers.tvUsersUsersDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept := Source = tvUsersPermissions;
end;

//------------------------------------------------------------------------------
//Se leen grupos de usuarios
//------------------------------------------------------------------------------
procedure TFrmUsers.RefreshGroups;
var
   SubNode,Node,Node02: TTreeNode;
begin
  Try
    tvGroupsGroups.Items.Clear;
    tvMembershipGroups.Items.Clear;
    with dso_AC_Groups.DataSet do
    begin
      First;
      while not EOF do
      begin
       { Node := tvMembershipGroups.Items.Add(nil, FieldByName((NameFields.GroupNameField)).AsString);
         Node.Data := Pointer(FieldByName((NameFields.GroupIDField)).AsInteger);
         Node.ImageIndex := 1;
         Node.SelectedIndex := 1;
        }
         Node := tvGroupsGroups.Items.Add(nil, FieldByName((NameFields.GroupNameField)).AsString);
         Node.Data := Pointer(FieldByName((NameFields.GroupIDField)).AsInteger);
         Node.ImageIndex := 1;
         Node.SelectedIndex := 1;

         // Add Permissions...
         with dso_AC_GroupAccess.DataSet do
         begin
            First;
            while not EOF do
            begin
              if FieldByName((NameFields.GroupIDField)).AsInteger = Integer(Node.Data) then
              begin
                SubNode := tvGroupsGroups.Items.AddChild(Node, FieldByName((NameFields.NameRolField)).AsString);
                SubNode.Data := Pointer(FieldByName((NameFields.GroupAccessIDField)).AsInteger);
                SubNode.ImageIndex := 7;
                SubNode.SelectedIndex := 7;
              end;
              Next;
            end;
         end;
         Next;
      end;      // while
   end;      // with
   tvGroupsGroups.FullExpand;
  except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;

//------------------------------------------------------------------------------
//Se leen los miembros de los grupos de usuarios
//------------------------------------------------------------------------------
procedure TFrmUsers.RefreshMembership;
var
   Node,
   SubNode: TTreeNode;

begin
  Try

   // tvGroupsGroups.Items.Clear;
    tvMembershipGroups.Items.Clear;

    //--------------------------------------------------------------------------
    //Se recorren los grupos de usuarios para agregarle los usuarios
    //--------------------------------------------------------------------------
    with dso_AC_Groups.DataSet do
    begin
      First;
      while not EOF do
      begin
        Node := tvMembershipGroups.Items.Add(nil, FieldByName((NameFields.GroupNameField)).AsString);
        Node.Data := Pointer(FieldByName((NameFields.GroupIDField)).AsInteger);
        Node.ImageIndex := 1;
        Node.SelectedIndex := 1;

        //Se recorren los miembros del grupo
        with dso_AC_GroupMembers.DataSet do
        begin
          First;
          while not EOF do
          begin
            if FieldByName((NameFields.GroupIDField)).AsInteger = Integer(Node.Data) then
            begin
              SubNode := tvMembershipGroups.Items.AddChild(Node, FieldByName((NameFields.UserNameField)).AsString);
              SubNode.Data := Pointer(FieldByName((NameFields.UserIDField)).AsInteger);
              SubNode.ImageIndex := 0;
              SubNode.SelectedIndex := 0;
            End;
            //Siguiente registro de la tabla de Usuarios x grupos
            next;
          end;
        end;
        //Siguiente registro del grupo
        Next
      end;

    end;
    //Se expande los grupos de usuarios
    tvMembershipGroups.FullExpand;

   except
    On E:Exception do
    begin
      //Log De error
      MessageDlg(E.Message, mterror, [mbOK], 0);
      Exit;
    end;
  end;
end;

procedure TFrmUsers.AddChild(SenderTree : TTreeView; var Node : TTreeNode; AText : string; AImageNum : integer);
var
   CheckNode : TTreeNode;
begin
   while (Node.Parent <> nil) do
      Node := Node.Parent;

   // Check to see if the item is already in the list
   CheckNode := Node.GetFirstChild;
   while Assigned(CheckNode) do
   begin
      if (CheckNode.Text = AText) then Exit;
      CheckNode := CheckNode.GetNextSibling;
   end;      // while

   Node := SenderTree.Items.AddChild(Node, AText);
   if Assigned(Node) then
   begin
      Node.ImageIndex := AImageNum;
      Node.SelectedIndex := AImageNum;
   end;
end;


//------------------------------------------------------------------------------
//Gesti�n con el formulario de roles
//------------------------------------------------------------------------------
procedure TFrmUsers.pcPermissionsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := Source = tvGroupsPermissions;
end;

//------------------------------------------------------------------------------
//Mantenimiento de Roles
//------------------------------------------------------------------------------
procedure TFrmUsers.pFrmRoles (IdRol:Integer;DeleteReg:Boolean);
Var
   varfrmRol:TFrmRolesUsers;
begin
  try

    //--------------------------------------------------------------------------
    //se verifica si la aplicaci�n esta creada.
    //--------------------------------------------------------------------------
    varfrmRol:=TFrmRolesUsers.Create(self);
    try
      //se pasan los valores de Conexi�n a la base de datos
      varfrmRol.DatosConexionDB.ServerDB:= DatosConexionDB.ServerDB;
      varfrmRol.DatosConexionDB.NameDB:=DatosConexionDB.NameDBGestion;
      varfrmRol.DatosConexionDB.NameDBGestion:=DatosConexionDB.NameDBGestion;
      varfrmRol.DatosConexionDB.NameDBGeneral:=DatosConexionDB.NameDBGeneral;
      varfrmRol.DatosConexionDB.UserDB := DatosConexionDB.UserDB;
      varfrmRol.DatosConexionDB.PasswordDB := DatosConexionDB.PasswordDB;
      varfrmRol.DatosConexionDB.GeneraLogErrores :=DatosConexionDB.GeneraLogErrores;
      varfrmRol.DatosConexionDB.GeneraLogData :=DatosConexionDB.GeneraLogData;
      varfrmRol.DatosConexionDB.TConexDBLocal:=DatosConexionDB.TConexDBLocal;
      varfrmRol.DatosConexionDB.TConexDBRemota:=DatosConexionDB.TConexDBRemota;
      varfrmRol.DatosConexionDB.PathDB:=DatosConexionDB.PathDB;
      varfrmRol.TypeSecurity:=DatosConexionDB.TypeSecurity;//Tipo ICG
      varfrmRol.IdRol:=idRol;
      varfrmRol.DeleteReg:=DeleteReg;
      varfrmRol.Showmodal;
    finally
      varfrmRol.Free;
    end;
    //cerramos el data set de los roles
    with dso_AC_Roles.DataSet do
    Begin
      Close;
      Open;
    End;
    //refrescamos el Treevire
    RefreshRoles ;
    RefreshUsers;
    RefreshGroups;
  // Si ocurre un error
  Except On E:Exception do
    Begin
     MessageDlg('No se puede crear el formulario el error es : ' + #13#10 + E.Message,
     mterror, [mbok],0);
    End;
  End;


End;


//------------------------------------------------------------------------------
//Mantenimiento de Roles
//------------------------------------------------------------------------------
procedure TFrmUsers.pFrmUsers (IdUser:Integer;DeleteReg:Boolean);
Var
   varfrm:TFrmMttoUsers;
begin
  try

    //--------------------------------------------------------------------------
    //se verifica si la aplicaci�n esta creada.
    //--------------------------------------------------------------------------
    varfrm:=TFrmMttoUsers.Create(self);
    try
      //se pasan los valores de Conexi�n a la base de datos
      varfrm.DatosConexionDB.ServerDB:= DatosConexionDB.ServerDB;
      varfrm.DatosConexionDB.NameDB:=DatosConexionDB.NameDBGestion;
      varfrm.DatosConexionDB.NameDBGestion:=DatosConexionDB.NameDBGestion;
      varfrm.DatosConexionDB.NameDBGeneral:=DatosConexionDB.NameDBGeneral;
      varfrm.DatosConexionDB.UserDB := DatosConexionDB.UserDB;
      varfrm.DatosConexionDB.PasswordDB := DatosConexionDB.PasswordDB;
      varfrm.DatosConexionDB.GeneraLogErrores :=DatosConexionDB.GeneraLogErrores;
      varfrm.DatosConexionDB.GeneraLogData :=DatosConexionDB.GeneraLogData;
      varfrm.DatosConexionDB.TConexDBLocal:=DatosConexionDB.TConexDBLocal;
      varfrm.DatosConexionDB.TConexDBRemota:=DatosConexionDB.TConexDBRemota;
      varfrm.DatosConexionDB.PathDB:=DatosConexionDB.PathDB;
      varfrm.TypeSecurity:=DatosConexionDB.TypeSecurity;//Tipo ICG
      varfrm.IdUser:=idUser;
      varfrm.DeleteReg:=DeleteReg;

      varfrm.Showmodal;
    finally
      varfrm.Free;
    end;
    //cerramos el data set de los roles
    with dso_AC_Users.DataSet do
    Begin
      Close;
      Open;
    End;
    //refrescamos el Treevire
    RefreshUsers;
  // Si ocurre un error
  Except On E:Exception do
    Begin
     MessageDlg('No se puede crear el formulario el error es : ' + #13#10 + E.Message,
     mterror, [mbok],0);
    End;
  End;


End;


//------------------------------------------------------------------------------
//Gesti�n con el formulario de Grupos de usuarios
//------------------------------------------------------------------------------
procedure TFrmUsers.pFrmGrupos (IdGrupo:Integer;DeleteReg:Boolean);
Var
   varfrm:TFrmGroups;
begin
  try

    //--------------------------------------------------------------------------
    //se verifica si la aplicaci�n esta creada.
    //--------------------------------------------------------------------------
    varfrm:=TFrmGroups.Create(self);
    try
      //se pasan los valores de Conexi�n a la base de datos
      varfrm.DatosConexionDB.ServerDB:= DatosConexionDB.ServerDB;
      varfrm.DatosConexionDB.NameDB:=DatosConexionDB.NameDBGestion;
      varfrm.DatosConexionDB.NameDBGestion:=DatosConexionDB.NameDBGestion;
      varfrm.DatosConexionDB.NameDBGeneral:=DatosConexionDB.NameDBGeneral;
      varfrm.DatosConexionDB.UserDB := DatosConexionDB.UserDB;
      varfrm.DatosConexionDB.PasswordDB := DatosConexionDB.PasswordDB;
      varfrm.DatosConexionDB.GeneraLogErrores :=DatosConexionDB.GeneraLogErrores;
      varfrm.DatosConexionDB.GeneraLogData :=DatosConexionDB.GeneraLogData;
      varfrm.DatosConexionDB.TConexDBLocal:=DatosConexionDB.TConexDBLocal;
      varfrm.DatosConexionDB.TConexDBRemota:=DatosConexionDB.TConexDBRemota;
      varfrm.DatosConexionDB.PathDB:=DatosConexionDB.PathDB;
      varfrm.TypeSecurity:=DatosConexionDB.TypeSecurity;//Tipo ICG
      varfrm.IdGrupo:=idGrupo;
      varfrm.DeleteReg:=DeleteReg;
      varfrm.Showmodal;
    finally
      varfrm.Free;
    end;
    //cerramos el data set de los roles
    with dso_AC_Groups.DataSet do
    Begin
      Close;
      Open;
    End;


    //refrescamos el Treevire
    RefreshRoles ;
    RefreshUsers;
    RefreshGroups;
  // Si ocurre un error
  Except On E:Exception do
    Begin
     MessageDlg('No se puede crear el formulario el error es : ' + #13#10 + E.Message,
     mterror, [mbok],0);
    End;
  End;
End;



//------------------------------------------------------------------------------
//Se leen los Objetos del Tipo Formulario
//------------------------------------------------------------------------------
function TFrmUsers.LeerObjetosFrm(IdApps:integer;InfoConexion:TConexionDBInfo;
var MsgError :String ; VerMsgError:Boolean;TypeSecurity:Integer) :Boolean;
Var

  FDCn:TFDConnection;   //Conexi�n a base de datos
  FDLMSSQL : TFDPhysMSSQLDriverLink;
  FDLSQLite : TFDPhysSQLiteDriverLink;
  FDQry01: TFDQuery;
  i,
  ID,
  IDPadre:Integer;
  Descripcion,
  Objeto_Tipo,
  Objeto,tStrSQL:String;

  ObjetoValido,
  Activo:Boolean;

  Form: TForm;
  Comp: TComponent;

  El_ToolButton:TToolButton    ;
  El_TTabSheet:TTabSheet;
  El_TCategoryPanel:TCategoryPanel;
  El_TCategoryPanelGroup:TCategoryPanelGroup;
  El_TdxBarLargeButton:TdxBarLargeButton;
Begin
  Try

    //--------------------------------------------------------------------------
    //Se crea la conexi�n a la base de datosabre la conexi�n a la base de datos
    //--------------------------------------------------------------------------
    FDCn:=TFDConnection.Create(nil);
    FDQry01:= TFDQuery.Create(nil);
    FDQry01.Connection:=FDCn;
    with  FDCn do
    Begin
      if InfoConexion.TConexDBLocal <=1 then  //Sql Server
      begin
          //asignamos el driver de conexi�n
          FDLMSSQL := TFDPhysMSSQLDriverLink.Create(nil);
          FDCn.DriverName :=  FDLMSSQL.BaseDriverId;
          //se abre la base de datos de Gesti�n
          Conectado:=Open_DB_FD (FDCn,12,InfoConexion.ServerDB, InfoConexion.NameDBGestion
          , InfoConexion.UserDB, InfoConexion.PasswordDB,InfoConexion.PathDB, MsgError,VerMsgError);
      end
      else //Sql Lite
      begin
          FDLSQLite := TFDPhysSqliteDriverLink.Create(nil);
          FDCn.DriverName := FDLSQLite.BaseDriverId;

          {   if FileExists(usrRutaDBLocal+usrNameDBLocal)then
            begin
              try
                //------------------------------------------------------------------------
                // Parametros de Conexion a la base de datos
                //------------------------------------------------------------------------
                DriverName:='SQLite';
                Params.Add('Database=' +usrRutaDBLocal+usrNameDBLocal );
                Open();
                Connected:=True;
                Conectado:=Connected;
              except
              on E: EDatabaseError do
                Begin
                  // Se guarda el error en el log
                  Mensaje:=E.Message;


                End;
              end;
            end
            else
            begin
              MessageDlg('No se pudo conectar a la base de datos', mterror, [mbOK], 0);
              Exit;
            end;
          end;}
      End;
    end;
    //------------------------------------------------------------------------
    //Si no hay conexi�n se interrumpe la ejecuci�n de la aplicaci�n
    //------------------------------------------------------------------------
    if Conectado=False then
    Begin
      MsgError:='No se pudo conectar a la base de datos ,' + MsgError;
      // si ocurrio un error y se debe mostrar
      if (VerMsgError)  And (MsgError <> '') then
      Begin
        MessageDlg(MsgError,mterror, [mbok],0);
      End;
      FDQry01.Close;
      FDQry01.Free;
      FDCn.Close;
      FDCn.Free;
      Exit;
      Result := false;
    End;


    //----------------------------------------------------------------------------
    //Se Cierra los Objetos ADO de cada Formulario
    //----------------------------------------------------------------------------
    FDQry01.Close;
    //----------------------------------------------------------------------------
    // Se llenan los Objetos del formulario
    //----------------------------------------------------------------------------
    for  i:=0  to Application.ComponentCount-1 do
    begin
      tStrSQL:='';
      DESCRIPCION:='';
      //--------------------------------------------------------------------------
      //Si el Objeto tiene Nombre
      //--------------------------------------------------------------------------
      //if  (Application.Components[i].Name=Application.MainForm.Name) Then
      if Application.Components[i] is TForm then
      Begin
        //----------------------------------------------------------------------
        //Se mueve a Variables los valores de la tabla
        //----------------------------------------------------------------------
        DESCRIPCION:=Application.Components[i].Name;
        Form := Application.FindComponent(DESCRIPCION) as TForm;
        //Si el formulario se cargo
        if Assigned(Form)  then
        Begin
          DESCRIPCION:=Form.Hint;
          //Se prepara el SQL para leer los Formularios de la aplicaci�n
          tStrSQL:=tStrSQL + 'SELECT IDOBJMAIN  FROM ICGDO_TB_acObjMain_Apps WITH (NOLOCK)  WHERE ACTIVO=1 AND OBJETO=' +
          QuotedStr((Application.MainForm.Name))+
          ' AND IDAPPS=' +intTostr(idApps) ;
          //Se Abre el DataSet
          with FDQry01 do
          Begin
            Close;
            SQL.Text:=tStrSQL;
            //se abre el dataset
            Active := true;
            //si esta abierto
            if Active then
            begin
              //si tiene registros
              if RecordCount>0 then
              begin
                //primer registro
                First;
              end;
            end;
          End;

          //Activamos los Objetos de la base de datos al Grid
          IDPADRE:= FDQry01.Fields[0].AsInteger;
          if Application.Components[i].Name=Application.MainForm.Name then   IDPADRE:=0;     //   Application.Create(self);
          //--------------------------------------------------------------------
          //Si el Formulario no Tiene Descripci�n no se muestra en las opciones
          //----------------------------------------------------------------------
          if DESCRIPCION <> '' then
          Begin
            tStrSQL:= 'IF NOT EXISTS(SELECT IDAPPS FROM ICGDO_TB_acObjMain_Apps WHERE OBJETO= ' + QuotedStr((Application.Components[i].Name))  + ' AND IDAPPS=' + IntTostr(IdApps) +')' +  sLineBreak +
            ' BEGIN '  + sLineBreak +
            ' INSERT INTO ICGDO_TB_acObjMain_Apps (IDPADRE,IDAPPS,DESCRIPCION,OBJETO,ACTIVO)'  + sLineBreak +
            ' VALUES ( ' + InTToStr(IDPADRE) + ','  +  Inttostr(IdApps) + ',' +  QuotedStr((DESCRIPCION))   + ',' +  QuotedStr((Application.Components[i].Name))  + ',1)' + sLineBreak +
            ' END' ;

            //------------------------------------------------------------------------
            //Se ejecuta el SQL para Verificar o crear la Tabla
            //------------------------------------------------------------------------
            FDCn.ExecSQL(tStrSQL);
          End;
        End;
      End;
    End;

    //----------------------------------------------------------------------------
    // Cierra el Recordset
    //----------------------------------------------------------------------------
    FDQry01.Close;
    //-------------------------------------------------------------------------
    // Se recorren los objetos de cada formulario
    //-------------------------------------------------------------------------
    tStrSQL:='';
    tStrSQL:=' SELECT IDOBJMAIN, DESCRIPCION, OBJETO, ACTIVO FROM  ICGDO_TB_acObjMain_Apps WHERE ACTIVO=1 ' +
    ' AND IDAPPS=' + intToStr(idApps) ;
    //Se Abre el DataSet
    FDQry01.Close;
    FDQry01.SQL.Text:=tStrSQL;
    //se abre el dataset
    FDQry01.Active := true;
    //si esta abierto
    if FDQry01.Active then
    begin
      //si tiene registros
      if FDQry01.RecordCount>0 then
      begin
        //primer registro
        FDQry01.First;
      end;
    end;



     with FDQry01 do
     Begin
      if not eof then
      Begin
        //Se recorre la tabla de Formularios
        While not eof do
        begin
          IdPadre:=Fields[0].AsInteger;
          DESCRIPCION:=(Fields[1].AsString); //Decrypt(Fields[1].AsString);
          OBJETO:=(Fields[2].AsString);//Decrypt(Fields[2].AsString);
          ACTIVO:=Fields[3].AsBoolean;
          oFrmObjetos:=TForm(Application.FindComponent(OBJETO));
          Conectado:=(oFrmObjetos<>nil);

          if Conectado Then
          Begin
            //se Recorre todos los Objetos del Formulario
            for  i:=0  to oFrmObjetos.ComponentCount-1 do
            begin
              if oFrmObjetos.Components[i].Name <> ''   then
              Begin
                ObjetoValido:=False;
                DESCRIPCION:=oFrmObjetos.Components[i].Name;
                Objeto:=oFrmObjetos.Components[i].Name;

                //Asignamos al Objeto de Componentes el tipo de Componentes
                Comp :=oFrmObjetos.FindComponent(oFrmObjetos.Components[i].Name);
                if Assigned (Comp) then
                Begin
                  //Si es un Boton de una barra de Botones
                  If (Comp Is TToolButton)    Then
                  Begin
                    El_ToolButton:=oFrmObjetos.FindComponent(oFrmObjetos.Components[i].Name)  as TToolButton;
                    Conectado:=(TToolButton <>nil);
                    Objeto_tipo:='TToolButton';
                    if Conectado Then
                    Begin
                    ObjetoValido:=True;
                      DESCRIPCION:=El_ToolButton.Hint;
                    End;
                  End;
                  //Si es un TabSheet asociado a una pagina de control
                  If (Comp Is TTabSheet)    Then
                  Begin
                    El_TTabSheet:=oFrmObjetos.FindComponent(oFrmObjetos.Components[i].Name)  as TTabSheet;
                    Conectado:=(TTabSheet <>nil);
                    if Conectado Then
                    Begin
                      Objeto_tipo:='TTabSheet';
                      ObjetoValido:=True;
                      DESCRIPCION:=El_TTabSheet.Hint;
                    End;
                  End;
                  //Si es un Large Button
                  If (Comp Is TdxBarLargeButton)    Then
                  Begin
                    El_TdxBarLargeButton:=oFrmObjetos.FindComponent(oFrmObjetos.Components[i].Name)  as TdxBarLargeButton;
                    Conectado:=(TdxBarLargeButton <>nil);
                    if Conectado Then
                    Begin
                      Objeto_tipo:='TdxBarLargeButton';
                      ObjetoValido:=True;
                      DESCRIPCION:=El_TdxBarLargeButton.Hint;
                    End;
                  End;





                  if DESCRIPCION='' then  ObjetoValido:=False;
                  if ObjetoValido then
                  Begin
                  //Se inserta los objetos
                    InsertObjetos (FDCn,Descripcion,Objeto,Objeto_Tipo,IdPadre,ObjetoValido,False);
                  End;

                  //Si es un TCategoryPanel asociado a una pagina de control
                  //TCategoryPanelGroup
                  If (Comp Is  TCategoryPanelGroup) Then
                  Begin
                    El_TCategoryPanelGroup:=oFrmObjetos.FindComponent(oFrmObjetos.Components[i].Name)  as TCategoryPanelGroup;
                    Conectado:=El_TCategoryPanelGroup <>nil;
                    if Conectado Then
                    Begin
                      ObjetoValido:=True;
                      Descripcion:='Grupo de Paneles' ;//El_TCategoryPanel.Hint;
                      if Descripcion='' then ObjetoValido:=False;
                      LeerComponentesRecursivo(FDCn,oFrmObjetos,IdPadre);
                    End;
                  End;
                  //--------------------------------------------------------------
                  //
                  //--------------------------------------------------------------
                End;
              end;
            End;
          End;
          //Siguiente Registro
          Next;
          //Se Libera el Objeto
          El_ToolButton:=Nil;
          Comp:=Nil;
        End;
      end;
    End;


  except
    On E:Exception do
    begin
      MsgError:=E.Message;
      // si ocurrio un error y se debe mostrar
      if (VerMsgError)  And (MsgError <> '') then
      Begin
        MessageDlg(MsgError,mterror, [mbok],0);
      End;
      Result := false;
      Exit;
    end;
  end;

  //Se cierran los objetos de base de datos
  FDQry01.Close;
  FDQry01.Free;
  FDCn.Close;
  FDCn.Free;
  //----------------------------------------------------------------------------
  //Se libera el Objeto
  //----------------------------------------------------------------------------
  Form:=Nil;
  Result := True;
End;

//------------------------------------------------------------------------------
//Se Verifica si el objeto existe para insertarlo
//------------------------------------------------------------------------------
procedure TFrmUsers.InsertObjetos (FDCn:TFDConnection;Descripcion,Objeto,TipoObjeto:String;IdPadre:Integer; ObjetoValido,Encriptado:Boolean);
Var
  vStrSQL:String;
Begin

  try

    //--------------------------------------------------------------
    // Se veirifica si el objeto tiene valor
    //--------------------------------------------------------------
    if Descripcion='' then  ObjetoValido:=False;
    if ObjetoValido then
    Begin
      vStrSQL:= 'IF NOT EXISTS(SELECT IDOBJETO FROM ICGDO_TB_acObjetos_Apps WHERE OBJETO= ' + QuotedStr((Objeto))  + ' AND IDPADRE=' + IntTostr(IdPadre) +')' +  sLineBreak +
      ' BEGIN '  + sLineBreak +
      ' INSERT INTO   ICGDO_TB_acObjetos_Apps (IDPADRE,DESCRIPCION,OBJETO,ACTIVO,TIPOOBJETO)'  + sLineBreak +
      ' VALUES (' +  (IntTostr(IdPadre)) + ',' +  QuotedStr((Descripcion)) + ',' +  QuotedStr((Objeto))  + ',1,' + QuotedStr((TipoObjeto)) + ')' + sLineBreak +
      ' END' ;

      if Encriptado=False then
      Begin
        vStrSQL:= 'IF NOT EXISTS(SELECT IDOBJETO FROM ICGDO_TB_acObjetos_Apps WHERE OBJETO= ' + QuotedStr((Objeto))  + ' AND IDPADRE=' + IntTostr(IdPadre) +')' +  sLineBreak +
      ' BEGIN '  + sLineBreak +
      ' INSERT INTO ICGDO_TB_acObjetos_Apps (IDPADRE,DESCRIPCION,OBJETO,ACTIVO,TIPOOBJETO)'  + sLineBreak +
      ' VALUES (' +  (IntTostr(IdPadre)) + ',' +  QuotedStr((Descripcion)) + ',' +  QuotedStr((Objeto))  + ',1,' + QuotedStr(TipoObjeto)  +')' + sLineBreak +
      ' END' ;

      End;

      //------------------------------------------------------------------------
      //Se ejecuta el SQL
      //------------------------------------------------------------------------
      FDCn.ExecSQL(vStrSQL);
      //------------------------------------------------------------------------
    End;
  except
    On E:Exception do
    begin

      MsgError:=  E.Message;

      // si ocurrio un error y se debe mostrar
      if (VerMsgError)  And (MsgError <> '') then
      Begin
        MessageDlg(MsgError,mterror, [mbok],0);
      End;
      Exit;
    end;
  end;
End;

//------------------------------------------------------------------------------
//Se ubica el ID del Objeto para asignarlo a los Objetos Hijos
//------------------------------------------------------------------------------
procedure TFrmUsers.LeerComponentesRecursivo(FDCn:TFDConnection;const AControl: TControl; IdPadre:Integer);
var
  i,
  i1,
  j,
  j1,
  j2,
  k,
  l,
  NivelNodo,
  IdPadreMain:Integer;
  s,
  Objeto_Tipo,
  Objeto_Name,
  Objeto_Name_Padre,
  Objeto_Descripcion,
  Name_Treeview:String;
  panel: TCategoryPanel;
  surface: TCategoryPanelSurface;
  Objeto_Valido:boolean;
  IdPadreArray: array of Integer;

begin
  try
    //Si el objeto es nulo se cancela el proceso de lectura
    if AControl=nil then Exit;
    IdPadreMain:=IdPadre;
    //Se lee los objetos del Formulario
    for i := 0 to (AControl.ComponentCount-1)  do
    begin
      //Verificamos si el tipo de Objeto. Solo nos intereza los TCPanel
      if  (AControl.Components[i] is  TCategoryPanelGroup) then
      Begin
        //Recorremos el Panel Group
        for i1 := 0 to (TCategoryPanelGroup(AControl.Components[i])).Panels.Count-1 do
        begin

          //Se toman las propiedades del Objeto
          SetLength(IdPadreArray, 30 );
          NivelNodo:=0;
          Objeto_Valido:=False;
          panel := (TCategoryPanelGroup(AControl.Components[i])).Panels[i1];
          Objeto_Tipo:='TCategoryPanel';
          Objeto_Name:=panel.Name;
          Objeto_Descripcion :=panel.Caption;
          Objeto_Name_Padre:=Objeto_Name;
          IdPadre:=IdPadreMain;



          if Objeto_Descripcion <> '' then
          Begin
            Objeto_Valido:=True;
            //Se inserta el objeto
            InsertObjetos (FDCn,Objeto_Descripcion,Objeto_Name,Objeto_Tipo,IdPadre, Objeto_Valido,False);
            //Se Ubica el valor del objeto
            IdPadre:=BuscarIdPadre_Objeto (FDCn,Objeto_Name,False);
          End;

          //Ubicamos los objetos del category panel
          surface := panel.Controls[0] as TCategoryPanelSurface;
          //Se recorre los Objetos del CTPanel
          for j1 := 0 to surface.ControlCount-1 do
          begin
            //Verificamos si es un Tree view
            if (surface.Controls[j1] is  TTreeView) then
            Begin
              Objeto_Name:=TTreeView(surface.Controls[j1]).Name;
              Objeto_Tipo:='TTreeView';
              Name_Treeview:=Objeto_Name;
              Objeto_Descripcion :=TTreeView(surface.Controls[j1]).Hint;
              Objeto_Valido:=False;
              if Objeto_Descripcion<>'' then
              Begin
                Objeto_Valido:=True;
                //Se inserta el objeto
                InsertObjetos (FDCn,Objeto_Descripcion,Objeto_Name,Objeto_Tipo,IdPadre, Objeto_Valido,False);
                //Se Ubica el valor del objeto
                Objeto_Name_Padre:=Objeto_Name;
              End;
              IdPadre:=BuscarIdPadre_Objeto (FDCn,Objeto_Name_Padre,False);

              //Se recorreo el Treeview para tomar cada Nodo

              //Se asigna el valor al arreglo de 31 niveles
              SetLength(IdPadreArray, 30 );
              NivelNodo:=0;
              for j2:=0 to (TTreeView(surface.Controls[j1]).Items.Count - 1) do
              begin
                NivelNodo:= ((TTreeView(surface.Controls[j1]).Items[j2] as TTreeNode ).Level );
                //IdPadreArray[NivelNodo]:=IdPadre;
                if NivelNodo=0 then  IdPadre:=BuscarIdPadre_Objeto (FDCn,Objeto_Name_Padre,False);
                Objeto_Name:=Name_Treeview + '_' + TTreeView(surface.Controls[j1]).items[j2].Text + '_' +
                IntToStr( ( TTreeView(surface.Controls[j1]).Items[j2] as TTreeNode ).Level );
                Objeto_Descripcion :=TTreeView(surface.Controls[j1]).items[j2].Text;
                Objeto_Valido:=False;
                Objeto_Tipo:='TTreeNode';
                IdPadreArray[NivelNodo]:=IdPadre;
                if (Objeto_Descripcion<>'') Then //And (NivelNodo <10)then
                Begin
                  Objeto_Valido:=True;
                  //Se inserta el objeto
                  InsertObjetos (FDCn,Objeto_Descripcion,Objeto_Name,Objeto_Tipo, IdPadre, Objeto_Valido,false);
                  if NivelNodo <1 then
                    IdPadre:=BuscarIdPadre_Objeto (FDcn,Objeto_Name,false)
                  Else
                    IdPadre:= IdPadreArray[NivelNodo];
                End;

              end;

            end;
          end;
        end;
      End;
    end;

   except
    On E:Exception do
    begin
      MsgError:=E.Message;
      // si ocurrio un error y se debe mostrar
      if (VerMsgError)  And (MsgError <> '') then
      Begin
        MessageDlg(MsgError,mterror, [mbok],0);
      End;
      Exit;
    end;
  end;
end;


//------------------------------------------------------------------------------
//Se ubica el ID del Objeto para asignarlo a los Objetos Hijos
//------------------------------------------------------------------------------
function TFrmUsers.BuscarIdPadre_Objeto (FDCn:TFDConnection;Objeto:String;Encriptado:Boolean): Integer;
Var
IdObjeto:Integer;
vStrSQL:String;
FDQry01: TFDQuery;
Begin
  try
    IdObjeto:=0;
    //--------------------------------------------------------------
    //Se busca el Id del Objeto Padre
    //--------------------------------------------------------------
    FDQry01:=TFDQuery.Create(self);

    //Se prepara el SQL para leer los Formularios de la aplicaci�n
    vStrSQL:='';
    vStrSQL:= 'SELECT IDOBJETO FROM ICGDO_TB_acObjetos_Apps WHERE OBJETO= ' +
    QuotedStr((Objeto))+
    ' AND IDAPPS=' + intToStr(idApps) ;

    //Se Abre el DataSet

    //Activamos los Objetos de la base de datos al Grid
    IdObjeto:= FDQry01.Fields[0].AsInteger;
    if Objeto='' Then   IdObjeto:=0;
  except
    On E:Exception do
    begin
     MsgError:=E.Message;
     // si ocurrio un error y se debe mostrar
     if (VerMsgError)  And (MsgError <> '') then
     Begin
        MessageDlg(MsgError,mterror, [mbok],0);
     End;
     FDQry01.Close;
     FDQry01.free;
     Exit;
    end;
  end;
  Result:= IdObjeto;
  FDQry01.Close;
  FDQry01.free;
End;




//------------------------------------------------------------------------------
//Se guardan los permisos de los usuarios en la aplicaci�n
//------------------------------------------------------------------------------
procedure TFrmUsers.Update_Accesos_Usuarios   ;
var
  i,IdRol,IdObjeto:integer;
  Node: TTreeNode;
  tSQL:String;
  Activo:Boolean;
begin

 Try

  //se recorre el tree para tomar el valor seleccionado del ID Rol
  for i := 0 to trvRolesPermission.Items.Count - 1 do
    Begin
    if trvRolesPermission.Items[i].Selected then
    Begin
      IdRol:=Integer(trvRolesPermission.Items[i].data);
      Break;
    End;
  End;



  //---------------------------------------------------------------------------
  // Se Borran los permisos actuales del grupo
  //----------------------------------------------------------------------------

  tSQL:='';

  tSQL:= 'DELETE FROM ICGDO_TB_acObjetos_Rol WHERE  IDAPPS = ' + Inttostr(IdApps) +
    ' AND IdRol=' + (IntTostr(idRol)) ;
  //------------------------------------------------------------------------
  //Se ejecuta el SQL para eliminar los permisos del usuario
  //------------------------------------------------------------------------
  FDCnGestion.ExecSQL(tSQL);

  //Se recorre el trewiew de permisos del usuario para insertar en la tabla
  // Recorrer los nodos del �rbol
  for i:=0 to (tvPermissionsRoles.Items.Count - 1) do
    begin
    node := tvPermissionsRoles.items[i];
    node.selected:=True;
    idObjeto:=Integer(Node.Data);
    Activo:=False;
    //Se verifica si el nodo esta checked;
    if IsChecked(node) then
       Activo:=True;
    //se inserta el usuario en la base de datos
    tSQL:= 'INSERT INTO ICGDO_TB_acObjetos_Rol WITH (ROWLOCK) (IDObjeto, IDApps, IDRol, Activo)' +
    'VALUES (' + intToSTR(idObjeto) + ',' +
    (Inttostr(IdApps)) + ','   + (IntTostr(IdRol)) +  ',' +
    BoolToSTR(Activo) + ')' ;

    //------------------------------------------------------------------------
    //Se ejecuta el SQL para eliminar los permisos del usuario
    //------------------------------------------------------------------------
    FDCnGestion.ExecSQL(tSQL);
  end;

 except
  On E:Exception do
  begin
    MsgError:=E.Message;
    // si ocurrio un error y se debe mostrar
    if (VerMsgError)  And (MsgError <> '') then
    Begin
      MessageDlg(MsgError,mterror, [mbok],0);
    End;
    Exit;
  end;
 End;
end;


//-----------------------------------------------------------------------------
//Se activa o desactiva los objetos del formulario
//-----------------------------------------------------------------------------
function TFrmUsers.OnOffFormLocal (InfoConexion:TConexionDBInfo;
  InfoUsr:TUserData; TypeSecurity,IdApps:Integer; var MsgError:string ;
  VerMsgError:Boolean): Boolean;
Var

    i,j,IdGroup:integer;
    sDescripcion,NameObj:String;
    bObjVisible,NoesNil:boolean;

Begin
  try

    //si el usuario es administrador, todas las opciones estan disponibles
    if InfoUsr.SysAdmin then
    Begin
      Result:=True;
      Exit;
    End;
    bObjVisible:=true;

    //se recorren todos los objetos del formulario
    for I:=0 to ComponentCount-1 do
    Begin
      //se toma el nombre del objeto
      NameObj:=Components[i].Name;

      bObjVisible:=true;


      //--------------------------------------------------------------------------
      //Buscar el objeto en el dataset
      //--------------------------------------------------------------------------
      //se verifican las opciones del usuario
      if NameObj <> '' then
      Begin
        bObjVisible:= OnOffFrmObj (NameObj, InfoConexion,
        InfoUsr, TypeSecurity,IdApps,
        MsgError,  VerMsgError);
      End;
      //si es un ToolButton
      if (Components[I] is TToolButton)
        then
          TToolButton(Components[I]).visible:=bObjVisible
        Else
          //si es un TTabSheet
          If (Components[I] Is TTabSheet)
            Then
              TTabSheet(Components[I]).TabVisible:=bObjVisible
    End;


    Result := true;
    Except On E:Exception  do
    Begin                       // Se guarda el error en el log
        MsgError:=E.Message;
        WriteLogApps(2, 'Error ====> : ' + DateTimeToStr(now)  + '  , ' + MsgError,Infoconexion.GeneraLogErrores,
        InfoConexion.GeneraLogData, MsgError , VerMsgError);
        // si ocurrio un error y se debe mostrar
        if (VerMsgError)  And (MsgError <> '') then
        Begin
          MessageDlg(MsgError,mterror, [mbok],0);
        End;
        Result := false;
      End;
    End;
End;




end.
