unit uFrmMttoUsers;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs  ,

  Data.Win.ADODB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Phys.SQLite, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.Phys.MSSQLDef, FireDAC.Phys.ODBCBase,
  FireDAC.Phys.MSSQL  ,

  Vcl.StdCtrls, Vcl.ExtCtrls, System.ImageList,
  Vcl.ImgList, cxImageList, cxGraphics,

  uUsersV03,uDBManagement,
  uSecurityManagement
   ;


type
  TFrmMttoUsers = class(TForm)
    cxImgMain: TcxImageList;
    grpMain: TGridPanel;
    pnlTop: TPanel;
    lblIdUser: TLabel;
    lblLogin: TLabel;
    edtIdUsers: TEdit;
    edtLogin: TEdit;
    pnlLeft: TPanel;
    btnOk: TButton;
    pnlRigth: TPanel;
    btnCancelar: TButton;
    edtName: TEdit;
    lblName: TLabel;
    lblPassword: TLabel;
    edtPassword: TEdit;
    chkSysAdmin: TCheckBox;
    chkActivo: TCheckBox;
    procedure btnCancelarClick(Sender: TObject);
    procedure edtIdUsersKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    TypeSecurity,IdUser:Integer;
    DatosConexionDB:TConexionDBInfo ;
    DeleteReg,ChangePassword:boolean;
  end;

var
  FrmMttoUsers: TFrmMttoUsers;
  FDCnGestion:TFDConnection;
  FDLinkMSSQL : TFDPhysMSSQLDriverLink;
  FDLinkSQLite : TFDPhysSQLiteDriverLink;
  FDQryUsers: TFDQuery;
  MsgError:String;
  VerMsgError, NewRol:Boolean;
implementation

{$R *.dfm}

procedure TFrmMttoUsers.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

//------------------------------------------------------------------------------
//clic sobre el boton clic
//------------------------------------------------------------------------------
procedure TFrmMttoUsers.btnOkClick(Sender: TObject);
Var
  Valor,MsgError, lc_Password:String;
  buttonSelected : Integer;
begin

  // Si se indico que debe borrarse el registro
  if DeleteReg
    then
      buttonSelected := messagedlg('�Esta Seguro de BORRAR la informaci�n?',mtWarning , [mbYes,mbCancel], 0)
    Else
       buttonSelected := messagedlg('�Esta Seguro de guardar la informaci�n?',mtInformation, [mbYes,mbCancel], 0);
 // se cancela el proceso de guardar o borrar la informaci�n
  if buttonSelected = mrNo
    Then
      Exit;
  try
    //--------------------------------------------------------------------------
    // Se verifica si los valores a guardar en el registro estan correctos
    //--------------------------------------------------------------------------

    if edtLogin.Text='' then
    Begin
      MessageDlg('Indique por favor el Login del usuario',mterror, [mbok],0);
      edtLogin.SetFocus;
    End;

    if edtName.Text='' then
    Begin
      MessageDlg('Indique por favor el Nombre del usuario',mterror, [mbok],0);
      edtName.SetFocus;
    End;

    if edtPassword.Text='' then
    Begin
      MessageDlg('Indique por favor el Password del usuario',mterror, [mbok],0);
      edtPassword.SetFocus;
    End;

    //si se debe eliminar el registro
    if DeleteReg Then
    Begin
      FDQryUsers.Delete ;
      //Borrar las dependencias que se tienen con los roles.
      //Borrar el usuario de los grupos a los que pertenece
      FDCnGestion.ExecSQL('DELETE FROM ICGDO_TB_acGroupMembers WHERE UserID=' + IntToStr(IdUser));
      //Borrar los roles x  usuarios
      FDCnGestion.ExecSQL('DELETE FROM ICGDO_TB_acUsersAccess WHERE UserID=' + IntToStr(IdUser));
    End
    Else
    Begin //se edita el registro
      lc_Password:= Encode64(edtpassword.Text,MsgError,True)  ;
      FDQryusers.FieldByName('LoginName').Value:=edtLogin.Text;
      FDQryusers.FieldByName('FullName').Value:=edtName.Text;
      FDQryusers.FieldByName('Password').Value:=lc_Password;
      FDQryusers.FieldByName('Enabled').Value:=boolTostr(chkActivo.Checked);
      FDQryusers.FieldByName('SysAdmin').Value:=boolTostr(chkSysAdmin.Checked);
     //se aplican los cambios en la base de datos
      FDQryUsers.Post;
    End;

    //cerramos el formulario
    Close;
  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      MessageDlg(E.Message,mterror, [mbok],0);
    End;
  End;

end;

procedure TFrmMttoUsers.edtIdUsersKeyPress(Sender: TObject; var Key: Char);
begin
 If Key = #13 Then Begin
    If HiWord(GetKeyState(VK_SHIFT)) <> 0 then
     SelectNext(Sender as TWinControl,False,True)
    else
     SelectNext(Sender as TWinControl,True,True) ;
     Key := #0
   end;
end;

//------------------------------------------------------------------------------
//Se crea el formulario
//------------------------------------------------------------------------------
procedure TFrmMttoUsers.FormCreate(Sender: TObject);
begin
  try
    MsgError:='';
    VerMsgError:=True;
  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje

      MessageDlg(E.Message,mterror, [mbok],0);
    End;
  End;
end;

//------------------------------------------------------------------------------
//Se despliega el formulario
//------------------------------------------------------------------------------
procedure TFrmMttoUsers.FormShow(Sender: TObject);
begin
  Try
    //--------------------------------------------------------------------------
    //Se crean los objetos de base de datos
    //--------------------------------------------------------------------------
    FDCnGestion:=TFDConnection.Create(Self);
    FDQryUsers:= TFDQuery.Create(self);
    //--------------------------------------------------------------------------
    //Se abre la conexi�n a la base de datos
    //--------------------------------------------------------------------------
    with  FDCnGestion do
    Begin
      if DatosConexionDB.TConexDBLocal <=1 then  //Sql Server
      begin
        //asignamos el driver de conexi�n
        FDLinkMSSQL := TFDPhysMSSQLDriverLink.Create(Self);
        FDCnGestion.DriverName :=  FDLinkMSSQL.BaseDriverId;
        //se abre la base de datos de Gesti�n
        Conectado:=Open_DB_FD (FDCnGestion,12,DatosConexionDB.ServerDB, DatosConexionDB.NameDBGestion
        , DatosConexionDB.UserDB, DatosConexionDB.PasswordDB,DatosConexionDB.PathDB, MsgError,VerMsgError);

        if Conectado then
        Begin
          //Qry Roles de usuarios
          FDQryUsers.SQL.Text:='';
          FDQryUsers.SQL.Text:= 'SELECT UserID, LoginName, FullName, Password,  Enabled,  SysAdmin ' +
          ' FROM ICGDO_TB_acUsers WITH (NOLOCK) WHERE  UserID=' + IntToStr(IdUser);
          FDQryUsers.Connection:=FDCnGestion;
        end;
      end
      else //Sql Lite
      begin
        FDLinkSQLite := TFDPhysSqliteDriverLink.Create(Self);
        FDCnGestion.DriverName := FDLinkSQLite.BaseDriverId;

     {   if FileExists(usrRutaDBLocal+usrNameDBLocal)then
        begin
          try
            //------------------------------------------------------------------------
            // Parametros de Conexion a la base de datos
            //------------------------------------------------------------------------
            DriverName:='SQLite';
            Params.Add('Database=' +usrRutaDBLocal+usrNameDBLocal );
            Open();
            Connected:=True;
            Conectado:=Connected;
          except
          on E: EDatabaseError do
            Begin
              // Se guarda el error en el log
              Mensaje:=E.Message;


            End;
          end;
        end
        else
        begin
          MessageDlg('No se pudo conectar a la base de datos', mterror, [mbOK], 0);
          Exit;
        end;
      end;}
      End;
      //------------------------------------------------------------------------
      //Si no hay conexi�n se interrumpe la ejecuci�n de la aplicaci�n
      //------------------------------------------------------------------------
      if Conectado=False then
      Begin
        MessageDlg('No se pudo conectar a la base de datos: ' + MsgError, mterror, [mbOK], 0);
        PostMessage(Self.Handle,wm_close,0,0);
      End;
    end;
    //--------------------------------------------------------------------------
    //Abrir los data set
    //--------------------------------------------------------------------------
    //usuarios
    with FDQryUsers do
    Begin
      Connection:=FDCnGestion;
      Close;
      //se abre el dataset
      Active := true;
      NewRol:=True;
      //si esta abierto
      if Active then
      begin
        //si tiene registros
        if RecordCount>0 then
        begin
          //primer registro
          First;
          NewRol:=False;
        end;
      end;
      //------------------------------------------------------------------------
      //si es un nuevo rol
      //------------------------------------------------------------------------
      if NewRol then
      Begin
        edtIdUsers.Text:='0';
        edtLogin.Text:='';
        edtName.Text:='';
        edtPassword.Text:='';
        chkSysAdmin.Checked:=false;
        chkActivo.Checked:=true;
        Append;
      End
      Else //ya existe
      Begin

        // se toman los valores de la taba
        edtIdUsers.Text:=FieldByName('UserID').AsString;
        edtLogin.Text:=FieldByName('LoginName').AsString;
        edtName.Text:=FieldByName('FullName').AsString;
        edtPassword.Text:=Decode64(FieldByName('Password').AsString,MsgError,True);
        chkActivo.Checked:=FieldByName('Enabled').Asboolean;
        chkSysAdmin.Checked:=FieldByName('SysAdmin').Asboolean;

        //si es una edici�n se coloca el data set en modo edici�n.
        if DeleteReg=False
          then
            Edit
          Else
          Begin
            //se desectiva los objetos
            edtIdUsers.enabled:=false;
            edtLogin.enabled:=false;
            edtName.enabled:=false;
            edtPassword.enabled:=false;
            chkSysAdmin.enabled:=false;
            chkActivo.enabled:=false;
          end;
      End;
    End;

    //Si es cambio de password se bloquean algunos campos
    if ChangePassword then
    Begin
      edtLogin.Enabled:=False;
      chkActivo.Enabled:=False;
      chkSysAdmin.Enabled:=False;
    End;


  Except On E:Exception do
    Begin
      //Si hay un error se verifica si debe mostrarse el error o solo enviar el mensaje
      MessageDlg(E.Message,mterror, [mbok],0);
    End;
  End;
end;


end.
