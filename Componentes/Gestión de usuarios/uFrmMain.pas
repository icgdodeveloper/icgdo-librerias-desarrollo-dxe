unit uFrmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.ImageList,
  Vcl.ImgList, cxImageList, cxGraphics;

type
  TfrmMain = class(TForm)
    btnCreateDB: TButton;
    cxImageList1: TcxImageList;
    procedure btnCreateDBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation
uses uFrmCreateDB;
{$R *.dfm}

procedure TfrmMain.btnCreateDBClick(Sender: TObject);
var
  varfrmDB:TFrmCreateDB;
begin
  varfrmDB:=TFrmCreateDB.Create(self);
  try

    varfrmDB.Showmodal;
  finally

    varfrmDB.Free;
  end;

end;

end.
