//------------------------------------------------------------------------------
//Unit para el manejo de la selecci�n
//que permite la selecci�n de los registros de aerep�erto y nacionalidad
//ICG Dominicana, SRL
//Rafael Rangel
//Creaci�n:20161101
//Modificado:20161101
//------------------------------------------------------------------------------
unit UnitSelectRecord;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ExtCtrls,
  Vcl.StdCtrls, Data.DB, Vcl.DBGrids,
  Vcl.Touch.Keyboard, uDBManagementADO,
   ADODB, Vcl.Grids, System.ImageList, Vcl.ImgList, Vcl.ToolWin
  ;


//-----------------------------------------------------------------------------
//Tipos de Registros  de Parametros Generales
//------------------------------------------------------------------------------

//Parametros de la Aplicaci�n
type TSeleccionRecord   = record
    ServerDB, NameDB,  UsersDB,  PasswordDB, StrSQL:String;
    Teclado:Integer;
end;

type
  TFrmSeleccion = class(TForm)
    tobMain: TToolBar;
    pnlCriterios: TPanel;
    pnlData: TPanel;
    rbgBusqueddas: TRadioGroup;
    edtValor: TEdit;
    lblValor: TLabel;
    tbbAceptar: TToolButton;
    ImageList1: TImageList;
    tbbCodigo: TToolButton;
    tbbDescripcion: TToolButton;
    ToolButton4: TToolButton;
    tbbSalir: TToolButton;
    pnlKeyboard: TPanel;
    tkbGeneral: TTouchKeyboard;
    StatusBar1: TStatusBar;
    tbtTeclado: TToolButton;
    dbgRegistros: TDBGrid;
    procedure tbbSalirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tbbCodigoClick(Sender: TObject);
    procedure tbbDescripcionClick(Sender: TObject);
    procedure rbgBusqueddasClick(Sender: TObject);
    procedure edtValorKeyPress(Sender: TObject; var Key: Char);
    procedure tbbAceptarClick(Sender: TObject);
    procedure tbtTecladoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgRegistrosEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    DatosConexionDB:TConexionDBInfo ;
    TypeSecurity,IdApps:integer;
    InfoSRecord:TSeleccionRecord

  end;

var
  FrmSeleccion: TFrmSeleccion;

  Cn100: TADOConnection; //Conexion a la Base de datos
  rs100: TADOQuery; //Tabla de Consultas Aerepouertos / Nacionalidades
  ds100: TDataSource; //

  //-------------------------
  InfoRecord: array of string;

  //Variables del Formulario
  ServerDB,
  NameDB,
  UsersDB,
  PasswordDB,
  StrSQL,
  StrSQL02,
  MsgError,
  Msg:String;
  Conectado,
  Objetos,VerMsgError:Boolean;
  Codigo,
  Descripcion,
  Campo01,
  Campo02,
  Campo03,
  Campo04,
  Campo05,
  Campo06,
  Campo07,
  Campo08,

  ArchivoXML:String;
implementation

{$R *.dfm}

//------------------------------------------------------------------------------
//Combierte el enter como un tab
//------------------------------------------------------------------------------
procedure TFrmSeleccion.dbgRegistrosEnter(Sender: TObject);
begin
 // tbbAceptarClick(Sender);
end;

procedure TFrmSeleccion.edtValorKeyPress(Sender: TObject; var Key: Char);
begin
If Key = #13 Then Begin
    If HiWord(GetKeyState(VK_SHIFT)) <> 0 then
    Begin
     SelectNext(Sender as TWinControl,False,True)
    End
    else
    Begin
     SelectNext(Sender as TWinControl,True,True) ;
     Key := #0;
     tbbAceptarClick(Sender);
    End;
   end;
end;

//------------------------------------------------------------------------------
//Se crea el formulario
//------------------------------------------------------------------------------
procedure TFrmSeleccion.FormCreate(Sender: TObject);
Var
i:Integer;
begin

  //------------------------------------------------------------------------
  // Se Crean los Onjetos ADO
  //------------------------------------------------------------------------
  cn100 := TADOConnection.Create(Self);
  rs100 := TADOQuery.Create(Self);
  ds100 :=  TDataSource.Create(Self);

  //----------------------------------------------------------------------------
  ServerDB:='';
  NameDB:=  '';
  UsersDB:= '';
  PasswordDB:= '';
  StrSQL:='';

end;


procedure TFrmSeleccion.FormShow(Sender: TObject);
var
  i:integer;
begin
  Try
    ServerDB:= InfoSRecord.ServerDB;
    NameDB:=  InfoSRecord.NameDB;
    UsersDB:= InfoSRecord.UsersDB;
    PasswordDB:= InfoSRecord.PasswordDB;
    StrSQL:=InfoSRecord.StrSQL;
    If InfoSRecord.Teclado= 0 Then tbtTecladoClick(Sender);
    //----------------------------------------------------------------------------

    //----------------------------------------------------------------------------
    //Se Verifica si existen los valores que requiera la aplicaci�n para realizar la consulta
    //----------------------------------------------------------------------------
    if (ServerDB='') Or (NameDB='') Or (UsersDB='') Or (PasswordDB='') Or (StrSQL='')
      then
        Exit;


    //--------------------------------------------------------------------------
    //Conexi�n a la Base de datos
    //--------------------------------------------------------------------------
    Conectado:=Open_DB_ADO (cn100 , 12 , ServerDB, NameDB, UsersDB, PasswordDB, '',MsgError,True );
    //-------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    //si NO hay conexi�n el proceso de aborta
    //--------------------------------------------------------------------------
    if Conectado= False then
    Begin
      Exit;
    End;


    //----------------------------------------------------------------------------
    //Se Abren los dataset
    //----------------------------------------------------------------------------
    Conectado:= Open_ADO_Qry(Cn100, rs100,StrSQL,false,VerMsgError,MsgError);
  
    //----------------------------------------------------------------------------
    // Verifica si se Abrio el DataSet
    //----------------------------------------------------------------------------
    if Conectado=False then
    Begin
      Exit;   //Si Hay un error se cierra la aplicaci�n.
    End;

    //Se colocan los datos en el grid
    ds100.DataSet:= rs100;
    ds100.DataSet.Open;
    dbgRegistros.DataSource:=ds100;
    with dbgRegistros do
    begin
      TabOrder := 0;
      //Se coloca el Titulo en Negrita
      for i := 0 to FieldCount -1do
      begin
        Columns.Items[i].Title.Font.Style:=[fsBold] ;
      end;
    end;

    //se crea el
    SetLength(InfoRecord, (rs100.FieldCount) );
  except
    On E:Exception do
    begin
        //Log De error
        MsgError:=E.Message;
        if VerMsgError
          then
            MessageDlg(E.Message, mterror, [mbOK], 0);
    end;
  end;
end;

//------------------------------------------------------------------------------
//Se realiza la busqueda del valor
//------------------------------------------------------------------------------
procedure TFrmSeleccion.rbgBusqueddasClick(Sender: TObject);
Var
  temp,
  ElFiltro : string;
begin
  ElFiltro:='';
  temp:='';
  With rs100 do
  Begin

    temp := Stringreplace(EdtValor.Text, '''', '''''', [rfReplaceAll, rfIgnoreCase]);
    if temp =  '' then Exit;

    //--------------------------------------------------------------------------
    //Se Arma la busqueda del campo se acuerdo a los valores indicados
    //--------------------------------------------------------------------------

    Case rbgBusqueddas.ItemIndex of
      0:
      Begin
        lblValor.Caption:='C�digo';
         ElFiltro := '([C�digo] LIKE ''%' + temp +'%'')' ;
      End;
      1:
      Begin
        lblValor.Caption:='Descripci�n';
        ElFiltro := '([Descripci�n]  LIKE ''%' + temp + '%'')' ;
      End;
    End;
    //--------------------------------------------------------------------------
    // Se activa el Flitro
    //--------------------------------------------------------------------------
    rs100.Filter:=ElFiltro;
    Filtered:=True;
  End;
   //---------------------------------------------------------------------------
   EdtValor.Setfocus;

end;

//------------------------------------------------------------------------------
// Se acepta la campo seleccionado
//------------------------------------------------------------------------------
procedure TFrmSeleccion.tbbAceptarClick(Sender: TObject);
var
  i:Integer;
begin
  Codigo:=rs100.Fields[0].AsString;
  Descripcion:=rs100.Fields[1].AsString;

  //Se coloca el Titulo en Negrita
  for i := 0 to rs100.FieldCount -1do
  begin
    InfoRecord[i]:=rs100.Fields[i].AsString;
  end;
  Close;
end;


//------------------------------------------------------------------------------
// Se coloca como selecci�n el codigo
//------------------------------------------------------------------------------
procedure TFrmSeleccion.tbbCodigoClick(Sender: TObject);
begin
  rbgBusqueddas.ItemIndex:=0;
end;


//------------------------------------------------------------------------------
// Se coloca como selecci�n el Nombre
//------------------------------------------------------------------------------
procedure TFrmSeleccion.tbbDescripcionClick(Sender: TObject);
begin
  rbgBusqueddas.ItemIndex:=1;
end;


//------------------------------------------------------------------------------
// Se Cancela la selecci�n
//------------------------------------------------------------------------------
procedure TFrmSeleccion.tbbSalirClick(Sender: TObject);
begin
  Codigo:='';
  Descripcion:='';
  Close;
end;

//------------------------------------------------------------------------------
// Se muestra u oculta el teclado
//------------------------------------------------------------------------------
procedure TFrmSeleccion.tbtTecladoClick(Sender: TObject);
Var
ValorObj:Boolean;
begin
  ValorObj:=Not(tkbGeneral.Visible);
  tkbGeneral.Visible:=ValorObj;
  pnlKeyboard.Visible:=ValorObj;

end;

end.
